import { Directive,  ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[infiniteScroll]'
})
export class InfiniteScrollDirective {

 @Input() scrollThreshold = 200; // px
 @Input('infiniteScroll') loadMore;
  constructor(private element: ElementRef) { }

  @HostListener("window:scroll", ["$event"])
   onWindowScroll() {
  	alert();
    const scrolled = this.element.nativeElement.scrollTop;
    const height = this.element.nativeElement.offsetHeight;

    // if we have reached the threshold and we scroll down
    /*if (height - scrolled < this.scrollThreshold) {
      this.loadMore();
    }*/

  }
}
