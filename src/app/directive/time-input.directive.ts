import {Directive, Input, ElementRef, Renderer, AfterViewInit} from '@angular/core';

import flatpickr from "flatpickr";
@Directive({
  selector: '[appTimeInput]'
})
export class TimeInputDirective implements AfterViewInit{

  @Input() mindate = '';

  @Input() maxdate = '';

  @Input() format: string;

  @Input() nocalendar = false;

  @Input() enabletime = false;

  @Input() enableseconds = false;

  @Input() enabletimepicker = false;

  @Input() mode: 'single' | 'multiple' | 'range';

  @Input() disablefrom: '';

  @Input() disableto: '';


  constructor(private el: ElementRef, private redered: Renderer) {


  }

  ngAfterViewInit(): void {
    if (this.enabletimepicker) {
      flatpickr(this.el.nativeElement, {
        enableTime: true,
        noCalendar: true,
        dateFormat: this.format
      });

    } else {
      flatpickr(this.el.nativeElement, {
        dateFormat: this.format,
        enableTime: this.enabletime,
        enableSeconds: this.enableseconds,
        noCalendar: this.nocalendar,
        maxDate: this.maxdate ? new Date(this.maxdate): '',
        minDate: this.mindate ? new Date(this.mindate): '',
        mode: this.mode,
        disable: [{from: this.disablefrom, to: this.disableto}]
      });
    }

	}

}
