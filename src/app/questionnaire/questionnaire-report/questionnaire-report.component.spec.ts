import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionnaireReportComponent } from './questionnaire-report.component';

describe('QuestionnaireReportComponent', () => {
  let component: QuestionnaireReportComponent;
  let fixture: ComponentFixture<QuestionnaireReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionnaireReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionnaireReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
