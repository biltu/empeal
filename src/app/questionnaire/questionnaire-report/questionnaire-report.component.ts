import { Component, OnInit } from '@angular/core';
import {Global} from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../services/message.service';

@Component({
  selector: 'app-questionnaire-report',
  templateUrl: './questionnaire-report.component.html',
  styleUrls: ['./questionnaire-report.component.css']
})
export class QuestionnaireReportComponent implements OnInit {
	report_date: string;
  ansDateModel: string ='';
	questionnaires: any=[];
	barcolor: any=[];
  bardata: any=[];
	pieChartOptions = {
        responsive: false,
        maintainAspectRatio: false,
        tooltips: {
            // callbacks: {
            //     label: function(tooltipItem) {
            //         return "$" + Number(tooltipItem.yLabel) + " and so worth it !";
            //     }
            // },
            callbacks: {
                label: function(tooltipItem, data) {
                    var dataset = data.datasets[tooltipItem.datasetIndex];
                    var index = tooltipItem.index;
                    return dataset.labels[index] + ': ' + dataset.data[index];
                }
            },
            displayColors: false

        },
        legend: {
                display: false,
            },
            title: {
                display: true,
                fontSize: 20,
                padding: 16,
                text: 'Personal Cross Profile Analysis'
            },
            "scales": {
                "yAxes": [{
                    "ticks": {
                        "beginAtZero": true
                    }
                }],
                xAxes: [{
                    ticks: {
                        autoSkip: false,
                        //fontSize: 11
                    }
                }]
            }
    };
    pieChartLabels =  [];
  
    // CHART COLOR.
    pieChartData:any = [];
  //questionnaires_sc: any=[];
  questionnaires_ps: any =[];
  questionnaires_fit: any =[];
  questionnaires_dtp: any=[];
  questionnaires_lp: any=[];
  questionnaires_es: any=[];
  questionnaires_ip: any=[];
  questionnaires_cvp: any=[];
  questionnaires_mo: any=[];
  questionnaires_wo: any=[];
  questionnaires_ef: any=[];
  questionnaires_diet: any=[];
  enduser_id: any=[];
  questionids: any=[];
  group_id:any;
  useransmonths: any=[];
  //questionnaires_tc: any=[];
  constructor(private message:MessageService ,private global: Global,private authService: AuthService, private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) { 
    this.report_date = this.activeRoute.snapshot.paramMap.get("date");
    this.enduser_id = this.activeRoute.snapshot.paramMap.get("id");
  	//this.asyncInit();
  }

  ngOnInit() {
  	this.ansDateModel = this.report_date;
    this.answerdetails(this.enduser_id,this.report_date);
  }

/**
* Get Questionnaries report data
*/
  asyncInit(){
    
  }

/**
* get optional chart lebel
*/
  getOptionLebel(options, answer){
    let optionsData = options.split('|');
    let returnValue ='';
    optionsData.forEach(option =>{
      let optionArr = option.split('~');
      if(Number(optionArr[0]) ==Number(answer)){
         returnValue =optionArr[1];
      }
    });
    return returnValue;
  }

/**
* get fixed optional chart lebel
*/
  getFixedOptionLebel(answer){
    let dataArr =[{no:4, level:'Never'},{no:3,level:'Sometimes'},{no:2,level:'Often'}
    ,{no:1,level:'Always'},{no:0, level:'Never'}];
    let result = dataArr.find(data=> data.no == answer);
    return result.level;
  }

  onChartClick(data){

  }

  checkForDuplicates(array) {
    return new Set(array).size !== array.length
  }

  monthReportDetails(date,userid){
    this.questionnaires = [];
    this.questionnaires_ps =[];
    this.questionnaires_fit =[];
    this.questionnaires_dtp=[];
    this.questionnaires_lp=[];
    this.questionnaires_es=[];
    this.questionnaires_ip=[];
    this.questionnaires_cvp=[];
    this.questionnaires_mo=[];
    this.questionnaires_wo=[];
    this.questionnaires_ef=[];
    this.questionnaires_diet=[];
    this.ansDateModel = date;
    this.report_date = date;
    this.answerdetails(userid,date);
  }

  answerdetails(userid,report_date){
    this.commonService.getAll(this.global.apiUrl+'/api/report/questionnarie/'+userid+'/'+report_date)
      .subscribe((data)=>{
        this.questionnaires = data.data;
        //console.log(this.questionnaires);
        this.pieChartData = data.pcpreport[0];
        this.pieChartLabels = data.pcpreport[2];
        this.group_id = data.usergroups[0];
        this.useransmonths = data.useransmonths;
        // this.questionnaires_sc = this.questionnaires.SC.map((quetion)=> {
        //  return {
        //    ques_sub_type: quetion.ques_sub_type,lebel:this.getOptionLebel(quetion.option_labels, quetion.answer_given),
        //    question: quetion.question, ques_type: quetion.ques_type, status:quetion.status, answer_given: quetion.answer_given 
        //  } 
        // });

        if(this.questionnaires.PS){
          this.questionnaires_ps = this.questionnaires.PS.map((quetion)=> {
            this.questionids.push(quetion.id);
            let dataArr =[{no:5, level:'Never'}, {no:4, level:'Almost Never'},{no:3,level:'Sometimes'},{no:2,level:'Fairly Often'}
            ,{no:1,level:'Very Often'},{no:0,level:'Never'}];
            let result = dataArr.find(data=> data.no == quetion.answer_given);
           return {
             ques_sub_type: quetion.ques_sub_type,lebel:result.level,
             question: quetion.question, ques_type: quetion.ques_type, status:quetion.status, answer_given: quetion.answer_given,
             answer_date: quetion.answer_date
           } 
          });
          this.questionnaires_ps.is_duplicate = this.checkForDuplicates(this.questionids);
        }
        if(this.questionnaires.FIT){
          this.questionids = [];
          this.questionnaires_fit = this.questionnaires.FIT.map((quetion)=> {
            this.questionids.push(quetion.id);
           return {
             ques_sub_type: quetion.ques_sub_type,lebel:this.getOptionLebel(quetion.option_labels,quetion.answer_given),
             question: quetion.question, ques_type: quetion.ques_type, status:quetion.status, answer_given: quetion.answer_given,
             answer_date: quetion.answer_date
           } 
          });
          this.questionnaires_fit.is_duplicate = this.checkForDuplicates(this.questionids);
        }
        if(this.questionnaires.DTP){
          this.questionids = [];
          this.questionnaires_dtp = this.questionnaires.DTP.map((quetion)=> {
            this.questionids.push(quetion.id);
           return {
             ques_sub_type: quetion.ques_sub_type,lebel:this.getFixedOptionLebel(quetion.answer_given),
             question: quetion.question, ques_type: quetion.ques_type, status:quetion.status, answer_given: quetion.answer_given,
             answer_date: quetion.answer_date 
           } 
          });
          this.questionnaires_dtp.is_duplicate = this.checkForDuplicates(this.questionids);
        }

        if(this.questionnaires.LP){
          this.questionids = [];
          this.questionnaires_lp = this.questionnaires.LP.map((quetion)=> {
            this.questionids.push(quetion.id);
           return {
             ques_sub_type: quetion.ques_sub_type,lebel:this.getFixedOptionLebel(quetion.answer_given),
             question: quetion.question, ques_type: quetion.ques_type, status:quetion.status, answer_given: quetion.answer_given,
             answer_date: quetion.answer_date 
           } 
          });
          this.questionnaires_lp.is_duplicate = this.checkForDuplicates(this.questionids);
        }
        if(this.questionnaires.ES){ 
          this.questionids = [];
          this.questionnaires_es = this.questionnaires.ES.map((quetion)=> {
            this.questionids.push(quetion.id);
           return {
             ques_sub_type: quetion.ques_sub_type,lebel:this.getFixedOptionLebel(quetion.answer_given),
             question: quetion.question, ques_type: quetion.ques_type, status:quetion.status, answer_given: quetion.answer_given,
             answer_date: quetion.answer_date 
           } 
          });
          this.questionnaires_lp.is_duplicate = this.checkForDuplicates(this.questionids); 
        }
        if(this.questionnaires.IP){
          this.questionids = [];
          this.questionnaires_ip = this.questionnaires.IP.map((quetion)=> {
            this.questionids.push(quetion.id);
           return {
             ques_sub_type: quetion.ques_sub_type,lebel:this.getFixedOptionLebel(quetion.answer_given),
             question: quetion.question, ques_type: quetion.ques_type, status:quetion.status, answer_given: quetion.answer_given,
             answer_date: quetion.answer_date 
           } 
          }); 
          this.questionnaires_ip.is_duplicate = this.checkForDuplicates(this.questionids);
        }
        if(this.questionnaires.CVP){
          this.questionids = [];
          this.questionnaires_cvp = this.questionnaires.CVP.map((quetion)=> {
            this.questionids.push(quetion.id);
           return {
             ques_sub_type: quetion.ques_sub_type,lebel:this.getFixedOptionLebel(quetion.answer_given),
             question: quetion.question, ques_type: quetion.ques_type, status:quetion.status, answer_given: quetion.answer_given,
             answer_date: quetion.answer_date 
           } 
          }); 
          this.questionnaires_cvp.is_duplicate = this.checkForDuplicates(this.questionids);
        }
        if(this.questionnaires.MO){
          this.questionids = [];
          this.questionnaires_mo = this.questionnaires.MO.map((quetion)=> {
            this.questionids.push(quetion.id);
           return {
             ques_sub_type: quetion.ques_sub_type,lebel:this.getFixedOptionLebel(quetion.answer_given),
             question: quetion.question, ques_type: quetion.ques_type, status:quetion.status, answer_given: quetion.answer_given,
             answer_date: quetion.answer_date 
           } 
          });
          this.questionnaires_mo.is_duplicate = this.checkForDuplicates(this.questionids);
        }
        if(this.questionnaires.WO){
          this.questionids = [];
          this.questionnaires_wo = this.questionnaires.WO.map((quetion)=> {
            this.questionids.push(quetion.id);
           return {
             ques_sub_type: quetion.ques_sub_type,lebel:this.getFixedOptionLebel(quetion.answer_given),
             question: quetion.question, ques_type: quetion.ques_type, status:quetion.status, answer_given: quetion.answer_given,
             answer_date: quetion.answer_date 
           } 
          }); 
          this.questionnaires_wo.is_duplicate = this.checkForDuplicates(this.questionids);
        }
        if(this.questionnaires.EF){
          this.questionids = [];
          this.questionnaires_ef = this.questionnaires.EF.map((quetion)=> {
            this.questionids.push(quetion.id);
           return {
             ques_sub_type: quetion.ques_sub_type,lebel:this.getFixedOptionLebel(quetion.answer_given),
             question: quetion.question, ques_type: quetion.ques_type, status:quetion.status, answer_given: quetion.answer_given,
             answer_date: quetion.answer_date 
           } 
          }); 
          this.questionnaires_ef.is_duplicate = this.checkForDuplicates(this.questionids);
        }
        if(this.questionnaires.DIET){
          this.questionids = [];
          this.questionnaires_diet = this.questionnaires.DIET.map((quetion)=> {
            this.questionids.push(quetion.id);
           return {
             ques_sub_type: quetion.ques_sub_type,lebel:this.getFixedOptionLebel(quetion.answer_given),
             question: quetion.question, ques_type: quetion.ques_type, status:quetion.status, answer_given: quetion.answer_given,
             answer_date: quetion.answer_date 
           } 
          });
          this.questionnaires_diet.is_duplicate = this.checkForDuplicates(this.questionids);
        } 
        // if(this.questionnaires.TC){
        //   this.questionnaires_tc = this.questionnaires.TC.map((quetion)=> {
        //    return {
        //      ques_sub_type: quetion.ques_sub_type,lebel:this.getFixedOptionLebel(quetion.answer_given),
        //      question: quetion.question, ques_type: quetion.ques_type, status:quetion.status, answer_given: quetion.answer_given 
        //    } 
        //   }); 
        // }
      }, (error)=>{

      });
  }

}
