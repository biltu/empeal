import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {Global} from '../../global';

@Component({
  selector: 'app-questionnary',
  templateUrl: './questionnary.component.html',
  styleUrls: ['./questionnary.component.css']
})
export class QuestionnaryComponent implements OnInit {
  pieChartOptions = {
        responsive: false,
        maintainAspectRatio: false,
        tooltips: {
            // callbacks: {
            //     label: function(tooltipItem) {
            //         return "$" + Number(tooltipItem.yLabel) + " and so worth it !";
            //     }
            // },
            callbacks: {
                label: function(tooltipItem, data) {
                    var dataset = data.datasets[tooltipItem.datasetIndex];
                    var index = tooltipItem.index;
                    return dataset.labels[index] + ': ' + dataset.data[index];
                }
            },
            displayColors: false

        },
        legend: {
                display: false,
            },
            title: {
                display: true,
                fontSize: 14,
                padding: 16,
                text: 'Personal Cross Profile Analysis'
            },
            "scales": {
                "yAxes": [{
                    "ticks": {
                        "beginAtZero": true
                    }
                }],
                xAxes: [{
                    ticks: {
                        autoSkip: false,
                        //fontSize: 11
                    }
                }]
            }
    };
    pieChartLabels =  [];
  
    // CHART COLOR.
    pieChartData = [];
    profileanalyticsdate = [];
    is_exist: any;
    archiveModel: string ='';
    date:any;
  constructor(private commonService: CommonService, private router: Router, private global: Global) { 
  	this.commonService.getAll(this.global.apiUrl + '/api/questionnarie')
  		.subscribe((data)=>{
  			// if(data.state !='SC'){
  			// 	this.router.navigate(['/questionnaire/question']);
  			// }
        this.archiveModel = '';
        this.pieChartData = data.profileanalytics[0];
        this.profileanalyticsdate = data.profileanalyticsdate;
        this.is_exist = data.is_exist;
        this.pieChartLabels = data.profileanalytics[2];
  		}, (error)=>{});
  }

  ngOnInit() {
  }

/**
* get archive by date and set chart
*/
  getArchiveByDate(value){
      this.date = value;
      this.commonService.getAll(this.global.apiUrl + '/api/questionnarie?date='+this.date)
      .subscribe((data)=>{
        this.pieChartData = data.profileanalytics[0];
        this.profileanalyticsdate = data.profileanalyticsdate;
        this.pieChartLabels = data.profileanalytics[2];
        this.is_exist = data.is_exist;
      }, (error)=>{});
    }

}
