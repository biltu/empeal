import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Global} from '../../../global';
import { CommonService } from '../../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../../services/message.service';
declare var Swal: any;

@Component({
  selector: 'app-endocrine-system',
  templateUrl: './endocrine-system.component.html',
  styleUrls: ['./endocrine-system.component.css']
})
export class EndocrineSystemComponent implements OnInit {

  @Input() question_data;
  @Output() sentEvent = new EventEmitter<string>();
  @Input() isUpdate;
  answerId:number;
  sub_type: any;
  filter_data: any;
  heading:string;
  title: string;
  active_subtype:number;
  draft_answerscore: any=[];
  ansrespns: any;
  isSaved: any={
    'LEADSP':'',
    'LAGSP':''
  };
  answerModels:any={};
  answerValues:any={
  	type:'ES',
  	sub_type:{
	  'LEADSP':{},
    'LAGSP':{}
  	}
  	
  }
  constructor(private global: Global, private commonService: CommonService, private message: MessageService) {
    this.sub_type = [
      {'no':1,'name':'LEADSP'},
      {'no':2,'name':'LAGSP'}
    ];
  }

ngOnInit() { 
  	this.heading = this.sub_type[0].name;
    this.title = (this.heading =='LEADSP')?'Lead Symptoms Profile':'Lag Symptoms Profile';
    this.active_subtype = this.sub_type[0].no;
  	this.filter_data = this.question_data.filter(qus=>qus.ques_sub_type == this.sub_type[0].name);
    this.asyncInit();
  }

/**
* Get endocrine data and set form if exist
*/
  asyncInit(){
      this.commonService.getAll(this.global.apiUrl + '/api/questionnarie/question-answer/' + 'ES')
        .subscribe((data)=>{
          if(data.data){
            this.answerId = data.answer_id;
            this.draft_answerscore = data.draft_answerscore;
            if(data.data.LEADSP){
            let lead = [];
            data.data.LEADSP.map((dat)=>{lead.push([dat.ques_id, String(dat.answer_given)])});
            this.answerValues.sub_type.LEADSP = lead.reduce((prev,curr)=>{prev[curr[0]]=curr[1];return prev;},{});
            this.answerModels = this.answerValues.sub_type.LEADSP;
            this.isSaved.LEADSP =1;
            }
            if(data.data.LAGSP){
            let lagsp = [];
            data.data.LAGSP.map((dat)=>{lagsp.push([dat.ques_id, String(dat.answer_given)])});
            this.answerValues.sub_type.LAGSP = lagsp.reduce((prev,curr)=>{prev[curr[0]]=curr[1];return prev;},{});
            this.isSaved.LAGSP =1;
            }
          }
        }, (error)=>{});
  }

  asyncNxt(){
          this.commonService.getAll(this.global.apiUrl + '/api/questionnarie/question-answer/' + 'ES')
        .subscribe((data)=>{
          if(data.data){
            this.answerId = data.answer_id;
            if(data.data.LEADSP){
            let lead = [];
            data.data.LEADSP.map((dat)=>{lead.push([dat.ques_id, String(dat.answer_given)])});
            this.answerValues.sub_type.LEADSP = lead.reduce((prev,curr)=>{prev[curr[0]]=curr[1];return prev;},{});
            this.isSaved.LEADSP =1;
            }
            if(data.data.LAGSP){
            let lagsp = [];
            data.data.LAGSP.map((dat)=>{lagsp.push([dat.ques_id, String(dat.answer_given)])});
            this.answerValues.sub_type.LAGSP = lagsp.reduce((prev,curr)=>{prev[curr[0]]=curr[1];return prev;},{});
            this.isSaved.LAGSP =1;
            }
          }
        }, (error)=>{});
  }

  goToNxt(subtype){
    if(Object.entries(this.answerModels).length < 10){
      this.message.error('All questions must be checked.');
      return false;
    }
      switch (this.heading) {
        case "LEADSP":
          this.answerValues.sub_type.LEADSP = this.answerModels;
          break;
        case "LAGSP":
          this.answerValues.sub_type.LAGSP = this.answerModels;
          break;
        default:
          // code...
          break;
      }
    this.answerModels={};
    this.heading = this.sub_type[subtype].name;
    this.title = (this.heading =='LEADSP')?'Lead Symptoms Profile':'Lag Symptoms Profile';
    this.active_subtype = this.sub_type[subtype].no;
    this.filter_data = this.question_data.filter(qus=>qus.ques_sub_type == this.sub_type[subtype].name);
    switch (this.heading) {
      case "LAGSP":
        this.answerModels = Object.entries(this.answerValues.sub_type.LAGSP).length ?this.answerValues.sub_type.LAGSP : this.answerModels;
        break;
      
      default:
        // code...
        break;
    }

  }

  goToPrevious(subtype){
    this.asyncInit();
    this.heading = this.sub_type[subtype].name;
    this.active_subtype = this.sub_type[subtype].no;
    this.filter_data = this.question_data.filter(qus=>qus.ques_sub_type == this.sub_type[subtype].name);

      switch (this.heading) {
        case "LEADSP":
          this.answerModels = this.answerValues.sub_type.LEADSP;
          break;
        case "LAGSP":
          this.answerModels = this.answerValues.sub_type.LAGSP;
          break;
        default:
          // code...
          break;
      }
  }

/**
* submit value
*/
  submitValues(){
    if(Object.entries(this.answerModels).length < 10){
      this.message.error('All questions must be checked');
      return false;
    }
    this.answerValues.sub_type.LEADSP = this.answerModels;
    if(this.isSaved.LEADSP ==1){
      this.commonService.update(this.global.apiUrl + '/api/questionnarie', this.answerId,this.answerValues)
          .subscribe((data)=>{
            this.commonService.callQuestionnarie('data');
            this.message.success('Successfully Updated');
          },(error)=>{});
    }else{
      this.commonService.create(this.global.apiUrl + '/api/questionnarie', this.answerValues)
      .subscribe(data=>{
        if(data.status ==200){
          Swal.fire({
            text: "Thank You for completing body communication questions. Please click on any incomplete sheet above.",
            type: 'success',
            customClass: {
              confirmButton: 'okBtn'
            },
            showCancelButton: false,
            confirmButtonColor: '#9bd025',
            cancelButtonColor: '#d33',
            confirmButtonText: 'OK'
          }).then((result) => {
            if (result.value) {
              this.commonService.callQuestionnarie('data');
              this.isSaved.LEADSP =1;
            }
          });
        }
      }, error=>{});
    }
  }


}
