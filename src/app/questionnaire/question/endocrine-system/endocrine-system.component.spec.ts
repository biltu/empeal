import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndocrineSystemComponent } from './endocrine-system.component';

describe('EndocrineSystemComponent', () => {
  let component: EndocrineSystemComponent;
  let fixture: ComponentFixture<EndocrineSystemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndocrineSystemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndocrineSystemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
