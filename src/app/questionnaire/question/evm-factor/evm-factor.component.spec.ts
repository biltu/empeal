import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EvmFactorComponent } from './evm-factor.component';

describe('EvmFactorComponent', () => {
  let component: EvmFactorComponent;
  let fixture: ComponentFixture<EvmFactorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EvmFactorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EvmFactorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
