import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Global} from '../../../global';
import { CommonService } from '../../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../../services/message.service';
declare var Swal: any;

@Component({
  selector: 'app-evm-factor',
  templateUrl: './evm-factor.component.html',
  styleUrls: ['./evm-factor.component.css']
})
export class EvmFactorComponent implements OnInit {

  @Input() question_data;
  @Output() sentEvent = new EventEmitter<string>();
  @Input() isUpdate;
  answerId:number;
  sub_type: any;
  filter_data: any;
  heading:string;
  active_subtype:number;
  draft_answerscore: any=[];
  answerModels:any={};
  isSaved:boolean;
  answerValues:any={
  	type:'EF',
  	sub_type:{
	  'ENV':{}
  	}
  	
  }
  constructor(private global: Global, private commonService: CommonService, private message: MessageService) {
    this.isSaved = false;
  }

ngOnInit() {
  	this.heading = 'Environmental Factors';
  	this.filter_data = this.question_data;
    //if(this.isUpdate){
      this.commonService.getAll(this.global.apiUrl + '/api/questionnarie/question-answer/' + 'EF')
          .subscribe(data=>{
            if(data.data.ENV){
            this.answerId = data.answer_id;
            this.draft_answerscore = data.draft_answerscore;
            let env = [];
            data.data.ENV.map((dat)=>{env.push([dat.ques_id, String(dat.answer_given)])});
            this.answerValues.sub_type.ENV = env.reduce((prev,curr)=>{prev[curr[0]]=curr[1];return prev;},{});
            this.answerModels = this.answerValues.sub_type.ENV;
            this.isSaved = true;
            }
          });
    //}
  }

/**
* submit value
*/
  submitValues(){
  	if(Object.entries(this.answerModels).length < 6){
  		this.message.error('All questions must be checked');
  		return false;
  	}
  	this.answerValues.sub_type.ENV = this.answerModels;
    if(this.isSaved){
      this.commonService.update(this.global.apiUrl + '/api/questionnarie', this.answerId,this.answerValues)
          .subscribe((data)=>{
            this.commonService.callQuestionnarie('data');
            this.message.success('Successfully Updated');
          },(error)=>{});
    }else{
  	  this.commonService.create(this.global.apiUrl + '/api/questionnarie', this.answerValues)
  		.subscribe(data=>{
  			if(data.status ==200){
  				Swal.fire({
            text: "Thank You for completing Environment. Please click on any incomplete sheet above.",
            type: 'success',
            showCancelButton: false,
            customClass: {
              confirmButton: 'okBtn'
            },
            confirmButtonColor: '#9bd025',
            cancelButtonColor: '#d33',
            confirmButtonText: 'OK'
          }).then((result) => {
            if (result.value) {
              this.commonService.callQuestionnarie('data');
              this.isSaved = true;
            }
          });
  			}
  		}, error=>{});
    }
  }


}
