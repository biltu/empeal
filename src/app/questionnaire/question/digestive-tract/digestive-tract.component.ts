import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Global} from '../../../global';
import { CommonService } from '../../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../../services/message.service';
declare var Swal: any;

@Component({
  selector: 'app-digestive-tract',
  templateUrl: './digestive-tract.component.html',
  styleUrls: ['./digestive-tract.component.css']
})
export class DigestiveTractComponent implements OnInit {
   
  @Input() question_data;
  @Output() sentEvent = new EventEmitter<string>();
  @Input() isUpdate;
  answerId:number;
  sub_type: any;
  filter_data: any;
  heading:string;
  active_subtype:number;
  draft_answerscore: any=[];
  answerModels:any={};
  ansrespns: any;
  isSaved: any={
    'REACTIVE':'',
    'RESULTING':''
  };
  answerValues:any={
  	type:'DTP',
  	sub_type:{
	  'REACTIVE':{},
    'RESULTING':{}
  	}
  	
  }
  constructor(private global: Global, private commonService: CommonService, private message: MessageService) {
    this.sub_type = [
      {'no':1,'name':'REACTIVE'},
      {'no':2,'name':'RESULTING'}
    ];
  }

ngOnInit() {
  	this.heading = this.sub_type[0].name;
    this.active_subtype = this.sub_type[0].no;
  	this.filter_data = this.question_data.filter(qus=>qus.ques_sub_type == this.sub_type[0].name);
    this.asyncInit();
  }

/**
* get digestive data and set value to form if exist
*/
  asyncInit(){
     this.commonService.getAll(this.global.apiUrl + '/api/questionnarie/question-answer/' + 'DTP')
        .subscribe((data)=>{
          if(data.data){
            this.answerId = data.answer_id;
            this.draft_answerscore = data.draft_answerscore;
            if(data.data.REACTIVE){
              let react = [];
              data.data.REACTIVE.map((dat)=>{react.push([dat.ques_id, String(dat.answer_given)])});
              this.answerValues.sub_type.REACTIVE = react.reduce((prev,curr)=>{prev[curr[0]]=curr[1];return prev;},{});
              this.answerModels = this.answerValues.sub_type.REACTIVE;
              this.isSaved.REACTIVE =1;
            }
            if(data.data.RESULTING){
              let resu = [];
              data.data.RESULTING.map((dat)=>{resu.push([dat.ques_id, String(dat.answer_given)])});
              this.answerValues.sub_type.RESULTING = resu.reduce((prev,curr)=>{prev[curr[0]]=curr[1];return prev;},{});
              this.isSaved.RESULTING =1;
            }
          }
        }, (error)=>{});
  }

  asyncNxt(){
         this.commonService.getAll(this.global.apiUrl + '/api/questionnarie/question-answer/' + 'DTP')
        .subscribe((data)=>{
          if(data.data){
            this.answerId = data.answer_id;
            if(data.data.REACTIVE){
              let react = [];
              data.data.REACTIVE.map((dat)=>{react.push([dat.ques_id, String(dat.answer_given)])});
              this.answerValues.sub_type.REACTIVE = react.reduce((prev,curr)=>{prev[curr[0]]=curr[1];return prev;},{});
              this.isSaved.REACTIVE =1;
            }
            if(data.data.RESULTING){
              let resu = [];
              data.data.RESULTING.map((dat)=>{resu.push([dat.ques_id, String(dat.answer_given)])});
              this.answerValues.sub_type.RESULTING = resu.reduce((prev,curr)=>{prev[curr[0]]=curr[1];return prev;},{});
              this.isSaved.RESULTING =1;
            }
          }
        }, (error)=>{});
  }

  goToNxt(subtype){
    if(Object.entries(this.answerModels).length < 10){
      this.message.error('All questions must be checked.');
      return false;
    }
      switch (this.heading) {
        case "REACTIVE":
          this.answerValues.sub_type.REACTIVE = this.answerModels;
          break;
        case "RESULTING":
          this.answerValues.sub_type.RESULTING = this.answerModels;
          break;
        default:
          // code...
          break;
      }
    this.answerModels={};
    this.heading = this.sub_type[subtype].name;
    this.active_subtype = this.sub_type[subtype].no;
    this.filter_data = this.question_data.filter(qus=>qus.ques_sub_type == this.sub_type[subtype].name);
    switch (this.heading) {
      case "RESULTING":
        this.answerModels = Object.entries(this.answerValues.sub_type.RESULTING).length ?this.answerValues.sub_type.RESULTING : this.answerModels;
        break;
      
      default:
        // code...
        break;
    }

  }

  goToPrevious(subtype){
    this.asyncInit();
    this.answerModels={};
    this.heading = this.sub_type[subtype].name;
    this.active_subtype = this.sub_type[subtype].no;
    this.filter_data = this.question_data.filter(qus=>qus.ques_sub_type == this.sub_type[subtype].name);

        switch (this.heading) {
        case "REACTIVE":
          this.answerModels = this.answerValues.sub_type.REACTIVE;
          break;
        case "RESULTING":
          this.answerModels = this.answerValues.sub_type.RESULTING;
          break;
        default:
          // code...
          break;
      }
  }

/**
* submit value
*/
  submitValues(){
    if(Object.entries(this.answerModels).length < 10){
      this.message.error('All questions must be checked');
      return false;
    }
    this.answerValues.sub_type.REACTIVE = this.answerModels;
    if(this.isSaved.REACTIVE ==1){
      this.commonService.update(this.global.apiUrl + '/api/questionnarie', this.answerId,this.answerValues)
          .subscribe((data)=>{
            this.commonService.callQuestionnarie('data');
            this.message.success('Successfully Updated');
          },(error)=>{});
    }else{
      this.commonService.create(this.global.apiUrl + '/api/questionnarie', this.answerValues)
      .subscribe(data=>{
        if(data.status ==200){
          Swal.fire({
            text: "Thank You for completing gut health questions. Please click on any incomplete sheet above.",
            type: 'success',
            customClass: {
              confirmButton: 'okBtn'
            },
            showCancelButton: false,
            confirmButtonColor: '#9bd025',
            cancelButtonColor: '#d33',
            confirmButtonText: 'OK'
          }).then((result) => {
            if (result.value) {
              this.commonService.callQuestionnarie('data');
              this.isSaved.REACTIVE =1;
            }
          });
        }
      }, error=>{});
    }
  }

}
