import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenOnlyComponent } from './men-only.component';

describe('MenOnlyComponent', () => {
  let component: MenOnlyComponent;
  let fixture: ComponentFixture<MenOnlyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenOnlyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenOnlyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
