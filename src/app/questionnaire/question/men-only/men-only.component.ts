import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Global} from '../../../global';
import { CommonService } from '../../../services/common.service';
import {AuthService} from '../../../services/auth.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../../services/message.service';
declare var Swal: any;

@Component({
  selector: 'app-men-only',
  templateUrl: './men-only.component.html',
  styleUrls: ['./men-only.component.css']
})
export class MenOnlyComponent implements OnInit {

  @Input() question_data;
  @Output() sentEvent = new EventEmitter<string>();
  @Input() isUpdate;
  answerId:number;
  sub_type: any;
  filter_data: any;
  heading:string;
  user_data: any;
  user_detail: any;
  isSaved:boolean;
  active_subtype:number;
  answerModels:any={};
  answerValues:any={};

  constructor(private global: Global, private commonService: CommonService, private message: MessageService,
              private authService: AuthService) {
     this.user_data = this.authService.isUserLoginIn ? JSON.parse(localStorage.getItem('user')):'';
     this.user_detail = this.user_data? this.user_data.user_detail:'';
       if(this.user_detail && this.user_detail.gender =='M'){
         this.answerValues ={
               type:'MO',
               sub_type:{
                'MEN':{}
               }
          }
       }else{
         this.answerValues ={
               type:'WO',
               sub_type:{
                'WOMEN':{}
               }
          }
       }
      this.isSaved = false;
  }

ngOnInit() {
  	this.heading = (this.user_detail && this.user_detail.gender =='M')?'He':'She';
  	this.filter_data = this.question_data;
    // if(this.isUpdate){
      let type = (this.user_detail && this.user_detail.gender =='M')?'MO':'WO';
      this.commonService.getAll(this.global.apiUrl + '/api/questionnarie/question-answer/' + type)
          .subscribe(data=>{
            if(data.data){
            this.answerId = data.answer_id;
            if(type =='MO'){
              let men_type =[];
              data.data.MEN.map((dat)=>{men_type.push([dat.ques_id, String(dat.answer_given)])});
              this.answerValues.sub_type.MEN = men_type.reduce((prev,curr)=>{prev[curr[0]]=curr[1];return prev;},{});
              this.answerModels = this.answerValues.sub_type.MEN;
            }else{
              let women_type = [];
              data.data.WOMEN.map((dat)=>{women_type.push([dat.ques_id, String(dat.answer_given)])});
              this.answerValues.sub_type.WOMEN = women_type.reduce((prev,curr)=>{prev[curr[0]]=curr[1];return prev;},{});
              this.answerModels = this.answerValues.sub_type.WOMEN;
              this.isSaved = true;
            }            
           }
          });
    // }
  }

/**
* submit value
*/
  submitValues(){
  	if(Object.entries(this.answerModels).length < 5){
  		this.message.error('All questions must be checked');
  		return false;
  	}
    if(this.user_detail && this.user_detail.gender =='M'){
      this.answerValues.sub_type.MEN = this.answerModels;
    }else{
      this.answerValues.sub_type.WOMEN = this.answerModels;
    }

    if(this.isSaved){
      this.commonService.update(this.global.apiUrl + '/api/questionnarie', this.answerId,this.answerValues)
          .subscribe((data)=>{
            this.commonService.callQuestionnarie('data');
            this.message.success('Successfully Updated');
          },(error)=>{});
    }else{ 	
  	  this.commonService.create(this.global.apiUrl + '/api/questionnarie', this.answerValues)
  		.subscribe(data=>{
  			if(data.status ==200){
  				Swal.fire({
            text: "Thank You for completing "+this.heading+" questions. Please click on any incomplete sheet above.",
            type: 'success',
            customClass: {
              confirmButton: 'okBtn'
            },
            showCancelButton: false,
            confirmButtonColor: '#9bd025',
            cancelButtonColor: '#d33',
            confirmButtonText: 'OK'
          }).then((result) => {
            if (result.value) {
              this.commonService.callQuestionnarie('data');
              this.isSaved = true;
            }
          });
  			}
  		}, error=>{});
    }
    
  }


}
