import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LiverProfileComponent } from './liver-profile.component';

describe('LiverProfileComponent', () => {
  let component: LiverProfileComponent;
  let fixture: ComponentFixture<LiverProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LiverProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiverProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
