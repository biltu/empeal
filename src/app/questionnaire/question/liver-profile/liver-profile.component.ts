import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Global} from '../../../global';
import { CommonService } from '../../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../../services/message.service';
declare var Swal: any;

@Component({
  selector: 'app-liver-profile',
  templateUrl: './liver-profile.component.html',
  styleUrls: ['./liver-profile.component.css']
})
export class LiverProfileComponent implements OnInit {

  @Input() question_data;
  @Output() sentEvent = new EventEmitter<string>();
  @Input() isUpdate;
  answerId:number;
  sub_type: any;
  filter_data: any;
  heading:string;
  active_subtype:number;
  answerModels:any={};
  isSaved:boolean;

  answerValues:any={
  	type:'LP',
  	sub_type:{
	  'LP':{}
  	}
  	
  }
  constructor(private global: Global, private commonService: CommonService, private message: MessageService) {
    this.isSaved = false;
  }

ngOnInit() {
  	this.heading = 'Liver Profile';
  	this.filter_data = this.question_data;
    // if(this.isUpdate){
      this.commonService.getAll(this.global.apiUrl + '/api/questionnarie/question-answer/' + 'LP')
          .subscribe(data=>{
            if(data.data.LP){
            this.answerId = data.answer_id;
            let lp = [];
            data.data.LP.map((dat)=>{lp.push([dat.ques_id, String(dat.answer_given)])});
            this.answerValues.sub_type.LP = lp.reduce((prev,curr)=>{prev[curr[0]]=curr[1];return prev;},{});
            this.answerModels = this.answerValues.sub_type.LP;
            this.isSaved = true;
            }
          });
    // }
  }

/**
* submit value
*/
  submitValues(){
  	if(Object.entries(this.answerModels).length < 10){
  		this.message.error('All questions must be checked');
  		return false;
  	}
  	this.answerValues.sub_type.LP = this.answerModels;
    if(this.isSaved){
      this.commonService.update(this.global.apiUrl + '/api/questionnarie', this.answerId,this.answerValues)
          .subscribe((data)=>{
            this.commonService.callQuestionnarie('data');
            this.message.success('Successfully Updated');
          },(error)=>{});
    }else{
  	this.commonService.create(this.global.apiUrl + '/api/questionnarie', this.answerValues)
  		.subscribe(data=>{
  			if(data.status ==200){
  				Swal.fire({
            text: "Thank You for completing cleansing system questions. Please click on any incomplete sheet above.",
            type: 'success',
            customClass: {
              confirmButton: 'okBtn'
            },
            showCancelButton: false,
            confirmButtonColor: '#9bd025',
            cancelButtonColor: '#d33',
            confirmButtonText: 'OK'
          }).then((result) => {
            if (result.value) {
              this.commonService.callQuestionnarie('data');
              this.isSaved = true;
            }
          });
  			}
  		}, error=>{});
    }

  }

}
