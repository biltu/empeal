import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImmuneProfileComponent } from './immune-profile.component';

describe('ImmuneProfileComponent', () => {
  let component: ImmuneProfileComponent;
  let fixture: ComponentFixture<ImmuneProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImmuneProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImmuneProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
