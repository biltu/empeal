import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Global} from '../../../global';
import { CommonService } from '../../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../../services/message.service';
declare var Swal: any;

@Component({
  selector: 'app-immune-profile',
  templateUrl: './immune-profile.component.html',
  styleUrls: ['./immune-profile.component.css']
})
export class ImmuneProfileComponent implements OnInit {

  @Input() question_data;
  @Output() sentEvent = new EventEmitter<string>();
  @Input() isUpdate;
  answerId:number;
  sub_type: any;
  filter_data: any;
  heading:string;
  active_subtype:number;
  draft_answerscore: any=[];
  set_data: any;
  answerModels:any={};
  isSaved:boolean;
  answerValues:any={
  	type:'IP',
  	sub_type:{
	  'IMMUNE':{}
  	}
  	
  }
  constructor(private global: Global, private commonService: CommonService, private message: MessageService) {
    this.sub_type = [
      {'no':1,'name':'IMMUNE'}
    ];
    this.isSaved = false;
  }

ngOnInit() {
  	this.heading = this.sub_type[0].name;
    this.active_subtype = this.sub_type[0].no;
    this.filter_data = this.question_data;
    // if(this.isUpdate){
        this.commonService.getAll(this.global.apiUrl + '/api/questionnarie/question-answer/' + 'IP')
        .subscribe((data)=>{
          if(data.data){
            this.answerId = data.answer_id;
            this.draft_answerscore = data.draft_answerscore;
            if(data.data.IMMUNE){
            let immune = [];
            data.data.IMMUNE.map((dat)=>{immune.push([dat.ques_id, String(dat.answer_given)])});
            this.answerValues.sub_type.IMMUNE = immune.reduce((prev,curr)=>{prev[curr[0]]=curr[1];return prev;},{});
            this.answerModels = this.answerValues.sub_type.IMMUNE;
            this.isSaved = true;
            }
          }
        }, (error)=>{});
    // }
  }

  goToNxt(subtype){
    if(Object.entries(this.answerModels).length < 8){
      this.message.error('At least 8 questions must be checked.');
      return false;
    }
      switch (this.heading) {
        case "IMMUNE":
          this.answerValues.sub_type.IMMUNE = this.answerModels;
          break;
        default:
          // code...
          break;
      }
    this.active_subtype = this.sub_type[subtype].no;
    this.filter_data = this.set_data.filter(qus=>qus.ques_sub_type == this.sub_type[subtype].name);

  }

  goToPrevious(subtype){
    this.heading = this.sub_type[subtype].name;
    this.active_subtype = this.sub_type[subtype].no;
    this.filter_data = this.set_data.filter(qus=>qus.ques_sub_type == this.sub_type[subtype].name);

        switch (this.heading) {
        case "IMMUNE":
          this.answerModels = this.answerValues.sub_type.IMMUNE;
          break;
        default:
          // code...
          break;
      }
  }

/**
* submit value
*/

  submitValues(){
  	if(Object.entries(this.answerModels).length < 8){
  		this.message.error('All questions must be checked');
  		return false;
  	}
  	this.answerValues.sub_type.IMMUNE = this.answerModels;

    if(this.isSaved){
      this.commonService.update(this.global.apiUrl + '/api/questionnarie', this.answerId,this.answerValues)
          .subscribe((data)=>{
            this.commonService.callQuestionnarie('data');
            this.message.success('Successfully Updated');
          },(error)=>{});
    }else{
  	this.commonService.create(this.global.apiUrl + '/api/questionnarie', this.answerValues)
  		.subscribe(data=>{
  			if(data.status ==200){
  				Swal.fire({
            text: "Thank You for completing defence & repair questions. Please click on any incomplete sheet above.",
            type: 'success',
            showCancelButton: false,
            customClass: {
              confirmButton: 'okBtn'
            },
            confirmButtonColor: '#9bd025',
            cancelButtonColor: '#d33',
            confirmButtonText: 'OK'
          }).then((result) => {
            if (result.value) {
              this.commonService.callQuestionnarie('data');
              this.isSaved = true;
            }
          });
  			}
  		}, error=>{});
    }
  }


}
