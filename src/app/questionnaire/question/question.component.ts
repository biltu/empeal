import { Component, OnInit } from '@angular/core';
import {Global} from '../../global';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../services/message.service';
import {AuthService} from '../../services/auth.service';
declare var Swal: any;
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {
	questions: any=[];
	question_state: string;
	completedState: any=[];
  user_data: any;
  user_detail: any;
  current_state: string;
  isUpdatePrev: boolean =false;
  draft_answerscore: any=[];
  is_enable_submit: any;
  permissions: any=[];
  smodule:string;
  constructor(private message:MessageService ,private global: Global,
  private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute,
  private authService: AuthService) { 
    this.user_data = this.authService.isUserLoginIn ? JSON.parse(localStorage.getItem('user')):'';
    this.user_detail = this.user_data? this.user_data.user_detail:''; 
    // this.questionnermodules();
    // console.log(this.permissions);
    this.router.events.pipe(filter((event: any) => event instanceof NavigationEnd)).subscribe((e)=>{
      //this.smodule = this.permissions.indexOf('PS') !== -1 ? 'PS' : this.permissions.slice(1, 0);
      this.commonService.getAll(this.global.apiUrl + '/api/user/permission/modules').subscribe((data)=>{
        this.permissions = data.data.filter(obj=>obj.sub_module !=null).map(obj=>obj.sub_module);
        this.smodule = this.permissions.indexOf('PS') !== -1 ? 'PS' : this.permissions[0];
        this.current_state = this.activeRoute.snapshot.paramMap.get("type")? this.activeRoute.snapshot.paramMap.get("type"):this.smodule;
        this.asyncInit(this.current_state);
      });
    });
  }

  ngOnInit() {
    this.commonService.updateQuestionnarie.subscribe(data=>{
      this.asyncInit(this.current_state);
    });
  }

  asyncInit(state){
    let statte = state ? state:'PS';
    //console.log(statte);
  	this.commonService.getAll(this.global.apiUrl + '/api/questionnarie?ques_type='+statte)
  		.subscribe((data)=>{
  			this.questions = data.data;
  			this.question_state = data.state;
        this.current_state = data.state;
        this.draft_answerscore = data.draft_answerscore;
        this.is_enable_submit = data.enable_submit;
        this.completedState = data.completed_state ? data.completed_state:[];
  		}, (error)=>{});
  }

  getNewState(event){
    
  }

  gotToPrevious(state){
     
  }

  getCurrentStateValue(state){

  }

  questionnermodules(){
    this.commonService.getAll(this.global.apiUrl + '/api/user/permission/modules').subscribe((data)=>{
      this.permissions = data.data.filter(obj=>obj.sub_module !=null).map(obj=>obj.sub_module);
    });    
  }

  completeProcess(){
    this.commonService.create(this.global.apiUrl + '/api/questionnarie/final-process', '')
      .subscribe(data=>{
        if(data.status ==200){
          Swal.fire({
            text: "Thank You for completing questionnaire.",
            type: 'success',
            showCancelButton: false,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'OK'
          }).then((result) => {
            if (result.value) {
              this.router.navigate(['/questionnaire']);
            }
          });
          
          //personal-cross-profile
          //this.sentEvent.emit(data.state);
        }
      }, error=>{});
  }

}
