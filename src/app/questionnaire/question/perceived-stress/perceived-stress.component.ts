import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Global} from '../../../global';
import { CommonService } from '../../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../../services/message.service';
declare var Swal: any;

@Component({
  selector: 'app-perceived-stress',
  templateUrl: './perceived-stress.component.html',
  styleUrls: ['./perceived-stress.component.css']
})
export class PerceivedStressComponent implements OnInit {

  @Input() question_data;
  @Output() sentEvent = new EventEmitter<string>();
  @Input() isUpdate;
  answerId:number;
  sub_type: any;
  filter_data: any;
  heading:string;
  draft_answerscore: any=[];
  active_subtype:number;
  answerModels:any={};
  isSaved:boolean;
  answerValues:any={
  	type:'PS',
  	sub_type:{
	  'PS':{}
  	}
  	
  }
  constructor(private global: Global, private commonService: CommonService, private message: MessageService) {
    this.isSaved = false;
  }

  ngOnInit() {
  	this.heading = 'PERCEIVED STRESS';
  	this.filter_data = this.question_data;
    /*if(this.isUpdate){*/
      this.commonService.getAll(this.global.apiUrl + '/api/questionnarie/question-answer/' + 'PS')
          .subscribe(data=>{
            if(data.data){
            this.answerId = data.answer_id;
            this.draft_answerscore = data.draft_answerscore;
            let ps = [];
            data.data.PS.map((dat)=>{ps.push([dat.ques_id, String(dat.answer_given)])});
            this.answerValues.sub_type.PS = ps.reduce((prev,curr)=>{prev[curr[0]]=curr[1];return prev;},{});
            this.answerModels = this.answerValues.sub_type.PS;
            this.isSaved = true;
            }
          });
   /* }*/
  }

/**
* submit value
*/
  submitValues(){
  	if(Object.entries(this.answerModels).length < 10){
  		this.message.error('All questions must be checked');
  		return false;
  	}
  	this.answerValues.sub_type.PS = this.answerModels;
    if(this.isSaved){
      this.commonService.update(this.global.apiUrl + '/api/questionnarie', this.answerId,this.answerValues)
          .subscribe((data)=>{
            this.commonService.callQuestionnarie('data');
            this.message.success('Successfully Updated');
          },(error)=>{});
    }else{
  	this.commonService.create(this.global.apiUrl + '/api/questionnarie', this.answerValues)
  		.subscribe(data=>{
  			if(data.status ==200){
  				Swal.fire({
            text: "Thank You for completing Perceived Stress questions.Please click on any incomplete sheet above.",
            type: 'success',
            customClass: {
              confirmButton: 'okBtn'
            },
            showCancelButton: false,
            confirmButtonColor: '#9bd025',
            cancelButtonColor: '#d33',
            confirmButtonText: 'OK'
          }).then((result) => {
            if (result.value) {
               this.commonService.callQuestionnarie('data');
               this.isSaved = true;
            }
          });
  			}
  		}, error=>{});
    }

  }

  questionStrReplace(data){
    let str = data;
    return str.replace('In the last month, how often have you','');
  }

}
