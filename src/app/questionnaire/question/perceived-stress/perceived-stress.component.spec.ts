import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PerceivedStressComponent } from './perceived-stress.component';

describe('PerceivedStressComponent', () => {
  let component: PerceivedStressComponent;
  let fixture: ComponentFixture<PerceivedStressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerceivedStressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PerceivedStressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
