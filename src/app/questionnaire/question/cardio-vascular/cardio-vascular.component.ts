import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Global} from '../../../global';
import { CommonService } from '../../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../../services/message.service';
declare var Swal: any;

@Component({
  selector: 'app-cardio-vascular',
  templateUrl: './cardio-vascular.component.html',
  styleUrls: ['./cardio-vascular.component.css']
})
export class CardioVascularComponent implements OnInit {

  @Input() question_data;
  @Output() sentEvent = new EventEmitter<string>();
  @Input() isUpdate;
  answerId:number;
  sub_type: any;
  filter_data: any;
  heading:string;
  draft_answerscore: any=[];
  active_subtype:number;
  isSaved:boolean;
  answerModels:any={};
  answerValues:any={
  	type:'CVP',
  	sub_type:{
	  'CVP':{}
  	}
  	
  }
   constructor(private global: Global, private commonService: CommonService, private message: MessageService) {
     this.isSaved = false;
  }

  ngOnInit() {
  	this.heading = 'Cardiovascular Profile';
  	this.filter_data = this.question_data;
      this.commonService.getAll(this.global.apiUrl + '/api/questionnarie/question-answer/' + 'CVP')
          .subscribe(data=>{
            if(data.data && data.data.CVP.length){
            this.answerId = data.answer_id;
            this.draft_answerscore = data.draft_answerscore;
            let cvp = [];
            data.data.CVP.map((dat)=>{cvp.push([dat.ques_id, String(dat.answer_given)])});
            this.answerValues.sub_type.CVP = cvp.reduce((prev,curr)=>{prev[curr[0]]=curr[1];return prev;},{});
            this.answerModels = this.answerValues.sub_type.CVP;
            this.isSaved = true;
            }
          });
  }

/**
* Submit questions value
*/
  submitValues(){
  	if(Object.entries(this.answerModels).length < 7){
  		this.message.error('All questions must be checked');
  		return false;
  	}
  	this.answerValues.sub_type.CVP = this.answerModels;

    if(this.isSaved){
      this.commonService.update(this.global.apiUrl + '/api/questionnarie', this.answerId,this.answerValues)
        .subscribe((data)=>{
          this.commonService.callQuestionnarie('data');
          this.message.success('Successfully Updated');
        },(error)=>{});
    }else{
  	this.commonService.create(this.global.apiUrl + '/api/questionnarie', this.answerValues)
  		.subscribe(data=>{
  			if(data.status ==200){
  				Swal.fire({
            text: "Thank You for completing Heart Health questions. Please click on any incomplete sheet above.",
            type: 'success',
            customClass: {
              confirmButton: 'okBtn'
            },
            showCancelButton: false,
            confirmButtonColor: '#9bd025',
            cancelButtonColor: '#d33',
            confirmButtonText: 'OK'
          }).then((result) => {
            if (result.value) {
              this.commonService.callQuestionnarie('data');
              this.isSaved = true;
            }
          });
  			}
  		}, error=>{});
    }

  }


}
