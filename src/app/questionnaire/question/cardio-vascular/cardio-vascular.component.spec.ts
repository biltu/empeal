import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardioVascularComponent } from './cardio-vascular.component';

describe('CardioVascularComponent', () => {
  let component: CardioVascularComponent;
  let fixture: ComponentFixture<CardioVascularComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardioVascularComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardioVascularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
