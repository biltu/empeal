import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Global} from '../../../global';
import { CommonService } from '../../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../../services/message.service';
declare var Swal: any;

@Component({
  selector: 'app-dietary',
  templateUrl: './dietary.component.html',
  styleUrls: ['./dietary.component.css']
})
export class DietaryComponent implements OnInit {

  @Input() question_data;
  @Output() sentEvent = new EventEmitter<string>();
  @Input() isUpdate;
  answerId:number;
  sub_type: any;
  filter_data: any;
  heading:string;
  active_subtype:number;
  draft_answerscore: any=[];
  isSaved:boolean;
  answerModels:any={};
  answerValues:any={
  	type:'DIET',
  	sub_type:{
	  'DYD':{}
  	}
  	
  }
   constructor(private global: Global, private commonService: CommonService, private message: MessageService) {
    this.sub_type = [
      {'no':1,'name':'DYD'}
    ];
    this.isSaved = false;
  }

  ngOnInit() {
  	this.heading = this.sub_type[0].name;
    this.active_subtype = this.sub_type[0].no;
  	this.filter_data = this.question_data.filter(qus=>qus.ques_sub_type == this.sub_type[0].name);
        this.commonService.getAll(this.global.apiUrl + '/api/questionnarie/question-answer/' + 'DIET')
        .subscribe((data)=>{
          if(data.data){
            this.answerId = data.answer_id;
            this.draft_answerscore = data.draft_answerscore;
            let dyd = [];
            data.data.DYD.map((dat)=>{dyd.push([dat.ques_id, String(dat.answer_given)])});
            this.answerValues.sub_type.DYD = dyd.reduce((prev,curr)=>{prev[curr[0]]=curr[1];return prev;},{});
            this.answerModels = this.answerValues.sub_type.DYD;
            this.isSaved = true;
          }
        }, (error)=>{});
  }

  goToNxt(subtype){
    if(Object.entries(this.answerModels).length < 10){
      this.message.error('All questions must be checked');
      return false;
    }
      switch (this.heading) {
        case "DYD":
          this.answerValues.sub_type.DYD = this.answerModels;
          break;
        default:
          // code...
          break;
      }
    this.heading = this.sub_type[subtype].name;
    this.active_subtype = this.sub_type[subtype].no;
    this.filter_data = this.question_data.filter(qus=>qus.ques_sub_type == this.sub_type[subtype].name);

  }

  goToPrevious(subtype){
    this.answerModels={};
    this.heading = this.sub_type[subtype].name;
    this.active_subtype = this.sub_type[subtype].no;
    this.filter_data = this.question_data.filter(qus=>qus.ques_sub_type == this.sub_type[subtype].name);

        switch (this.heading) {
        case "DYD":
          this.answerModels = this.answerValues.sub_type.DYD;
          break;
        default:
          // code...
          break;
      }
  }

/**
* submit value
*/

  submitValues(){
  	if(Object.entries(this.answerModels).length < 10){
  		this.message.error('All questions must be checked');
  		return false;
  	}
  	this.answerValues.sub_type.DYD = this.answerModels;
    if(this.isSaved){
      this.commonService.update(this.global.apiUrl + '/api/questionnarie', this.answerId,this.answerValues)
          .subscribe((data)=>{
            this.commonService.callQuestionnarie('data');
            this.message.success('Successfully Updated');
          },(error)=>{});
    }else{
  	  this.commonService.create(this.global.apiUrl + '/api/questionnarie', this.answerValues)
  		.subscribe(data=>{
  			if(data.status ==200){
  				Swal.fire({
            text: "Thank You for completing Food Choices questions. Please click on any incomplete sheet above.",
            type: 'success',
            customClass: {
              confirmButton: 'okBtn'
            },
            showCancelButton: false,
            confirmButtonColor: '#9bd025',
            cancelButtonColor: '#d33',
            confirmButtonText: 'OK'
          }).then((result) => {
            if (result.value) {
              this.commonService.callQuestionnarie('data');
              this.isSaved = true;
            }
          });
  			}
  		}, error=>{});
    }
  }


}
