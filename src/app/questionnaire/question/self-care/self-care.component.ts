import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Global} from '../../../global';
import { CommonService } from '../../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../../services/message.service';
declare var Swal: any;

@Component({
  selector: 'app-self-care',
  templateUrl: './self-care.component.html',
  styleUrls: ['./self-care.component.css']
})
export class SelfCareComponent implements OnInit {

  @Input() question_data;
  @Output() sentEvent = new EventEmitter<string>();
  @Input() isUpdate;
  answerId:number;
  sub_type: any;
  filter_data: any;
  heading:string;
  set_data: any;
  active_subtype:number;
  answerModels:any={};
  answerValues:any={
  	type:'SC',
  	sub_type:{
	  'PHYSICAL':{},
	  'MENTAL':{},
	  'PROFESSIONAL':{},
	  'SOCIAL':{},
    'Ergonomics':{}
  	}
  	
  }
  initialValues:any=[];
  constructor(private global: Global, private commonService: CommonService, private message: MessageService) { 
  	this.sub_type = [
	  	{'no':1,'name':'PHYSICAL'},
	  	{'no':2,'name':'MENTAL'},
	  	{'no':3,'name':'PROFESSIONAL'},
	  	{'no':4,'name':'SOCIAL'},
      {'no':5,'name':'Ergonomics'}
  	];
  }

  ngOnInit() {
  	this.heading = this.sub_type[0].name;
  	this.active_subtype = this.sub_type[0].no;
    this.set_data = this.question_data.map((qus)=>{
      return {'id':qus.id, 'question':qus.question,'ques_sub_type':qus.ques_sub_type,'options':qus.option_labels.split('|')}
    });
  	this.filter_data = this.set_data.filter(qus=>qus.ques_sub_type == this.sub_type[0].name);
    if(this.isUpdate){
        this.commonService.getAll(this.global.apiUrl + '/api/questionnarie/question-answer/' + 'SC')
        .subscribe((data)=>{
          if(data.data){
            this.answerId = data.answer_id;
            let phy = [];
            data.data.PHYSICAL.map((dat)=>{phy.push([dat.ques_id, String(dat.answer_given)])});
            this.answerValues.sub_type.PHYSICAL = phy.reduce((prev,curr)=>{prev[curr[0]]=curr[1];return prev;},{});
            this.answerModels = this.answerValues.sub_type.PHYSICAL;
            let ment = [];
            data.data.MENTAL.map((dat)=>{ment.push([dat.ques_id, String(dat.answer_given)])});
            this.answerValues.sub_type.MENTAL = ment.reduce((prev,curr)=>{prev[curr[0]]=curr[1];return prev;},{});
            let prof =  [];
            data.data.PROFESSIONAL.map((dat)=>{prof.push([dat.ques_id, String(dat.answer_given)])});
            this.answerValues.sub_type.PROFESSIONAL = prof.reduce((prev,curr)=>{prev[curr[0]]=curr[1];return prev;},{});
            let social = [];
            data.data.SOCIAL.map((dat)=>{social.push([dat.ques_id, String(dat.answer_given)])});
            this.answerValues.sub_type.SOCIAL = social.reduce((prev,curr)=>{prev[curr[0]]=curr[1];return prev;},{});
            let erog = [];
            data.data.Ergonomics.map((dat)=>{erog.push([dat.ques_id, String(dat.answer_given)])});
            this.answerValues.sub_type.Ergonomics = erog.reduce((prev,curr)=>{prev[curr[0]]=curr[1];return prev;},{});
          }
          
        }, (error)=>{});
    }

  }

  setValues(event){
  	this.initialValues = Object.entries(this.answerModels);
  }

  goToNxt(subtype){
  	if(Object.entries(this.answerModels).length < 5){
  		this.message.error('At least 5 questions must be checked.');
  		return false;
  	}
  		switch (this.heading) {
  			case "PHYSICAL":
  				this.answerValues.sub_type.PHYSICAL = this.answerModels;
  				break;
  			case "MENTAL":
  				this.answerValues.sub_type.MENTAL = this.answerModels;
  				break;
  			case "PROFESSIONAL":
  				this.answerValues.sub_type.PROFESSIONAL = this.answerModels;
  				break;
  			case "SOCIAL":
  				this.answerValues.sub_type.SOCIAL = this.answerModels;
  				break;
        case "Ergonomics":
          this.answerValues.sub_type.Ergonomics = this.answerModels;
          break;
  			default:
  				// code...
  				break;
  		}
  	this.answerModels={};
  	this.heading = this.sub_type[subtype].name;
  	this.active_subtype = this.sub_type[subtype].no;
  	this.filter_data = this.set_data.filter(qus=>qus.ques_sub_type == this.sub_type[subtype].name); 
      switch (this.heading) {
        case "PHYSICAL":
          this.answerModels = Object.entries(this.answerValues.sub_type.PHYSICAL).length ?this.answerValues.sub_type.PHYSICAL : this.answerModels;
          break;
        case "MENTAL":
          this.answerModels =  Object.entries(this.answerValues.sub_type.MENTAL).length ?this.answerValues.sub_type.MENTAL : this.answerModels;
          break;
        case "PROFESSIONAL":
          this.answerModels = Object.entries(this.answerValues.sub_type.PROFESSIONAL).length ?this.answerValues.sub_type.PROFESSIONAL : this.answerModels;
          break;
        case "SOCIAL":
          this.answerModels = Object.entries(this.answerValues.sub_type.SOCIAL).length ?this.answerValues.sub_type.SOCIAL : this.answerModels;
          break;
        case "Ergonomics":
          this.answerModels = Object.entries(this.answerValues.sub_type.Ergonomics).length ?this.answerValues.sub_type.Ergonomics : this.answerModels;
          break;
        default:
          // code...
          break;
      }

  }

  goToPrevious(subtype){
  	this.answerModels={};
  	this.heading = this.sub_type[subtype].name;
  	this.active_subtype = this.sub_type[subtype].no;
  	this.filter_data = this.set_data.filter(qus=>qus.ques_sub_type == this.sub_type[subtype].name);

  	  	switch (this.heading) {
  			case "PHYSICAL":
  				this.answerModels = this.answerValues.sub_type.PHYSICAL;
  				break;
  			case "MENTAL":
  				this.answerModels = this.answerValues.sub_type.MENTAL;
  				break;
  			case "PROFESSIONAL":
  				this.answerModels = this.answerValues.sub_type.PROFESSIONAL;
  				break;
  			case "SOCIAL":
  				this.answerModels = this.answerValues.sub_type.SOCIAL;
  				break;
        case "Ergonomics":
          this.answerModels = this.answerValues.sub_type.Ergonomics;
          break;
  			default:
  				// code...
  				break;
  		}
  }

/**
* submit value
*/
  submitValues(){
  	if(Object.entries(this.answerModels).length < 5){
  		this.message.error('At least 5 questions must be checked.');
  		return false;
  	}
  	this.answerValues.sub_type.Ergonomics = this.answerModels;

    if(this.isUpdate){
      this.commonService.update(this.global.apiUrl + '/api/questionnarie', this.answerId,this.answerValues)
          .subscribe((data)=>{
            this.message.success('Successfully Updated');
          },(error)=>{});
    }else{
      this.commonService.create(this.global.apiUrl + '/api/questionnarie', this.answerValues)
      .subscribe(data=>{
        if(data.status ==200){
          this.answerModels={};
          Swal.fire({
            text: "Thank You for completing self care questions. Next question Sheet is "+ data.state_name +".",
            type: 'success',
            className: 'detailBtn',
            customClass: {
              confirmButton: 'okBtn'
            },
            showCancelButton: false,
            confirmButtonColor: '#9bd025',
            cancelButtonColor: '#d33',
            confirmButtonText: 'OK'
          }).then((result) => {
            if (result.value) {
              this.sentEvent.emit(data.state);
            }
          });          
        }
      }, error=>{});

    }


  }


}
