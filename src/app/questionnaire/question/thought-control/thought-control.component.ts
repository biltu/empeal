import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Global} from '../../../global';
import { CommonService } from '../../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../../services/message.service';
declare var Swal: any;

@Component({
  selector: 'app-thought-control',
  templateUrl: './thought-control.component.html',
  styleUrls: ['./thought-control.component.css']
})
export class ThoughtControlComponent implements OnInit {

  @Input() question_data;
  @Output() sentEvent = new EventEmitter<string>();
  @Input() isUpdate;
  sub_type: any;
  filter_data: any;
  heading:string;
  active_subtype:number;
  answerModels:any={};
  answerValues:any={
  	type:'TC',
  	sub_type:{
	  'OCD':{},
    'Depression':{}
  	}
  	
  }
  constructor(private global: Global, private commonService: CommonService, private message: MessageService, 
    private router: Router) {
    this.sub_type = [
      {'no':1,'name':'OCD'},
      {'no':2,'name':'Depression'}
    ];
  }

ngOnInit() {
  	this.heading = this.sub_type[0].name;
    this.active_subtype = this.sub_type[0].no;
  	this.filter_data = this.question_data.filter(qus=>qus.ques_sub_type == this.sub_type[0].name);
  }

  goToNxt(subtype){
    if(Object.entries(this.answerModels).length < 5){
      this.message.error('At least 5 questions must be checked.');
      return false;
    }
      switch (this.heading) {
        case "OCD":
          this.answerValues.sub_type.OCD = this.answerModels;
          break;
        case "Depression":
          this.answerValues.sub_type.Depression = this.answerModels;
          break;
        default:
          // code...
          break;
      }
    this.answerModels={};
    this.heading = this.sub_type[subtype].name;
    this.active_subtype = this.sub_type[subtype].no;
    this.filter_data = this.question_data.filter(qus=>qus.ques_sub_type == this.sub_type[subtype].name);

  }

  goToPrevious(subtype){
    this.answerModels={};
    this.heading = this.sub_type[subtype].name;
    this.active_subtype = this.sub_type[subtype].no;
    this.filter_data = this.question_data.filter(qus=>qus.ques_sub_type == this.sub_type[subtype].name);

        switch (this.heading) {
        case "OCD":
          this.answerModels = this.answerValues.sub_type.OCD;
          break;
        case "Depression":
          this.answerModels = this.answerValues.sub_type.Depression;
          break;
        default:
          // code...
          break;
      }
  }
/**
* submit value
*/
  submitValues(){
  	if(Object.entries(this.answerModels).length < 5){
  		this.message.error('At least 5 questions must be checked.');
  		return false;
  	}
  	this.answerValues.sub_type.Depression = this.answerModels;
  	this.commonService.create(this.global.apiUrl + '/api/questionnarie', this.answerValues)
  		.subscribe(data=>{
  			if(data.status ==200){
  				this.answerModels={};
          Swal.fire({
            text: "Thank You for completing Thought Control.",
            type: 'success',
            showCancelButton: false,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'OK'
          }).then((result) => {
            if (result.value) {
              this.router.navigate(['/questionnaire']);
            }
          });
  			}
  		}, error=>{});
  }

}
