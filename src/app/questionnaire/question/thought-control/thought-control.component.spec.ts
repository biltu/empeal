import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThoughtControlComponent } from './thought-control.component';

describe('ThoughtControlComponent', () => {
  let component: ThoughtControlComponent;
  let fixture: ComponentFixture<ThoughtControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThoughtControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThoughtControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
