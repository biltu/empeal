import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CoreModule} from '../core/core.module';

import { QuestionnaireRoutingModule } from './questionnaire-routing.module';
import { QuestionnaryComponent } from './questionnary/questionnary.component';
import { QuestionComponent } from './question/question.component';
import { SelfCareComponent } from './question/self-care/self-care.component';
import { PerceivedStressComponent } from './question/perceived-stress/perceived-stress.component';
import { FitnessComponent } from './question/fitness/fitness.component';
import { DigestiveTractComponent } from './question/digestive-tract/digestive-tract.component';
import { LiverProfileComponent } from './question/liver-profile/liver-profile.component';
import { EndocrineSystemComponent } from './question/endocrine-system/endocrine-system.component';
import { ImmuneProfileComponent } from './question/immune-profile/immune-profile.component';
import { CardioVascularComponent } from './question/cardio-vascular/cardio-vascular.component';
import { MenOnlyComponent } from './question/men-only/men-only.component';
import { EvmFactorComponent } from './question/evm-factor/evm-factor.component';
import { DietaryComponent } from './question/dietary/dietary.component';
import { ThoughtControlComponent } from './question/thought-control/thought-control.component';
import { PersonalCrossProfileComponent } from './personal-cross-profile/personal-cross-profile.component';
import { QuestionnaireReportComponent } from './questionnaire-report/questionnaire-report.component';

@NgModule({
  declarations: [QuestionnaryComponent, QuestionComponent, SelfCareComponent, PerceivedStressComponent, FitnessComponent, DigestiveTractComponent, LiverProfileComponent, EndocrineSystemComponent, ImmuneProfileComponent, CardioVascularComponent, MenOnlyComponent, EvmFactorComponent, DietaryComponent, ThoughtControlComponent, PersonalCrossProfileComponent, QuestionnaireReportComponent],
  imports: [
    CommonModule,
    QuestionnaireRoutingModule,
    CoreModule
  ]
})
export class QuestionnaireModule { }
