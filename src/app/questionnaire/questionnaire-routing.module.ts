import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './../auth.guard';
import { QuestionnaryComponent } from './questionnary/questionnary.component';
import { QuestionComponent } from './question/question.component';
import { PersonalCrossProfileComponent } from './personal-cross-profile/personal-cross-profile.component';
import { QuestionnaireReportComponent } from './questionnaire-report/questionnaire-report.component';
import { StatutoryGuard } from './../statutory.guard';

const routes: Routes = [
	{path:'', canActivate: [AuthGuard, StatutoryGuard], component: QuestionnaryComponent},
	{path:'question', canActivate: [AuthGuard, StatutoryGuard], component: QuestionComponent},
	{path:'question/:type', canActivate: [AuthGuard, StatutoryGuard], component: QuestionComponent},
	{path:'personal-cross-profile', canActivate: [AuthGuard, StatutoryGuard], component: PersonalCrossProfileComponent},
	{path:'report/:id/:date', canActivate: [AuthGuard, StatutoryGuard], component: QuestionnaireReportComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuestionnaireRoutingModule { }
