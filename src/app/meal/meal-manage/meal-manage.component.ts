import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../services/message.service';
import {debounceTime,distinctUntilChanged,map} from 'rxjs/operators';
import { Subject } from "rxjs/Subject";
import { Observable } from "rxjs/Observable";
import { Chart, ChartOptions } from 'chart.js';
import { introJs } from "intro.js/intro.js";
import { CookieService } from 'ngx-cookie-service';
import { DeviceDetectorService } from 'ngx-device-detector';
declare var Swal: any;
declare var $:any;
interface IWindow extends Window {
	webkitSpeechRecognition: any;
}
@Component({
  selector: 'app-meal-manage',
  templateUrl: './meal-manage.component.html',
  styleUrls: ['./meal-manage.component.css']
})
export class MealManageComponent implements OnInit {
	introJS = introJs();
	isModalOpen: boolean;
	appointments: any=[];
	mealAddForm: FormGroup;
	display: string;
	chartdisplay:string;
  	model: any;
  	getitems: any=[];
  	today: any;
  	show_jb_title_srh: boolean;
  	totalmeals: number;
  	meals: any=[];
  	show:boolean = false;
  	soption: any=[];
  	nutrients: any=[];
  	bardata: any=[];
  	allnutrns: any=[];
  	gender:any;
  	validationmsg:any;
  	activinfogrph: string;
  	screenwidth:any;
    isMobile:any;
  	pieChartOptions:any = {
        //responsive: true,
     	//  chartArea: {
     	//	backgroundColor: 'rgba(251, 85, 85, 0.4)'
    	// },
        hover: {
	      animationDuration: 0
	    },
	    animation: {
	      duration: 1,
	      onComplete: function() {
	      	let that=this;
	        let chartInstance = this.chart,
	        ctx = chartInstance.ctx;
	        ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
	        ctx.textAlign = 'center';
	        ctx.textBaseline = 'bottom';
	        let user_data = JSON.parse(localStorage.getItem('user'));
      		let user_detail = user_data? user_data.user_detail:'';

	        this.data.datasets.forEach(function(dataset, i) {
	          let meta = chartInstance.controller.getDatasetMeta(i);
	          meta.data.forEach(function(bar, index) {
	            let data = dataset.data[index];
	            let mxval = 0;
	            if(bar._model.label == 'Carbohydrate'){
	            	ctx.fillText(((data*275)/100).toFixed(2) + ' gm', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Protien'){
	            	mxval = (user_detail.gender=='F')? 55:62.5;
	            	ctx.fillText(((data*mxval)/100).toFixed(2) + ' gm', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Fat'){
	            	mxval = (user_detail.gender=='F')? 55:62.5;
	            	ctx.fillText(((data*mxval)/100).toFixed(2) + ' gm', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Sugar'){
	            	mxval = (user_detail.gender=='F')? 15:21.25;
	            	ctx.fillText(((data*mxval)/100).toFixed(2) + ' gm', bar._model.x, bar._model.y);
	            }else if(bar._model.label == 'Fibre'){
	            	mxval = (user_detail.gender=='F')? 30:55;
	            	ctx.fillText(((data*mxval)/100).toFixed(2) + ' gm', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'O6/O3'){
	            	mxval = (user_detail.gender=='F')? 6:6;
	            	ctx.fillText(((data*mxval)/100).toFixed(2) + ' Ratio', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Calcium'){
	            	mxval = (user_detail.gender=='F')? 1350:1100;
	            	ctx.fillText(((data*mxval)/100).toFixed(2) + ' mg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Magensium'){
	            	mxval = (user_detail.gender=='F')? 525:610;
	            	ctx.fillText(((data*mxval)/100).toFixed(2) + ' mg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Iron'){
	            	mxval = (user_detail.gender=='F')? 24:11.5;
	            	ctx.fillText(((data*mxval)/100).toFixed(2) + ' mg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Zinc'){
	            	mxval = (user_detail.gender=='F')? 22.5:37.5;
	            	ctx.fillText(((data*mxval)/100).toFixed(2) + ' mg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Selenium'){
	            	mxval = (user_detail.gender=='F')? 200:250;
	            	ctx.fillText(((data*mxval)/100).toFixed(2) + ' mcg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Iodine'){
	            	mxval = (user_detail.gender=='F')? 225:225;
	            	ctx.fillText(((data*mxval)/100).toFixed(2) + ' mcg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Sodium'){
	            	mxval = (user_detail.gender=='F')? 2750:2250;
	            	ctx.fillText(((data*mxval)/100).toFixed(2) + ' mg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Vitamin A'){
	            	mxval = (user_detail.gender=='F')? 2100:2250;
	            	ctx.fillText(((data*mxval)/100).toFixed(2) + ' mcg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Vitamin D'){
	            	mxval = (user_detail.gender=='F')? 11.25:12.5;
	            	ctx.fillText(((data*mxval)/100).toFixed(2) + ' mcg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Vitamin E'){
	            	mxval = (user_detail.gender=='F')? 326:326;
	            	ctx.fillText(((data*mxval)/100).toFixed(2) + ' mg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Vitamin K1'){
	            	mxval = (user_detail.gender=='F')? 200:375;
	            	ctx.fillText(((data*mxval)/100).toFixed(2) + ' mcg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Thiamin B1'){
	            	mxval = (user_detail.gender=='F')? 15.5:25.7;
	            	ctx.fillText(((data*mxval)/100).toFixed(2) + ' mg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Riboflavin B2'){
	            	mxval = (user_detail.gender=='F')? 15.6:25.8;
	            	ctx.fillText(((data*mxval)/100).toFixed(2) + ' mg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Niacin B3'){
	            	mxval = (user_detail.gender=='F')? 57.5:110;
	            	ctx.fillText(((data*mxval)/100).toFixed(2) + ' mg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Pantothenate B5'){
	            	mxval = (user_detail.gender=='F')? 128.5:128.5;
	            	ctx.fillText(((data*mxval)/100).toFixed(2) + ' mg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Vitamin B6'){
	            	mxval = (user_detail.gender=='F')? 26:51.25;
	            	ctx.fillText(((data*mxval)/100).toFixed(2) + ' mg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Folates B9'){
	            	mxval = (user_detail.gender=='F')? 600:600;
	            	ctx.fillText(((data*mxval)/100).toFixed(2) + ' mcg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Vitamin B12'){
	            	mxval = (user_detail.gender=='F')? 101.5:101.5;
	            	ctx.fillText(((data*mxval)/100).toFixed(2) + ' mcg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Biotin B7'){
	            	mxval = (user_detail.gender=='F')? 275:275;
	            	ctx.fillText(((data*mxval)/100).toFixed(2) + ' mcg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Vitamin C'){
	            	mxval = (user_detail.gender=='F')? 537.5:1045;
	            	ctx.fillText(((data*mxval)/100).toFixed(2) + ' mg', bar._model.x+30, bar._model.y+7);	
	            }else{

	            }
	            
	          });
	        });
	      }
	    },
        axisX:{
			labelBackgroundColor: "rgb(231,233,237)",
			labelFontColor: "rgb(231,233,237)"
		},
        //maintainAspectRatio: false,
        legend: {
                display: false
            },
        title: {
            display: false,
            fontSize: 20,
            padding: 16,
            text: 'Daily Nutrients Distribution'
            },
        tooltips: {
	          callbacks: {
	            label: function(tooltipItem, data) {
	                return data['datasets'][0]['data'][tooltipItem['index']] + '%';
	            },
	          },
	          position: 'custom',

	          //yAlign: 'bottom'
	        },
	    scales: {
            yAxes: [{
                ticks: {
                    //beginAtZero: true,
                    display: true,
                    fontFamily: "'Amarnath'" //Raleway
                },
    			gridLines: {
				   display: false
				}
            }],
            xAxes: [{
                ticks: {
                    //beginAtZero: true,
                    steps: 10,
                    stepSize: 10,
                    stepValue: 10,
                    max: 100,
                    min: 0,
                    display: false,
                    fontFamily: "'Amarnath'",
                    //backgroundColor: 'rgb(231,233,237)'
                },
                gridLines: {
				   display: false
				}
            }]
	    },
    };
    pieChartLabels =  ['Carbohydrate', 'Protien', 'Fat', 'Sugar', 'Fibre', 'O6/O3',
     'Calcium', 'Magensium', 'Iron', 'Zinc', 'Selenium', 'Iodine', 'Sodium', 'Vitamin A',
      'Vitamin D', 'Vitamin E', 'Vitamin K1', 'Thiamin B1', 'Riboflavin B2', 'Niacin B3',
       'Pantothenate B5', 'Vitamin B6', 'Folates B9', 'Vitamin B12', 'Biotin B7', 'Vitamin C'];
  
    // CHART COLOR.
    pieChartColor:any = [
        {
            backgroundColor: ['rgb(54, 162, 235)','rgb(54, 162, 235)','rgb(54, 162, 235)',
            'rgb(54, 162, 235)','rgb(54, 162, 235)','rgb(54, 162, 235)','rgb(54, 162, 235)',
            'rgb(54, 162, 235)','rgb(54, 162, 235)','rgb(54, 162, 235)','rgb(54, 162, 235)',
            'rgb(54, 162, 235)','rgb(54, 162, 235)','rgb(54, 162, 235)','rgb(54, 162, 235)',
            'rgb(54, 162, 235)','rgb(54, 162, 235)','rgb(54, 162, 235)','rgb(54, 162, 235)',
            'rgb(54, 162, 235)','rgb(54, 162, 235)','rgb(54, 162, 235)','rgb(54, 162, 235)',
            'rgb(54, 162, 235)','rgb(54, 162, 235)','rgb(54, 162, 235)'],
        }
    ];

    pieChartData:any = [
        { 
            data: []
        }
    ];

    topChartOptions = {
    	responsive: false,
    	maintainAspectRatio: false,
    	legend: {
                display: false,
                position: 'bottom'
            },
	    title: {
	      display: true,
	      text: ''
	    },
	    tooltips: {
	      mode: 'index',
	      intersect: true
	    },
	    scales: {
	      xAxes: [{
	        stacked: true,
	      }],
	      yAxes: [{
	        id: 'A',
	        type: 'linear',
	        position: 'left',
	        ticks: {
	          min: 0,
	          fontFamily: "'Amarnath'"
	        }
	      }, {
	        id: 'B',
	        type: 'linear',
	        position: 'right',
	        ticks: {
	          min: 0,
	          fontFamily: "'Amarnath'"
	        },
	        gridLines: {
	            display:false
	        }
	      }]
	    }
    };
    topChartLabels =  [];

    topChartData:any = [{
	      type: 'bar',
	      label: 'Carbohydrate',
	      backgroundColor: 'rgb(255, 99, 132)',
	      hoverBackgroundColor:'rgb(255, 99, 132)',
	      stack: 'Stack 0',
	      data: [],
	      borderColor: 'white',
	      borderWidth: 2,
	      yAxisID: 'A'
	    }, {
	      type: 'bar',
	      label: 'Protein',
	      backgroundColor: 'rgb(54, 162, 235)',
	      hoverBackgroundColor:'rgb(54, 162, 235)',
	      stack: 'Stack 0',
	      data: [],
	      borderColor: 'white',
	      borderWidth: 2,
	      yAxisID: 'A'
	    }, {
	      type: 'bar',
	      label: 'Fat',
	      backgroundColor: 'rgb(255, 159, 64)',
	      hoverBackgroundColor:'rgb(255, 159, 64)',
	      stack: 'Stack 0',
	      data: [],
	      borderColor: 'white',
	      borderWidth: 2,
	      yAxisID: 'A'
	    }, {
	      type: 'line',
	      label: 'Water',
	      borderColor: 'rgb(153, 102, 255)',
	      hoverBackgroundColor:'rgb(153, 102, 255)',
	      borderWidth: 2,
	      fill: false,
	      data: [],
	      yAxisID: 'B'
    }];

    popupChartOptions = {
    	responsive: false,
    	maintainAspectRatio: false,
    	legend: {
                display: false,
                position: 'bottom'
            },
	    title: {
	      display: true,
	      text: ''
	    },
	    tooltips: {
	      mode: 'index',
	      intersect: true
	    },
	    scales: {
	      xAxes: [{
	        stacked: true,
	      }],
	      yAxes: [{
	        id: 'A',
	        type: 'linear',
	        position: 'left',
	        ticks: {
	          min: 0,
	          fontFamily: "'Amarnath'"
	        }
	      }]
	    }
    };
    popupChartLabels =  [];

    popupChartData:any = [{
	      type: 'bar',
	      label: '',
	      backgroundColor: 'rgb(255, 99, 132)',
	      hoverBackgroundColor:'rgb(255, 99, 132)',
	      stack: 'Stack 0',
	      data: [],
	      borderColor: 'white',
	      borderWidth: 2,
	      yAxisID: 'A'
	    }];

  	foodkey:any;
  	singlemeal:any;
  	is_introshow:boolean = false;
  	todaytotal_meal:number;
  	foodImage= new FormControl('');
  	foodPlaceholder:string;
  	
  constructor(private fb:FormBuilder, private deviceService: DeviceDetectorService, private cookie:CookieService, private message:MessageService ,private global: Global,private authService: AuthService, private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) { 
  	this.foodPlaceholder = 'food / recipe name';
  	this.asyncInit();
  	this.gender='F';
  	this.today = new Date;
  	this.screenwidth = (<any>window.innerWidth > 0) ? <any>window.innerWidth : screen.width;
    this.isMobile = this.deviceService.isMobile();

  	// this.introJS.setOptions({
   //    steps: [
   //      { 
   //        intro: "Your Nutrients Report"
   //      },
   //      {
   //        element: '#step1',
   //        intro: "Last 30 Days Carbohydrate, Protien, Fat, Water Report!"
   //      },
   //      {
   //        element: '#step2',
   //        intro: "Click on bar!"
   //      }
   //      // {
   //      //   element: document.querySelectorAll('#step2')[0],
   //      //   intro: "Ok, wasn't that fun?",
   //      //   position: 'right'
   //      // },
   //      // {
   //      //   element: '#step3',
   //      //   intro: 'More features, more fun.',
   //      //   position: 'left'
   //      // },
   //      // {
   //      //   element: '#step4',
   //      //   intro: "Another step.",
   //      //   position: 'bottom'
   //      // },
   //      // {
   //      //   element: '#step5',
   //      //   intro: 'Get it, use it.'
   //      // }
   //    ]
   //  });

    
    if(cookie.get('mealintroshow')=='1'){
    	this.is_introshow = false;
    }else{
    	cookie.set('mealintroshow','1',7,'/meal');
    	this.is_introshow = true;
    }

    Chart.Tooltip.positioners.custom = function(elements, eventPosition) {
    /** @type {Chart.Tooltip} */
    var tooltip = this;

    /* ... */

	    return {
	        x: eventPosition.x,
	        y: eventPosition.y
	    };
	}

	setTimeout(()=>{
        $('.chart').easyPieChart({
            animate: 2000,
            lineWidth: 3,
            lineCap: 'butt',
            scaleColor: false,      
            barColor: '#a4d439',
            trackColor: '#ddd',
        });
    },1000);

  }

  	ngOnInit() {
  		this.mealAddForm = this.fb.group({
	  		mealdate:['', [Validators.required]],
	  		periodtype:['', [Validators.required]],
	 		itemname:['', [Validators.required]],
	 		itemqty:['', []],
	 		itemqtytype:['', []],
	 		liquidqty:['', []],
	 		itemid:['', []]
	  	});
	  	this.isModalOpen = false;
	  	if(this.is_introshow==true){
	  		this.introJS.start();
	  	}

	  	this.commonService.currentAppointment.subscribe((data)=>{
	      this.appointments = data.appointments;
	    });

	    // this.mealAddForm.controls['itemname'].valueChanges.subscribe(value=>{
	    // 	if(value){
	    // 		//alert(value);
	    // 		this.getValues(value);
	    // 	}
	    // });
  	}
/**
* Display food search result
*/
  	getValues(foodkey) {
  		if(foodkey.length >= 3){
	    return this.commonService.getAll(this.global.apiUrl + '/api/search/meal?foodname='+foodkey)
	    .subscribe((data)=>{
	    	if(data.data.length>0){
	    		this.getitems = data.data;
	        	this.show_jb_title_srh =true;
	    	}else{
	    		this.show_jb_title_srh =false;
	    	}
	    }, (error)=>{});
		}else{
			this.show_jb_title_srh =false;
		}
	}

	search($event) {
	    this.getValues($event.target.value);
	}
/**
* search food by voice recognition
*/
	startDictation() {
		this.mealAddForm.patchValue({
	      itemname:''
	    });
		this.foodPlaceholder = 'Speak now';
		setTimeout(() => {
          this.foodPlaceholder = 'Listening....';
        }, 500);

        setTimeout(() => {
          this.foodPlaceholder = 'food / recipe name';
        }, 8000);
	    if ((<any>window).hasOwnProperty('webkitSpeechRecognition')) {
	      const { webkitSpeechRecognition }: IWindow = <IWindow>window;
	      var recognition = new webkitSpeechRecognition();
	      let thisd = this;
	      recognition.continuous = false;
	      recognition.interimResults = false;
	      recognition.lang = "en-US";
	      recognition.start();
	      recognition.onresult = function(e) {
	      	this.foodPlaceholder = 'food / recipe name';
	      	var srchtxt = e.results[0][0].transcript;
	      	//console.log(srchtxt);
	      	var resarray = srchtxt.split(" ");
	      	var mealtype = '';
	      	var mealqty = '';
	      	var mealqtytype = '';
	      	var foodname = '';
	      	var word = '';
	      	var timearray = ["Breakfast","Lunch",'Dinner','Snacks'];
	      	var mealqtyarray = ['Portion','Unit','Quantity'];
	      	var rmwords = ['i','am','is','are','at','and','art','launch','the','in','on','was','were','have','has','had','eat','eaten','to','will','shell','he','we','they','she']
	      	var i = 0;
	      	let indexes = [];
	      	let notMatch =[];
	      resarray.forEach((val,i)=>{
	      		word = thisd.strtoUpperCase(val);
	      		if(timearray.indexOf(word) != -1){
	      			mealtype = word;
	      			indexes = [...indexes, val];
	      		}

	      		else if(mealqtyarray.indexOf(word) != -1){
	      			mealqty = word;
	      			indexes = [...indexes, val];
	      		}
	      		else if(rmwords.indexOf(val) != -1){
	      			indexes = [...indexes, val];
	      		}
	      		else{
	      			notMatch = [...notMatch, val];
	      		}
	      	});

	      	thisd.mealAddForm.patchValue({
		      periodtype: mealtype,
		      itemname:notMatch.toString().replace(",", " "), 
		      itemqty: mealqty,
		      //itemqtytype: this.singlemeal.meal_qty_type
		    });

	      	recognition.stop();
	      	thisd.getValues(notMatch.toString().replace(",", " "));
	      };
	      recognition.onerror = function(e) {
	      	this.foodPlaceholder = 'food / recipe name';
	        recognition.stop();
	      }
	    }

  	}

  	strtoUpperCase(string){
  		let str = string.trim();
  		return str[0].toUpperCase() + str.substr(1);
  	}
/**
* Display user food list
*/
  	asyncInit(){
  	this.commonService.getAll(this.global.apiUrl + '/api/meal')
  		.subscribe((data)=>{
        this.meals = data.data;
        this.totalmeals = data.totalmeals;
        this.nutrients = (data.nutrients.length>0)?data.nutrients[0]:[];
        this.allnutrns = (data.allnutrns.length>0)?data.allnutrns[0]:[];
        this.bardata.push(this.allnutrns.carbohydrate);
        this.bardata.push(this.allnutrns.protein);
        this.bardata.push(this.allnutrns.fat);
        this.bardata.push(this.allnutrns.sugars);
        this.bardata.push(this.allnutrns.fibre);
        this.bardata.push(this.allnutrns.n3poly);
        this.bardata.push(this.allnutrns.calcium);
        this.bardata.push(this.allnutrns.magnesium);
        this.bardata.push(this.allnutrns.iron);
        this.bardata.push(this.allnutrns.zinc);
        this.bardata.push(this.allnutrns.selenium);
        this.bardata.push(this.allnutrns.iodine);
        this.bardata.push(this.allnutrns.sodium);
        this.bardata.push(this.allnutrns.vita);
        this.bardata.push(this.allnutrns.vitd);
        this.bardata.push(this.allnutrns.vite);
        this.bardata.push(this.allnutrns.vitk);
        this.bardata.push(this.allnutrns.thiamin);
        this.bardata.push(this.allnutrns.riboflavin);
        this.bardata.push(this.allnutrns.niacineqv);
        this.bardata.push(this.allnutrns.pantothenate);
        this.bardata.push(this.allnutrns.vitb6);
        this.bardata.push(this.allnutrns.folate);
        this.bardata.push(this.allnutrns.vitb12);
        this.bardata.push(this.allnutrns.biotin);
        this.bardata.push(this.allnutrns.vitc);
        this.pieChartData[0].data = this.bardata;
        this.todaytotal_meal = (data.todaytotal_meal>0)?data.todaytotal_meal:0;
        this.topChartLabels = data.mealnutrients[0];
        this.topChartData[0].data = data.mealnutrients[1];
        this.topChartData[1].data = data.mealnutrients[2];
        this.topChartData[2].data = data.mealnutrients[3];
        this.topChartData[3].data = data.mealnutrients[4];
        //console.log(this.pieChartOptions.title.text);
  		}, (error)=>{});
  	}

 //  	changeModelEvent(){
 //  		if(this.mealAddForm.value.itemname && this.mealAddForm.value.itemname.length>2){
 //  			this.commonService.getAll(this.global.apiUrl + '/api/search/meal?foodname='+this.mealAddForm.value.itemname)
	//   		.subscribe((data)=>{
	//   			if(data.status==200){
	//   				this.show_jb_title_srh =true;
	//   				this.getitems = data.data;
	//   			}
	//   		}, (error)=>{});
 //  		}else{
 //  			this.show_jb_title_srh =false;
 //  			this.getitems = '';
 //  		}  		
	// }

  	openModalDialog(){
    	this.display='block'; //Set block css
    	this.isModalOpen = true;
	}

	closeModalDialog(){
		this.display='none'; //set none css after close dialog
		this.isModalOpen = false;
	}

	closeChartModalDialog(){
    this.chartdisplay='none'; //Set block css
    this.isModalOpen = false;
  	}
	changeQtytype(e){
		if(e =='Quantity'){
			this.show = true;
			this.soption = [
				{ name: 'gm', value: "g" },
				{ name: 'ml', value: "ml" }
			];
		}else if(e =='Portion'){
			this.soption = [
				{ name: 'Small', value: "p1" },
				{ name: 'Medium', value: "p2" },
				{ name: 'Large', value: "p3" }
			];
			this.show = false;
		}else if(e=='Unit'){
			this.soption = [
				{ name: '1', value: "1x" },
				{ name: '2', value: "2x" },
				{ name: '3', value: "3x" },
				{ name: '4', value: "4x" },
				{ name: '5', value: "5x" }
			];
			this.show = false;
		}else{
			this.show = false;
		}
		
	}

  	autoCompleteSearch(id){
  		const itemdtl =  this.getitems.find((data)=>data.id == Number(id));
  		this.mealAddForm.controls.itemname.setValue(itemdtl.name);
  		this.mealAddForm.controls.itemid.setValue(itemdtl.id);
  		this.show_jb_title_srh = false;
	}
/**
* Store food in user food list
*/
	mealStore(){
		this.commonService.create(this.global.apiUrl + '/api/meal',this.mealAddForm.value)
	        .subscribe((data)=>{
	          if(data.status ==200){
	          	this.meals.push(data.data);
	          	this.meals = this.meals.sort(function compare(a, b) {
				  var dateA:any = new Date(a.meal_date);
				  var dateB:any = new Date(b.meal_date);
				  return dateB - dateA;
				});
	            this.message.success(data.status_text);
	            this.display='none';
	            this.validationmsg = '';
	            this.mealAddForm.reset();
	            this.isModalOpen = false;
	          }

	          if(data.status ==422){
	          		this.validationmsg = data.error;
	      		}

	          if(data.status ==400){
	          	this.validationmsg = '';
	          	this.message.error(data.status_text);
	          }

	          if(data.status ==500){
	          	this.validationmsg = '';
	          	this.message.error('Please search and select food/receipe from dropdown list.');
	          }

	        }, (error)=>{
	        });
	}

	setForm(){
	    this.mealAddForm.patchValue({
	      //mealdate:this.singlemeal.meal_date,
	      periodtype: this.singlemeal.meal_type,
	      itemname:this.singlemeal.nutritics_food_name, 
	      itemqty: this.singlemeal.meal_qty,
	      itemqtytype: this.singlemeal.meal_qty_type,
	      // liquidqty: '',
	      itemid:this.singlemeal.nutritics_food_id
	    });
	}
/**
* Copy food from meal list
*/
	copyFood(id){
		this.commonService.getAll(this.global.apiUrl + '/api/user/meal/clone/'+id)
	        .subscribe((data)=>{
	          if(data.status ==200){
	          	this.singlemeal = data.data;
	          	this.changeQtytype(data.data.meal_qty_type)
	            //this.message.success(data.status_text);
	          }

	          if(data.status ==400){
	          	this.message.error(data.status_text);
	          }

	          if(data.status ==500){
	          	this.message.error('Please search and select food/receipe from dropdown list.');
	          }
	          this.setForm();
	          this.display='block'; //Set block css
    		  this.isModalOpen = true;
	        }, (error)=>{
	        });
	}
/**
* Remove food from user food list
*/
	deleteFood(id, index){
		Swal.fire({
		  title: 'Are you sure?',
		  text: "You won't be able to revert this!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!'
		}).then((result) => {
			if (result.value) {
				this.commonService.delete(this.global.apiUrl + '/api/meal', id)
	  			.subscribe((data)=>{
		  			if(data.status ==200){	 
		  				this.meals.splice(index,1);					
					    Swal.fire(
					      'Deleted!',
					      'Your meal has been deleted.',
					      'success'
					    )
		  			}
	  			},(error)=>{});
		  	}
		}); 
	}

	onChartClick(event) {
		let nutrname ='';
		if(event.active[0]._index==0){
			nutrname ='carbohydrate';
		}else if(event.active[0]._index==1){
			nutrname ='protein';
		}else if(event.active[0]._index==2){
			nutrname ='fat';
		}else if(event.active[0]._index==3){
			nutrname ='sugars';
		}else if(event.active[0]._index==4){
			nutrname ='fibre';
		}else if(event.active[0]._index==5){
			nutrname ='n6poly';
		}else if(event.active[0]._index==6){
			nutrname ='calcium';
		}else if(event.active[0]._index==7){
			nutrname ='magnesium';
		}else if(event.active[0]._index==8){
			nutrname ='iron';
		}else if(event.active[0]._index==9){
			nutrname ='zinc';
		}else if(event.active[0]._index==10){
			nutrname ='selenium';
		}else if(event.active[0]._index==11){
			nutrname ='iodine';
		}else if(event.active[0]._index==12){
			nutrname ='sodium';
		}else if(event.active[0]._index==13){
			nutrname ='vita';
		}else if(event.active[0]._index==14){
			nutrname ='vitd';
		}else if(event.active[0]._index==15){
			nutrname ='vite';
		}else if(event.active[0]._index==16){
			nutrname ='vitk';
		}else if(event.active[0]._index==17){
			nutrname ='thiamin';
		}else if(event.active[0]._index==18){
			nutrname ='riboflavin';
		}else if(event.active[0]._index==19){
			nutrname ='niacineqv';
		}else if(event.active[0]._index==20){
			nutrname ='pantothenate';
		}else if(event.active[0]._index==21){
			nutrname ='vitb6';
		}else if(event.active[0]._index==22){
			nutrname ='folate';
		}else if(event.active[0]._index==23){
			nutrname ='vitb12';
		}else if(event.active[0]._index==24){
			nutrname ='biotin';
		}else if(event.active[0]._index==25){
			nutrname ='vitc';
		}else{

		}

		this.commonService.getAll(this.global.apiUrl + '/api/user/meal/single-nutrition?nutrns_name='+nutrname)
	        .subscribe((data)=>{
	          if(data.status ==200){
	            if(nutrname=='vitk'){
	            	nutrname = 'vitamin K1';
	            }else if(nutrname=='vita'){
	            	nutrname = 'vitamin A';
	            }else if(nutrname=='vitd'){
	            	nutrname = 'vitamin D';
	            }else if(nutrname=='vite'){
	            	nutrname = 'vitamin E';
	            }else if(nutrname=='vitc'){
	            	nutrname = 'vitamin C';
	            }else if(nutrname=='vitb12'){
	            	nutrname = 'vitamin B12';
	            }else if(nutrname=='vitb6'){
	            	nutrname = 'vitamin B6';
	            }else if(nutrname=='thiamin'){
	            	nutrname = 'thiamin B1';
	            }else if(nutrname=='riboflavin'){
	            	nutrname = 'riboflavin B2';
	            }else if(nutrname=='niacineqv'){
	            	nutrname = 'niacin B3';
	            }else if(nutrname=='pantothenate'){
	            	nutrname = 'pantothenate B5';
	            }else if(nutrname=='folate'){
	            	nutrname = 'folate B9';
	            }else if(nutrname=='biotin'){
	            	nutrname = 'biotin B7';
	            }else if(nutrname=='n6poly'){
	            	nutrname = 'O6/O3';
	            }else if(nutrname=='sugars'){
	            	nutrname = 'sugar';
	            }
	            document.getElementById("topcharthding").innerHTML = nutrname.charAt(0).toUpperCase() + nutrname.slice(1)+ ' for last 30 days';
	          	if(this.isMobile && this.screenwidth<=425){
	          	  this.popupChartLabels = data.mealdate;
		          this.popupChartData[0].data = data.data;
		          this.popupChartData[0].label = nutrname.charAt(0).toUpperCase() + nutrname.slice(1);
			      this.chartdisplay='block'; //Set block css
			      this.isModalOpen = true;
			      document.getElementById("mbltopcharthding").innerHTML = nutrname.charAt(0).toUpperCase() + nutrname.slice(1)+ ' for last 30 days';
			    }else{
			      this.chartdisplay='none'; //Set block css
			      this.isModalOpen = false;
			      document.getElementById("mbltopcharthding").innerHTML = '';
			      this.topChartLabels = data.mealdate;
		          this.topChartData[0].data = data.data;
		          this.topChartData[0].label = nutrname.charAt(0).toUpperCase() + nutrname.slice(1);
		          this.topChartData[1].data = [];
		          this.topChartData[2].data = [];
		          this.topChartData[3].data = [];
		          this.topChartData[1].label = '';
		          this.topChartData[2].label = '';
		          this.topChartData[3].label = '';
		          this.popupChartLabels = [];
		          this.popupChartData[0].data = [];
		          this.popupChartData[0].label = ''
			    }
	          }

	          if(data.status ==400){
	          	//this.message.error(data.status_text);
	          }

	          if(data.status ==500){
	          	//this.message.error('Please search and select food/receipe from dropdown list.');
	          }
        }, (error)=>{
        });
    	
  	}

  	resetchart(event){
  		this.redirectTo(this.router.url);
  	}

  	redirectTo(uri) {
		this.router.navigateByUrl('/', {skipLocationChange: true}).then(() =>
		this.router.navigate([uri]));
	}
/**
* Display meal chart
*/
	topchartchange(type){
		this.commonService.getAll(this.global.apiUrl + '/api/user/meal/single-nutrition?nutrns_name=energyKcal')
	        .subscribe((data)=>{
	          if(data.status ==200){
	          	//this.meals.push(data.data);
	          	this.topChartLabels = data.mealdate;
		        this.topChartData[0].data = data.data;
		        this.topChartData[1].data = [];
		        this.topChartData[2].data = [];
		        this.topChartData[3].data = [];
		        this.topChartData[0].label = 'Energy Calories';
		        this.topChartData[1].label = '';
		        this.topChartData[2].label = '';
		        this.topChartData[3].label = '';
	            document.getElementById("topcharthding").innerHTML = 'Energy Calories for last 30 days';
	          }

	          if(data.status ==400){
	          	//this.message.error(data.status_text);
	          }

	          if(data.status ==500){
	          	//this.message.error('Please search and select food/receipe from dropdown list.');
	          }
        }, (error)=>{
        });
	}
/**
* Food list pagination
*/
	viewMore(){
	    let limit = 10 + this.meals.length;
	    this.commonService.getAll(this.global.apiUrl + '/api/meal?limit='+ Number(limit))
	      .subscribe((data)=>{
	        this.meals = data.data;
	        this.totalmeals = data.totalmeals;
	      }, (error)=>{});
  	}

  	onSelectFile(event: any){
      let image = event.target.files[0];
      this.foodImage.setValue(image);
      const formData = new FormData();
      formData.append('food_image', this.foodImage.value);
      this.commonService.create(this.global.apiUrl + '/api/user/meal/image-recognization', formData)
        .subscribe((data)=>{
          if(data.status ==200){
            this.message.success(data.status_text);
          } 

          if(data.status ==422){
            this.message.error(data.error.user_image);
          }
        }, (error)=>{});

  }

}
