import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CoreModule} from '../core/core.module';
import {MealRoutingModule} from './meal-routing.module';
import {MealManageComponent} from './meal-manage/meal-manage.component';

@NgModule({
  declarations: [MealManageComponent],
  imports: [
    CommonModule,
    MealRoutingModule,
    CoreModule
  ]
})
export class MealModule { }
