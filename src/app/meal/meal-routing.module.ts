import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './../auth.guard';
import { EmployeeGuard } from './../employee.guard';
import { CoachGuard } from './../coach.guard';
import { MealManageComponent } from './meal-manage/meal-manage.component';
import { StatutoryGuard } from './../statutory.guard';

const routes: Routes = [
	{path:'', canActivate: [AuthGuard,EmployeeGuard, StatutoryGuard], component: MealManageComponent}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MealRoutingModule { }
