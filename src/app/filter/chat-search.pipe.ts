import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'chatsearch'
})
export class ChatSearchPipe implements PipeTransform {

  transform(value, keys: string, term: string): any {
     if (!term) { return value;
    }
    return (value || []).filter( (item) => keys.split(',')
    	.some(key => (
    	(item.user_detail.hasOwnProperty(key) && new RegExp(term, 'gi').test(item.user_detail[key]))) 
    		)    
    	);
  }

}
