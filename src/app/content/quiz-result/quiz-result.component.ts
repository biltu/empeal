import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../services/message.service';
declare var Swal: any;

@Component({
  selector: 'app-quiz-result',
  templateUrl: './quiz-result.component.html',
  styleUrls: ['./quiz-result.component.css']
})
export class QuizResultComponent implements OnInit {

  quizData: any=[];
  content_id: any='';
  content_name : string='';
  content_category_name: string='';
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
   private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) {
  		this.content_id = this.activeRoute.snapshot.paramMap.get("id");
  		this.asyncInit(this.content_id);
   }

  ngOnInit() {
  }

/**
* Get user quiz result
*/
   asyncInit(cont_id){
  	this.commonService.getAll(this.global.apiUrl + '/api/user-quiz/result/'+ cont_id)
  		.subscribe((data)=>{
  			this.quizData = data.data;
			this.content_name = data.content.title;
  			this.content_category_name = data.content.category.category_name;	
  			console.log(data);
  		}, (error)=>{});
  }

}
