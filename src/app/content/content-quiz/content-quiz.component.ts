import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../services/message.service';
declare var Swal: any;

@Component({
  selector: 'app-content-quiz',
  templateUrl: './content-quiz.component.html',
  styleUrls: ['./content-quiz.component.css']
})
export class ContentQuizComponent implements OnInit {

  content_id: any='';
  questions: any=[];
  content_name : string='';
  content_category_name: string='';
  question_model: any={};
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
   private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) { 
  	this.content_id = this.activeRoute.snapshot.paramMap.get("id");
  	this.asyncInit(this.content_id);
  }

  ngOnInit() {
  }

/**
* Get Single content and content questions
*/
    asyncInit(cont_id){
  	this.commonService.getAll(this.global.apiUrl + '/api/user/get-content/question/'+ cont_id)
  		.subscribe((data)=>{
  			this.questions = data.data;
  			this.content_name = data.content.title;
  			this.content_category_name = data.content.category.category_name;			
  		}, (error)=>{});
  }

/**
* Submit question
*/
  submitQuestion(){
  	let formValue ={content_id:this.content_id, questions:this.question_model};
  	this.commonService.create(this.global.apiUrl + '/api/user/save-qus-ans', formValue)
  		.subscribe((data)=>{
  			if(data.status ==200){
          this.commonService.callUpdatePoint(data.user_points);
  				this.message.success(data.status_text);
  				this.router.navigate(['content/quiz-result',this.content_id]);
  			}
  		}, (error)=>{});
  }

}
