import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Global } from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import { MessageService } from '../../services/message.service';
declare var Swal: any;

@Component({
  selector: 'app-add-content',
  templateUrl: './add-content.component.html',
  styleUrls: ['./add-content.component.css']
})
export class AddContentComponent implements OnInit {

  contentForm: FormGroup;
  contentAttrForm: FormGroup;
  active_tab: number;
  groups: any = [];
  categories: any = [];
  contests: any = [];
  appointments: any = [];
  content_id: any;
  content_data: any = {};
  users: any = [];
  desc: String;
  content_attached_url: string;
  user_role: any = {};
  user_data: any = {};
  contest_id: any = {};

  constructor(private fb: FormBuilder, private message: MessageService, private global: Global, private authService: AuthService,
    private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) {
    this.user_data = this.authService.isUserLoginIn ? JSON.parse(localStorage.getItem('user')) : '';
    this.user_role = this.user_data ? this.user_data.role : '';
    this.content_id = this.activeRoute.snapshot.paramMap.get("id") ? this.activeRoute.snapshot.paramMap.get("id") : '';
    this.active_tab = 1;
    this.asyncInit();


    this.desc = '<p>hi</p>';
  }

  ngOnInit() {
    this.commonService.currentAppointment.subscribe((data) => {
      this.appointments = data.appointments;
    });
    let form;
    if (this.user_role.slug == 'subcoach') { 
     
        form = {
      id: '',
      category_id: ['', Validators.required],
      group_id: ['', Validators.required],
       contest_id: ['', Validators.required],
      user_id: [''],
      title: ['', Validators.required],
      description: ['', Validators.required],
      video_url: [''],
      attached_file: [''],
      status: ['', Validators.required],
      sticky: [''],

    };
    }
    else {
      
          form = {
      id: '',
      category_id: ['', Validators.required],
      group_id: ['', Validators.required],
       contest_id: [''],
      user_id: [''],
      title: ['', Validators.required],
      description: ['', Validators.required],
      video_url: [''],
      attached_file: [''],
      status: ['', Validators.required],
      sticky: [''],

    };
    }
    
    this.contentForm = this.fb.group(form);

    this.contentAttrForm = this.fb.group({
      id: '',
      heart_condition: [''],
      chest_pain_during_activity: [''],
      chest_pain_not_doing_activity: [''],
      palpitation: [''],
      dizziness: [''],
      high_blood_pressure: [''],
      joint_problem: [''],
      asthma: [''],
      prescribed_medication: [''],
      pregnant: [''],
      surgery: [''],
      not_participate_physical_exercise: [''],
      arthritis: [''],
      back_pain: [''],
      joint_pain: [''],
      muscle_pain: [''],
      other_pain: [''],
      fainting: [''],
      chest_pain: [''],
      shortness_of_breath: [''],
      hernia: [''],
      smoke: [''],
      elevated_triglyceride_levels: [''],
      elevated_cholesterol_levels: [''],
      difficulty_with_physical_exercise: [''],
      asthma_emphysema_bronchitis: [''],
      blood_pressure: [''],
      coronary_disease: [''],
      heart_problem: [''],
      stroke: [''],
      epilepsy: [''],
      diabetes_or_thyroid_condition: [''],
      thyroid_condition: [''],
      hypoglycemia: [''],
      pregnant_in_the_last_4_months: [],
      family_history_of_coronary_disease: [''],
      family_history_of_atherosclerotic_disease: [''],
      hospitalised_in_the_last_12_months: [''],
      advice_from_gp_health_practitioner_not_to_exercise: [''],
      none_above_medical: [''],
    });
  }

  /**
  * Get Groups and check content and get content data
  */

  asyncInit() {
    if (this.content_id) {
      this.commonService.getAll(this.global.apiUrl + '/api/content/' + this.content_id)
        .subscribe((data) => {
          this.content_data = data.data;

          if (this.content_data.content_visibility) {

            this.commonService.getAll(this.global.apiUrl + '/api/group/useres?id=' + this.content_data.content_visibility.id)
              .subscribe((data) => {
                this.users = data.data;
              }, (error) => { });
          }
          this.groups = data.common_data.groups;
          this.categories = data.common_data.categories;
          this.contests = data.common_data.contests;
          // this.contest_id = data.contest_id;
          this.content_attached_url = this.content_data.attached_file_url ? this.content_data.attached_file_url : '';
          // console.log(data.contest_id);
          this.setForm();
        }, (error) => { });
    } else {
      this.commonService.getAll(this.global.apiUrl + '/api/content/common-data')
        .subscribe((data) => {
          this.groups = data.data.groups;
          this.categories = data.data.categories;
          this.contests = data.data.contests;

        }, (error) => { });
    }

  }

  groupUser(event) {
    event.preventDefault();
    console.log(event.target.value);
    if (event.target.value > 0) {
      this.commonService.getAll(this.global.apiUrl + '/api/group/useres?id=' + event.target.value)
        .subscribe((data) => {
          this.users = data.data;
        }, (error) => { });
    }
  }

  contestUser(event) {
    event.preventDefault();
    console.log(event.target.value);
    if (event.target.value > 0) {
      this.commonService.getAll(this.global.apiUrl + '/api/contest/useres?id=' + event.target.value)
        .subscribe((data) => {
          this.users = data.data;
        }, (error) => { });
    }
  }

  /**
  * Set Content form
  */
  setForm() {

    let group_id = this.content_data.content_visibility == 'All' ? 'all' : this.content_data.content_visibility.id;

    this.contentForm.patchValue({
      id: this.content_data.id,
      category_id: this.content_data.category.id,
      group_id: group_id,
      user_id: this.content_data.user_id,
      title: this.content_data.title,
      description: this.content_data.description,
      video_url: this.content_data.video_url,
      //attached_file: this.content_data.attached_file,
      //visibility: ['', Validators.required],
      sticky: this.content_data.sticky,
      status: this.content_data.status,
      contest_id: this.content_data.contest_id
    });
    console.log('this.contentForm.value', this.contentForm.value);
    this.contentAttrForm.patchValue({
      id: this.content_data.content_attribute.id,
      heart_condition: this.content_data.content_attribute.heart_condition ? String(this.content_data.content_attribute.heart_condition) : '',
      chest_pain_during_activity: this.content_data.content_attribute.chest_pain_during_activity ? String(this.content_data.content_attribute.chest_pain_during_activity) : '',
      chest_pain_not_doing_activity: this.content_data.content_attribute.chest_pain_not_doing_activity ? String(this.content_data.content_attribute.chest_pain_not_doing_activity) : '',
      palpitation: this.content_data.content_attribute.palpitation ? String(this.content_data.content_attribute.palpitation) : '',
      dizziness: this.content_data.content_attribute.dizziness ? String(this.content_data.content_attribute.dizziness) : '',
      high_blood_pressure: this.content_data.content_attribute.high_blood_pressure ? String(this.content_data.content_attribute.high_blood_pressure) : '',
      joint_problem: this.content_data.content_attribute.joint_problem ? String(this.content_data.content_attribute.joint_problem) : '',
      asthma: this.content_data.content_attribute.asthma ? String(this.content_data.content_attribute.asthma) : '',
      prescribed_medication: this.content_data.content_attribute.prescribed_medication ? String(this.content_data.content_attribute.prescribed_medication) : '',
      pregnant: this.content_data.content_attribute.pregnant ? String(this.content_data.content_attribute.pregnant) : '',
      surgery: this.content_data.content_attribute.surgery ? String(this.content_data.content_attribute.surgery) : '',
      not_participate_physical_exercise: this.content_data.content_attribute.not_participate_physical_exercise ? String(this.content_data.content_attribute.not_participate_physical_exercise) : '',
      arthritis: this.content_data.content_attribute.arthritis ? String(this.content_data.content_attribute.arthritis) : '',
      back_pain: this.content_data.content_attribute.back_pain ? String(this.content_data.content_attribute.back_pain) : '',
      joint_pain: this.content_data.content_attribute.joint_pain ? String(this.content_data.content_attribute.joint_pain) : '',
      muscle_pain: this.content_data.content_attribute.muscle_pain ? String(this.content_data.content_attribute.muscle_pain) : '',
      other_pain: this.content_data.content_attribute.other_pain ? String(this.content_data.content_attribute.other_pain) : '',
      fainting: this.content_data.content_attribute.fainting ? String(this.content_data.content_attribute.fainting) : '',
      chest_pain: this.content_data.content_attribute.chest_pain ? String(this.content_data.content_attribute.chest_pain) : '',
      shortness_of_breath: this.content_data.content_attribute.shortness_of_breath ? String(this.content_data.content_attribute.shortness_of_breath) : '',
      hernia: this.content_data.content_attribute.hernia ? String(this.content_data.content_attribute.hernia) : '',
      smoke: this.content_data.content_attribute.smoke ? String(this.content_data.content_attribute.smoke) : '',
      elevated_triglyceride_levels: this.content_data.content_attribute.elevated_triglyceride_levels ? String(this.content_data.content_attribute.elevated_triglyceride_levels) : '',
      elevated_cholesterol_levels: this.content_data.content_attribute.elevated_cholesterol_levels ? String(this.content_data.content_attribute.elevated_cholesterol_levels) : '',
      difficulty_with_physical_exercise: this.content_data.content_attribute.difficulty_with_physical_exercise ? String(this.content_data.content_attribute.difficulty_with_physical_exercise) : '',
      asthma_emphysema_bronchitis: this.content_data.content_attribute.asthma_emphysema_bronchitis ? String(this.content_data.content_attribute.asthma_emphysema_bronchitis) : '',
      blood_pressure: this.content_data.content_attribute.blood_pressure ? String(this.content_data.content_attribute.blood_pressure) : '',
      coronary_disease: this.content_data.content_attribute.coronary_disease ? String(this.content_data.content_attribute.coronary_disease) : '',
      heart_problem: this.content_data.content_attribute.heart_problem ? String(this.content_data.content_attribute.heart_problem) : '',
      stroke: this.content_data.content_attribute.stroke ? String(this.content_data.content_attribute.stroke) : '',
      epilepsy: this.content_data.content_attribute.epilepsy ? String(this.content_data.content_attribute.epilepsy) : '',
      diabetes_or_thyroid_condition: this.content_data.content_attribute.diabetes_or_thyroid_condition ? String(this.content_data.content_attribute.diabetes_or_thyroid_condition) : '',
      thyroid_condition: this.content_data.content_attribute.thyroid_condition ? String(this.content_data.content_attribute.thyroid_condition) : '',
      hypoglycemia: this.content_data.content_attribute.hypoglycemia ? String(this.content_data.content_attribute.hypoglycemia) : '',
      pregnant_in_the_last_4_months: this.content_data.content_attribute.pregnant_in_the_last_4_months ? String(this.content_data.content_attribute.pregnant_in_the_last_4_months) : '',
      family_history_of_coronary_disease: this.content_data.content_attribute.family_history_of_coronary_disease ? String(this.content_data.content_attribute.family_history_of_coronary_disease) : '',
      family_history_of_atherosclerotic_disease: this.content_data.content_attribute.family_history_of_atherosclerotic_disease ? String(this.content_data.content_attribute.family_history_of_atherosclerotic_disease) : '',
      hospitalised_in_the_last_12_months: this.content_data.content_attribute.hospitalised_in_the_last_12_months ? String(this.content_data.content_attribute.hospitalised_in_the_last_12_months) : '',
      advice_from_gp_health_practitioner_not_to_exercise: this.content_data.content_attribute.advice_from_gp_health_practitioner_not_to_exercise ? String(this.content_data.content_attribute.advice_from_gp_health_practitioner_not_to_exercise) : '',
      none_above_medical: this.content_data.content_attribute.none_above_medical ? String(this.content_data.content_attribute.none_above_medical) : '',

    });

  }

  get cn_f() { return this.contentForm.controls; }

  /**
  * Form check
  */
  formCheck(form, active_tab) {
    if (form == 'content' && this.contentForm.status === 'INVALID') {
      return false;
    }
    this.active_tab = active_tab;

  }

  /**
  * Submit Content
  */
  contentStore() {
    let form = { content: this.contentForm.value, content_attr: this.contentAttrForm.value };
    this.commonService.create(this.global.apiUrl + '/api/content', form)
      .subscribe((data) => {
        if (data.status == 200) {
          this.message.success(data.status_text);
          this.router.navigate(['content/manage/list']);
        }
      }, (error) => { });
  }

  /**
  * Update Content
  */
  contentUpdate() {
    let form = { content: this.contentForm.value, content_attr: this.contentAttrForm.value };
    this.commonService.update(this.global.apiUrl + '/api/content', this.content_id, form)
      .subscribe((data) => {
        if (data.status == 200) {
          this.message.success(data.status_text);
          this.router.navigate(['content/manage/list']);
        }
      }, (error) => { });
  }

  /**
  * File attached
  */
  fileAttach(event) {
    let formData = new FormData();
    formData.append('attached_file', event.target.files[0]);
    this.commonService.create(this.global.apiUrl + '/api/attached-file/upload', formData)
      .subscribe(data => {
        if (data.status == 200) {
          this.contentForm.get('attached_file').setValue(data.data);
        }

        if (data.status == 422) {
          this.message.error(data.error);
        }
      }, error => { });

  }

  /**
  * Download file
  */
  downloadAttachedFile() {
    this.commonService.getAll(this.global.apiUrl + '/api/download-attched-file/' + this.content_data.attached_file)
      .subscribe(data => { });
  }

  formDataSet(form) {
    let formData = new FormData();
    Object.keys(form.controls).forEach(key => {
      console.log(key);
      formData.append(key, form.get(key).value);
    });
    return formData;
  }

}
