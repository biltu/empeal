import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './../auth.guard';
import { EmployeeGuard } from './../employee.guard';
import { StatutoryGuard } from './../statutory.guard';
import { CoachGuard } from './../coach.guard';
import { SecondaryCoachGuard } from './../secondary-coach.guard';

import { ContentComponent } from './content/content.component';
import { ContentDetailComponent } from './content-detail/content-detail.component';
import { ContentQuizComponent } from './content-quiz/content-quiz.component';
import { QuizResultComponent } from './quiz-result/quiz-result.component';
import { ContentListComponent } from './content-list/content-list.component';
import { AddContentComponent } from './add-content/add-content.component';
import { AddQuizComponent } from './add-quiz/add-quiz.component';
import { ViewQuizComponent } from './view-quiz/view-quiz.component';
import { EditQuizComponent } from './edit-quiz/edit-quiz.component';

const routes: Routes = [
	{path:':category', canActivate: [AuthGuard, EmployeeGuard, StatutoryGuard], component: ContentComponent},
	{path:'detail/:id', canActivate: [AuthGuard, EmployeeGuard, StatutoryGuard], component: ContentDetailComponent},
	{path:'quiz/:id', canActivate: [AuthGuard, EmployeeGuard, StatutoryGuard], component: ContentQuizComponent},
	{path:'quiz-result/:id', canActivate: [AuthGuard, EmployeeGuard, StatutoryGuard], component: QuizResultComponent},
	{path:'manage/list', canActivate: [AuthGuard, CoachGuard], component: ContentListComponent},
	{path:'manage/add', canActivate: [AuthGuard, CoachGuard], component: AddContentComponent},
	{path:'manage/edit/:id', canActivate: [AuthGuard, CoachGuard], component: AddContentComponent},
	{path:'manage/quiz-add/:id', canActivate: [AuthGuard, CoachGuard], component: AddQuizComponent},
	{path:'manage/quiz-edit/:id', canActivate: [AuthGuard, CoachGuard], component: EditQuizComponent},
	{path:'manage/quiz-view/:id', canActivate: [AuthGuard, CoachGuard], component: ViewQuizComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContentRoutingModule { }
