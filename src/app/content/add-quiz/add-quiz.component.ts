import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../services/message.service';
declare var Swal: any;

@Component({
  selector: 'app-add-quiz',
  templateUrl: './add-quiz.component.html',
  styleUrls: ['./add-quiz.component.css']
})
export class AddQuizComponent implements OnInit {

  questions: FormArray;
  questionForm: FormGroup;
  content_id: any;
  count_points: number;
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
   private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) { 
    this.content_id = this.activeRoute.snapshot.paramMap.get("id");
    this.count_points =0;
  }

  ngOnInit() {
  	this.questionForm = this.fb.group({
  		questions: this.fb.array([this.createQst()])
  	});

    this.questionForm.controls['questions'].valueChanges.subscribe((data)=>{
      let map_num = data.map((obj, i)=>{
        let tot = (Number(obj.point1) + Number(obj.point2) 
          + Number(obj.point3) + Number(obj.point4));
        return tot;
      });
      this.count_points = map_num.reduce((sum, a)=> sum +a);
      if(this.count_points > 100){
        Swal.fire({
          type: 'warning',
          title: 'Points setting error',
          text: 'Cannot set more than 100 points!'
        });
      }
    });
  }

  createQst(): FormGroup{
  	return this.fb.group({
  		question:[''],
  		question_info:[''],
  		option1:[''],
  		point1:[''],
  		option2:[''],
  		point2:[''],
  		option3:[''],
  		point3:[''],
  		option4:[''],
  		point4:['']
  	});
  }

/**
* Add question into array variable
*/
  addQuestionArray(): void {
  	this.questions = this.questionForm.get('questions') as FormArray;
  	this.questions.push(this.createQst());
  }

/**
* Delete question from array
*/
  deleteQuestionArray(index: number){
  	this.questions = this.questionForm.get('questions') as FormArray;
  	this.questions.removeAt(index);
  }

  get formData() { return <FormArray>this.questionForm.get('questions')['controls']; }

/**
* Submit quiz
*/
  submitQuiz(){

    this.commonService.create(this.global.apiUrl + '/api/content/quiz-create/' + this.content_id, this.questionForm.value)
      .subscribe((data)=>{
        if(data.status == 422){
          this.message.error(data.status_text);
          return false;
        }
        if(data.status ===200){
          this.message.success(data.status_text);
          this.router.navigate(['content/manage/list'])
        }
        console.log(data);
      },(error)=>{});

  }
/**
* Quiz point check 
*/
  checktotPoints(e, i, cont_name){
   let questions = this.questionForm.get('questions') as FormArray;
   if(Number(this.count_points)  > 100){
      questions.controls[i].get(cont_name).setValue('');

    } 
  }


}
