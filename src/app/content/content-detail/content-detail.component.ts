import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../services/message.service';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
declare var Swal: any;

@Component({
  selector: 'app-content-detail',
  templateUrl: './content-detail.component.html',
  styleUrls: ['./content-detail.component.css']
})
export class ContentDetailComponent implements OnInit {
  qscontentcount: any;
  content_id: any;
  content: any={};
  category_name: string;
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
   private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute, private sanitizer: DomSanitizer) { 
  	this.content_id = this.activeRoute.snapshot.paramMap.get("id");
  	this.asyncInit(this.content_id);
  	this.readContent();
  }

  ngOnInit() {
  }

/**
* Get Single content detail
*/

  asyncInit(cont_id){
  	this.commonService.getAll(this.global.apiUrl + '/api/content/'+ cont_id)
  		.subscribe((data)=>{
  			this.content = data.data;
  			this.category_name = data.data.category.category_name;
  			this.commonService.callUpdatePoint(data.common_data.user_points);
        this.qscontentcount = data.qs_count;
  		}, (error)=>{});
  }

/**
* Set read content
*/
  readContent(){
  	this.commonService.getAll(this.global.apiUrl + '/api/user-view/store/'+ this.content_id)
  		.subscribe((data)=>{ 		
  		}, (error)=>{});
  }

  videoSanitize(link){
    if(link){
      return this.sanitizer.bypassSecurityTrustResourceUrl(link.replace("watch?v=", "embed/"));
    }
    
  }
  
}
