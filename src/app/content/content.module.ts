import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CoreModule} from '../core/core.module';

import { ContentRoutingModule } from './content-routing.module';
import { ContentComponent } from './content/content.component';
import { ContentDetailComponent } from './content-detail/content-detail.component';
import { ContentQuizComponent } from './content-quiz/content-quiz.component';
import { QuizResultComponent } from './quiz-result/quiz-result.component';
import { ContentListComponent } from './content-list/content-list.component';
import { AddContentComponent } from './add-content/add-content.component';
import { AddQuizComponent } from './add-quiz/add-quiz.component';
import { ViewQuizComponent } from './view-quiz/view-quiz.component';
import { EditQuizComponent } from './edit-quiz/edit-quiz.component';

@NgModule({
  declarations: [ContentComponent, ContentDetailComponent, ContentQuizComponent, QuizResultComponent, ContentListComponent, AddContentComponent, AddQuizComponent, ViewQuizComponent, EditQuizComponent],
  imports: [
    CommonModule,
    ContentRoutingModule,
    CoreModule
  ]
})
export class ContentModule { }
