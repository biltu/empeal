import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/common.service';
import {Global} from '../../global';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';

@Component({
  selector: 'app-view-quiz',
  templateUrl: './view-quiz.component.html',
  styleUrls: ['./view-quiz.component.css']
})
export class ViewQuizComponent implements OnInit {
  
  content_id: any;
  contentquestion: any;

  constructor(private commonService: CommonService, private global: Global, private activeRoute: ActivatedRoute) {
  	this.content_id = this.activeRoute.snapshot.paramMap.get("id");
  	this.commonService.getAll(this.global.apiUrl + '/api/content/quiz-view/' + this.content_id)
  		.subscribe((data)=>{
  			if(data.status == 200){
  				this.contentquestion = data.data.content_question ? data.data.content_question:[];
  			}
  			
  		},(error)=>{});

  }

  ngOnInit() {
  }

}
