import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../services/message.service';
declare var Swal: any;

@Component({
  selector: 'app-edit-quiz',
  templateUrl: './edit-quiz.component.html',
  styleUrls: ['./edit-quiz.component.css']
})
export class EditQuizComponent implements OnInit {

  content_id: any;
  contentquestion: any;
  questions: FormArray;
  questionForm: FormGroup;
  count_points: number;
  constructor(private commonService: CommonService, private global: Global, private activeRoute: ActivatedRoute,
  	 private fb:FormBuilder, private message:MessageService, private router: Router) {
  	 this.content_id = this.activeRoute.snapshot.paramMap.get("id");
  		
  }

  ngOnInit() {
  	this.questionForm = this.fb.group({
  		questions: this.fb.array([this.createQst()])
  	});
  	this.asyncInit();

  	this.questionForm.controls['questions'].valueChanges.subscribe((data)=>{
      let map_num = data.map((obj, i)=>{
        let tot = (Number(obj.point1) + Number(obj.point2) 
          + Number(obj.point3) + Number(obj.point4));
        return tot;
      });
      this.count_points = map_num.reduce((sum, a)=> sum +a);
      if(this.count_points > 100){
        Swal.fire({
          type: 'warning',
          title: 'Points setting error',
          text: 'Cannot set more than 100 points!'
        });
        //return false;
      }
    });
  }

/**
* get content questions and set quetions to form
*/
  asyncInit(){
  	this.commonService.getAll(this.global.apiUrl + '/api/content/quiz-view/' + this.content_id)
  		.subscribe((data)=>{
  			if(data.status == 200){
  				this.contentquestion = data.data.content_question ? data.data.content_question:[];
  				this.questions = <FormArray> this.questionForm.get('questions');  				
  				this.contentquestion.forEach((qst, i)=>{
  					this.questions.controls[i].get('content_question_id').setValue(qst.id);
  					this.questions.controls[i].get('question').setValue(qst.question);
  					this.questions.controls[i].get('question_info').setValue(qst.question_info);
  					this.questions.controls[i].get('option1_id').setValue(qst.question_options[0].id);
  					this.questions.controls[i].get('option1').setValue(qst.question_options[0].option);
  					this.questions.controls[i].get('point1').setValue(qst.question_options[0].point);
  					this.questions.controls[i].get('option2_id').setValue(qst.question_options[1].id);
  					this.questions.controls[i].get('option2').setValue(qst.question_options[1].option);
  					this.questions.controls[i].get('point2').setValue(qst.question_options[1].point);
  					this.questions.controls[i].get('option3_id').setValue(qst.question_options[2].id);
  					this.questions.controls[i].get('option3').setValue(qst.question_options[2].option);
  					this.questions.controls[i].get('point3').setValue(qst.question_options[2].point);
  					this.questions.controls[i].get('option4_id').setValue(qst.question_options[3].id);
  					this.questions.controls[i].get('option4').setValue(qst.question_options[3].option);
  					this.questions.controls[i].get('point4').setValue(qst.question_options[3].point);
  					if((this.contentquestion.length -i) !=1){
  						this.questions.push(this.createQst());
  					}
  					
  				});
  			}
  			
  		},(error)=>{});
  }

   createQst(): FormGroup{
  	return this.fb.group({
  		content_question_id:[''],
  		question:[''],
  		question_info:[''],
  		option1_id:[''],
  		option1:[''],
  		point1:[''],
  		option2_id:[''],
  		option2:[''],
  		point2:[''],
  		option3_id:[''],
  		option3:[''],
  		point3:[''],
  		option4_id:[''],
  		option4:[''],
  		point4:['']
  	});
  }

/**
* Add new question to form
*/
  addQuestionArray(): void {
  	this.questions = this.questionForm.get('questions') as FormArray;
  	this.questions.push(this.createQst());
  }

/**
* Delete question from form
*/
  deleteQuestionArray(index: number){
  	this.questions = this.questionForm.get('questions') as FormArray;
  	this.questions.removeAt(index);
  }

  get formData() { return <FormArray>this.questionForm.get('questions')['controls']; }

/**
* Submit updated quiz
*/
  updateQuiz(){
  	this.commonService.create(this.global.apiUrl + '/api/content/quiz-update/' + this.content_id, this.questionForm.value)
  		.subscribe((data)=>{
  		if(data.status == 422){
          this.message.error(data.status_text);
          return false;
        }
        if(data.status ===200){
          this.message.success(data.status_text);
          this.router.navigate(['content/manage/list'])
        }
  		},(error)=>{});
  }

/**
* Check quiz points
*/
  checktotPoints(e, i, cont_name){
   let questions = this.questionForm.get('questions') as FormArray;
   if(Number(this.count_points)  > 100){
      questions.controls[i].get(cont_name).setValue('');

    } 
  }

}
