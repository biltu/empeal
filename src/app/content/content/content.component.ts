import { Component, OnInit,ElementRef,HostListener,Output,EventEmitter,Input } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../services/message.service';
declare var Swal: any;
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {
  content_cat: string;
  contents: any=[];
  appointments: any=[];
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
   private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute, private eRef: ElementRef) { 
  	router.events.pipe(filter((event: any) => event instanceof NavigationEnd)).subscribe((e)=>{
  		this.content_cat = this.activeRoute.snapshot.paramMap.get("category");
  		this.asyncInit(this.content_cat);
  	});

  }

  ngOnInit() {

    console.log('content');
  	this.commonService.currentAppointment.subscribe((data)=>{
      this.appointments = data.appointments;
    });
  }

/**
* Get content data
*/
  asyncInit(category){
  	this.commonService.getAll(this.global.apiUrl + '/api/user-view/content/'+ category)
  		.subscribe((data)=>{
  			this.contents = data.data;
  		}, (error)=>{});
  }

/**
* View more content
*/
   viewMore(){
    let limit = 5 + this.contents.length;
    this.commonService.getAll(this.global.apiUrl + '/api/user-view/content/'+ this.content_cat + '?limit='+ Number(limit))
      .subscribe((data)=>{
        this.contents = data.data;
      }, (error)=>{});
  }

}
