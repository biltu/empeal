import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../services/message.service';
declare var Swal: any;

@Component({
  selector: 'app-content-list',
  templateUrl: './content-list.component.html',
  styleUrls: ['./content-list.component.css']
})
export class ContentListComponent implements OnInit {

  content_all: any=[];
  contentSearchForm: FormGroup;
  categories: any=[];
  groups: any=[];
  contests: any=[];
  count_data:number;
  active_page: number;
	appointments: any = [];
	  user_role: any = {};
  user_data: any = {};
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
	  private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) { 
	    this.user_data = this.authService.isUserLoginIn ? JSON.parse(localStorage.getItem('user')) : '';
    this.user_role = this.user_data ? this.user_data.role : '';
  	this.count_data =0;
  	this.active_page =0;
    this.asyncInit();
  }

  ngOnInit() {
    this.commonService.currentAppointment.subscribe((data)=>{
      this.appointments = data.appointments;
    });
    
  	this.contentSearchForm = this.fb.group({
  		category:[''],
  		group: [''],
      contest:[''],
      username: [''],
  		type: [''],
  		from_date:[''],
  		to_date:['']
  	});
  }

/**
* Get content data
*/
  asyncInit(){
  	this.commonService.getAll(this.global.apiUrl + '/api/content?off_set=' + this.active_page)
  		.subscribe((data)=>{
  			this.content_all = data.data;
  			this.categories = data.categories;
  			this.groups = data.groups;
        this.contests = data.contests;
  			this.count_data = data.data_count;
  			this.pagination();
  		}, (error)=>{});
  }

/**
* pagination function
*/
  pagination(){
  	let tot_number =  (Number(this.count_data) /25);
  	var items: number[] = [];
	for(var i = 1; i <= tot_number; i++){
	   items.push(i);
	}
	return items;
  }

/**
* pagination function
*/
  paginationCall(index){
  	this.active_page = index;
  	let search_f = this.contentSearchForm.controls;
  	if(search_f.category.value =='' && search_f.group.value =='' && search_f.type.value =='' && 
  		search_f.from_date.value=='' && search_f.to_date.value==''){
  		this.asyncInit();
  	}else{
  		this.searchContent(index);
  	}
	
  }

/**
* Search content
*/
  searchContent(index){
  	this.active_page =index;
  	let search_cont = '';
  	if(this.contentSearchForm.get('category').value){
  		search_cont += '&category=' + this.contentSearchForm.get('category').value;
  	}if(this.contentSearchForm.get('username').value){
      search_cont += '&username=' + this.contentSearchForm.get('username').value;
    }if(this.contentSearchForm.get('group').value){
  		search_cont += '&group=' + this.contentSearchForm.get('group').value;
  	}if(this.contentSearchForm.get('contest').value){
      search_cont += '&contest=' + this.contentSearchForm.get('contest').value;
    }if(this.contentSearchForm.get('type').value){
  		search_cont += '&type=' + this.contentSearchForm.get('type').value;
  	}if(this.contentSearchForm.get('from_date').value && this.contentSearchForm.get('to_date').value){
  		search_cont += '&from_date=' + this.contentSearchForm.get('from_date').value + '&to_date=' + this.contentSearchForm.get('to_date').value;
  	}

  	this.commonService.getAll(this.global.apiUrl + '/api/content?off_set=' + this.active_page + search_cont)
  		.subscribe((data)=>{
  			this.content_all = data.data;
  			this.categories = data.categories;
  			this.groups = data.groups;
  			this.count_data = data.data_count;
  			this.pagination();
  		}, (error)=>{});
  }

/**
* Delete content
*/
  DeleteContent(id, index){
		Swal.fire({
		  title: 'Are you sure?',
		  text: "You won't be able to revert this!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!'
		}).then((result) => {
			if (result.value) {
				this.commonService.delete(this.global.apiUrl + '/api/content', id)
	  			.subscribe((data)=>{
	  			if(data.status ==200){	 
	  				 this.content_all.splice(index,1);					
				    Swal.fire(
				      'Deleted!',
				      'Your file has been deleted.',
				      'success'
				    )
	  			 }
	  			},(error)=>{});

		  }
				  
		}); 	
  }

/**
* Clone content 
*/
  cloneContent(id){
  	this.commonService.getAll(this.global.apiUrl + '/api/content/clone/'+ id)
  		.subscribe((data)=>{
  			if(data.status ==200){
  				this.content_all.push(data.data);
  				this.message.success(data.status_text);
  			}else if(data.status ==500){
  				this.message.success(data.status_text);
  			}
  			
  		}, (error)=>{});

  }

}
