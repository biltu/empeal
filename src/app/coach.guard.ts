import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CoachGuard implements CanActivate {
  user_data: any={};
  role: any={};	
  constructor(private router: Router){
  	this.user_data = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')):'';
  	this.role = this.user_data ? this.user_data.role :'';
	}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    
    
    if (this.role.slug == 'admin' || this.role.slug == 'subcoach') {
      // console.log('this.role.slug', this.role.slug);
  		return true;
  	}
     this.router.navigate(['/dashboard']);
      return false;
  }
}
