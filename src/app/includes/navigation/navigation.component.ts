import { Component, OnInit } from '@angular/core';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import { Router} from '@angular/router';
import {CommonService} from '../../services/common.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {
  
  user_role: any={};
  user_data: any={};
  currentUrl: any;
  userMailcount: number;
  setAcUrl:string;
  unreadChats: any=[];
  permissions: any=[];
  isLoaded: boolean =false;
  constructor(private global: Global,private authService: AuthService,
        private router: Router, private commonService: CommonService) {
  	this.user_data = this.authService.isUserLoginIn ? JSON.parse(localStorage.getItem('user')):'';
   	this.user_role = this.user_data ? this.user_data.role:'';
    this.currentUrl = this.router.url;
   }

  ngOnInit() {
    this.commonService.getAll(this.global.node_server_url + '/chat/unread-chat/' + this.user_data.id)
          .subscribe((data)=>{
            let unreadData =  data.data.filter(obj=>obj.to == this.user_data.id);
            if(data.unread_user == this.user_data.id){
              this.unreadChats[this.user_data.id] = unreadData.length;
            }
          },(error)=>{});

    this.commonService.chatUpdateNewData.subscribe(data=>{
        if(data.unread_user == this.user_data.id){
          this.unreadChats[this.user_data.id] = data.unread_data.length;
        }

    });

    this.commonService.navigationChangeUpdate.subscribe(data=>{

      this.commonService.getAll(this.global.apiUrl + '/api/user/unopened/mail')
          .subscribe(data=>{
            this.userMailcount = data.data.count;
          },error=>{});
    });

    this.commonService.updateMailNotify.subscribe(value=>{
       this.commonService.getAll(this.global.apiUrl + '/api/get-unreadmail')
           .subscribe(data=>{
             if(data.status ==200){
                this.userMailcount = data.unread_mails? data.unread_mails.length:'';
             }
           },error=>{});
      //this.userMailcount = data.unread ? data.unread.length :'';
      });
    
    /*this.commonService.unopenedMailCount.subscribe(data=>{
      this.userMailcount = data ? data.length :'';
      });*/

      this.commonService.getAll(this.global.apiUrl + '/api/user/permission/modules').subscribe(data=>{
        this.permissions = data.data ? data.data.map(obj=>obj.name):[];
        this.isLoaded = true;
        this.commonService.callgroupaccessmodules({permissions:this.permissions});
      },error=>{});
  }

  checkActiveUrl(value){
    if((this.router.url.includes('content/') && value =='dashboard')){
      return true;
    }else if(this.router.url.includes('dashboard') && value =='dashboard'){
      return true;
    }else if(this.router.url.includes('target') && value =='dashboard'){
      return true;
    }else if(this.router.url.includes('questionnaire') && value =='analytics'){
      return true;
    }else if(this.router.url.includes('meal') && value =='analytics'){
      return true;
    }else if(this.router.url.includes('activity') && value =='analytics'){
      return true;
    }else if(this.router.url.includes('profile') && value =='profile'){
      return true;
    }else if(this.router.url.includes('update-password') && value =='profile'){
      return true;
    }else if(this.router.url.includes('appointment') && value =='appointment'){
      return true;
    }else if(this.router.url.includes('wearable') && value =='wearable'){
      return true;
    }else if(this.router.url.includes('mail') && value =='mail'){
      return true;
    }else if(this.router.url.includes('community') && value =='community'){
      return true;
    }else if(this.router.url.includes('chat') && value =='chat'){
      return true;
    }else if(this.router.url.includes('contents') && value =='contents'){
      return true;
    }else if(this.router.url.includes('groups') && value =='groups'){
      return true;
    }else if(this.router.url.includes('grouptargets') && value =='grouptargets'){
      return true;
    }else if(this.router.url.includes('supports') && value =='supports'){
      return true;
    }else if(this.router.url.includes('support') && value =='support'){
      return true;
    }

   //return this.router.url.includes(value)? true: false;
  }

}
