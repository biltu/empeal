import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/common.service';
import { Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {Global} from '../../global';
declare var Pusher: any;

@Component({
  selector: 'app-has-appointment',
  templateUrl: './has-appointment.component.html',
  styleUrls: ['./has-appointment.component.css']
})
export class HasAppointmentComponent implements OnInit {
  appointments: any=[];

  constructor(private commonService: CommonService, private router: Router, private authService: AuthService, private global: Global) { }

  ngOnInit() {
  	this.commonService.currentAppointment.subscribe((data)=>{
      this.commonService.getAll(this.global.apiUrl + '/api/booked/schedules')
        .subscribe(data=>{
          this.appointments = data.data;
      }, error=>{});
    });

     var pusher = new Pusher('ff8fc4d385774217ebf5', {
        cluster: 'ap2',
        encrypted: true
      });

      var that = this;
      // subscribe pusher event
      var channel = pusher.subscribe('notify-channel');
      channel.bind('notify-event', function(data) {
        if(data.notify == 'new_appointment'){
          that.commonService.getAll(that.global.apiUrl + '/api/booked/schedules')
              .subscribe(data=>{
                that.appointments = data.data;
              }, error=>{});
        }
      });
  }



}
