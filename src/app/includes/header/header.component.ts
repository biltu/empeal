import { Component, OnInit } from '@angular/core';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import {MessageService} from '../../services/message.service';
import {CommonService} from '../../services/common.service';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart  } from '@angular/router';
import { MessagingService } from "../../services/messaging.service";
import { DeviceDetectorService } from 'ngx-device-detector';
declare var Pusher: any;
import * as io from 'socket.io-client';
declare var $;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  user_data: any={};
  user_detail: any={};
  imageIcon: string;
  isModalOpen: boolean;
  user_role: any={};
  userPoint: string;
  userMailcount: string;
  userPostcount: string;
  usercontestcount: string;
  display: string;
  //userContentcount: string;
  userMaillist: any=[];
  userPostlist: any={};
  userContestlist: any=[];
  //userContentlist: any={};
  singlecontest: any={};
  appointments: any={};
  loginUser: any={};
  unreadNotifications: any=[];
  private socket;
  notificationtoken:any;
  pushmessage:any;
  permissions: any=[];
  today:any;
  constructor(private messagingService: MessagingService, private sanitizer: DomSanitizer, private message:MessageService ,private global: Global,private authService: AuthService,
  			private router: Router, private commonService: CommonService, private deviceService: DeviceDetectorService) { 
    this.userDataSet();
    this.loginUser = JSON.parse(localStorage.getItem('user'));
    this.user_role = this.user_data ? this.user_data.role:'';
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    this.today = yyyy+'-'+mm+'-'+dd;
    
    
    this.asyncInit();
    this.bookedAppointments();
    this.commonService;
    if(this.loginUser && this.user_role.slug ==='employee'){
      setTimeout(() => {
        this.addChatbot();
      }, 4000);
    }

    if(this.loginUser){
      const width = (<any>window.innerWidth > 0) ? <any>window.innerWidth : screen.width;
      const isMobile = this.deviceService.isMobile();
      const userId = this.loginUser.id;
      if(isMobile && width<=425){
        this.notificationtoken = localStorage.getItem('userpushtoken');
        let saas = this.messagingService.requestPermission(userId);
        //this.messagingService.receiveMessage();
        //this.pushmessage = this.messagingService.currentMessage;
        if(this.notificationtoken !=''){
          this.pushTokenUpdate(this.notificationtoken);
        }
      }
    }
    
  }

  ngOnInit() {
    this.commonService.pointUpdateNew.subscribe(point=>{
        this.userPoint = point;
    });

    this.commonService.profileImageChange.subscribe(value=>{
      this.userDataSet();
    });
    this.commonService.updateNewData.subscribe(data=>{
      //if(data.unread_count>0){
        this.userPostcount = data.unread_count;
        this.userPostlist = data.unread_list;
      //}
      
    });

    this.commonService.navigationChangeUpdate.subscribe(data=>{
      this.asyncInit();
    });

    this.isModalOpen = false;
    /** pusher **/
    //Pusher.logToConsole = true;

      var pusher = new Pusher('ff8fc4d385774217ebf5', {
        cluster: 'ap2',
        encrypted: true
      });

      var that = this;
      // subscribe pusher event
      var channel = pusher.subscribe('notify-channel');
      channel.bind('notify-event', function(data) {
       // alert();
          that.commonService.getAll(that.global.apiUrl + '/api/user/unread-nofification')
              .subscribe(data=>{
                that.unreadNotifications = data.data;
              }, error=>{});
      });

      this.commonService.checkaccessmodules.subscribe((data)=>{
        this.permissions = data.permissions;
      });
      
  } 

  asyncInit(){
    if(this.loginUser){
    this.commonService.getAll(this.global.apiUrl + '/api/user/unopened/list')
      .subscribe((data)=>{
        console.log(data);
        this.userPoint = data.data ? data.data.user_point:'';
        this.userMailcount = data.data ?data.data.user_mailcount:'';
        this.commonService.callunreadmailcount(this.userMailcount);
        //this.userMaillist = data.data ?data.data.user_maillist:[];
        this.userPostcount = data.data ?data.data.user_postcount:'';
        this.userPostlist = data.data ?data.data.user_postlist:'';
        console.log(this.userPostlist);
        this.usercontestcount = data.data ? data.data.new_contest_count:0;
        this.userContestlist = data.data ? data.data.contest:[];
        //this.userContentcount = data.data ?data.data.user_contentcount:'';
        //this.userContentlist = data.data ?data.data.user_contentlist:'';
        this.unreadNotifications = data.data? data.data.notificationlist:[];
        localStorage.setItem('user_postlist',JSON.stringify(this.userPostlist));
        localStorage.setItem('notificationlist',JSON.stringify(this.unreadNotifications));
      }, (error)=>{
          //console.log(error);
          //console.log(error.status);
        if(error.status == 401 && error.statusText=="OK" && error.error.message =="Unauthenticated."){
          localStorage.removeItem('token');
          localStorage.removeItem('user');
          localStorage.removeItem('userpushtoken');
          localStorage.removeItem('conversationId');
          localStorage.removeItem('teamid');
          localStorage.removeItem('user_postlist');
          localStorage.removeItem('notificationlist');
          this.router.navigate(['login']);
          window.location.href = this.global.domain_url +"/login";
        }
      });
    }
  }

  goToProfile(){
  	this.router.navigate(['profile']);
  }

  logOut(){
  	   this.authService.logOut(this.global.apiUrl + '/api/logout').subscribe((data)=>{
       if(data.status ==200){
         localStorage.removeItem('token');
         localStorage.removeItem('user');
         localStorage.removeItem('userpushtoken');
         localStorage.removeItem('conversationId');
         localStorage.removeItem('teamid');
         this.router.navigate(['login']);
         window.location.href = this.global.domain_url +"/login";
       }
     });
  }

  userDataSet(){
    this.user_data = this.authService.isUserLoginIn ? JSON.parse(localStorage.getItem('user')):'';
    this.user_detail = this.user_data? this.user_data.user_detail:'';
    this.imageIcon = this.user_detail.thumb_profile_picture;
  }

  bookedAppointments(){
    if(this.loginUser){
    this.commonService.getAll(this.global.apiUrl + '/api/booked/schedules')
      .subscribe((data)=>{
        this.appointments = data.data;
        this.commonService.appointSource({appointments:this.appointments});
      }, (error)=>{

      });
    }
  }

  readNotification(id, geturl, dataid=''){
    let url = geturl=='contest'?'community':geturl;
    this.commonService.getAll(this.global.apiUrl + '/api/user/read-notification/' + id)
        .subscribe((data)=>{
          if(data.status == 200){
            this.unreadNotifications = data.data;
            if(url =='community'){
               this.router.navigate(['/' + url], {queryParams: {comment_id: dataid}});
            }else{
              this.router.navigate(['/' + url]);
            }
            
          }
          
        },(error)=>{});
  }

  readAllNotification(){
    this.commonService.getAll(this.global.apiUrl +  '/api/user/read-notification-all')
      .subscribe((data)=>{
        if(data.status ==200){
          this.unreadNotifications.length =0;
        }
      }, (error)=>{});
  }

  openModalDialog(){
      this.display='block'; //Set block css
      this.isModalOpen = true;
  }

  closeModalDialog(){
    this.display='none'; //set none css after close dialog
    this.isModalOpen = false;
  }

  contestRedirect(cid){
    this.singlecontest = this.userContestlist.filter(function (el) {
          return el.id==cid;
        });
    this.router.navigate(['/dashboard']);
    this.commonService.callDashboardLoad({contest:this.singlecontest, url:this.router.url});
  }

  updateStyle(html) {
      return this.sanitizer.bypassSecurityTrustHtml(html);
  }
  addChatbot(){
    var userdata = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')):'';
      (<any>window).webchatData = {};
      (<any>window).webchatMethods = {
        getMemory: (conversationId) => {
          if (!(<any>window).webchatData.oriUrl) {
            (<any>window).webchatData.oriUrl = window.location.href
          }
          // merge: false - reset the conversation if the user
          // switched to another page since the first message
          if ((<any>window).webchatData.oriUrl !== window.location.href) {
            return { memory: {}, merge: false }
          }
          localStorage.setItem('conversationId',conversationId);
          return { memory: { fullname: userdata.user_detail.firstname+' '+userdata.user_detail.lastname,userid:userdata.id }, merge: true }
        }
      }

        var script = document.createElement("script");
        script.type = "text/javascript";
        script.setAttribute("channelId","fbe9a7dc-d29c-4f18-a0c1-a1ea5f94bfa7");
        script.setAttribute("token","a91162c01674b663b661293378002a9e");
        script.setAttribute("id","cai-webchat");
    if (userdata.id > 0) {
        script.src = "https://cdn.cai.tools.sap/webchat/webchat.js";
    } else {
        script.src = "";
    }
    document.body.appendChild(script);
  }


  pushTokenUpdate(value){
    this.commonService.create(this.global.apiUrl + '/api/user/push-token/update', {push_token:value})
      .subscribe(data=>{
        if(data.status ==200){
          //this.message.success(data.status_text);
        }  
      },error=>{});
  }

}
