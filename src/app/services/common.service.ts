import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MainService } from './main.service';
import { Observable, Subject, of, throwError, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommonService extends MainService{
  private callLoginModal = new BehaviorSubject<string>('login');
  private profileImageUpdate = new BehaviorSubject<string>('image');
  private appointmentSource = new BehaviorSubject<any>('schedule');
  private updateData = new BehaviorSubject<any>('data');
  private chatUpdateData = new BehaviorSubject<any>('data'); 
  private pointUpdate = new BehaviorSubject<any>('data');
  private mailSent = new BehaviorSubject<any>('data');
  private mailNotification = new BehaviorSubject<any>('data');
  private questinnaryScore = new BehaviorSubject<any>('data');
  private unreadmailcount = new BehaviorSubject<any>('data');
  private navigationChange = new BehaviorSubject<any>('data');
  private dashboardLoad = new BehaviorSubject<any>('data');
  private accessmodules = new BehaviorSubject<any>('data');

  currentAppointment = this.appointmentSource.asObservable();
  checklogin = this.callLoginModal.asObservable();
  profileImageChange = this.profileImageUpdate.asObservable();
  updateNewData = this.updateData.asObservable();
  chatUpdateNewData = this.chatUpdateData.asObservable();
  pointUpdateNew= this.pointUpdate.asObservable();
  mailNew = this.mailSent.asObservable();
  updateMailNotify = this.mailNotification.asObservable();
  unopenedMailCount = this.unreadmailcount.asObservable();
  updateQuestionnarie = this.questinnaryScore.asObservable();
  navigationChangeUpdate = this.navigationChange.asObservable();
  checkDashboardLoad = this.dashboardLoad.asObservable();
  checkaccessmodules = this.accessmodules.asObservable();
  constructor(http: HttpClient) {
    super(http);
  }

/**
* Check login obsevable
*/
  checkCallLogin(check){
  	this.callLoginModal.next(check);
  }

/**
* Change profile image obsevable
*/
  changeProfileImage(image){
  	this.profileImageUpdate.next(image);
  }

/**
* Apponitment obsevable
*/
  appointSource(appointment) {
    this.appointmentSource.next(appointment)
  }

/**
* Update data obsevable
*/
  callUpdateData(data){
    this.updateData.next(data);
  }

/**
* Update Chat obsevable
*/
  callChatUpdate(data){
    this.chatUpdateData.next(data);
  }

/**
* Update point obsevable
*/
  callUpdatePoint(data){
    this.pointUpdate.next(data);
  }

/**
* Update send mail obsevable
*/
  updateSentMail(data){
    this.mailSent.next(data);
  }

/**
* Mail Notification obsevable
*/
  callmailNotification(data){
    this.mailNotification.next(data);
  }

/**
* Unread mail count obsevable
*/
  callunreadmailcount(data){
    this.unreadmailcount.next(data);
  }

/**
* Questionnarie data obsevable
*/  
  callQuestionnarie(data){
    this.questinnaryScore.next(data);
  }

/**
* Navigation data obsevable
*/  
  callNavigationUpdate(data){
    this.navigationChange.next(data);
  }

/**
* Dashboard data obsevable
*/  
  callDashboardLoad(data){
    this.dashboardLoad.next(data);
  }

  callgroupaccessmodules(data){
    this.accessmodules.next(data);
  }
  
}
