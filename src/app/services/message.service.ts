import { Injectable } from '@angular/core';
import * as toastr from "toastr";

@Injectable({
  providedIn: 'root'
})
export class MessageService {

   constructor() {
  	 toastr.options = {
      'closeButton': false,
      'debug': false,
      'newestOnTop': false,
      'progressBar': false,
      'positionClass': 'toast-top-right',
      'preventDuplicates': false,
      'onclick': null,
      'showDuration': '300',
      'hideDuration': '1000',
      'timeOut': '5000',
      'extendedTimeOut': '1000',
      'showEasing': 'swing',
      'hideEasing': 'linear',
      'showMethod': 'fadeIn',
      'hideMethod': 'fadeOut'
    };
   }

    success(param) {
	    toastr.success(param);
	}

    warning(param) {
    	toastr.warning(param + ' !');
  	}

  	error(param) {
    	toastr.error(param + ' !');
  	}
}
