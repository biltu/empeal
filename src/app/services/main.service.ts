import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, Subject, of, throwError } from 'rxjs';
import { filter, map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MainService {
 validationError: any = [];

  constructor(private http: HttpClient) { }

/**
* GET HTTP request send 
*/
   getAll(url): Observable<any> {
    return this.http.get(url, this.getHttpOptions())
      .pipe(map((res: Response) => res), catchError(this._errorHandler));
  }

/**
* POST HTTP request send 
*/
  create(url, resource): Observable<any> {
    return this.http.post(url, resource, this.getHttpOptions())
      .pipe(map((res: Response) => res), catchError(this._errorHandler));
  }

/**
* GET HTTP request send and passing ID
*/
  show(url, id): Observable<any> {
    return this.http.get(url + '/' + id, this.getHttpOptions())
      .pipe(map((res: Response) => res), catchError(this._errorHandler));
  }

/**
* PUT HTTP request send and passing ID
*/
  update(url, id, data): Observable<any> {
    return this.http.put(url + '/' + id, data, this.getHttpOptions())
      .pipe(map((res: Response) => res), catchError(this._errorHandler));
  }

/**
* PATCH HTTP request send and passing ID
*/
  updateWithPatch(url, id, data): Observable<any> {
    return this.http.patch(url + '/' + id, data, this.getHttpOptions())
      .pipe(map((res: Response) => res), catchError(this._errorHandler));
  }

/**
* DELETE HTTP request send and passing ID
*/
  delete(url, id): Observable<any> {
    return this.http.delete(url + '/' + id, this.getHttpOptions());
  }

  getToken(): string {
    return localStorage.getItem('token');
  }

   getAllNodeApi(url): Observable<any> {
    return this.http.get(url, this.nodeHttpOptions())
      .pipe(map((res: Response) => res), catchError(this._errorHandler));
  }

/**
* Header option add
*/
   getHttpOptions() {
    const httpOption = {
      headers: new HttpHeaders({'Accept': 'application/json', 'X-Requested-With': 'XMLHttpRequest','Authorization': 'Bearer ' + this.getToken()})
      //headers: new HttpHeaders({'X-Requested-With': 'XMLHttpRequest', 'Authorization': 'Bearer ' + this.getToken()})
    };
    return httpOption;
  }

/**
* Header option add
*/
  nodeHttpOptions(){
    const httpOption = {
      headers: new HttpHeaders({'Accept': 'application/json', 'Content-Type': 'application/json'})
      //headers: new HttpHeaders({'X-Requested-With': 'XMLHttpRequest', 'Authorization': 'Bearer ' + this.getToken()})
    };
    return httpOption;
  }

/**
* Error handle
*/
    _errorHandler(error: HttpErrorResponse) {
    // console.log(error.error.error);
    if (error.status === 422) {
      this.validationError = error.error;
    }
    return throwError(error || 'some error');
  }
}
