import { Injectable } from '@angular/core';
import { WebsocketService } from './websocket.service';
import { WebsocketUserService } from './websocket-user.service';
import { Observable, Subject} from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

}
