import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { mergeMapTo, take } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import {CommonService} from './common.service';
import {Global} from '../global';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart  } from '@angular/router';

@Injectable()
export class MessagingService {

  currentMessage = new BehaviorSubject(null);
  loginUser: any={};
  constructor(
    private router: Router,
    private global: Global,
    private commonService: CommonService,
    private angularFireDB: AngularFireDatabase,
    private angularFireAuth: AngularFireAuth,
    private angularFireMessaging: AngularFireMessaging) {
    this.angularFireMessaging.messaging.subscribe(
      (_messaging) => {
        //_messaging.onMessage = _messaging.onMessage.bind(_messaging);
        _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
      }
    );
    this.loginUser = JSON.parse(localStorage.getItem('user'));
  }

  /**
   * update token in firebase database
   * 
   * @param userId userId as a key 
   * @param token token as a value
   */
  updateToken(userId, token) {
    // we can change this function to request our backend service
    this.angularFireAuth.authState.pipe(take(1)).subscribe(
      () => {
        const data = {};
        data[userId] = token
        this.angularFireDB.object('fcmTokens/').update(data)
      })
  }

  /**
   * request permission for notification from firebase cloud messaging
   * 
   * @param userId userId
   */
  requestPermission(userId) {
    this.angularFireMessaging.requestToken.subscribe(
      (token) => {
         localStorage.setItem('userpushtoken',token)
         const userId = this.loginUser.id;
         if(token !=''){
           this.pushTokenUpdate(token);
         }
         this.updateToken(userId, token);
        return token;
      },
      (err) => {
        if(err.message.includes('messaging/notifications-blocked')==true){

        }else if(err.message.includes('messaging/token-unsubscribe-failed')==true){
          setTimeout(() => {
            window.location.reload();
            document.location.reload();
          }, 100);
        }else{
          
        }
        console.error('Unable to get permission to notify.', err);
      }
    );
  }

  /**
   * hook method when new notification received in foreground
   */
  receiveMessage() {
    this.angularFireMessaging.messages.subscribe(
      (payload) => {
        console.log("new message received. ", payload);
        //this.currentMessage.next(payload);
      })
  }

  pushTokenUpdate(value){
    this.commonService.create(this.global.apiUrl + '/api/user/push-token/update', {push_token:value})
      .subscribe(data=>{
        if(data.status ==200){
          //this.message.success(data.status_text);
        }  
      },error=>{});
  }

}