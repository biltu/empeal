import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../services/message.service';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Component({
  selector: 'app-mail-sentbox',
  templateUrl: './mail-sentbox.component.html',
  styleUrls: ['./mail-sentbox.component.css']
})
export class MailSentboxComponent implements OnInit {
  	sentboxmails: any=[];
    composeForm: FormGroup;
  	mailSearchForm: FormGroup;
    isModalOpen: boolean;
  	count_data:number;
    memberList: any=[];
    groupList: any=[];
    search_memder_model: string;
  	main_data: any;
    user_role: any;
    child_data: any=[];
    active_mail: number;
    replydisplay: any;
    mailreplyForm: FormGroup;
    pmailid: any;
    search_model: string;
    groupModel: any;
    mailfltr:  any=[];
    display: string;
    user_data: any=[];

  constructor(private _sanitizer: DomSanitizer, private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService, private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) { 
  	this.asyncInit();
  	this.count_data = 0;
    this.isModalOpen = false;
    this.user_data = this.authService.isUserLoginIn ? JSON.parse(localStorage.getItem('user')):'';
    this.user_role = this.user_data ? this.user_data.role:'';
  }

	ngOnInit() {
    this.mailreplyForm = this.fb.group({
        parentid:'',
        mailbody:['', [Validators.required]],
        attachfile:''
      });	  	
    this.groupModel ='';
    this.commonService.mailNew.subscribe(data=>{
        this.sentboxmails.unshift(data);
        this.main_data = this.sentboxmails[0];
        this.pmailid = this.main_data.id;
        this.child_data = this.main_data.child;
        this.active_mail = 0;

    });

    this.composeForm = this.fb.group({
        mailusrid:'',
        mailbody:['', [Validators.required]],
        mailsubject:'',
        mailemail:[''],
        attachfile:''
      });
	}

  updateStyle(html) {
      return this._sanitizer.bypassSecurityTrustHtml(html);
  }
  
  openReplyModal(){
    this.replydisplay='block';
    this.setForm();
    this.isModalOpen = true;
  }
  
  closeReplyModal(){
    this.replydisplay='none';
    this.isModalOpen = false;
  }

/**
* get user groups and mail inbox data
*/

	asyncInit(){
  	this.commonService.getAll(this.global.apiUrl + '/api/user/mail/sent')
  		.subscribe((data)=>{
        this.groupList = data.user_groups;
        this.mailfltr = data.data;
  			this.sentboxmails = this.mailfltr;
  			this.main_data = this.sentboxmails[0];
        if(this.sentboxmails[0].length>0){
          this.readMail(this.main_data.id, this.main_data);
        }
        
        this.pmailid = this.main_data.id;
        this.child_data = this.main_data.child;
        this.active_mail = 0;
        this.setForm();
  		}, (error)=>{});
	}

/**
* get last reply 
*/
  getLastChild(id){
    let data = this.sentboxmails.find(obj=>obj.id == id);
    if(data.child.length){
     return data.child[0];
    }else{
     return data;
    }
    
    console.log(data);
  }
    
/**
* get single mail data 
*/

	getMailData(id, sendmail){
      let index = this.sentboxmails.findIndex(obj=>obj.id == id);
      this.readMail(id, sendmail);
      this.active_mail =index;
      this.main_data = this.sentboxmails[index];
      this.pmailid = this.main_data.id;
      this.child_data = this.main_data.child;
      this.setForm();
  }

  setForm(){
      this.mailreplyForm.patchValue({
        parentid:this.pmailid, 
        mailbody:''
      });
    }

/**
* get mail reply data
*/
  mailReply(){
    let form = {composemail:this.mailreplyForm.value};
    if(this.mailreplyForm.invalid){
      if(this.mailreplyForm.get('mailbody').value.length<10){
        this.message.error('Mail body field is required');
      }
      return false;
    }else{
      let newFormData = new FormData();
      Object.keys(this.mailreplyForm.controls).forEach(key => { 
           newFormData.append(key, this.mailreplyForm.get(key).value ? this.mailreplyForm.get(key).value:'');
       });
      this.commonService.create(this.global.apiUrl + '/api/user/mail/reply',newFormData)
          .subscribe((data)=>{
            if(data.status ==200){
              this.message.success(data.status_text);
              this.replydisplay='none';
              this.mailreplyForm.reset();
              this.isModalOpen = false;
              let singlemail = this.sentboxmails.find(obj=>obj.id == data.data.parent_id);
              singlemail.child.unshift(data.data);
              this.readMail(singlemail.id, singlemail);
            }

            if(data.status ==400){
              this.message.error(data.status_text);
            }

            if(data.status ==422){
              this.message.error(data.error);
            }

            if(data.status ==500){
              this.message.error('Please search and select mail from dropdown list.');
            }

          }, (error)=>{
          });
      }
  }

/**
* set read mail
*/
  readMail(id, sendmail){
    this.commonService.getAll(this.global.apiUrl + '/api/mail/set-read/' + id)
        .subscribe(data=>{
          if(data.status == 200){
            sendmail.child_from_unread ='';
          }
        });
  }

/**
* file select
*/
  onFileSelected(event) {
   if(event.target.files.length > 0) 
    {
      this.mailreplyForm.get('attachfile').setValue(event.target.files[0]);
    }else{
      this.message.error('Uploaded file is not a valid file.');
    }
  }

/**
* file select
*/
  onCompsFileSelected(event) {
   if(event.target.files.length > 0) 
    {
      this.composeForm.get('attachfile').setValue(event.target.files[0]);
    }else{
      this.message.error('Uploaded file is not a valid file.');
    }
  }

/**
* get member wise mail
*/
  getGroupWiseData(id, param_id=''){
    //localStorage.setItem('group_id', id);
    this.commonService.getAll(this.global.apiUrl + '/api/groupwise/member?id='+ id)
      .subscribe((data)=>{
        this.memberList = data.member_data;
        if(id>0){
          this.sentboxmails = this.mailfltr.filter(function (el) {
            return el.group_id==id;
          });
          this.main_data = this.sentboxmails[0];
          this.child_data = this.main_data.child;
        }else{
          this.sentboxmails = this.mailfltr
        }
      }, (error)=>{});
  }

/**
* open compose modal
*/

  openMailcompsDialog(id,e){
    this.display='block'; //Set block css
    this.isModalOpen = true;
    this.setCompsForm(id,e);
    this.sentboxmails = this.mailfltr.filter(function (el) {
          return el.message_received.user_id==id || el.message_sent.user_id==id;
        });
    this.main_data = this.sentboxmails[0];
    this.child_data = (this.main_data && this.main_data.hasOwnProperty('child'))?this.main_data.child:'';
  }

/**
* close compose modal
*/
  closeMailcompsDialog(){
    this.display='none'; //set none css after close dialog
    this.isModalOpen = false;
  }

/**
* Send mail
*/
  mailStore(){
    let newFormData = new FormData();
      Object.keys(this.composeForm.controls).forEach(key => { 
           newFormData.append(key, this.composeForm.get(key).value ? this.composeForm.get(key).value:'');
       });

    let form = {composemail:newFormData};
    if(this.composeForm.invalid){
      if(this.composeForm.get('mailbody').value.length<10){
        this.message.error('Mail body field is required');
      }
      return false;
    }else{
      this.commonService.create(this.global.apiUrl + '/api/mail',newFormData)
          .subscribe((data)=>{
            if(data.status ==200){
              this.message.success(data.status_text);
              this.display='none';
              this.isModalOpen = false;
              this.composeForm.reset();
              this.commonService.updateSentMail(data.data);
            }

            if(data.status ==400){
              this.message.error(data.status_text);
            }

            if(data.status ==422){
              this.message.error(data.error);
            }
            
            if(data.status ==500){
              this.message.error('Please search and select mail from dropdown list.');
            }

          }, (error)=>{
          });
      }
  }

/**
* Set compose form
*/
  setCompsForm(id,e){
      this.composeForm.patchValue({
        mailusrid:id,
        mailbody: '',
        mailsubject:'', 
        mailemail:e
      });
  }

/**
* reset chart
*/
  resetchart(event){
      this.redirectTo(this.router.url);
    }

    redirectTo(uri) {
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() =>
    this.router.navigate([uri]));
  }

}
