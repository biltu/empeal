import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MailSentboxComponent } from './mail-sentbox.component';

describe('MailSentboxComponent', () => {
  let component: MailSentboxComponent;
  let fixture: ComponentFixture<MailSentboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MailSentboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MailSentboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
