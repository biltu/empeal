import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CoreModule} from '../core/core.module';
import {MailRoutingModule} from './mail-routing.module';
import {MailInboxComponent} from './mail-inbox/mail-inbox.component';
import {MailSentboxComponent} from './mail-sentbox/mail-sentbox.component';
import { MailSidenavComponent } from './mail-sidenav/mail-sidenav.component';

@NgModule({
  declarations: [MailInboxComponent, MailSentboxComponent, MailSidenavComponent],
  imports: [
    CommonModule,
    MailRoutingModule,
    CoreModule
  ]
})
export class MailModule { }
