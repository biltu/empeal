import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../services/message.service';
import {Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
@Component({
  selector: 'app-mail-sidenav',
  templateUrl: './mail-sidenav.component.html',
  styleUrls: ['./mail-sidenav.component.css']
})
export class MailSidenavComponent implements OnInit {
	composeForm: FormGroup;
	isModalOpen: boolean;
  	count_data:number;
  	display: string;
  	model: any;
  	user_role: any;
  	email_data: any=[];
  	user_data: any=[];
  	replydisplay: any;
	formatter = (result: any) => result.email.toLowerCase();
  	search = (text$: Observable<string>) =>
	    text$.pipe(
	      debounceTime(200),
	      distinctUntilChanged(),
	      map(term => term === '' ? []
	        : this.email_data.filter(v => (v.email).toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
	    )
  	constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService, private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) {
  		this.user_data = this.authService.isUserLoginIn ? JSON.parse(localStorage.getItem('user')):'';
   		this.user_role = this.user_data ? this.user_data.role:'';
  		this.onInputChanged();
  		this.isModalOpen = false;
   	}

  	ngOnInit() {
  		this.composeForm = this.fb.group({
	  		mailusrid:'',
	  		mailbody:['', [Validators.required]],
	 		mailsubject:'',
	 		mailemail:['', [Validators.required]],
	 		attachfile:''
	  	});
  	}

/**
* change form value
*/
  	changeModelEvent(e){
		if(e && e.email){
			this.composeForm.controls.mailemail.setValue(e.email);
			this.composeForm.controls.mailusrid.setValue(e.id);
		}
		
	}

  	openModalDialog(){
    	this.display='block'; //Set block css
    	this.isModalOpen = true;
	}

	closeModalDialog(){
	  this.display='none'; //set none css after close dialog
	  this.isModalOpen = false;
	}

/**
* Send mail
*/
	mailStore(){
		let newFormData = new FormData();
    	Object.keys(this.composeForm.controls).forEach(key => { 
           newFormData.append(key, this.composeForm.get(key).value ? this.composeForm.get(key).value:'');
       });

		let form = {composemail:newFormData};
		if(this.composeForm.invalid){
			if(this.composeForm.get('mailemail').value==undefined){
				this.message.error('Email fiels is required');
			}

			if(this.composeForm.get('mailbody').value.length<10){
				this.message.error('Mail body field is required');
			}
			return false;
		}else{
			this.commonService.create(this.global.apiUrl + '/api/mail',newFormData)
	        .subscribe((data)=>{
	          if(data.status ==200){
	            this.message.success(data.status_text);
	            this.display='none';
	            this.isModalOpen = false;
	            this.composeForm.reset();
	            this.commonService.updateSentMail(data.data);
	            if(this.user_role.slug =='employee'){
	            	this.composeForm.get('mailusrid').setValue(this.email_data[0].id);
	            	this.composeForm.get('mailemail').setValue(this.email_data[0].email);
	            }
	          }

	          if(data.status ==400){
	          	this.message.error(data.status_text);
	          }

	          if(data.status ==422){
              	this.message.error(data.error);
              }

	          if(data.status ==500){
	          	this.message.error('Please search and select mail from dropdown list.');
	          }

	        }, (error)=>{
	        });
    	}
	}
/**
* Set form
*/
	setForm(){
	    this.composeForm.patchValue({
	      mailusrid:this.email_data[0].id,
	      mailbody: '',
	      mailsubject:'', 
	      mailemail:this.email_data[0].email
	    });
	}

/**
* get search user mail list
*/
	onInputChanged(){
		this.commonService.getAll(this.global.apiUrl + '/api/user/mail/list')
	  		.subscribe((data)=>{
	  			this.email_data = data.data;
	  			if(this.user_role.slug=='employee'){
	  			this.setForm();
	  			}
	  		}, (error)=>{});
	}

/**
* file select
*/
	onFileSelected(event) {
	   if(event.target.files.length > 0) 
	    {
	      this.composeForm.get('attachfile').setValue(event.target.files[0]);
	    }else{
	      this.message.error('Uploaded file is not a valid file.');
	    }
  	}


}
