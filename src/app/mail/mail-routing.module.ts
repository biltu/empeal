import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './../auth.guard';
import { EmployeeGuard } from './../employee.guard';
import { CoachGuard } from './../coach.guard';
import { MailInboxComponent } from './mail-inbox/mail-inbox.component';
import { MailSentboxComponent } from './mail-sentbox/mail-sentbox.component';
import { StatutoryGuard } from './../statutory.guard';
import {PermissionResolve} from './../permission-resolve';

const routes: Routes = [
	{path:'inbox', canActivate: [AuthGuard, StatutoryGuard], component: MailInboxComponent},
	{path:'sentbox', canActivate: [AuthGuard, StatutoryGuard], component: MailSentboxComponent,resolve: {
      permissions: PermissionResolve
    }},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MailRoutingModule { }
