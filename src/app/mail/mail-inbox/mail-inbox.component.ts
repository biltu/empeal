import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../services/message.service';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
@Component({
  selector: 'app-mail-inbox',
  templateUrl: './mail-inbox.component.html',
  styleUrls: ['./mail-inbox.component.css']
})
export class MailInboxComponent implements OnInit {
	  inboxmails: any=[];
    composeForm: FormGroup;
  	mailSearchForm: FormGroup;
    isModalOpen: boolean;
  	count_data:number;
    main_data: any;
    child_data: any=[];
    memberList: any=[];
    groupList: any=[];
    active_mail: number;
    search_model: string;
    search_memder_model: string;
    replydisplay:any;
    user_role: any;
    user_data: any=[];
    mailreplyForm: FormGroup;
    pmailid: any;
    groupModel: any;
    mailfltr:  any=[];
    display: string;
  constructor(private _sanitizer: DomSanitizer, private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService, private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) { 
  	this.asyncInit();
  	this.count_data = 0;
    this.isModalOpen = false;
    this.user_data = this.authService.isUserLoginIn ? JSON.parse(localStorage.getItem('user')):'';
    this.user_role = this.user_data ? this.user_data.role:'';
    console.log(this.activeRoute.snapshot.data['permissions']);
  }

  ngOnInit() {
    this.mailreplyForm = this.fb.group({
        parentid:'',
        mailbody:['', [Validators.required]],
        attachfile:''
      });
    this.groupModel ='';

    this.composeForm = this.fb.group({
        mailusrid:'',
        mailbody:['', [Validators.required]],
        mailsubject:'',
        mailemail:[''],
        attachfile:''
      });

  }

  updateStyle(html) {
      return this._sanitizer.bypassSecurityTrustHtml(html);
  }

  openReplyModal(){
    this.replydisplay='block';
    this.setForm();
    this.isModalOpen = true;
  }
  
  closeReplyModal(){
    this.replydisplay='none';
    this.isModalOpen = false;
  }

/**
* get group lists, inbox mails
*/

  asyncInit(){
  	this.commonService.getAll(this.global.apiUrl + '/api/mail')
  		.subscribe((data)=>{
          this.groupList = data.user_groups;
          this.mailfltr = data.data;
  			  this.inboxmails = this.mailfltr;
	        this.main_data = this.inboxmails[0];
          this.readMail(this.main_data.id, this.main_data);
          this.pmailid = this.main_data.id;
	        this.child_data = this.main_data.child;
	        this.active_mail = 0;
  			//this.count_data = data.data_count;
  			//this.pagination();
        this.setForm();
  		}, (error)=>{});
  	}

/**
* get single inbox data
*/
    getMailData(id, inboxmail){
      let index = this.inboxmails.findIndex(obj=>obj.id == id);
      this.readMail(id, inboxmail);
      this.active_mail =index;
      this.main_data = this.inboxmails[index];
      this.pmailid = this.main_data.id;
      this.child_data = this.main_data.child;
      this.setForm();
    }

/**
* Set mail form
*/
    setForm(){
      this.mailreplyForm.patchValue({
        parentid:this.pmailid, 
        mailbody:''
      });
    }

/**
* Mail Reply
*/
    mailReply(){
    let form = {composemail:this.mailreplyForm.value};
    if(this.mailreplyForm.invalid){
      if(this.mailreplyForm.get('mailbody').value.length<10){
        this.message.error('Mail body field is required');
      }
      return false;
    }else{
      let newFormData = new FormData();
      Object.keys(this.mailreplyForm.controls).forEach(key => { 
           newFormData.append(key, this.mailreplyForm.get(key).value ? this.mailreplyForm.get(key).value:'');
       });
      this.commonService.create(this.global.apiUrl + '/api/user/mail/reply',newFormData)
          .subscribe((data)=>{
            if(data.status ==200){
              this.message.success(data.status_text);
              this.replydisplay='none';
              this.mailreplyForm.reset();
              this.isModalOpen = false;
              let singlemail = this.inboxmails.find(obj=>obj.id == data.data.parent_id);
              singlemail.child.unshift(data.data);
              //this.readMail(singlemail.id, singlemail);
            }

            if(data.status ==400){
              this.message.error(data.status_text);
            }

            if(data.status ==422){
              this.message.error(data.error);
            }
            
            if(data.status ==500){
              this.message.error('Please search and select mail from dropdown list.');
            }

          }, (error)=>{
          });
      }
    }

/**
* Set mail read
*/
  readMail(id, inboxmail){
    this.commonService.getAll(this.global.apiUrl + '/api/mail/set-read-to/' + id)
        .subscribe(data=>{
          if(data.status == 200){
            inboxmail.is_read_to ='1';
            inboxmail.child_to_unread ='';
            this.commonService.callmailNotification({'unread':data.unread_mails});
          }
        });
  }

/**
* set file select
*/
  onFileSelected(event) {
   if(event.target.files.length > 0) 
    {
      this.mailreplyForm.get('attachfile').setValue(event.target.files[0]);
    }else{
      this.message.error('Uploaded file is not a valid file.');
    }
  }

/**
* set file select
*/
  onCompsFileSelected(event) {
   if(event.target.files.length > 0) 
    {
      this.composeForm.get('attachfile').setValue(event.target.files[0]);
    }else{
      this.message.error('Uploaded file is not a valid file.');
    }
  }

/**
* get group wise mail 
*/
  getGroupWiseData(id, param_id=''){
    //localStorage.setItem('group_id', id);
    this.commonService.getAll(this.global.apiUrl + '/api/groupwise/member?id='+ id)
      .subscribe((data)=>{
        this.memberList = data.member_data;
        if(id>0){
          this.inboxmails = this.mailfltr.filter(function (el) {
            return el.group_id==id;
          });
          this.main_data = this.inboxmails[0];
          this.child_data = this.main_data.child;
        }else{
          this.inboxmails = this.mailfltr;
        }
      }, (error)=>{});
  }

/**
* Open compose mail
*/
  openMailcompsDialog(id,e){
    this.display='block'; //Set block css
    this.isModalOpen = true;
    this.setCompsForm(id,e);
    this.inboxmails = this.mailfltr.filter(function (el) {
          return el.message_received.user_id==id || el.message_sent.user_id==id;
        });
    this.main_data = this.inboxmails[0];
    this.child_data = (this.main_data && this.main_data.hasOwnProperty('child'))?this.main_data.child:'';
  }

/**
* Close compose mail
*/
  closeMailcompsDialog(){
    this.display='none'; //set none css after close dialog
    this.isModalOpen = false;
  }

/**
* Send mail
*/
  mailStore(){
    let newFormData = new FormData();
      Object.keys(this.composeForm.controls).forEach(key => { 
           newFormData.append(key, this.composeForm.get(key).value ? this.composeForm.get(key).value:'');
       });

    let form = {composemail:newFormData};
    if(this.composeForm.invalid){
      if(this.composeForm.get('mailbody').value.length<10){
        this.message.error('Mail body field is required');
      }
      return false;
    }else{
      
      this.commonService.create(this.global.apiUrl + '/api/mail',newFormData)
          .subscribe((data)=>{
            if(data.status ==200){
              this.message.success(data.status_text);
              this.display='none';
              this.isModalOpen = false;
              this.composeForm.reset();
              this.commonService.updateSentMail(data.data);
            }

            if(data.status ==400){
              this.message.error(data.error);
            }

            if(data.status ==422){
              this.message.error(data.error);
            }

            if(data.status ==500){
              this.message.error('Please search and select mail from dropdown list.');
            }

          }, (error)=>{
          });
      }
  }

/**
* set Compose form
*/
  setCompsForm(id,e){
      this.composeForm.patchValue({
        mailusrid:id,
        mailbody: '',
        mailsubject:'', 
        mailemail:e
      });
  }

/**
* Reset chart
*/
  resetchart(event){
    this.redirectTo(this.router.url);
  }

  redirectTo(uri) {
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() =>
    this.router.navigate([uri]));
  }

}
