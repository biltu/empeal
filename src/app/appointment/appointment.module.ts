import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CoreModule} from '../core/core.module';

import { AppointmentRoutingModule } from './appointment-routing.module';
import { AppointmentManageComponent } from './appointment-manage/appointment-manage.component';
import { SlotListComponent } from './slot-list/slot-list.component';
import { CaledarComponent } from './caledar/caledar.component';
import { BookingListComponent } from './booking-list/booking-list.component';

@NgModule({
  declarations: [AppointmentManageComponent, SlotListComponent, CaledarComponent, BookingListComponent],
  imports: [
    CommonModule,
    AppointmentRoutingModule,
    CoreModule
  ]
})
export class AppointmentModule { }
