import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../services/message.service';
@Component({
  selector: 'app-booking-list',
  templateUrl: './booking-list.component.html',
  styleUrls: ['./booking-list.component.css']
})
export class BookingListComponent implements OnInit {
	appointments: any=[];
	groups: any=[];
	groupModel: string ='';
	group_id:string;
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService, private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) { 
  	this.asyncInit();
  }

  ngOnInit() {
  }

/**
* Get all booked scheduled
*/

  asyncInit(){
  	this.commonService.getAll(this.global.apiUrl + '/api/booked/schedules?group_id=all')
      .subscribe((data)=>{
      	if(data.status==200){
      		this.appointments = data.data;
          	this.groups = data.groups;

      	}
        
      }, (error)=>{});
  }

}
