import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../services/message.service';
declare var Swal: any;

@Component({
  selector: 'app-slot-list',
  templateUrl: './slot-list.component.html',
  styleUrls: ['./slot-list.component.css']
})
export class SlotListComponent implements OnInit {
	slots: any=[];
	appointments: any=[];
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService, private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) { 
    this.asyncInit();
  }

  ngOnInit() {
  	this.commonService.currentAppointment.subscribe((data)=>{
      this.appointments = data.appointments;
    });
  }

/**
* Get All created appointment
*/

  asyncInit(){
  	this.commonService.getAll(this.global.apiUrl + '/api/appointment')
  		.subscribe((data)=>{
        this.slots = data.data;
  		}, (error)=>{});
  }

/**
* Cancel Appointment
*/
  deleteSlot(id, index){
  		Swal.fire({
		  title: 'Are you sure?',
		  text: "You won't be able to revert this!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!'
		}).then((result) => {
			if (result.value) {
				this.commonService.delete(this.global.apiUrl + '/api/appointment', id)
	  			.subscribe((data)=>{
	  			if(data.status ==200){	 
	  				 this.slots.splice(index,1);					
				    Swal.fire(
				      'Deleted!',
				      'Your slot has been deleted.',
				      'success'
				    )
	  			 }
	  			},(error)=>{});

		  }
				  
		});
  }

}
