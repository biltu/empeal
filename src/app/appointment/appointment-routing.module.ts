import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './../auth.guard';
import { EmployeeGuard } from './../employee.guard';
import { CoachGuard } from './../coach.guard';
import { AppointmentManageComponent } from './appointment-manage/appointment-manage.component';
import { SlotListComponent } from './slot-list/slot-list.component';
import { CaledarComponent } from './caledar/caledar.component';
import { BookingListComponent } from './booking-list/booking-list.component';
import { StatutoryGuard } from './../statutory.guard';

const routes: Routes = [
	{path:'set', canActivate: [AuthGuard, StatutoryGuard], component: AppointmentManageComponent},
	{path:'slots', canActivate: [AuthGuard,CoachGuard], component: SlotListComponent},
	{path:'calendar', canActivate: [AuthGuard, StatutoryGuard], component: CaledarComponent},
	{path:'booking/list', canActivate: [AuthGuard, StatutoryGuard], component: BookingListComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppointmentRoutingModule { }
