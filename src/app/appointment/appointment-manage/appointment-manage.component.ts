import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../services/message.service';

@Component({
  selector: 'app-appointment-manage',
  templateUrl: './appointment-manage.component.html',
  styleUrls: ['./appointment-manage.component.css']
})
export class AppointmentManageComponent implements OnInit {
	today: any;
  groups: any=[];
  setslots: FormArray;
  AddForm: FormGroup;
  appointments: any=[];
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService, private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) {
  	this.asyncInit();
  	this.today = new Date;
  }

  ngOnInit() {
    this.AddForm = this.fb.group({
        setslots: this.fb.array([this.createQst()])
      });
    this.commonService.currentAppointment.subscribe((data)=>{
      this.appointments = data.appointments;
    });

  }

/**
* Get Groups data 
*/
  asyncInit(){
    this.commonService.getAll(this.global.apiUrl + '/api/appointment/create')
      .subscribe((data)=>{
        this.groups = data.groups;
      }, (error)=>{});
  }

  createQst(): FormGroup{
      return this.fb.group({
        group:[''],
        slot_date:[''],
        open_time:[''],
        end_time:[''],
        slot_lenth:['']
      });
    }

    addslotArray(): void {
      this.setslots = this.AddForm.get('setslots') as FormArray;
      this.setslots.push(this.createQst());
    }

    deleteSlotArray(index: number){
      this.setslots = this.AddForm.get('setslots') as FormArray;
      this.setslots.removeAt(index);
    }

/**
* submit appointment booking 
*/

    submitSlot(){
      this.commonService.create(this.global.apiUrl + '/api/appointment',this.AddForm.value)
        .subscribe((data)=>{
          if(data.status ===200){
            this.message.success(data.status_text);
            this.router.navigate(['appointment/slots']);
          }
          console.log(data);
        },(error)=>{});
    }

    get formData() { return <FormArray>this.AddForm.get('setslots')['controls']; }
}
