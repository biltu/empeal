import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Global } from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../services/message.service';
declare var Swal: any;

@Component({
  selector: 'app-caledar',
  templateUrl: './caledar.component.html',
  styleUrls: ['./caledar.component.css']
})
export class CaledarComponent implements OnInit {
	schedules:any=[];
	group_id:string;
  groups:any=[];
  today: any;
  currentday:any;
  weeklastday:any;
  nextdate:any;
  prevdate:any;
  bookedslots:any;
  groupModel: string ='';
  appointments: any=[];
  user_role: any={};
  user_data: any={};
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,
    private authService: AuthService, private router: Router, private commonService: CommonService, 
    private activeRoute: ActivatedRoute) { 
  	this.asyncInit();
    this.user_data = this.authService.isUserLoginIn ? JSON.parse(localStorage.getItem('user')):'';
    this.user_role = this.user_data ? this.user_data.role:'';
    this.today = new Date;
  }

  ngOnInit() {
    this.commonService.currentAppointment.subscribe((data)=>{
      this.appointments = data.appointments;
    });
  }

/**
* Display calendar data with booked
*/

  asyncInit(){
    this.commonService.getAll(this.global.apiUrl + '/api/calendar/schedules?group_id='+this.group_id)
      .subscribe((data)=>{
      	if(data.status==200){
      		this.schedules = Object.entries(data.data);
          this.groups = data.groups;
          this.groupModel = '';
          this.bookedslots = data.bookedslots;
          this.group_id = this.groups[0].id;
          this.groupModel = this.user_role.slug =='employee'? this.groups[0].id:'';
          this.weeklastday = data.weeklastday;
          this.currentday = data.currentday;

      	}
        
      }, (error)=>{});
  }

/**
* Get previous week booking
*/
  prevWeek(date){
    this.prevdate = date;
    this.commonService.getAll(this.global.apiUrl + '/api/calendar/schedules?group_id='+this.groupModel+'&prev='+this.prevdate)
      .subscribe((data)=>{
        if(data.status==200){
          this.schedules = Object.entries(data.data);
          this.groups = data.groups;
          this.bookedslots = data.bookedslots;
          //this.group_id = this.groups[0].id;
          this.weeklastday = data.weeklastday;
          this.currentday = data.currentday;
        }
        
      }, (error)=>{});
  }

/**
* Get next week booking
*/
  nextWeek(date){
    this.nextdate = date;
    this.commonService.getAll(this.global.apiUrl + '/api/calendar/schedules?group_id='+this.groupModel+'&next='+this.nextdate)
      .subscribe((data)=>{
        if(data.status==200){
          this.schedules = Object.entries(data.data);
          this.groups = data.groups;
          this.bookedslots = data.bookedslots;
          this.weeklastday = data.weeklastday;
          this.currentday = data.currentday;
        }
        
      }, (error)=>{});
  }

/**
* Get booking slot by group
*/
    getTargetByGrp(value){
      this.group_id = value;
      this.commonService.getAll(this.global.apiUrl + '/api/calendar/schedules?group_id='+this.group_id)
      .subscribe((data)=>{
        if(data.status==200){
          this.schedules = Object.entries(data.data);
          this.groups = data.groups;
          this.bookedslots = data.bookedslots;
          this.weeklastday = data.weeklastday;
          this.currentday = data.currentday;
        }
      }, (error)=>{});
    }

/**
* Checked Slot booking
*/
    chekcedBookedSlot(slot,date){
      let apntdate = new Date(date);
      let apntdate_day = (apntdate.getDate()<10)?'0'+apntdate.getDate():apntdate.getDate();
      let apntdate_month = apntdate.getUTCMonth()+1;
      let apointmnth = (apntdate_month<10)?'0'+apntdate_month:apntdate_month;
      let apntdate__year = this.today.getFullYear();
      let slttime = slot.split(' - ')[0];
      let appointdate = apntdate__year+'-'+apointmnth+'-'+apntdate_day;

      //let data = this.bookedslots.find(book_slot => String(book_slot.date_of_appointment) == String(appointdate));
      let data = this.bookedslots.find(book_slot => String(book_slot.slot_start_time) == String(slttime));
      let datadate = this.bookedslots.find(book_slotdate => String(book_slotdate.date_of_appointment) == String(appointdate));
      if(data && datadate){
        return true;
      }else{
        return false;
      }
    }
/**
* Checked previous Slot booking
*/
    checkprevslot(slot,date){

      let today_day = this.today.getDate();
      let today_year = this.today.getFullYear();
      let today_month = this.today.getMonth() + 1;
      let today_hours = this.today.getHours();
      let today_minut = this.today. getMinutes();
      let slotdate = new Date(date);
      let slotdate_day = slotdate.getDate();
      let slotdate_month = slotdate.getMonth()+1;
      let slttime = slot.split(' - ')[0];
      let onlyslttimehr = slttime.split(' ')[0].split(':')[0];
      let onlyslttimemnt = slttime.split(' ')[0].split(':')[1];
      let onlyslttime_ampm = slttime.split(' ')[1];
      let slthr = (onlyslttime_ampm=='pm')?parseInt(onlyslttimehr)+12:onlyslttimehr;
      console.log(slthr+'===='+today_hours+' === '+onlyslttimehr+' ==== '+slttime);
      if(slotdate_day>=today_day && slotdate_month>=today_month){
        if(slotdate_day==today_day && slthr>=today_hours){
          return true;
        }else if(slotdate_day>today_day){
          return true;
        }else{
          return false;
        }
      }else{
        return false;
      }
    }

/**
* reate slot booking
*/
    bookSlot(schtime,schedule,date,j){
      let start_time = schtime.split(' - ')[0];
      let end_time = schtime.split(' - ')[1];
      let bookingdate = schedule.split(',')[0]+ '-'+schedule.split(',')[1]+'-'+date.split('-')[2];

      let data = {start_time:start_time,end_time:end_time,date:bookingdate};
      Swal.fire({
      title: 'Are you sure?',
      text: "You want to book this slot!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, book it!'
    }).then((result) => {
        if (result.value) {
          console.log(result.value);
          this.commonService.create(this.global.apiUrl + '/api/slot/booking', data)
            .subscribe((data)=>{
              if(data.status ==200){   
                Swal.fire(
                'Confirm!',
                'Your slot has been booked.',
                'success'
                );
                this.redirectTo(this.router.url);
              }
            },(error)=>{});
        }
      });
    }

  redirectTo(uri) {
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() =>
    this.router.navigate([uri]));
  }
  
}


