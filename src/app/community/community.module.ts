import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CoreModule} from '../core/core.module';
import { CommunityRoutingModule } from './community-routing.module';
import { CommunityComponent } from './community/community.component';
import { EmployeeCommunityComponent } from './community/employee-community/employee-community.component';
import { CoachCommunityComponent } from './community/coach-community/coach-community.component';
import { SecondaryCoachCommunityComponent } from './community/secondary-coach-community/secondary-coach-community.component';
import { MessagesComponent } from './messages/messages.component';

@NgModule({
  declarations: [CommunityComponent, EmployeeCommunityComponent, CoachCommunityComponent, MessagesComponent,SecondaryCoachCommunityComponent],
  imports: [
    CommonModule,
    CommunityRoutingModule,
    CoreModule
  ]
})
export class CommunityModule { }
