import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-community',
  templateUrl: './community.component.html',
  styleUrls: ['./community.component.css']
})
export class CommunityComponent implements OnInit {

  user_role: any={};
  user_data: any={};
  constructor(private authService: AuthService) { 
  	this.user_data = this.authService.isUserLoginIn ? JSON.parse(localStorage.getItem('user')):'';
    this.user_role = this.user_data ? this.user_data.role:'';
  }

  ngOnInit() {
  }

}
