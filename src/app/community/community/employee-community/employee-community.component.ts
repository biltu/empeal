import { Component, OnInit, HostListener, ElementRef} from '@angular/core';
import { CommonService } from '../../../services/common.service';
import { MessageService } from '../../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import {Global} from '../../../global';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
declare var $ :any;
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-employee-community',
  templateUrl: './employee-community.component.html',
  styleUrls: ['./employee-community.component.css']
})
export class EmployeeCommunityComponent implements OnInit{
  closeResult: string;
  postModal: any;
  likeModal: any;
  reactDisplay: boolean;
  communityPostForm: FormGroup;
  messagePostForm: FormGroup;
  reactForm: FormGroup;
  postReadForm: FormGroup;
  postedData: any=[];
  memberList: any=[];
  showLikeIndex: string;
  react_data: any={
    likes:[],
    congrats:[],
    loves:[],
    insightfuls:[],
    curiouss:[]
  };
  seeGroupMem: boolean;
  show_message_all:any={};
  messageShowIndex: string;
  data_limit: number;
  eventCount: number;
  searchText: string;
  unreadpostIds: any=[];
  loggedInUser: any={};
  postError: any;
  comntfilename: string;
  paramId: any='';
  total_post:number=0;
  navigationSubscription:any;
  isModalOpen: boolean=false;
  paramcontestid:number;
  memberviewid: number;
  contestname: any;

  //@HostListener("window:scroll", ["$event.target"])
  // scrollHandler(elem) {
  //   let pos =  (document.documentElement.scrollTop) + document.documentElement.clientHeight;
  //   //$(window).scrollTop() + $(window).height() >= $(document).height()
  //   let max = document.documentElement.scrollHeight;
  //   if(pos >=max){     
  //     this.eventCount +=1;
  //     if(this.eventCount ==1){
  //       this.data_limit +=5;
  //       if(this.total_post>this.data_limit){
  //         console.log(this.eventCount,'true');
  //         this.asyncInit();
  //       }else{
  //         console.log(this.eventCount,'post end');
  //       }
  //     }else{
  //       console.log(this.eventCount,'false');
  //     }
      
  //   }
  // }

  constructor(private commonService: CommonService, private global: Global, private fb: FormBuilder, private sanitizer: DomSanitizer,
    private messageService: MessageService, private eRef: ElementRef, private route: Router, private activeRoute: ActivatedRoute) {
    this.loggedInUser = JSON.parse(localStorage.getItem('user'));
    this.paramcontestid = this.activeRoute.snapshot.queryParams['contest_id'] ? this.activeRoute.snapshot.queryParams['contest_id']:'';
    this.memberviewid = this.activeRoute.snapshot.queryParams['user_id'] ? this.activeRoute.snapshot.queryParams['user_id']:'';
    this.contestname = "Community";
    this.data_limit =0;
    this.asyncInit();
    this.showLikeIndex = '';
    this.seeGroupMem = false;
    //this.show_message_all = false;
    this.messageShowIndex = '';
    this.eventCount =0;

   this.navigationSubscription = this.route.events.subscribe((e: any) => {
     // If it is a NavigationEnd event re-initalise the component
     if (e instanceof NavigationEnd) {
       this.asyncInit();
     }
   });

  }

  ngOnInit() {
    this.postModal = 'none';
    this.likeModal ='none';
    this.reactDisplay =false;

    this.communityPostForm = this.fb.group({
      post_desc: ['', [Validators.required]],
      post_image: [''],
      post_image_text:[''],
      contest_id:this.paramcontestid?this.paramcontestid:0,
      post_video_link: ['', [Validators.required]],     
    });

    this.messagePostForm = this.fb.group({
      messages: ['', [Validators.required]],
      reply_id: '',
      attachfile:''
    });

    this.reactForm = this.fb.group({
      type:''
    });

    this.postReadForm = this.fb.group({
      user_id:'',
      post_id:'',
      is_visit:''
    });
  }

/**
* Get community all posts and comments
*/

  asyncInit(){  
     if ((this.navigationSubscription && this.route.url =='/community' && !this.activeRoute.snapshot.queryParams['comment_id']) || this.activeRoute.snapshot.queryParams['id'] ) {
       let param = this.activeRoute.snapshot.queryParams['id'] ? this.activeRoute.snapshot.queryParams['id']:'';
       this.paramId = param;
       this.paramcontestid = this.activeRoute.snapshot.queryParams['contest_id'] ? this.activeRoute.snapshot.queryParams['contest_id']:'';
       this.commonService.getAll(this.global.apiUrl + '/api/community?limit=' + this.data_limit+ '&param_id=' + param+ '&contest_id='+this.paramcontestid)
        .subscribe((data)=>{
          this.postedData = data.data;
          this.total_post = data.total_post;
          this.memberList = data.member_data;
          this.contestname = data.contest_name.length >0?data.contest_name[0]:'Community';
          this.eventCount =0;
          let notReadData = data.data.filter(obj=> !obj.post_read.length);
          if(notReadData.length){
            let unreadIds = notReadData.map(obj=> obj.id);
            this.commonService.create(this.global.apiUrl +'/api/community/view-store', {unread_ids: unreadIds})
                .subscribe(data=>{
                  this.commonService.callUpdateData(data.data);
                },error=>{});
          }
        }, (error)=>{});
    }else if((this.navigationSubscription && this.route.url =='/community' && !this.activeRoute.snapshot.queryParams['id']) || this.activeRoute.snapshot.queryParams['comment_id']){
      this.paramcontestid = this.activeRoute.snapshot.queryParams['contest_id'] ? this.activeRoute.snapshot.queryParams['contest_id']:'';
      let commentId =  this.activeRoute.snapshot.queryParams['comment_id'];
        this.commonService.getAll(this.global.apiUrl + '/api/community/get-message/post/' + commentId)
            .subscribe(data=>{
               let param = data.data ? data.data:'';
               this.paramId = param;
               this.total_post = data.total_post;
               this.paramcontestid = data.contest_id ? data.contest_id:'';
               this.commonService.getAll(this.global.apiUrl + '/api/community?limit=' + this.data_limit+ '&param_id=' + param+ '&contest_id='+this.paramcontestid+'&user_id='+this.memberviewid)
                   .subscribe(data=>{
                      this.postedData = data.data;
                      this.memberList = data.member_data;
                      this.contestname = data.contest_name.length >0?data.contest_name[0]:'Community';
                   },error=>{});
            },error=>{});
    }else if(this.activeRoute.snapshot.queryParams['contest_id']){
      this.memberviewid = this.activeRoute.snapshot.queryParams['user_id'] ? this.activeRoute.snapshot.queryParams['user_id']:'';
      this.paramcontestid = this.activeRoute.snapshot.queryParams['contest_id'] ? this.activeRoute.snapshot.queryParams['contest_id']:'';
      
      this.commonService.getAll(this.global.apiUrl + '/api/community?limit=' + this.data_limit+'&contest_id='+this.paramcontestid+'&user_id='+this.memberviewid)
        .subscribe((data)=>{
          this.postedData = data.data;
          this.total_post = data.total_post;
          this.contestname = data.contest_name.length >0?data.contest_name[0]:'Community';
          this.memberList = data.member_data;
          this.eventCount =0;
          let notReadData = data.data.filter(obj=> !obj.post_read.length);
          if(notReadData.length){
            let unreadIds = notReadData.map(obj=> obj.id);
            this.commonService.create(this.global.apiUrl +'/api/community/view-store', {unread_ids: unreadIds})
                .subscribe(data=>{
                  this.commonService.callUpdateData(data.data);
                },error=>{});
          }
        }, (error)=>{});
    }else if(this.activeRoute.snapshot.queryParams['user_id'] && !this.activeRoute.snapshot.queryParams['contest_id']){
      this.memberviewid = this.activeRoute.snapshot.queryParams['user_id'] ? this.activeRoute.snapshot.queryParams['user_id']:'';
      this.commonService.getAll(this.global.apiUrl + '/api/community?limit=' + this.data_limit+'&user_id='+this.memberviewid)
        .subscribe((data)=>{
          this.postedData = data.data;
          this.total_post = data.total_post;
          this.memberList = data.member_data;
          this.contestname = data.contest_name.length >0?data.contest_name[0]:'Community';
          this.eventCount =0;
          let notReadData = data.data.filter(obj=> !obj.post_read.length);
          if(notReadData.length){
            let unreadIds = notReadData.map(obj=> obj.id);
            this.commonService.create(this.global.apiUrl +'/api/community/view-store', {unread_ids: unreadIds})
                .subscribe(data=>{
                  this.commonService.callUpdateData(data.data);
                },error=>{});
          }
        }, (error)=>{});
    }
    else{
      if(this.route.url =='/community'){
        this.paramcontestid = 0;
        this.commonService.getAll(this.global.apiUrl + '/api/community?limit=' + this.data_limit+'&user_id='+this.memberviewid)
        .subscribe((data)=>{
          this.postedData = data.data;
          this.total_post = data.total_post;
          this.memberList = data.member_data;
          this.eventCount =0;
          let notReadData = data.data.filter(obj=> !obj.post_read.length);
          if(notReadData.length){
            let unreadIds = notReadData.map(obj=> obj.id);
            this.commonService.create(this.global.apiUrl +'/api/community/view-store', {unread_ids: unreadIds})
                .subscribe(data=>{
                  this.commonService.callUpdateData(data.data);
                },error=>{});
          }
        }, (error)=>{});
      }
    }
  }



/**
* Create post
*/
  submitPost(){
    let formValues = this.formDataSet(this.communityPostForm);
    this.commonService.create(this.global.apiUrl + '/api/community', formValues)
        .subscribe((data)=>{
          if(data.status == 422){
            this.postModal ='inline';
            this.postError = data.error;
          }

          if(data.status == 200){
            this.isModalOpen = false;
            this.postError ='';
            this.postModal ='none';
            this.postedData.unshift(data.data);
            this.messageService.success(data.status_text);
            this.commonService.callUpdatePoint(data.user_points);
            //this.communityPostForm.reset();
            this.setReset();
          }
        }, (error)=>{});
  }

 setCursorToTextBox(id){
   document.getElementById('msg_' + id).focus();
 }


/**
* file select 
*/
  onFileSelected(event) {
   if(event.target.files.length > 0 && event.target.files[0].type === 'image/jpeg' || event.target.files[0].type === 'image/png' || event.target.files[0].type === 'image/jpg') 
    {
      this.comntfilename = event.target.files[0].name;
      this.messagePostForm.get('attachfile').setValue(event.target.files[0]);
    }else{
      this.messageService.error('Uploaded file is not a valid image. Only image file are allowed');
    }
  }


/**
* Create comment
*/
  submitMessage(id){

    let singlePost = this.postedData.find(obj=>obj.id ==id);
    let singlePostIndex = this.postedData.findIndex(obj=>obj.id ==id);
    let newFormData = new FormData();
    Object.keys(this.messagePostForm.controls).forEach(key => { 
           newFormData.append(key, this.messagePostForm.get(key).value ? this.messagePostForm.get(key).value:'');
       });

    this.commonService.create(this.global.apiUrl + '/api/community/message-store/'+id, newFormData)
        .subscribe((data)=>{
          if(data.status ==200){
            this.show_message_all[singlePostIndex] =true;
            singlePost.post_mesage = data.data; 
            singlePost.post_message_count = data.data_count;   
            this.commonService.callUpdatePoint(data.user_points);        
            this.messagePostForm.reset();
            this.comntfilename = '';
          }

          if(data.status ==422){
            this.messageService.error(data.error);
          }
        }, (error)=>{});
  }

/**
* set reply comment
*/

  setReply(id, focus_id){
    this.messagePostForm.get('reply_id').setValue(id);
    document.getElementById('msg_' + focus_id).focus();
  }


/**
* set reply child comment
*/
  childSetReplyId($event){
    this.messagePostForm.get('reply_id').setValue($event.id);
    document.getElementById('msg_' + $event.post_id).focus();
  }


/**
* Submit react
*/
  submitReact(id, type){
    this.commonService.create(this.global.apiUrl + '/api/community/reaction-store/'+ id, {type: type})
        .subscribe((data)=>{
          if(data.status ==200){
            let singlePost = this.postedData.find(obj=>obj.id ==id);
            singlePost.post_reaction = data.data;
            this.commonService.callUpdatePoint(data.user_points);
            this.reactDisplay = false;
            this.messageService.success('Successfully react');
          }
        }, (error)=>{});
  }

/**
* Set post read
*/
  submitPostRead(){
    this.commonService.create(this.global.apiUrl + '/api/community/view-store', this.postReadForm.value)
        .subscribe((data)=>{
          
        }, (error)=>{});
 }

/**
* Select image file
*/
   onSelectImage(event: any){
      let image = event.target.files[0];
      this.communityPostForm.get('post_image').setValue(image);

  }

  formDataSet(form){
    this.communityPostForm.get('contest_id').setValue(this.paramcontestid);
   let formData = new FormData();
   Object.keys(form.controls).forEach(key => { 
       formData.append(key, form.get(key).value);
   });
   return formData;
 }

/**
* Submit react
*/
 showLikeModal(id){
   this.likeModal='inline';
   let singlePost = this.postedData.find(obj=>obj.id ==id);
   this.react_data.likes = singlePost.post_reaction.filter(obj=>obj.type ==1);
   this.react_data.congrats = singlePost.post_reaction.filter(obj=>obj.type ==2);
   this.react_data.loves = singlePost.post_reaction.filter(obj=>obj.type ==3);
   this.react_data.insightfuls = singlePost.post_reaction.filter(obj=>obj.type ==4);
   this.react_data.curiouss = singlePost.post_reaction.filter(obj=>obj.type ==5);

 }

/**
* Remove Comment
*/
 removePost(post_id){
   this.commonService.delete(this.global.apiUrl + '/api/community', post_id)
       .subscribe((data)=>{
         if(data.status ==200){
           let deleteIndex = this.postedData.findIndex(obj=>obj.id == post_id);
           this.postedData.splice(deleteIndex, 1);
           this.messageService.success(data.status_text);
         }
       }, (error)=>{});
 }

/**
* Reset form
*/
  setReset(){
    this.communityPostForm.get('post_desc').setValue('');
    this.communityPostForm.get('post_image').setValue('');
    this.communityPostForm.get('post_image_text').setValue('');
    this.communityPostForm.get('post_video_link').setValue('');
    this.communityPostForm.get('contest_id').setValue('');
    
  }

/**
* Check user reaction
*/
  
  checkUserAlreadyReact(data){
   return data.find(obj=>obj.user_id == this.loggedInUser.id) ? true: false;
  }

  videoSanitize(link){
    if(link){
      return this.sanitizer.bypassSecurityTrustResourceUrl(link.replace("watch?v=", "embed/"));
    }
    
  }

  updateStyle(html) {
      return this.sanitizer.bypassSecurityTrustHtml(this.linkify(html));
  }

  get_url_extension( url ) {
    let file_type = url.split(/[#?]/)[0].split('.').pop().trim();
    let flag = 0;
    switch(file_type){
      case 'png':
        flag = 1;
        break;
      case 'PNG':
        flag = 1;
        break;
      case 'JPEG':
        flag = 1;
        break;
      case 'jpeg':
        flag = 1;
        break;
      case 'JPG':
        flag = 1;
        break;
      case 'jpg':
        flag = 1;
        break;
      case 'GIF':
        flag = 1;
        break;
      case 'gif':
        flag = 1;
        break;
      default:
        flag = 0;
    }
    return flag;
  }

  loadMorePost(event){
    this.data_limit +=5;
    if(this.total_post>this.data_limit){
      this.asyncInit();
    }else{
      console.log(this.eventCount,'post end');
    }
  }

  /**
* Reset chart
*/
  resetpage(event){
    this.redirectTo(this.route.url);
  }

  redirectTo(uri) {
    this.route.navigateByUrl('/', {skipLocationChange: true}).then(() =>
    this.route.navigate([uri]));
  }

  linkify(inputText) {
    var replacedText, replacePattern1, replacePattern2, replacePattern3;

    //URLs starting with http://, https://, or ftp://
    replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
    replacedText = inputText.replace(replacePattern1, '<a href="$1" target="_blank">$1</a>');

    //URLs starting with "www." (without // before it, or it'd re-link the ones done above).
    replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
    replacedText = replacedText.replace(replacePattern2, '$1<a href="http://$2" target="_blank">$2</a>');

    //Change email addresses to mailto:: links.
    replacePattern3 = /(([a-zA-Z0-9\-\_\.])+@[a-zA-Z\_]+?(\.[a-zA-Z]{2,6})+)/gim;
    replacedText = replacedText.replace(replacePattern3, '<a href="mailto:$1">$1</a>');

    return replacedText;
  }

}
