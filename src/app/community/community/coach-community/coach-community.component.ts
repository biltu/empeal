import { Component, OnInit, HostListener, ElementRef} from '@angular/core';
import { CommonService } from '../../../services/common.service';
import { MessageService } from '../../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import {Global} from '../../../global';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-coach-community',
  templateUrl: './coach-community.component.html',
  styleUrls: ['./coach-community.component.css']
})
export class CoachCommunityComponent implements OnInit {

closeResult: string;
  postModal: any;
  likeModal: any;
  reactDisplay: boolean;
  communityPostForm: FormGroup;
  messagePostForm: FormGroup;
  reactForm: FormGroup;
  postReadForm: FormGroup;
  paramcontestid:any;
  postedData: any=[];
  memberList: any=[];
  groupList: any=[];
  contestList: any=[];
  showLikeIndex: string;
  react_data: any={
    likes:[],
    congrats:[],
    loves:[],
    insightfuls:[],
    curiouss:[]
  };
  seeGroupMem: boolean;
  show_message_all:any={};
  messageShowIndex: string;
  data_limit: number;
  eventCount: number;
  groupModel: string;
  total_post: number=0;
  contestModel: string;
  contestname: 'Community';
  searchText: string;
  loggedInUser: any={}; 
  postError: any;
  isModalOpen: boolean=false;
  comntfilename: string;
  paramId: any='';
  selected_group: any;
  memberviewid: any;
  navigationSubscription:any;

  // @HostListener("window:scroll", ["$event.target"])
  // scrollHandler(elem) {
  //   let pos =  (document.documentElement.scrollTop) + document.documentElement.clientHeight;
  //   let max = document.documentElement.scrollHeight;
  //   if(pos >=max){     
  //     this.eventCount +=1;      
  //     if(this.eventCount ==1 && this.groupModel){
  //       this.data_limit +=5;
  //       //if(this.total_post >this.data_limit){
  //         //console.log(this.total_post,'true');
  //         this.getGroupWiseDataLoadMore(this.groupModel);
  //       // }else{
  //       //   console.log(this.total_post,'false');
  //       // }
  //     }
  //   }
  // }

  constructor(private commonService: CommonService, private global: Global, private fb: FormBuilder,  private sanitizer: DomSanitizer,
    private messageService: MessageService, private eRef: ElementRef, private route: Router, private activeRoute: ActivatedRoute) {
    this.loggedInUser = JSON.parse(localStorage.getItem('user'));
    this.paramcontestid = this.activeRoute.snapshot.queryParams['contest_id'] ? this.activeRoute.snapshot.queryParams['contest_id']:'';
    this.memberviewid = this.activeRoute.snapshot.queryParams['user_id'] ? this.activeRoute.snapshot.queryParams['user_id']:'';
    this.data_limit =0;
    //this.total_post = 0;
    //console.log(this.contestModel);
    this.asyncInit();
    this.showLikeIndex = '';
    this.seeGroupMem = false;
    //this.show_message_all = false;
    this.messageShowIndex = '';
    this.eventCount =0;
    this.navigationSubscription = this.route.events.subscribe((e: any) => {
     // If it is a NavigationEnd event re-initalise the component
     if (e instanceof NavigationEnd) {
       this.asyncInit();
     }
   });
  }

  ngOnInit() {
    this.postModal = 'none';
    this.likeModal ='none';
    this.reactDisplay =false;
    this.contestModel='all';
    this.communityPostForm = this.fb.group({
      group_id: ['', [Validators.required]],
      post_desc: ['', [Validators.required]],
      post_image: [''],
      contest_id:this.paramcontestid?this.paramcontestid:0,
      post_image_text:[''],
      post_video_link: ['', [Validators.required]],     
    });

    this.messagePostForm = this.fb.group({
      messages: ['', [Validators.required]],
      reply_id: '',
      attachfile:''
    });

    this.reactForm = this.fb.group({
      type:''
    });

    this.postReadForm = this.fb.group({
      user_id:'',
      post_id:'',
      is_visit:''
    });
    console.log(this.paramcontestid);
  }

/**
* Get Community posts and comments
*/
  asyncInit(){
      if ((this.navigationSubscription && this.route.url =='/community' && !this.activeRoute.snapshot.queryParams['comment_id']) || this.activeRoute.snapshot.queryParams['id'] ) {
         let param = this.activeRoute.snapshot.queryParams['id'] ? this.activeRoute.snapshot.queryParams['id']:'';
         this.paramId = param;
         this.paramcontestid = this.activeRoute.snapshot.queryParams['contest_id'] ? this.activeRoute.snapshot.queryParams['contest_id']:'';
         this.commonService.getAll(this.global.apiUrl + '/api/community?param_id='+ param)
        .subscribe((data)=>{
            this.groupList = data.data;
            if(data.data.length){
              this.groupModel = param ? data.param_group.id :data.data[0].id;
              this.getGroupWiseData(this.groupModel, this.paramId);
            }
        }, (error)=>{});

      }else if((this.navigationSubscription && this.route.url =='/community' && !this.activeRoute.snapshot.queryParams['id']) || this.activeRoute.snapshot.queryParams['comment_id']){
        let commentId =  this.activeRoute.snapshot.queryParams['comment_id'];
        this.paramcontestid = this.activeRoute.snapshot.queryParams['contest_id'] ? this.activeRoute.snapshot.queryParams['contest_id']:'';
        this.commonService.getAll(this.global.apiUrl + '/api/community/get-message/post/' + commentId)
            .subscribe(data=>{
              let param = data.data ? data.data:'';
               this.paramId = param;
               this.paramcontestid = data.contest_id ? data.contest_id:'';
               this.commonService.getAll(this.global.apiUrl + '/api/community?param_id='+ param+ '&contest_id='+this.paramcontestid)
              .subscribe((data)=>{
                  this.groupList = data.data;
                  if(data.data.length){
                    this.groupModel = param ? data.param_group.id :data.data[0].id;
                    this.getGroupWiseData(this.groupModel, this.paramId);
                  }
            }, (error)=>{});
                },error=>{});
      }else if(this.activeRoute.snapshot.queryParams['user_id'] && this.activeRoute.snapshot.queryParams['user_id'] >0 && this.activeRoute.snapshot.queryParams['contest_id'] && this.activeRoute.snapshot.queryParams['contest_id']>0){
        this.paramcontestid = this.activeRoute.snapshot.queryParams['contest_id'] ? this.activeRoute.snapshot.queryParams['contest_id']:'';
        this.memberviewid = this.activeRoute.snapshot.queryParams['user_id'] ? this.activeRoute.snapshot.queryParams['user_id']:'';
        this.selected_group = this.activeRoute.snapshot.queryParams['group_id'] ? this.activeRoute.snapshot.queryParams['group_id']:'';
        this.commonService.getAll(this.global.apiUrl + '/api/community?contest_id='+this.paramcontestid+'&user_id='+this.memberviewid+'&id='+this.selected_group)
          .subscribe((data)=>{
              this.groupList = data.data;
              if(data.data.length){
                this.contestModel = this.paramcontestid;
                this.groupModel = this.selected_group;
                this.getGroupWiseData(this.selected_group);
              }
          }, (error)=>{});
      }else if(this.activeRoute.snapshot.queryParams['user_id'] && this.activeRoute.snapshot.queryParams['user_id'] >0){
        console.log('fffff bbbb');
        this.paramcontestid = this.activeRoute.snapshot.queryParams['contest_id'] ? this.activeRoute.snapshot.queryParams['contest_id']:'';
        this.memberviewid = this.activeRoute.snapshot.queryParams['user_id'] ? this.activeRoute.snapshot.queryParams['user_id']:'';
        this.selected_group = this.activeRoute.snapshot.queryParams['group_id'] ? this.activeRoute.snapshot.queryParams['group_id']:'';
        this.commonService.getAll(this.global.apiUrl + '/api/community?contest_id='+this.paramcontestid+'&user_id='+this.memberviewid+'&id='+this.selected_group)
          .subscribe((data)=>{
              this.groupList = data.data;
              if(data.data.length){
                this.contestModel = this.paramcontestid;
                this.groupModel = this.selected_group;
                this.getGroupWiseData(this.groupModel);
              }
          }, (error)=>{});
      }else if(this.activeRoute.snapshot.queryParams['contest_id'] && this.activeRoute.snapshot.queryParams['contest_id'] >0){
        console.log('dddd');
        this.paramcontestid = this.activeRoute.snapshot.queryParams['contest_id'] ? this.activeRoute.snapshot.queryParams['contest_id']:'';
        this.selected_group = this.activeRoute.snapshot.queryParams['group_id'] ? this.activeRoute.snapshot.queryParams['group_id']:'';
        this.commonService.getAll(this.global.apiUrl + '/api/community?contest_id='+this.paramcontestid+'&id='+this.selected_group)
          .subscribe((data)=>{
              this.groupList = data.data;
              if(data.data.length){
                this.contestModel = this.paramcontestid;
                this.groupModel = this.selected_group;
                this.getContestWiseData(this.paramcontestid);
              }
          }, (error)=>{});
      }
      else{
        if(this.route.url =='/community'){                                        
          console.log('last condition');
          this.paramcontestid = 0;
          this.memberviewid = 0;
          this.groupModel = 'all';
          this.commonService.getAll(this.global.apiUrl + '/api/community')
          .subscribe((data)=>{
              this.groupList = data.data;
              if(data.data.length){
                this.paramcontestid = 0;
                this.memberviewid = 0;
                this.groupModel = 'all';
                this.getGroupWiseData(this.groupModel);
              }
          }, (error)=>{});
        }
      }

      this.commonService.getAll(this.global.apiUrl + '/api/hc/contest/list')
        .subscribe((data)=>{
            this.contestList = data.data;
        }, (error)=>{});
  }

/**
* Get Community posts and comments group wise
*/

  getGroupWiseData(id, param_id=''){
  	this.data_limit =5;
    //this.total_post = 0;
    this.selected_group = id;
    console.log(this.memberviewid);
    this.memberviewid = this.activeRoute.snapshot.queryParams['user_id'] ? this.activeRoute.snapshot.queryParams['user_id']:'';
    //this.paramcontestid = this.activeRoute.snapshot.queryParams['contest_id'] ? this.activeRoute.snapshot.queryParams['contest_id']:'';
  	this.commonService.getAll(this.global.apiUrl + '/api/community/groupwise-data?id='+ id +'&limit=' + this.data_limit + '&param_id=' + param_id+'&user_id='+this.memberviewid+'&contest_id='+this.paramcontestid)
  		.subscribe((data)=>{
  		  this.postedData = data.data;
        this.memberList = data.member_data;
        this.total_post = data.total_post;
        this.contestname = data.contest_name.length >0?data.contest_name[0]:'Community';
        let notReadData = data.data.filter(obj=> !obj.post_read.length);
          if(notReadData.length){
            let unreadIds = notReadData.map(obj=> obj.id);
            this.commonService.create(this.global.apiUrl +'/api/community/view-store', {unread_ids: unreadIds})
              .subscribe(data=>{
                this.commonService.callUpdateData(data.data);
              },error=>{});
            }
  		}, (error)=>{});
  }

  getContestWiseData(id, param_id=''){

    this.paramcontestid = id;
    this.data_limit =5;
    this.memberviewid = this.activeRoute.snapshot.queryParams['user_id'] ? this.activeRoute.snapshot.queryParams['user_id']:'';
    this.commonService.getAll(this.global.apiUrl + '/api/community/groupwise-data?id='+this.selected_group+'&limit=' + this.data_limit + '&param_id=' + param_id+ '&contest_id='+id)
      .subscribe((data)=>{
        this.postedData = data.data;
        this.memberList = data.member_data;
        this.total_post = data.total_post;
        this.contestname = data.contest_name.length >0?data.contest_name[0]:'Community';
        let notReadData = data.data.filter(obj=> !obj.post_read.length);
          if(notReadData.length){
            let unreadIds = notReadData.map(obj=> obj.id);
            this.commonService.create(this.global.apiUrl +'/api/community/view-store', {unread_ids: unreadIds})
              .subscribe(data=>{
                this.commonService.callUpdateData(data.data);
              },error=>{});
            }
      }, (error)=>{});
  }

/**
* Get Community posts and comments group wise load more
*/
  getGroupWiseDataLoadMore(id){
    let paramId = this.activeRoute.snapshot.queryParams['id'] ? this.activeRoute.snapshot.queryParams['id']:( this.paramId?  this.paramId:'');
  	this.commonService.getAll(this.global.apiUrl + '/api/community/groupwise-data?id='+ id +'&limit=' + this.data_limit + '&param_id=' + paramId + '&contest_id='+ this.paramcontestid+ '&user_id='+ this.memberviewid)
  		.subscribe((data)=>{
  		  this.postedData = data.data;
          this.memberList = data.member_data;
          this.total_post = data.total_post;
          this.contestname = data.contest_name.length >0?data.contest_name[0]:'Community';
          this.eventCount =0;
          let notReadData = data.data.filter(obj=> !obj.post_read.length);
          if(notReadData.length){
            let unreadIds = notReadData.map(obj=> obj.id);
            this.commonService.create(this.global.apiUrl +'/api/community/view-store', {unread_ids: unreadIds})
              .subscribe(data=>{
                this.commonService.callUpdateData(data.data);
              },error=>{});
            }
  		}, (error)=>{});
  }

resetPostFilter(e){
  this.contestModel = 'all';
  this.groupModel = 'all';
  this.getGroupWiseData(this.groupModel);
}
/**
* Create new post
*/
  submitPost(){
    let formValues = this.formDataSet(this.communityPostForm);   
    this.commonService.create(this.global.apiUrl + '/api/community', formValues)
        .subscribe((data)=>{
          if(data.status == 422){
            this.postModal ='inline';
            this.postError = data.error;
          }
          if(data.status == 200){
            this.isModalOpen = false;
            this.postModal ='none';
            this.postError ='';
            this.postedData.unshift(data.data);
            this.messageService.success(data.status_text);
            //this.communityPostForm.reset();
            this.setReset();
          }
        }, (error)=>{});
  }

 setCursorToTextBox(id){
   document.getElementById('msg_' + id).focus();
 }

/**
* Create comment
*/
 submitMessage(id){
    let singlePost = this.postedData.find(obj=>obj.id ==id);
    let singlePostIndex = this.postedData.findIndex(obj=>obj.id ==id);
    let newFormData = new FormData();
    Object.keys(this.messagePostForm.controls).forEach(key => { 
           newFormData.append(key, this.messagePostForm.get(key).value ? this.messagePostForm.get(key).value:'');
       });

    this.commonService.create(this.global.apiUrl + '/api/community/message-store/'+id, newFormData)
        .subscribe((data)=>{
          if(data.status ==200){
            this.show_message_all[singlePostIndex] =true;
            singlePost.post_mesage = data.data; 
            singlePost.post_message_count = data.data_count;
            this.messagePostForm.reset();
            this.comntfilename = '';
          }

          if(data.status ==422){
            this.messageService.error(data.error);
          }

        }, (error)=>{});
  }

/**
* Set comment reply
*/
  setReply(id, focus_id){
    this.messagePostForm.get('reply_id').setValue(id);
    document.getElementById('msg_' + focus_id).focus();
  }

/**
* Set child comment reply
*/
  childSetReplyId($event){
    this.messagePostForm.get('reply_id').setValue($event.id);
    document.getElementById('msg_' + $event.post_id).focus();
  }

/**
* Submit react
*/
  submitReact(id, type){
    this.commonService.create(this.global.apiUrl + '/api/community/reaction-store/'+ id, {type: type})
        .subscribe((data)=>{
          if(data.status ==200){
          	let singlePost = this.postedData.find(obj=>obj.id ==id);
          	singlePost.post_reaction = data.data;
            this.messageService.success('Successfully react');
            this.reactDisplay = false;
          }
        }, (error)=>{});
  }

/**
* Submit post read
*/
  submitPostRead(){
    this.commonService.create(this.global.apiUrl + '/api/community/view-store', this.postReadForm.value)
      .subscribe((data)=>{
        
      }, (error)=>{});
  }

/**
* Image selection set
*/
  onSelectImage(event: any){
      let image = event.target.files[0];
      this.communityPostForm.get('post_image').setValue(image);

  }

  formDataSet(form){
   let formData = new FormData();
   Object.keys(form.controls).forEach(key => { 
       formData.append(key, form.get(key).value);
   });
   return formData;
 }
/**
* Show like modal
*/
  showLikeModal(id){
   this.likeModal='inline';
   let singlePost = this.postedData.find(obj=>obj.id ==id);
   this.react_data.likes = singlePost.post_reaction.filter(obj=>obj.type ==1);
   this.react_data.congrats = singlePost.post_reaction.filter(obj=>obj.type ==2);
   this.react_data.loves = singlePost.post_reaction.filter(obj=>obj.type ==3);
   this.react_data.insightfuls = singlePost.post_reaction.filter(obj=>obj.type ==4);
   this.react_data.curiouss = singlePost.post_reaction.filter(obj=>obj.type ==5);

 }

/**
* Remove post
*/
  removePost(post_id){
   this.commonService.delete(this.global.apiUrl + '/api/community', post_id)
       .subscribe((data)=>{
         if(data.status ==200){
           let deleteIndex = this.postedData.findIndex(obj=>obj.id == post_id);
           this.postedData.splice(deleteIndex, 1);
           this.messageService.success(data.status_text);
         }
       }, (error)=>{});
 }

/**
* Reset form value
*/
  setReset(){
    this.communityPostForm.get('group_id').setValue('');
    this.communityPostForm.get('post_desc').setValue('');
    this.communityPostForm.get('post_image').setValue('');
    this.communityPostForm.get('post_image_text').setValue('');
    this.communityPostForm.get('post_video_link').setValue('');
    this.communityPostForm.get('contest_id').setValue('');
  }

/**
* Checked user react
*/
  checkUserAlreadyReact(data){
   return data.find(obj=>obj.user_id == this.loggedInUser.id) ? true: false;
  }

/**
* File selection set
*/
  onFileSelected(event) {
   if(event.target.files.length > 0 && event.target.files[0].type === 'image/jpeg' || event.target.files[0].type === 'image/png' || event.target.files[0].type === 'image/jpg') 
    {
      this.comntfilename = event.target.files[0].name;
      this.messagePostForm.get('attachfile').setValue(event.target.files[0]);
    }else{
      this.messageService.error('Uploaded file is not a valid image. Only image file are allowed');
    }
  }

  videoSanitize(link){
    if(link){
      return this.sanitizer.bypassSecurityTrustResourceUrl(link.replace("watch?v=", "embed/"));
    }
    
  }

  updateStyle(html) {
      return this.sanitizer.bypassSecurityTrustHtml(this.linkify(html));
  }

  get_url_extension( url ) {
    let file_type = url.split(/[#?]/)[0].split('.').pop().trim();
    let flag = 0;
    switch(file_type){
      case 'png':
        flag = 1;
        break;
      case 'PNG':
        flag = 1;
        break;
      case 'JPEG':
        flag = 1;
        break;
      case 'jpeg':
        flag = 1;
        break;
      case 'JPG':
        flag = 1;
        break;
      case 'jpg':
        flag = 1;
        break;
      case 'GIF':
        flag = 1;
        break;
      case 'gif':
        flag = 1;
        break;
      default:
        flag = 0;
    }
    return flag;
  }

  loadMorePost(event){
    this.data_limit +=5;
    //if(this.total_post>this.data_limit){
      this.getGroupWiseDataLoadMore(this.groupModel);
    // }else{
    //   console.log(this.eventCount,'post end');
    // }
  }

  /**
* Reset chart
*/
  resetpage(event){
    this.redirectTo('/community');
  }

  redirectTo(uri) {
    this.route.navigateByUrl('/', {skipLocationChange: true}).then(() =>
    this.route.navigate([uri]));
  }

  linkify(inputText) {
    var replacedText, replacePattern1, replacePattern2, replacePattern3;

    //URLs starting with http://, https://, or ftp://
    replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
    replacedText = inputText.replace(replacePattern1, '<a href="$1" target="_blank">$1</a>');

    //URLs starting with "www." (without // before it, or it'd re-link the ones done above).
    replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
    replacedText = replacedText.replace(replacePattern2, '$1<a href="http://$2" target="_blank">$2</a>');

    //Change email addresses to mailto:: links.
    replacePattern3 = /(([a-zA-Z0-9\-\_\.])+@[a-zA-Z\_]+?(\.[a-zA-Z]{2,6})+)/gim;
    replacedText = replacedText.replace(replacePattern3, '<a href="mailto:$1">$1</a>');

    return replacedText;
  }

}
