import { Component, OnInit, Input,Output,EventEmitter} from '@angular/core';

@Component({
  selector: 'app-messages',
  template: `<ng-container *ngFor="let msg of msges">
  	<div class="chatholder other clear level_{{msg.level}}">
	  	<div class="chatbox">
		  	<div class="text">
			  	<span class="name">{{msg.user_name}}</span>
			  	<span class="textcontent">{{msg.messages}}
          <a href="{{msg.message_thumb_image}}" target="_blank" *ngIf="msg.message_thumb_image">
          <img src="{{msg.message_thumb_image }}" width="100" height="100" *ngIf="msg.message_thumb_image"></a>
          <a href="javascript:void(0)" (click)="sendEvent(msg.id)" class="reply"><img src="assets/images/reply2.png"></a> </span> 
		  	</div>
	  	 <span class="date">{{msg.created_at | date: 'MMMM d, y'}}</span>  	 	
	  	</div>
  		<span class="img">
      <img src="{{msg.profile_image}}" alt="dummy" *ngIf="msg.profile_image">
      <img src="assets/{{msg.avtar_image}}" alt="dummy" *ngIf="!msg.profile_image">
      </span>
  	</div>
  	<app-messages [msges]="msg.sub_cat" *ngIf="msg.sub_cat"></app-messages>
  </ng-container>`,
  //templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {

  @Input() msges;
  @Input() post_id;
  @Output() callEvent = new EventEmitter < any > ();
  constructor() { }

  ngOnInit() {
  }

  sendEvent(id){
    this.callEvent.emit({id:id, post_id: this.post_id});
  }

}
