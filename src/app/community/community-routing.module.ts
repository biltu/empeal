import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommunityComponent } from './community/community.component';
import { AuthGuard } from './../auth.guard';
import { StatutoryGuard } from './../statutory.guard';

const routes: Routes = [
	{path: '', canActivate: [AuthGuard, StatutoryGuard], component:CommunityComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommunityRoutingModule { }
