import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PermissionResolve} from './permission-resolve';
const routes: Routes = [
	{path: '', loadChildren:'./commonview/commonview.module#CommonviewModule'},
	{path: '', loadChildren:'./auth/auth.module#AuthModule'},
	{path: 'chat', loadChildren:'./chat/chat.module#ChatModule', data: { url: 'chat'},resolve: {
      permissions: PermissionResolve
    }},
	{path: 'questionnaire', loadChildren:'./questionnaire/questionnaire.module#QuestionnaireModule', data: { url: 'questionnaire'},resolve: {
      permissions: PermissionResolve
    }},
	{path:'wellness-analytics', loadChildren:'./wellness-analytics/wellness-analytics.module#WellnessAnalyticsModule'},
	{path:'appointment',loadChildren:'./appointment/appointment.module#AppointmentModule', data: { url: 'appointment'},resolve: {
      permissions: PermissionResolve
    }},
	{path:'mail',loadChildren:'./mail/mail.module#MailModule', data: { url: 'mail'},resolve: {
      permissions: PermissionResolve
    }},
	{path:'target',loadChildren:'./target/target.module#TargetModule', data: { url: 'target'},resolve: {
      permissions: PermissionResolve
    }},
	//{path:'contest',loadChildren:'./contest/contest.module#ContestModule'},
	{path:'bca',loadChildren:'./bca/bca.module#BcaModule', data: { url: 'bca'},resolve: {
      permissions: PermissionResolve
    }},
	{path:'meal',loadChildren:'./meal/meal.module#MealModule', data: { url: 'meal'},resolve: {
      permissions: PermissionResolve
    }},
	{path:'activity',loadChildren:'./activity/activity.module#ActivityModule', data: { url: 'activity'},resolve: {
      permissions: PermissionResolve
    }},
	{path:'content',loadChildren:'./content/content.module#ContentModule', data: { url: 'content'},resolve: {
      permissions: PermissionResolve
    }},
	{path:'admin',loadChildren:'./admin/admin.module#AdminModule', data: { url: 'admin'},resolve: {
      permissions: PermissionResolve
    }},
	{path:'community', loadChildren:'./community/community.module#CommunityModule', data: { url: 'community'},resolve: {
      permissions: PermissionResolve
    }},
  {path:'adhoc-quiz',loadChildren:'./adhoc-quiz/adhoc-quiz.module#AdhocQuizModule', data: { url: 'adhoc-quiz'},resolve: {
      permissions: PermissionResolve
    }},
	//{path: '', redirectTo: '', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
