import { Injectable } from "@angular/core";
import { environment } from "./../environments/environment";
@Injectable()
export class Global {
  // apiUrl = "http://localhost/emepal-laravel";
  // imageUrl = "http://localhost/emepal-laravel/public";
  // //node_server_url = "http://localhost:3000";
  // domain_url = "http://localhost:4200";

  // //domain_url = 'https://abp.windofchange.ie';
  // node_server_url = 'https://abp.windofchange.ie:4443';
  //apiUrl = 'https://abp.windofchange.ie/woc-v2/wocapi'
  //imageUrl = 'https://abp.windofchange.ie/woc-v2/wocapi/public';

  //domain_url ='https://app.windofchange.ie';
  //node_server_url = 'https://abp.windofchange.ie:3443';
  //apiUrl = 'https://app.windofchange.ie/woc-v2/wocapi'
  //imageUrl='https://app.windofchange.ie/woc-v2/wocapi/public';

  // apiUrl = "http://localhost/emepal-laravel";
  // imageUrl = "http://localhost/emepal-laravel/public";
  // node_server_url = "https://app.windofchange.ie:4443";
  // domain_url = "http://localhost:4200";

  //domain_url = 'https://abp.windofchange.ie';
  //node_server_url = 'https://abp.windofchange.ie:4443';
  //apiUrl = 'https://abp.windofchange.ie/woc-v2/wocapi';
  //imageUrl = 'https://abp.windofchange.ie/woc-v2/wocapi/public';

  // domain_url = "https://app.windofchange.ie";
  // node_server_url = "https://abp.windofchange.ie:3443";
  // apiUrl = "https://app.windofchange.ie/woc-v2/wocapi";
  // imageUrl = "https://app.windofchange.ie/woc-v2/wocapi/public";

  domain_url = environment.domain_url;
  node_server_url = environment.node_server_url;
  apiUrl = environment.apiUrl;
  imageUrl = environment.imageUrl;
}
