import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../services/message.service';

@Component({
  selector: 'app-bca',
  templateUrl: './bca.component.html',
  styleUrls: ['./bca.component.css']
})
export class BcaComponent implements OnInit {
	appointments: any=[];
	bcaarchive: any=[];
	bcadata: any=[];
  bcacount: any;
  archiveModel: string ='';
  date:any;
  archives: any =[];
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService, private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) { 
  	this.asyncInit();
  }

  ngOnInit() {
  }

/**
* Get BCA reports by date
*/
  asyncInit(){
    this.date = '';
  	this.commonService.getAll(this.global.apiUrl + '/api/user/bca/report?date='+this.date)
  		.subscribe((data)=>{
  			this.bcadata = data.data;
        this.bcacount = data.bcacount;
        this.archives = data.archive;
        this.archiveModel = this.archives[0].date;
  		}, (error)=>{});
  }

/**
* Get archive BCA report by date
*/
  getArchiveByDate(date){
    this.date = date;
    this.commonService.getAll(this.global.apiUrl + '/api/user/bca/report?date='+this.date)
      .subscribe((data)=>{
        this.bcadata = data.data;
        this.bcacount = data.bcacount;
        this.archives = data.archive;
      }, (error)=>{});
  }

}
