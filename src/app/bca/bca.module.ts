import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CoreModule} from '../core/core.module';
import { BcaComponent } from './bca/bca.component';
import {BcaRoutingModule} from './bca-routing.module';
@NgModule({
  declarations: [BcaComponent],
  imports: [
    CommonModule,
    BcaRoutingModule,
    CoreModule
  ]
})
export class BcaModule { }
