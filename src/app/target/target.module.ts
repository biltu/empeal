import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CoreModule} from '../core/core.module';
import {TargetRoutingModule} from './target-routing.module';
import { SetTargetComponent } from './set-target/set-target.component';
import { TargetListComponent } from './target-list/target-list.component';
import { TargetHistoryComponent } from './target-history/target-history.component';

@NgModule({
  declarations: [SetTargetComponent, TargetListComponent, TargetHistoryComponent],
  imports: [
    CommonModule,
    TargetRoutingModule,
    CoreModule
  ]
})
export class TargetModule { }
