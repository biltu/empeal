import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../services/message.service';

@Component({
  selector: 'app-target-list',
  templateUrl: './target-list.component.html',
  styleUrls: ['./target-list.component.css']
})
export class TargetListComponent implements OnInit {
	targets: any=[];
  sortByField: string;
  reverse: boolean =false;
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService, private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) { 
    this.asyncInit();
  }

  ngOnInit() {
  }

/**
* Get target list
*/
  asyncInit(){
  	this.commonService.getAll(this.global.apiUrl + '/api/coach/target/list')
  		.subscribe((data)=>{
        this.targets = data.data;
  		}, (error)=>{});
  }

}
