import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../services/message.service';
@Component({
  selector: 'app-set-target',
  templateUrl: './set-target.component.html',
  styleUrls: ['./set-target.component.css']
})
export class SetTargetComponent implements OnInit {
  category_id: string;
  group_id:string;
  targets: any=[];
  groups: any=[];
  categories: any = [];
  contests: any = [];
  contest_id: string;
  
  checkedModel: any={};
  AddForm: FormGroup;
  categoryModel: string ='';
  groupModel: string ='';
  today: any;
  users: any=[];
  appointments: any=[];
  validationmsg: any;
  user_role: any = {};
    user_data: any = {};
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService, private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) { 
    this.asyncInit();
    this.today = new Date;

     this.user_data = this.authService.isUserLoginIn ? JSON.parse(localStorage.getItem('user')) : '';
    this.user_role = this.user_data ? this.user_data.role : '';
  }

  ngOnInit() {
    this.AddForm = this.fb.group({
        groupid:[''],
        categoryid:[''],
        user_id:[''],
        contest_id:[''],
        start_date:[''],
        end_date:[''],
        target:[''],
        duration:['']
      });
    this.commonService.currentAppointment.subscribe((data)=>{
      this.appointments = data.appointments;
    });
  }

/**
* get groups and category
*/
  asyncInit(){
  	this.commonService.getAll(this.global.apiUrl + '/api/coach/target/set')
  		.subscribe((data)=>{
        this.groups = data.groups;
        this.categories = data.categories;
        this.contests = data.contests;
        console.log(this.contests);
  		}, (error)=>{});
  	}

/**
* Get target by category
*/
    getTargetByCat(value){
      this.category_id = value;
      this.commonService.getAll(this.global.apiUrl + '/api/coach/target/set?category_id='+ value +'&group_id=' + this.groupModel)
      .subscribe((data)=>{
        this.targets = data.data;
        this.groups = data.groups;
        this.categories = data.categories;
        this.users = data.users;

      }, (error)=>{});
    }

/**
* Get target by group
*/
    getTargetByGrp(value){
      this.group_id = value;
      this.commonService.getAll(this.global.apiUrl + '/api/coach/target/set?category_id='+ this.categoryModel +'&group_id=' + value)
      .subscribe((data)=>{
        this.targets = data.data;
        this.groups = data.groups;
        this.users = data.users;
      }, (error)=>{});
    }
  
    contestUser(event) {
    event.preventDefault();
    console.log(event.target.value);
    if (event.target.value > 0) {
      this.commonService.getAll(this.global.apiUrl + '/api/contest/useres?id=' + event.target.value)
        .subscribe((data) => {
          this.users = data.data;
        }, (error) => { });
    }
    }
  

/**
* submit target
*/
    submitTarget(){
      this.AddForm.value.groupid =this.group_id;
      this.AddForm.value.categoryid =this.category_id;
      this.commonService.create(this.global.apiUrl + '/api/coach/target/set',this.AddForm.value)
        .subscribe((data)=>{
          if(data.status ===200){
            this.validationmsg = '';
            this.message.success(data.status_text);
            this.router.navigate(['target/list']);
          }
          if(data.status ==422){
                this.validationmsg = data.error;
            }
        },(error)=>{});
    }

}
