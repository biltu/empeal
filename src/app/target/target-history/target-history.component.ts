import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../services/message.service';

@Component({
  selector: 'app-target-history',
  templateUrl: './target-history.component.html',
  styleUrls: ['./target-history.component.css']
})
export class TargetHistoryComponent implements OnInit {
	targets:any=[];
	target_id:string;
  sortByField: string;
  reverse: boolean =false;
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService, private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) { 
    this.target_id = this.activeRoute.snapshot.paramMap.get("id");
    this.asyncInit();
  }

  ngOnInit() {
  }

/**
* Get history by target
*/
  asyncInit(){
  	this.commonService.getAll(this.global.apiUrl + '/api/coach/target/history?target_id='+this.target_id)
  		.subscribe((data)=>{
        this.targets = data.data;
  		}, (error)=>{});
  }

}
