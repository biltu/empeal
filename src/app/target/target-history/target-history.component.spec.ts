import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TargetHistoryComponent } from './target-history.component';

describe('TargetHistoryComponent', () => {
  let component: TargetHistoryComponent;
  let fixture: ComponentFixture<TargetHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TargetHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TargetHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
