import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './../auth.guard';
import { EmployeeGuard } from './../employee.guard';
import { CoachGuard } from './../coach.guard';
import { SetTargetComponent } from './set-target/set-target.component';
import { TargetListComponent } from './target-list/target-list.component';
import { TargetHistoryComponent } from './target-history/target-history.component';
const routes: Routes = [
	{path:'set-target', canActivate: [AuthGuard], component: SetTargetComponent},
	{path:'list', canActivate: [AuthGuard], component: TargetListComponent},
	{path:'history/:id', canActivate: [AuthGuard], component: TargetHistoryComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TargetRoutingModule { }
