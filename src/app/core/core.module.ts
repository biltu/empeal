import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { RequestInterceptor } from "./../RequestInterceptor";

import { HeaderComponent } from ".././includes/header/header.component";
import { FooterComponent } from ".././includes/footer/footer.component";
import { NavigationComponent } from ".././includes/navigation/navigation.component";
import { HasAppointmentComponent } from ".././includes/has-appointment/has-appointment.component";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { DateInputDirective } from "./../directive/date-input.directive";
import { InfiniteScrollDirective } from "./../directive/infinite-scroll.directive";
import { NgxSummernoteModule } from "ngx-summernote";
import { SearchPipe } from "../filter/search.pipe";
import { SearchPostPipe } from "../filter/search-post.pipe";
import { ChatSearchPipe } from "../filter/chat-search.pipe";
import { ObjectSearchPipe } from "../filter/object-search.pipe";
import { GroupSearchPipe } from "../filter/group-search.pipe";
import { DataTableOrderByPipe } from "../filter/sort.pipe";
import { ChartsModule } from "ng2-charts";
import { NgApexchartsModule } from "ng-apexcharts";
import { CookieService } from "ngx-cookie-service";

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    HasAppointmentComponent,
    NavigationComponent,
    DateInputDirective,
    InfiniteScrollDirective,
    SearchPipe,
    SearchPostPipe,
    ChatSearchPipe,
    ObjectSearchPipe,
    GroupSearchPipe,
    DataTableOrderByPipe,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
    RouterModule,
    NgxSummernoteModule,
    ChartsModule,
    NgApexchartsModule,
  ],
  exports: [
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    HeaderComponent,
    FooterComponent,
    HasAppointmentComponent,
    NavigationComponent,
    DateInputDirective,
    NgbModule,
    RouterModule,
    NgxSummernoteModule,
    SearchPipe,
    SearchPostPipe,
    ChatSearchPipe,
    ObjectSearchPipe,
    GroupSearchPipe,
    ChartsModule,
    NgApexchartsModule,
    DataTableOrderByPipe,
  ],
  providers: [
    CookieService,
    { provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true },
  ],
})
export class CoreModule {}
