import { Injectable } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse,
} from '@angular/common/http';
import { NgxUiLoaderService } from 'ngx-ui-loader';

import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import {Router, Event, NavigationStart} from '@angular/router';
import {Global} from './global';
@Injectable()
export class RequestInterceptor implements HttpInterceptor {
  exceptionLoding: boolean;
  constructor(private global: Global, private router: Router, private ngxService: NgxUiLoaderService) {
    this.exceptionLoding = false;
  
  }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.exceptionUrl(req.url);
    if(!this.exceptionLoding){
      this.ngxService.start(); 
    }
   
    return next.handle(req).pipe(tap(evt => {
      if (evt instanceof HttpResponse) {
         //unread-nofification
         this.ngxService.stop();
      }
    }, err => {
      if (err instanceof HttpErrorResponse) {
        this.ngxService.stop();
        if (err.error.error === 'token_invalid' || err.error.error === 'token_expired') {

        }

        if(err.status == 401 && err.statusText=="OK" && err.error.message =="Unauthenticated."){
          localStorage.removeItem('token');
           localStorage.removeItem('user');
           localStorage.removeItem('userpushtoken');
           localStorage.removeItem('conversationId');
           localStorage.removeItem('teamid');
           this.router.navigate(['login']);
        }

        if(err.status == 500 &&  err.error.message.indexOf('Permission denied') !== -1 ){
         localStorage.removeItem('token');
         localStorage.removeItem('user');
         localStorage.removeItem('userpushtoken');
         localStorage.removeItem('conversationId');
         localStorage.removeItem('teamid');
         this.router.navigate(['login']);
        }
      }
    }));

  }

  exceptionUrl(reqUrl): void {
    const apiUrl = this.global.apiUrl;
    if (reqUrl === apiUrl + '/api/user/unread-nofification') {
      this.exceptionLoding = true;
    } 
    else if (reqUrl === apiUrl + '/api/booked/schedules') {
      this.exceptionLoding = true;
    }else {
      this.exceptionLoding = false;
    }

  }

}