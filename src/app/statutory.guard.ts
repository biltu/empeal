import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class StatutoryGuard implements CanActivate {

  constructor(private router: Router, private authService: AuthService){
    
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
  	if(this.authService.getUserDetail() && this.authService.getUserDetail().role.slug !='employee'){
  		return true;
  	}

    else if(this.authService.getUserDetail() && this.authService.getUserDetail().user_statutatory && this.authService.getUserDetail().user_statutatory.is_agree ==1){
    	//alert(this.user_data.user_statutatory.is_agree);

      return true;
    }else{
      //alert(this.user_data.user_statutatory.is_agree);
      this.router.navigate(['/edit-statutory-info']);
      return false;
    }
    
  }
}
