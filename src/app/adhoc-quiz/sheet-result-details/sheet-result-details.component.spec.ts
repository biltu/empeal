import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SheetResultDetailsComponent } from './sheet-result-details.component';

describe('SheetResultDetailsComponent', () => {
  let component: SheetResultDetailsComponent;
  let fixture: ComponentFixture<SheetResultDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SheetResultDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SheetResultDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
