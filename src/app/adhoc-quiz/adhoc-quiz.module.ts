import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CoreModule} from '../core/core.module';
import {AdhocQuizRoutingModule} from './adhoc-quiz-routing.module';
import { QuestionListComponent } from './question-list/question-list.component';
import { QuestionAddComponent } from './question-add/question-add.component';
import { QuizSheetsComponent } from './quiz-sheets/quiz-sheets.component';
import { QuizSheetResultComponent } from './quiz-sheet-result/quiz-sheet-result.component';
import { SheetResultDetailsComponent } from './sheet-result-details/sheet-result-details.component';
import { SheetAssignDetailsComponent } from './sheet-assign-details/sheet-assign-details.component';
@NgModule({
  declarations: [QuestionListComponent, QuestionAddComponent, QuizSheetsComponent, QuizSheetResultComponent, SheetResultDetailsComponent, SheetAssignDetailsComponent],
  imports: [
    CommonModule,
    AdhocQuizRoutingModule,
    CoreModule
  ]
})
export class AdhocQuizModule { 

}
