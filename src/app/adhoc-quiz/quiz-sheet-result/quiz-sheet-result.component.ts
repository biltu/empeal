import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../services/message.service';

@Component({
  selector: 'app-quiz-sheet-result',
  templateUrl: './quiz-sheet-result.component.html',
  styleUrls: ['./quiz-sheet-result.component.css']
})
export class QuizSheetResultComponent implements OnInit {
	adhocsheetresults: any=[];
  sheet_id:any;
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService, private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) { 
 	  this.sheet_id = this.activeRoute.snapshot.paramMap.get("id")?this.activeRoute.snapshot.paramMap.get("id"):'';
    this.asyncInit();
  }

  ngOnInit() {
  }

  /**
* Get question sheet list
*/
  asyncInit(){
  	this.commonService.getAll(this.global.apiUrl + '/api/adhoc-quiz/sheet/results/'+this.sheet_id)
  		.subscribe((data)=>{
        this.adhocsheetresults = data.data;
  		}, (error)=>{});
  }
}
