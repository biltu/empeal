import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuizSheetResultComponent } from './quiz-sheet-result.component';

describe('QuizSheetResultComponent', () => {
  let component: QuizSheetResultComponent;
  let fixture: ComponentFixture<QuizSheetResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizSheetResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizSheetResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
