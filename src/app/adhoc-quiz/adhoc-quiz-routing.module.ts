import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './../auth.guard';
import { EmployeeGuard } from './../employee.guard';
import { AdminGuard } from './../admin.guard';
import { QuestionListComponent } from './question-list/question-list.component';
import { QuestionAddComponent } from './question-add/question-add.component';
import { QuizSheetsComponent } from './quiz-sheets/quiz-sheets.component';
import { QuizSheetResultComponent } from './quiz-sheet-result/quiz-sheet-result.component';
import { SheetResultDetailsComponent } from './sheet-result-details/sheet-result-details.component';
import { SheetAssignDetailsComponent } from './sheet-assign-details/sheet-assign-details.component';

const routes: Routes = [
	{path:'questions', canActivate: [AuthGuard,AdminGuard], component: QuestionListComponent},
	{path:'question/add', canActivate: [AuthGuard,AdminGuard], component: QuestionAddComponent},
	{path:'question/edit/:id', canActivate: [AuthGuard,AdminGuard], component: QuestionAddComponent},
	{path:'quiz-sheets', canActivate: [AuthGuard,AdminGuard], component: QuizSheetsComponent},
	{path:'quiz-sheet/results/:id', canActivate: [AuthGuard,AdminGuard], component: QuizSheetResultComponent},
	{path:'sheet/result/details/:id', canActivate: [AuthGuard,AdminGuard], component: SheetResultDetailsComponent},
	{path:'sheet/assigned/details/:id', canActivate: [AuthGuard,AdminGuard], component: SheetAssignDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdhocQuizRoutingModule { }
