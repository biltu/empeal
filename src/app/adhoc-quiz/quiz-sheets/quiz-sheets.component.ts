import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../services/message.service';

@Component({
  selector: 'app-quiz-sheets',
  templateUrl: './quiz-sheets.component.html',
  styleUrls: ['./quiz-sheets.component.css']
})
export class QuizSheetsComponent implements OnInit {
  adhocsheets: any=[];
  sheetAssignForm: FormGroup;
  sheetAssignValidation: any=[];
  reverse: boolean =false;
  isModalOpen: boolean;
  sheetAssignModal: string;
  totalsheet: number;
  sheetid: number;
  groups: any=[];
  levels: any=[];
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService, private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) { 
 	this.asyncInit();
 	this.sheetAssignModal = 'none';
    this.isModalOpen = false;
  }

  ngOnInit() {
  	this.sheetAssignForm = this.fb.group({
      group_id:['', [Validators.required]],
      level_id:[],
      sheet_id:''
    });
  }

/**
* Get question sheet list
*/
  asyncInit(){
  	this.commonService.getAll(this.global.apiUrl + '/api/adhoc-quiz/question-sheets')
  		.subscribe((data)=>{
        this.adhocsheets = data.data;
  		}, (error)=>{});

  	this.commonService.getAll(this.global.apiUrl + '/api/group')
  		.subscribe((data)=>{
        this.groups = data.data;
  		}, (error)=>{});

  	this.commonService.getAll(this.global.apiUrl + '/api/levels')
  		.subscribe((data)=>{
        this.levels = data.data;
  		}, (error)=>{});
  }

  sheetAssignModalClose(){
  	this.sheetAssignModal='none';
    this.isModalOpen = false;
  }

  openSheetAssignDialog(id){
  	this.sheetAssignForm.patchValue({
        sheet_id:id,
        group_id: '',
        level_id: ''
      });
    this.sheetAssignModal='block';
    this.isModalOpen = true;
  }

  sheetAssignSave(){
  	this.commonService.create(this.global.apiUrl + '/api/adhoc-quiz/sheet/assign',this.sheetAssignForm.value)
        .subscribe((data)=>{
          if(data.status == 422){
            this.sheetAssignValidation = data.error;
          }
          if(data.status ==200){
            this.message.success(data.status_text);
            this.sheetAssignModal='none';
            this.redirectTo(this.router.url);
            this.isModalOpen = false;
          }

          if(data.status ==400){
            this.message.error(data.status_text);
          }
        }, (error)=>{
      });
  }

  redirectTo(uri) {
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() =>
    this.router.navigate([uri]));
  }

}
