import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuizSheetsComponent } from './quiz-sheets.component';

describe('QuizSheetsComponent', () => {
  let component: QuizSheetsComponent;
  let fixture: ComponentFixture<QuizSheetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizSheetsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizSheetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
