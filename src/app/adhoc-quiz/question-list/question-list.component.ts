import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../services/message.service';

@Component({
  selector: 'app-question-list',
  templateUrl: './question-list.component.html',
  styleUrls: ['./question-list.component.css']
})
export class QuestionListComponent implements OnInit {
	adhocquestions: any=[];
  adhocsheets: any=[];
	sortByField: string;
  sheetModel: string;
  totalsheet: number;
  sheetAddModal: any;
  sheetAddForm: FormGroup;
	reverse: boolean =false;
  isModalOpen: boolean;
  sheetAddValidation: any;
  checkedList:any=[];
  masterSelected:boolean;
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService, private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) { 
  	this.sheetAddModal = 'none';
    this.isModalOpen = false;
    this.asyncInit();
  }

  ngOnInit() {
    this.sheetAddForm = this.fb.group({
      sheet_name:['', [Validators.required]],
      questionids:[],
      status: ['', [Validators.required]],
      sheet_type: ''

    });

    this.commonService.getAll(this.global.apiUrl + '/api/adhoc-quiz/question-sheets')
      .subscribe((data)=>{
        this.adhocsheets = data.data;
        this.totalsheet = data.count;
        console.log(this.adhocsheets);
      }, (error)=>{});
  }

/**
* Get question list
*/
  asyncInit(){
  	this.commonService.getAll(this.global.apiUrl + '/api/adhoc-quiz/questions')
  		.subscribe((data)=>{
        this.adhocquestions = data.data;
  		}, (error)=>{});
  }

  openSheetAddDialog(){
    this.sheetAddModal='block';
    this.isModalOpen = true;
    this.setusergroupupdateForm();
  }

  sheetModalClose(){
    this.sheetAddModal='none';
    this.isModalOpen = false;
    //this.setusergroupupdateForm();
  }

  setusergroupupdateForm(){
      this.sheetAddForm.patchValue({
        questionids:this.checkedList,
        sheet_name: '',
        status: '',
        sheet_type: ''
      });
  }

  sheetSave(){
    let formdata = this.sheetAddForm.value;
    if(this.sheetAddForm.get('questionids').value.length <= 0){
      this.message.error('please select question from question list');
      return false;
    }
    this.commonService.create(this.global.apiUrl + '/api/adhoc-quiz/sheet/add',formdata)
        .subscribe((data)=>{
          if(data.status == 422){
            this.sheetAddValidation = data.error;
          }
          if(data.status ==200){
            this.message.success(data.status_text);
            this.sheetAddModal='none';
            this.redirectTo(this.router.url);
            this.isModalOpen = false;
          }

          if(data.status ==400){
            this.message.error(data.status_text);
          }
        }, (error)=>{
      });
  }

  checkUncheckAll() {
    for (var i = 0; i < this.adhocquestions.length; i++) {
      this.adhocquestions[i].isSelected = this.masterSelected;
    }
    this.getCheckedItemList();
  }
  isAllSelected() {
    this.masterSelected = this.adhocquestions.every(function(adhocquestions:any) {
        return adhocquestions.isSelected == true;
      })
    this.getCheckedItemList();
  }
 
  getCheckedItemList(){
    this.checkedList = [];
    for (var i = 0; i < this.adhocquestions.length; i++) {
      if(this.adhocquestions[i].isSelected)
      this.checkedList.push(this.adhocquestions[i].id);
    }
  }

  getQuestionBySheet(value){
    this.commonService.getAll(this.global.apiUrl + '/api/adhoc-quiz/sheet/questions/'+value)
      .subscribe((data)=>{
        this.adhocquestions = data.data;
      }, (error)=>{});
  }

  redirectTo(uri) {
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() =>
    this.router.navigate([uri]));
  }
  
}
