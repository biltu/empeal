import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SheetAssignDetailsComponent } from './sheet-assign-details.component';

describe('SheetAssignDetailsComponent', () => {
  let component: SheetAssignDetailsComponent;
  let fixture: ComponentFixture<SheetAssignDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SheetAssignDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SheetAssignDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
