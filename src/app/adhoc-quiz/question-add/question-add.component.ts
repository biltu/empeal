import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../services/message.service';

@Component({
  selector: 'app-question-add',
  templateUrl: './question-add.component.html',
  styleUrls: ['./question-add.component.css']
})
export class QuestionAddComponent implements OnInit {
	options: FormArray;
	AddForm: FormGroup;
	question_id: any;
	questiondata: any=[];
	questionoptions: any=[];
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService, private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) { 
  	this.question_id = this.activeRoute.snapshot.paramMap.get("id")?this.activeRoute.snapshot.paramMap.get("id"):'';
  	this.asyncInit();
  }

	ngOnInit() {
	  	this.AddForm = this.fb.group({
	  		question:[''],
	  		options: this.fb.array([this.createOptionst()]),
	  		status:['']
	  	});
	}

	asyncInit(){
	    if(this.question_id){
	      this.commonService.getAll(this.global.apiUrl + '/api/adhoc-quiz/questions/' + this.question_id)
	        .subscribe((data)=>{
	          this.questiondata = data.data;
	          this.questionoptions = data.data.options;
	            let getoptions =this.questionoptions.map(option=> this.fb.group({'oid':option.id,'option':option.option_label, 'point':option.point, 'status':option.status, 'is_correct':option.is_correct==1?true:''}));
	  			let getoptionsArray = this.fb.array(getoptions);
	  			console.log(getoptionsArray);
	          	this.AddForm.setControl('options', getoptionsArray);
	          	this.setForm();
	        }, (error)=>{}); 
	      }else{
	      
	    }
	}

	/**
	* Set question form
	*/
  	setForm(){
	    this.AddForm.patchValue({
	      question:this.questiondata.question,
	      status:this.questiondata.status,
	  	})
  	}

    createOptionst(): FormGroup{
	  	return this.fb.group({
	  		option:[''],
	  		point:[''],
	  		is_correct:[''],
	  		status:[''],
	  		oid:[''],
	  	});
  	}

  	addOptionArray(): void {
  		this.options = this.AddForm.get('options') as FormArray;
  		this.options.push(this.createOptionst());
  	}

  	deleteOptionArray(index: number){
  		this.options = this.AddForm.get('options') as FormArray;
  		this.options.removeAt(index);
  	}
  	get formData() { return <FormArray>this.AddForm.get('options')['controls']; }
	get quiz_f(){return this.AddForm.controls;}

  	submitQuestion(){
  		this.commonService.create(this.global.apiUrl + '/api/adhoc-quiz/question',this.AddForm.value)
	      .subscribe((data)=>{
	      	console.log(data);
	        if(data.status ===200){
	          this.message.success(data.status_text);
	          this.router.navigate(['adhoc-quiz/questions']);
	        }
	      },(error)=>{});
  	}

	/**
	* Update Question
	*/
  	updateQuestion(){
	    this.commonService.update(this.global.apiUrl + '/api/adhoc-quiz/questions',this.question_id,this.AddForm.value)
	        .subscribe((data)=>{
	          if(data.status ==200){
	            this.message.success(data.status_text);
	            this.router.navigate(['adhoc-quiz/questions']);
	          }
	        }, (error)=>{});
	  	}

}
