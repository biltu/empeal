import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { filter} from 'rxjs/operators';
import { AuthService } from './services/auth.service';
import { Router, NavigationEnd, NavigationStart, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Global } from './global';
import { CommonService } from './services/common.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  previousUrl: string;
  nextUrl: string;
  isEmployer: any;
  permissions: any= []; 
  stateUrl: String;
  constructor(private authService: AuthService, private router: Router, private global: Global,
   private commonservice: CommonService, private http: HttpClient) {
    this.nextUrl = this.router.url;
   
    router.events.pipe(filter((event: any) => event instanceof NavigationEnd))
      .subscribe((e: any) => {    
        this.previousUrl = this.nextUrl;
         localStorage.setItem('previousUrl', this.previousUrl);
         this.nextUrl = e.url;
      });
      router.events.pipe(filter((event: any) => event instanceof NavigationStart))
      .subscribe((e: any) => {    
        localStorage.setItem('currentUrl', e.url);
      });
      

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    this.stateUrl = state.root.children[0].data['url'];


    if (this.authService.isUserLoginIn()) {
        return true;    
    }
      this.router.navigate(['/login']);
      return false;
  }


}
