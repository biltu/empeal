import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { CommonService } from './services/common.service';
import { Global } from './global';

@Injectable()
export class PermissionResolve implements Resolve<any> {
  user_role: any={};
  loginUser: any={};
  constructor(private commonService: CommonService,  private global: Global, private router: Router) {
    this.loginUser = JSON.parse(localStorage.getItem('user'));
    this.user_role = this.loginUser.role.slug ? this.loginUser.role.slug:'';
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let stateUrl = state.root.children[0].data['url'];
    return this.commonService.getAll(this.global.apiUrl + '/api/user/permission/modules').subscribe(data=>{
      let permissions = data.data.map(obj=>obj.name);
      let subpermissions = data.data.map(obj=>obj.sub_module).filter(function (el) {
              return el != null;
            });
      let lastsegmnt = window.location.href.split("/").pop().toUpperCase();
      if(stateUrl == 'questionnaire' && permissions.indexOf(stateUrl) != -1){
        if(subpermissions.indexOf(lastsegmnt) != -1 || lastsegmnt=='QUESTIONNAIRE' || lastsegmnt=='QUESTION' || this.user_role=='superadmin'|| this.user_role=='admin' || this.user_role=='subcoach'  ){ 
          return true;
        }else{
          return this.router.navigate(['/dashboard']);
        }
      }else{
        if(permissions.indexOf(stateUrl) != -1 || this.user_role=='superadmin'|| this.user_role=='admin' || this.user_role=='subcoach'){
          return true;
        }else{
          return this.router.navigate(['/dashboard']);
        }
      }
    });
  }
}