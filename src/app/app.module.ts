import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {Global} from './global';
import {MainService} from './services/main.service';
import {CommonService} from './services/common.service';
import {AuthService} from './services/auth.service';
import {MessageService} from './services/message.service';
import {WebsocketService} from './services/websocket.service';
import {WebsocketUserService} from './services/websocket-user.service';
import {ChatService} from './services/chat.service';

import {AuthGuard} from './auth.guard';
import {EmployeeGuard} from './employee.guard';
import {CorporateGuard} from './corporate.guard';
import {CoachGuard} from './coach.guard';
// import { SecondaryCoachGuard } from './secondary-coach.guard';

import {StatutoryGuard} from './statutory.guard';

import {CoreModule} from './core/core.module';
import { NgxUiLoaderModule } from  'ngx-ui-loader';
import { TimeInputDirective } from './directive/time-input.directive';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { AsyncPipe } from '../../node_modules/@angular/common';
import { MessagingService } from './services/messaging.service';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { DeviceDetectorModule } from 'ngx-device-detector';
import {PermissionResolve} from './permission-resolve';
@NgModule({
  declarations: [
    AppComponent,
    TimeInputDirective    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CoreModule,
    NgxUiLoaderModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireMessagingModule,
    AngularFireModule.initializeApp(environment.firebase),
    DeviceDetectorModule.forRoot()
  ],
  providers: [
    Global,
    MainService,
    CommonService,
    AuthService,
    MessageService,
    WebsocketService,
    WebsocketUserService,
    ChatService,
    AuthGuard,
    EmployeeGuard,
    CorporateGuard,
    CoachGuard,
    StatutoryGuard,
    MessagingService,
    AsyncPipe,
    PermissionResolve,
    // SecondaryCoachGuard,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
