import { Component, OnInit } from "@angular/core";
import { Global } from "../../global";
import { AuthService } from "../../services/auth.service";
import { CommonService } from "../../services/common.service";
import { MessageService } from "../../services/message.service";
import {
  Router,
  ActivatedRoute,
  NavigationEnd,
  NavigationStart,
} from "@angular/router";
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators,
} from "@angular/forms";
@Component({
  selector: "app-attachments",
  templateUrl: "./attachments.component.html",
  styleUrls: ["./attachments.component.css"],
})
export class AttachmentsComponent implements OnInit {
  appointments: any = [];
  user_data: any = {};
  notes: any = [];
  noteModal: any;
  groupModel: string = "";
  groupid: Number;
  validationError: any;
  isModalOpen: boolean = false;
  noteAssignModal: any;
  groupList: any = [];
  users: any = [];
  NoteAdd: FormGroup;
  NoteAssignfrm: FormGroup;
  searchForm: FormGroup;
  user_role: any = {};
  today: any;
  constructor(
    private messageService: MessageService,
    private fb: FormBuilder,
    private global: Global,
    private authService: AuthService,
    private router: Router,
    private commonService: CommonService,
    private activeRoute: ActivatedRoute
  ) {
    this.user_data = localStorage.getItem("user")
      ? JSON.parse(localStorage.getItem("user"))
      : "";
    this.user_role = this.user_data ? this.user_data.role : "";
    //this.today = new Date;
    this.asyncInit();
  }

  ngOnInit() {
    this.commonService.currentAppointment.subscribe((data) => {
      this.appointments = data.appointments;
    });
    this.noteModal = "none";
    this.noteAssignModal = "none";
    this.NoteAdd = this.fb.group({
      group_id: [""],
      note_title: [""],
      note_date: [""],
      attachfile: "",
      status: 1,
      user_id: [""],
    });
    this.NoteAssignfrm = this.fb.group({
      group_id: [""],
      user_id: [""],
      note_id: [""],
    });
    this.searchForm = this.fb.group({
      group_id: [""],
      user_id: [""],
      from_date: [""],
      to_date: [""],
    });
  }

  /**
   * Get health coach notes
   */
  asyncInit() {
    let search_cont = "";
    if (this.activeRoute.snapshot.queryParams) {
      search_cont += this.activeRoute.snapshot.queryParams["group_id"]
        ? "group_id=" + this.activeRoute.snapshot.queryParams["group_id"]
        : "";
      search_cont += this.activeRoute.snapshot.queryParams["user_id"]
        ? "user_id=" + this.activeRoute.snapshot.queryParams["user_id"]
        : "";
    }

    this.commonService
      .getAll(this.global.apiUrl + "/api/hc/notes?" + search_cont)
      .subscribe(
        (data) => {
          this.notes = data.data;
          this.groupList = data.user_groups;
        },
        (error) => {}
      );
  }
  /**
   * File selection set
   */

  getUserByGrp(value) {
    this.groupid = value;
    this.NoteAdd.controls.group_id.setValue(this.groupid);
    this.NoteAssignfrm.controls.group_id.setValue(this.groupid);
    this.searchForm.controls.group_id.setValue(this.groupid);
    //this.NoteAdd.controls.status = 1;
    if (value == "all") {
      this.commonService
        .getAll(this.global.apiUrl + "/api/group/useres")
        .subscribe(
          (data) => {
            this.users = data.data;
          },
          (error) => {}
        );
    } else {
      this.commonService
        .getAll(this.global.apiUrl + "/api/group/useres?id=" + value)
        .subscribe(
          (data) => {
            this.users = data.data;
          },
          (error) => {}
        );
    }
  }

  onFileSelected(event: any) {
    if (event.target.files.length > 0) {
      this.NoteAdd.get("attachfile").setValue(event.target.files[0]);
    } else {
      this.messageService.error("Uploaded file is not a valid file.");
    }
  }
  /**
   * Create new notes
   */
  submitPost() {
    let newFormData = new FormData();
    Object.keys(this.NoteAdd.controls).forEach((key) => {
      newFormData.append(
        key,
        this.NoteAdd.get(key).value ? this.NoteAdd.get(key).value : ""
      );
    });
    //console.log(newFormData);
    this.commonService
      .create(this.global.apiUrl + "/api/hc/notes/upload", newFormData)
      .subscribe(
        (data) => {
          if (data.status == 422) {
            this.noteModal = "inline";
            this.validationError = data.error;
          }
          if (data.status == 200) {
            this.isModalOpen = false;
            this.noteModal = "none";
            this.validationError = "";
            this.notes.unshift(data.data);
            this.messageService.success(data.status_text);
            this.NoteAdd.reset();
            this.redirectTo(this.router.url);
            //this.setReset();
          }
        },
        (error) => {}
      );
  }

  noteAssign(id) {
    this.noteAssignModal = "block";
    this.isModalOpen = true;
    this.NoteAssignfrm.controls.note_id.setValue(id);
  }

  noteRemove(id) {
    let newFormData = { id: id };
    this.commonService
      .create(this.global.apiUrl + "/api/hc/notes/remove", newFormData)
      .subscribe(
        (data) => {
          if (data.status == 422) {
          }
          if (data.status == 200) {
            this.messageService.success(data.status_text);
            this.redirectTo(this.router.url);
          }
        },
        (error) => {}
      );
  }

  noteAssignProcess() {
    let newFormData = new FormData();
    Object.keys(this.NoteAssignfrm.controls).forEach((key) => {
      newFormData.append(
        key,
        this.NoteAssignfrm.get(key).value
          ? this.NoteAssignfrm.get(key).value
          : ""
      );
    });

    this.commonService
      .create(this.global.apiUrl + "/api/hc/notes/assign", newFormData)
      .subscribe(
        (data) => {
          console.log(data);
          if (data.status == 422) {
            this.noteAssignModal = "inline";
            this.validationError = data.error;
            console.log(this.validationError);
          }
          if (data.status == 200) {
            this.isModalOpen = false;
            this.noteAssignModal = "none";
            this.validationError = "";
            this.notes.unshift(data.data);
            this.messageService.success(data.status_text);
            this.NoteAssignfrm.reset();
            this.redirectTo(this.router.url);
            //this.setReset();
          }
        },
        (error) => {}
      );
  }

  noteSearch(event) {
    let search_cont = "";
    if (this.searchForm.get("group_id").value) {
      search_cont += "&group_id=" + this.searchForm.get("group_id").value;
    }
    if (this.searchForm.get("user_id").value) {
      search_cont += "&user_id=" + this.searchForm.get("user_id").value;
    }
    if (this.searchForm.get("from_date").value) {
      search_cont += "&from_date=" + this.searchForm.get("from_date").value;
    }
    if (this.searchForm.get("to_date").value) {
      search_cont += "&to_date=" + this.searchForm.get("to_date").value;
    }

    this.commonService
      .getAll(this.global.apiUrl + "/api/hc/notes?" + search_cont)
      .subscribe(
        (data) => {
          console.log(data);
          this.notes = data.data;
          this.groupList = data.user_groups;
        },
        (error) => {}
      );
  }

  redirectTo(uri) {
    this.router
      .navigateByUrl("/", { skipLocationChange: true })
      .then(() => this.router.navigate([uri]));
  }
}
