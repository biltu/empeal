import { Component, OnInit } from '@angular/core';
import {Global} from '../../global';
import {NgbPopoverConfig} from '@ng-bootstrap/ng-bootstrap';
import {AuthService} from '../../services/auth.service';
import {CommonService} from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart  } from '@angular/router';

@Component({
  selector: 'app-wearable',
  templateUrl: './wearable.component.html',
  styleUrls: ['./wearable.component.css']
})
export class WearableComponent implements OnInit {
	wearable:any;
  status:any;
  source:any;
  constructor(private message:MessageService ,private global: Global,private authService: AuthService,
   private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) {
    this.status = this.activeRoute.snapshot.queryParamMap.get("status") ? this.activeRoute.snapshot.queryParamMap.get("status"):'';
    this.source = this.activeRoute.snapshot.queryParamMap.get("source") ? this.activeRoute.snapshot.queryParamMap.get("source"):''; 
  	this.asyncInit();
  }

  ngOnInit() {

  }

/**
* wearable data status update
*/
  asyncInit(){
    if(this.status !='' && this.source !=''){
      let form = {status:this.status,source:this.source};
      this.commonService.create(this.global.apiUrl + '/api/wearable/status/update',form)
          .subscribe((data)=>{
            this.wearable = data.data;
            if(data.status ==200){
              this.message.success(data.status_text);
            }

            if(data.status ==400){
              this.message.error(data.status_text);
            }
          }, (error)=>{
          });
    }else{
    	this.commonService.getAll(this.global.apiUrl + '/api/wearable/urls')
    		.subscribe((data)=>{
    			this.wearable = data.data;
    		}, (error)=>{
    		});
    }
  }

}
