import { Component, OnInit } from '@angular/core';
import {Global} from '../../global';
import {NgbPopoverConfig} from '@ng-bootstrap/ng-bootstrap';
import {AuthService} from '../../services/auth.service';
import {CommonService} from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart  } from '@angular/router';

@Component({
  selector: 'app-corporate-detail',
  templateUrl: './corporate-detail.component.html',
  styleUrls: ['./corporate-detail.component.css']
})
export class CorporateDetailComponent implements OnInit {

  param: number;
  corp_id: number;
  details: any=[];
  title: string;
  current_month: number;
  current_year: number;
  iscontestid: number;
  searchInput:any={
    by_month:'',
    by_year:''
  };
  teamName: String ='';

  constructor(private global: Global,private router: Router, private commonService: CommonService,
  	 private route: ActivatedRoute, private message: MessageService) { 
  	 this.param = this.route.snapshot.params['name'];
     this.corp_id = this.route.snapshot.params['id']? this.route.snapshot.params['id']:'';
  	 this.getName(this.param);
     this.teamName = this.route.snapshot.queryParams['team_id'];
     this.iscontestid = this.route.snapshot.queryParams['contest_id'];
     let url = this.corp_id ? '/api/corporate/dashboard-data?id=' + this.corp_id + '&name=' + this.param + '&team=' +this.teamName + '&contest_id=' +this.iscontestid : '/api/corporate/dashboard-data?name=' + this.param + '&team=' +this.teamName + '&contest_id=' +this.iscontestid;
  	 this.commonService.getAll(this.global.apiUrl + url)
  	 	 .subscribe((data)=>{
          //console.log(data);
  	 	 	if(data.data.length && this.param != 1){
  	 	 		this.details = data.data.map((obj)=>{
  	 	 			return {name: obj.user_detail.firstname + ' ' + obj.user_detail.lastname, email: obj.email, location: obj.user_detail.location, team: obj.user_detail.team}
  	 	 		});
  	 	 	}else if(data.data.length && this.param == 1){
  	 	 		this.details = data.data.map((obj)=>{
  	 	 			return {name: obj.name, email: obj.email_id, location: obj.location, team: obj.team}
  	 	 		});
  	 	 	}
  	 	 }, (error)=>{});
  }

  ngOnInit() {
    var d = new Date();
    this.current_month= d.getMonth();
    this.current_year = d.getFullYear();
    this.searchInput.by_month = '';
    this.searchInput.by_year = '';
    /*this.searchInput.by_month = this.current_month +1;
    this.searchInput.by_year = this.current_year;*/
  }

  getName(param){
  	let value ='';
  	switch (Number(param)) {
  		case 1:
  			value = 'No Of Invited';
  			break;
  		case 2:
  			value = 'No Of Registered';
  			break;
  		case 3:
  			value = 'No Of BCA';
  			break;
  		case 4:
  			value = 'No Of Engagement';
  			break;
  		default:
  			// code...
  			break;
  	}

  	this.title = value;
  }


/**
* Search user by month or year 
* @returns  user data
*/

  searchData(){
    let submit = true;
    if(this.searchInput.by_month ==''){
      this.message.error('Please select month');
      submit = false;
    }
    if(this.searchInput.by_year ==''){
      this.message.error('Please select year');
      submit = false;
    }
    if(submit == false){
      return false;
    }

    this.commonService.getAll(this.global.apiUrl + '/api/corporate/engaged-users?by_month=' 
          + this.searchInput.by_month + '&by_year=' + this.searchInput.by_year)
          .subscribe((data)=>{
            if(data.data.length){
               this.details = data.data.map((obj)=>{
              return {name: obj.user_detail.firstname + ' ' + obj.user_detail.lastname,
               email: obj.email, 
               location: obj.user_detail.location ? obj.user_detail.location:'', 
               team: obj.user_detail.team ? obj.user_detail.team: ''
             }
              });
            }else{
              this.details =[];
            }
            
          }, (error)=>{});


  }

}
