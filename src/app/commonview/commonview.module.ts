import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CoreModule} from '../core/core.module';

import { CommonviewRoutingModule } from './commonview-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProfileComponent } from './profile/profile.component';
import { GeneralInformationComponent } from './edit-profile/general-information/general-information.component';
import { StatutoryInformationComponent } from './edit-profile/statutory-information/statutory-information.component';
import { MedicalConditionComponent } from './edit-profile/medical-condition/medical-condition.component';
import { PainPointsComponent } from './edit-profile/pain-points/pain-points.component';
import { AllergyComponent } from './edit-profile/allergy/allergy.component';
import { WearableComponent } from './wearable/wearable.component';
import { CorporateProfileComponent } from './profile/corporate-profile/corporate-profile.component';
import { UserProfileComponent } from './profile/user-profile/user-profile.component';
import { CorporateDashboardComponent } from './dashboard/corporate-dashboard/corporate-dashboard.component';
import { UserDashboardComponent } from './dashboard/user-dashboard/user-dashboard.component';
import { CorporateEditProfileComponent } from './edit-profile/corporate-edit-profile/corporate-edit-profile.component';
import { InvitationComponent } from './invitation/invitation.component';

import { CoachDashboardComponent } from './dashboard/coach-dashboard/coach-dashboard.component';
import { AdminDashboardComponent } from './dashboard/admin-dashboard/admin-dashboard.component';
import { GroupDashboardComponent } from './group-dashboard/group-dashboard.component';
import { GroupUserListComponent } from './group-user-list/group-user-list.component';
import { WearableCallbackComponent } from './wearable-callback/wearable-callback.component';

import { CoachProfileComponent } from './profile/coach-profile/coach-profile.component';
import { CoachEditProfileComponent } from './edit-profile/coach-edit-profile/coach-edit-profile.component';


import { UpdatePasswordComponent } from './update-password/update-password.component';
import { TermConditionComponent } from './term-condition/term-condition.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { ExplainerVideoComponent } from './explainer-video/explainer-video.component';
import { CorporateDetailComponent } from './corporate-detail/corporate-detail.component';
import { UserReportComponent } from './user-report/user-report.component';
import { SupportComponent } from './support/support.component';
import { JoinedContestComponent } from './dashboard/joined-contest/joined-contest.component';
import { ContestRedirectionComponent } from './dashboard/contest-redirection/contest-redirection.component';
import { ManualNotificationComponent } from './manual-notification/manual-notification.component';
import { UserReportPdfComponent } from './user-report-pdf/user-report-pdf.component';
import { AttachmentsComponent } from './attachments/attachments.component';

// SecondaryCoach
import { SecondaryCoachDashboardComponent } from './dashboard/secondary-coach-dashboard/secondary-coach-dashboard.component';
import { SecondaryCoachProfileComponent } from './profile/secondary-coach-profile/secondary-coach-profile.component';
import { SecondaryCoachEditProfileComponent } from './edit-profile/secondary-coach-edit-profile/secondary-coach-edit-profile.component';

@NgModule({
  declarations: [
    DashboardComponent,
    ProfileComponent,
    GeneralInformationComponent,
    StatutoryInformationComponent,
    MedicalConditionComponent,
    WearableComponent,
    CorporateProfileComponent,
    UserProfileComponent,
    AllergyComponent,
    CorporateDashboardComponent,
    UserDashboardComponent,
    CorporateEditProfileComponent,
    InvitationComponent,
    CoachDashboardComponent,
    AdminDashboardComponent,
    GroupDashboardComponent,
    GroupUserListComponent,
    WearableCallbackComponent,
    CoachProfileComponent,
    CoachEditProfileComponent,
    UpdatePasswordComponent,
    TermConditionComponent,
    PrivacyPolicyComponent,
    ExplainerVideoComponent,
    CorporateDetailComponent,
    UserReportComponent,
    PainPointsComponent,
    SupportComponent,
    JoinedContestComponent,
    ContestRedirectionComponent,
    ManualNotificationComponent,
    UserReportPdfComponent,
    AttachmentsComponent,

    SecondaryCoachDashboardComponent,
    SecondaryCoachProfileComponent,
    SecondaryCoachEditProfileComponent,
  ],

  imports: [
    CommonModule,
    CommonviewRoutingModule,
    CoreModule
  ]
})
export class CommonviewModule { }
