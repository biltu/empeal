import { Component, OnInit } from '@angular/core';
import {Global} from '../../global';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {NgbPopoverConfig} from '@ng-bootstrap/ng-bootstrap';
import {AuthService} from '../../services/auth.service';
import {CommonService} from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart  } from '@angular/router';

@Component({
  selector: 'app-group-user-list',
  templateUrl: './group-user-list.component.html',
  styleUrls: ['./group-user-list.component.css']
})
export class GroupUserListComponent implements OnInit {
	group_id: string;
	users: any=[];
  display: string;
  isModalOpen: boolean;
  userStandardsForm: FormGroup;
  standardFormValidation: any;
  ansDateModel: string ='';
  user_role: any = {};
  user_data: any = {};

  constructor(private fb:FormBuilder,private message:MessageService ,private global: Global,private authService: AuthService,
   private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) 
  { 
    this.group_id = this.activeRoute.snapshot.paramMap.get("id");
    this.user_data = this.authService.isUserLoginIn ? JSON.parse(localStorage.getItem('user')) : '';
    this.user_role = this.user_data ? this.user_data.role : '';
    
  	this.asyncInit();
  }

  ngOnInit() {
    this.userStandardsForm = this.fb.group({
        distance:['', [Validators.required]],
        active_time:['', [Validators.required]],
        fat_burn:['', [Validators.required]],
        floors:['', [Validators.required]],
        user_id:['']
      });
  }

/**
* get Group wise users
*/

  asyncInit(){
  	this.commonService.getAll(this.global.apiUrl + '/api/group/useres?id='+this.group_id)
  		.subscribe((data)=>{
  			this.users = data.data;
  		}, (error)=>{

  		});
  }

  openGroupModalDialog(index,id){
    console.log(id);
      this.display='block'; //Set block css
      let data = '';
      if(index=='edit' && id > 0){
        data = this.users.find((jb)=>jb.user_id ==id);
        console.log(data);
        this.setFromValue(data);
      }
      this.isModalOpen = true;
  }

  closeGroupModalDialog(){
    this.display='none'; //set none css after close dialog
    this.isModalOpen = false;
  }

/**
* Set user standard form
*/

  setFromValue(data){
    this.userStandardsForm.patchValue({
      distance : data.standards.distance,
      active_time : data.standards.active_time,
      fat_burn : data.standards.fat_burn,
      floors : data.standards.floors,
      user_id:data.user_id
    });
  }

/**
* Update standards
*/

  updateStandards(){
    let formdata = this.userStandardsForm.value;
    this.commonService.create(this.global.apiUrl + '/api/user/standard/update',formdata)
      .subscribe((data)=>{
        if(data.status == 422){
          this.standardFormValidation = data.error;
        }

        if(data.status ==200){
          this.message.success(data.status_text);
          this.display='none';
          this.isModalOpen = false;
          this.redirectTo(this.router.url);
        }

        if(data.status ==400){
          this.message.error(data.status_text);
        }
      }, (error)=>{
    });
  }

  monthReportDetails(date,userid){
    this.router.navigateByUrl('/questionnaire/report/'+userid+'/'+date, {skipLocationChange: true});
  }

  redirectTo(uri) {
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() =>
    this.router.navigate([uri]));
  }

}
