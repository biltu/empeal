import { Component, OnInit } from '@angular/core';
import {Global} from '../../global';
import {NgbPopoverConfig} from '@ng-bootstrap/ng-bootstrap';
import {AuthService} from '../../services/auth.service';
import {CommonService} from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart  } from '@angular/router';
import { Chart, ChartOptions } from 'chart.js';
declare var ApexCharts: any;
declare var anychart: any;
@Component({
  selector: 'app-user-report',
  templateUrl: './user-report.component.html',
  styleUrls: ['./user-report.component.css']
})
export class UserReportComponent implements OnInit {
	userid:any;
	user_data: any={};
  userlast_qsnans: any=[];
	hasStatutory: boolean;
  permissions: any=[];
  user_detail: any={};
  sortByField: string;
  reverse: boolean =false;
  meals: any=[];
  totalmeals: number;
	bardata: any=[];
  	allnutrns: any=[];
  	pieChartOptions = {
      responsive: true,
      maintainAspectRatio: true,
     //  chartArea: {
     //    	backgroundColor: 'rgba(251, 85, 85, 0.4)'
    	// },
        hover: {
	      animationDuration: 0
	    },
	    animation: {
	      duration: 1,
	      onComplete: function() {
	        let chartInstance = this.chart,
	        ctx = chartInstance.ctx;
	        ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
	        ctx.textAlign = 'center';
	        ctx.textBaseline = 'bottom';

	        this.data.datasets.forEach(function(dataset, i) {
	          let meta = chartInstance.controller.getDatasetMeta(i);
	          meta.data.forEach(function(bar, index) {
	            let data = dataset.data[index];
	            if(bar._model.label == 'Carbohydrate'){
	            	ctx.fillText(((data*275)/100).toFixed(2) + ' gm', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Protien'){
	            	ctx.fillText(((data*62.5)/100).toFixed(2) + ' gm', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Fat'){
	            	ctx.fillText(((data*62.5)/100).toFixed(2) + ' gm', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Sugar'){
	            	ctx.fillText(((data*21.25)/100).toFixed(2) + ' gm', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Fibre'){
	            	ctx.fillText(((data*55)/100).toFixed(2) + ' gm', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'O6/O3'){
	            	ctx.fillText(((data*6)/100).toFixed(2) + ' Ratio', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Calcium'){
	            	ctx.fillText(((data*1100)/100).toFixed(2) + ' mg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Magensium'){
	            	ctx.fillText(((data*610)/100).toFixed(2) + ' mg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Iron'){
	            	ctx.fillText(((data*11.5)/100).toFixed(2) + ' mg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Zinc'){
	            	ctx.fillText(((data*37.5)/100).toFixed(2) + ' mg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Selenium'){
	            	ctx.fillText(((data*250)/100).toFixed(2) + ' mcg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Iodine'){
	            	ctx.fillText(((data*225)/100).toFixed(2) + ' mcg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Sodium'){
	            	ctx.fillText(((data*2250)/100).toFixed(2) + ' mg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Vitamin A'){
	            	ctx.fillText(((data*2250)/100).toFixed(2) + ' mcg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Vitamin D'){
	            	ctx.fillText(((data*12.5)/100).toFixed(2) + ' mcg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Vitamin E'){
	            	ctx.fillText(((data*326)/100).toFixed(2) + ' mg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Vitamin K1'){
	            	ctx.fillText(((data*375)/100).toFixed(2) + ' mcg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Thiamin B1'){
	            	ctx.fillText(((data*25.7)/100).toFixed(2) + ' mg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Riboflavin B2'){
	            	ctx.fillText(((data*25.8)/100).toFixed(2) + ' mg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Niacin B3'){
	            	ctx.fillText(((data*110)/100).toFixed(2) + ' mg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Pantothenate B5'){
	            	ctx.fillText(((data*128.5)/100).toFixed(2) + ' mg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Vitamin B6'){
	            	ctx.fillText(((data*51.25)/100).toFixed(2) + ' mg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Folates B9'){
	            	ctx.fillText(((data*600)/100).toFixed(2) + ' mcg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Vitamin B12'){
	            	ctx.fillText(((data*101.5)/100).toFixed(2) + ' mcg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Biotin B7'){
	            	ctx.fillText(((data*275)/100).toFixed(2) + ' mcg', bar._model.x+30, bar._model.y+7);
	            }else if(bar._model.label == 'Vitamin C'){
	            	ctx.fillText(((data*1045)/100).toFixed(2) + ' mg', bar._model.x+30, bar._model.y+7);	
	            }else{

	            }
	            
	          });
	        });
	      }
	    },
        axisX:{
			labelBackgroundColor: "rgb(231,233,237)",
			labelFontColor: "rgb(231,233,237)"
		},
        //maintainAspectRatio: false,
        legend: {
                display: false
            },
       title: {
            display: true,
            fontSize: 20,
            padding: 16,
            text: 'Last 30 Days Nutrients Distribution'
            },
        tooltips: {
	          callbacks: {
	            label: function(tooltipItem, data) {
                console.log(data['datasets'][0]['data'][tooltipItem['index']]);
                let toltipdata = '';
                  //return ((data['datasets'][0]['data'][tooltipItem['index']]*62.5)/100).toFixed(2) + ' gm ('+data['datasets'][0]['data'][tooltipItem['index']] + '%)';
	              if(data['labels'][tooltipItem['index']] == 'Carbohydrate'){
                  toltipdata = ((data['datasets'][0]['data'][tooltipItem['index']]*275)/100).toFixed(2) + ' gm ('+data['datasets'][0]['data'][tooltipItem['index']] + '%)';
                }else if(data['labels'][tooltipItem['index']] == 'Protien'){
                  toltipdata =  ((data['datasets'][0]['data'][tooltipItem['index']]*62.5)/100).toFixed(2) + ' gm ('+data['datasets'][0]['data'][tooltipItem['index']] + '%)';
                }else if(data['labels'][tooltipItem['index']] == 'Fat'){
                  toltipdata =  ((data['datasets'][0]['data'][tooltipItem['index']]*62.5)/100).toFixed(2) + ' gm ('+data['datasets'][0]['data'][tooltipItem['index']] + '%)';
                }else if(data['labels'][tooltipItem['index']] == 'Sugar'){
                  toltipdata =  ((data['datasets'][0]['data'][tooltipItem['index']]*21.25)/100).toFixed(2) + ' gm ('+data['datasets'][0]['data'][tooltipItem['index']] + '%)';
                }else if(data['labels'][tooltipItem['index']] == 'Fibre'){
                  toltipdata =  ((data['datasets'][0]['data'][tooltipItem['index']]*55)/100).toFixed(2) + ' gm ('+data['datasets'][0]['data'][tooltipItem['index']] + '%)';
                }else if(data['labels'][tooltipItem['index']] == 'O6/O3'){
                  toltipdata =  ((data['datasets'][0]['data'][tooltipItem['index']]*6)/100).toFixed(2) + ' Ratio ('+data['datasets'][0]['data'][tooltipItem['index']] + '%)';
                }else if(data['labels'][tooltipItem['index']] == 'Calcium'){
                  toltipdata =  ((data['datasets'][0]['data'][tooltipItem['index']]*1100)/100).toFixed(2) + ' mg ('+data['datasets'][0]['data'][tooltipItem['index']] + '%)';
                }else if(data['labels'][tooltipItem['index']] == 'Magensium'){
                  toltipdata =  ((data['datasets'][0]['data'][tooltipItem['index']]*610)/100).toFixed(2) + ' mg ('+data['datasets'][0]['data'][tooltipItem['index']] + '%)';
                }else if(data['labels'][tooltipItem['index']] == 'Iron'){
                  toltipdata =  ((data['datasets'][0]['data'][tooltipItem['index']]*11.5)/100).toFixed(2) + ' mg ('+data['datasets'][0]['data'][tooltipItem['index']] + '%)';
                }else if(data['labels'][tooltipItem['index']] == 'Zinc'){
                  toltipdata =  ((data['datasets'][0]['data'][tooltipItem['index']]*37.5)/100).toFixed(2) + ' mg ('+data['datasets'][0]['data'][tooltipItem['index']] + '%)';
                }else if(data['labels'][tooltipItem['index']] == 'Selenium'){
                  toltipdata =  ((data['datasets'][0]['data'][tooltipItem['index']]*250)/100).toFixed(2) + ' mcg ('+data['datasets'][0]['data'][tooltipItem['index']] + '%)';
                }else if(data['labels'][tooltipItem['index']] == 'Iodine'){
                  toltipdata =  ((data['datasets'][0]['data'][tooltipItem['index']]*225)/100).toFixed(2) + ' mcg ('+data['datasets'][0]['data'][tooltipItem['index']] + '%)';
                }else if(data['labels'][tooltipItem['index']] == 'Sodium'){
                  toltipdata =  ((data['datasets'][0]['data'][tooltipItem['index']]*2250)/100).toFixed(2) + ' mg ('+data['datasets'][0]['data'][tooltipItem['index']] + '%)';
                }else if(data['labels'][tooltipItem['index']] == 'Vitamin A'){
                  toltipdata =  ((data['datasets'][0]['data'][tooltipItem['index']]*2250)/100).toFixed(2) + ' mcg ('+data['datasets'][0]['data'][tooltipItem['index']] + '%)';
                }else if(data['labels'][tooltipItem['index']] == 'Vitamin D'){
                  toltipdata =  ((data['datasets'][0]['data'][tooltipItem['index']]*12.5)/100).toFixed(2) + ' mcg ('+data['datasets'][0]['data'][tooltipItem['index']] + '%)';
                }else if(data['labels'][tooltipItem['index']] == 'Vitamin E'){
                  toltipdata =  ((data['datasets'][0]['data'][tooltipItem['index']]*326)/100).toFixed(2) + ' mg ('+data['datasets'][0]['data'][tooltipItem['index']] + '%)';
                }else if(data['labels'][tooltipItem['index']] == 'Vitamin K1'){
                  toltipdata =  ((data['datasets'][0]['data'][tooltipItem['index']]*375)/100).toFixed(2) + ' mcg ('+data['datasets'][0]['data'][tooltipItem['index']] + '%)';
                }else if(data['labels'][tooltipItem['index']] == 'Thiamin B1'){
                  toltipdata =  ((data['datasets'][0]['data'][tooltipItem['index']]*25.7)/100).toFixed(2) + ' mg ('+data['datasets'][0]['data'][tooltipItem['index']] + '%)';
                }else if(data['labels'][tooltipItem['index']] == 'Riboflavin B2'){
                  toltipdata =  ((data['datasets'][0]['data'][tooltipItem['index']]*25.8)/100).toFixed(2) + ' mg ('+data['datasets'][0]['data'][tooltipItem['index']] + '%)';
                }else if(data['labels'][tooltipItem['index']] == 'Niacin B3'){
                  toltipdata =  ((data['datasets'][0]['data'][tooltipItem['index']]*110)/100).toFixed(2) + ' mg ('+data['datasets'][0]['data'][tooltipItem['index']] + '%)';
                }else if(data['labels'][tooltipItem['index']] == 'Pantothenate B5'){
                  toltipdata =  ((data['datasets'][0]['data'][tooltipItem['index']]*128.5)/100).toFixed(2) + ' mg ('+data['datasets'][0]['data'][tooltipItem['index']] + '%)';
                }else if(data['labels'][tooltipItem['index']] == 'Vitamin B6'){
                  toltipdata =  ((data['datasets'][0]['data'][tooltipItem['index']]*51.25)/100).toFixed(2) + ' mg ('+data['datasets'][0]['data'][tooltipItem['index']] + '%)';
                }else if(data['labels'][tooltipItem['index']] == 'Folates B9'){
                  toltipdata =  ((data['datasets'][0]['data'][tooltipItem['index']]*600)/100).toFixed(2) + ' mcg ('+data['datasets'][0]['data'][tooltipItem['index']] + '%)';
                }else if(data['labels'][tooltipItem['index']] == 'Vitamin B12'){
                  toltipdata =  ((data['datasets'][0]['data'][tooltipItem['index']]*101.5)/100).toFixed(2) + ' mcg ('+data['datasets'][0]['data'][tooltipItem['index']] + '%)';
                }else if(data['labels'][tooltipItem['index']] == 'Biotin B7'){
                  toltipdata =  ((data['datasets'][0]['data'][tooltipItem['index']]*275)/100).toFixed(2) + ' mcg ('+data['datasets'][0]['data'][tooltipItem['index']] + '%)';
                }else if(data['labels'][tooltipItem['index']] == 'Vitamin C'){
                  toltipdata =  ((data['datasets'][0]['data'][tooltipItem['index']]*1045)/100).toFixed(2) + ' mg ('+data['datasets'][0]['data'][tooltipItem['index']] + '%)'; 
                }else{
                  toltipdata = '';
                }
              return toltipdata;
              },
	          },
	          position: 'custom'
	        },
	    scales: {
            yAxes: [{
                ticks: {
                    //beginAtZero: true,
                    display: true,
                    fontFamily: "'Amarnath'" //Raleway
                },
    			gridLines: {
				   display: false
				}
            }],
            xAxes: [{
                ticks: {
                    //beginAtZero: true,
                    steps: 10,
                    stepSize: 10,
                    stepValue: 10,
                    max: 100,
                    min: 0,
                    display: false,
                    fontFamily: "'Amarnath'",
                    //backgroundColor: 'rgb(231,233,237)'
                },
                gridLines: {
				   display: false
				}
            }]
	    }

    };
    pieChartLabels =  ['Carbohydrate', 'Protien', 'Fat', 'Sugar', 'Fibre', 'O6/O3',
     'Calcium', 'Magensium', 'Iron', 'Zinc', 'Selenium', 'Iodine', 'Sodium', 'Vitamin A',
      'Vitamin D', 'Vitamin E', 'Vitamin K1', 'Thiamin B1', 'Riboflavin B2', 'Niacin B3',
       'Pantothenate B5', 'Vitamin B6', 'Folates B9', 'Vitamin B12', 'Biotin B7', 'Vitamin C'];
  
    // CHART COLOR.
    pieChartColor:any = [
        {
            backgroundColor: ['rgb(54, 162, 235)','rgb(54, 162, 235)','rgb(54, 162, 235)',
            'rgb(54, 162, 235)','rgb(54, 162, 235)','rgb(54, 162, 235)','rgb(54, 162, 235)',
            'rgb(54, 162, 235)','rgb(54, 162, 235)','rgb(54, 162, 235)','rgb(54, 162, 235)',
            'rgb(54, 162, 235)','rgb(54, 162, 235)','rgb(54, 162, 235)','rgb(54, 162, 235)',
            'rgb(54, 162, 235)','rgb(54, 162, 235)','rgb(54, 162, 235)','rgb(54, 162, 235)',
            'rgb(54, 162, 235)','rgb(54, 162, 235)','rgb(54, 162, 235)','rgb(54, 162, 235)',
            'rgb(54, 162, 235)','rgb(54, 162, 235)','rgb(54, 162, 235)'],
        }
    ];

    pieChartData:any = [
        { 
            data: []
        }
    ];

    topChartOptions = {
    	responsive: false,
      maintainAspectRatio: false,
    	legend: {
                display: true,
                position: 'bottom'
            },
	    title: {
	      display: true,
	      text: 'Last 30 days Record'
	    },
	    tooltips: {
	      mode: 'index',
	      intersect: true
	    },
	    scales: {
	      xAxes: [{
	        stacked: true,
	      }],
	      yAxes: [{
	        id: 'A',
	        type: 'linear',
	        position: 'left',
	        ticks: {
	          min: 0,
	          fontFamily: "'Amarnath'"
	        }
	      }, {
	        id: 'B',
	        type: 'linear',
	        position: 'right',
	        ticks: {
	          min: 0,
	          fontFamily: "'Amarnath'"
	        },
	        gridLines: {
	            display:false
	        }
	      }]
	    }
    };
    topChartLabels =  [];

    topChartData:any = [{
	      type: 'bar',
	      label: 'Carbohydrate',
	      backgroundColor: 'rgb(255, 99, 132)',
        hoverBackgroundColor:'rgb(255, 99, 132)',
        hoverBorderColor: 'white',
	      stack: 'Stack 0',
	      data: [],
	      borderColor: 'white',
	      borderWidth: 2,
	      yAxisID: 'A'
	    }, {
	      type: 'bar',
	      label: 'Protein',
	      backgroundColor: 'rgb(54, 162, 235)',
        hoverBackgroundColor:'rgb(54, 162, 235)',
        hoverBorderColor: 'white',
	      stack: 'Stack 0',
	      data: [],
	      borderColor: 'white',
	      borderWidth: 2,
	      yAxisID: 'A'
	    }, {
	      type: 'bar',
	      label: 'Fat',
	      backgroundColor: 'rgb(255, 159, 64)',
        hoverBackgroundColor:'rgb(255, 159, 64)',
        hoverBorderColor: 'white',
	      stack: 'Stack 0',
	      data: [],
	      borderColor: 'white',
	      borderWidth: 2,
	      yAxisID: 'A'
	    }, {
	      type: 'line',
	      label: 'Water',
	      borderColor: 'rgb(153, 102, 255)',
        hoverBackgroundColor:'rgb(153, 102, 255)',
        hoverBorderColor: 'white',
	      borderWidth: 2,
	      fill: false,
	      data: [],
	      yAxisID: 'B'
    }];

	options = {
            chart: {
                height: 295,
                type: 'radialBar',
            },
            plotOptions: {
              radialBar: {
                offsetY: -10,
                startAngle: 0,
                endAngle: 270,
                hollow: {
                  margin: 5,
                  size: '30%',
                  background: 'transparent',
                  image: undefined,
                },
                dataLabels: {
                  name: {
                    show: false,

                  },
                  value: {
                    show: false,
                  }
                }
              }
            },
            series: [],
            labels: ['Nutrition', 'Fitness', 'Behavioural', 'Stress'],
            legend: {
            show: true,
            floating: true,
            fontSize: '14px',
            position: 'left',
            offsetX: 100,
            offsetY: 0,
            labels: {
                useSeriesColors: true,
            },
            markers: {
                size: 0
            },
            formatter: function(seriesName, opts) {
                if(seriesName=='Stress'){
                  return seriesName + " <span style='font-size:10px;'>(Lower is better)</span>:  " + (opts.w.globals.series[opts.seriesIndex]/10).toFixed(1)
                }else{
                  return seriesName + ":  " + (opts.w.globals.series[opts.seriesIndex]/10).toFixed(1)
                }
            },
            itemMargin: {
                horizontal: 1,
            }
          },
          responsive: [{
              breakpoint: 480,
              options: {
                  legend: {
                      show: true,
                      floating: true,
                      fontSize: '12px',
                      position: 'left',
                      offsetX: -50,
                      offsetY: 0,
                      labels: {
                          useSeriesColors: true,
                      },
                      markers: {
                          size: 0
                      },
                      formatter: function(seriesName, opts) {
                        if(seriesName=='Stress'){
                          return seriesName + " <span style='font-size:10px;'>(Lower is better)</span>:  " + (opts.w.globals.series[opts.seriesIndex]/10).toFixed(1)
                        }else{
                          return seriesName + ":  " + (opts.w.globals.series[opts.seriesIndex]/10).toFixed(1)
                        }
                      },
                      itemMargin: {
                          horizontal: 1,
                      }
                  }
              }
          }]
            
        };

        targetoptions = {
            chart: {
                height: 275,
                type: 'area',
                toolbar: {
                  show: false, 
                }
            },
            colors: ['#2E93fA', '#66DA26', '#E91E63','#546E7A'],
            dataLabels: {
                enabled: false
            },
            stroke: {
                curve: 'smooth'
            },
            series: [
              {
                  name: 'Target',
                  data: []
              }, {
                  name: 'Achieved',
                  data: []
              }, {
                  name: 'Failed',
                  data: []
              }, {
                  name: 'Expired',
                  data: []
              }
            ],

            xaxis: {
                type: 'date',
                categories: [],                
            },
            tooltip: {
                x: {
                    format: 'dd/mm'
                },
            }
        }


    fatmaslChartOptions = {
      responsive: false,
      maintainAspectRatio: false,
      title: {
        display: true,
        text: ''
      },
      legend: {
          display: true,
          position: 'bottom'
      },
      tooltips: {
        mode: 'index',
        intersect: true
      },
      scales: {
        xAxes: [{
        display: false,
        id: "main-x",
        categoryPercentage: 1,
        stacked: true
      }, {
        display: false,
        id: "sub-x",
        type: 'category',
        categoryPercentage: 0.8,
        barPercentage: 1,
        stacked: true
      }],
        yAxes: [{
          id: 'A',
          type: 'linear',
          position: 'left'
        }, {
          id: 'B',
          type: 'linear',
          position: 'right',
          ticks: {
            min: 0
          },
          gridLines: {
              display:false
          } 
        }]
      }
    };
    fatmaslChartLabels =  [];

    fatmaslChartData:any = [{
      label: 'Calories Consumed',
      backgroundColor: 'rgba(255, 127, 15, 0.2)',
      hoverBackgroundColor: 'rgba(255, 127, 15, 0.2)',
      data: [],
      borderWidth: 2,
      borderColor: 'red',
      yAxisID: 'A',
      xAxisID: "sub-x",
      fill: false
    }, {
      label: 'Calories Burned',
      backgroundColor: 'blue',
      hoverBackgroundColor: 'blue',
      data: [],
      borderWidth: 2,
      yAxisID: 'A',
      xAxisID: "sub-x",
      fill: false
    },
    
    {
      type: 'line',
      label: 'Activity Minutes',
      backgroundColor: 'purple',
      borderColor: 'purple',
      hoverBackgroundColor: 'purple',
      fill: false,
      data: [],
      yAxisID: 'B'
    }];


    personalcrossOptions = {
        responsive: false,
        maintainAspectRatio: false,
        tooltips: {
            // callbacks: {
            //     label: function(tooltipItem) {
            //         return "$" + Number(tooltipItem.yLabel) + " and so worth it !";
            //     }
            // },
            callbacks: {
                label: function(tooltipItem, data) {
                    var dataset = data.datasets[tooltipItem.datasetIndex];
                    var index = tooltipItem.index;
                    return dataset.labels[index] + ': ' + dataset.data[index];
                }
            },
            displayColors: false

        },
        legend: {
                display: false,
            },
            title: {
                display: true,
                fontSize: 20,
                padding: 16,
                text: 'Personal Cross Profile Analysis'
            },
            "scales": {
                "yAxes": [{
                    "ticks": {
                        "beginAtZero": true
                    }
                }],
                xAxes: [{
                    ticks: {
                        autoSkip: false,
                        //fontSize: 11
                    }
                }]
            }
    };
    personalcrossLabels =  [];
  	
    // CHART COLOR.
    personalcrossData:any = [];

    activityChartOptions = {
    	responsive: false,
      maintainAspectRatio: false,
	    title: {
	      display: true,
	      text: ''
	    },
	    tooltips: {
	      mode: 'index',
	      intersect: true
	    },
	    scales: {
	      xAxes: [{
	        stacked: true,
	      }],
	      yAxes: [{
	        id: 'A',
	        type: 'linear',
	        position: 'left',
	        ticks: {
	          min: 0
	        }
	      }, {
	        id: 'B',
	        type: 'linear',
	        position: 'right',
	        ticks: {
	          min: 0
	        },
	        gridLines: {
	            display:false
	        }
	      }]
	    }
    };
    activityChartLabels =  [];

    activityChartData:any = [
    	{
	      type: 'bar',
	      label: 'Steps',
	      backgroundColor: 'rgb(255, 99, 132)',
        hoverBackgroundColor: 'rgb(255, 99, 132)',
        hoverBorderColor: 'rgb(255, 99, 132)',
	      stack: 'Stack 0',
	      data: [],
	      borderColor: 'white',
	      borderWidth: 2,
	      yAxisID: 'A'
	    },
	    {
	      type: 'bar',
	      label: 'Distance',
	      backgroundColor: 'rgb(54, 162, 235)',
        hoverBackgroundColor: 'rgb(54, 162, 235)',
        hoverBorderColor: 'rgb(54, 162, 235)',
	      stack: 'Stack 0',
	      data: [],
	      borderColor: 'white',
	      borderWidth: 2,
	      yAxisID: 'A'
	    },
	    {
	      type: 'bar',
	      label: 'Fat Burn',
	      backgroundColor: 'rgb(255, 159, 64)',
        hoverBackgroundColor: 'rgb(255, 159, 64)',
        hoverBorderColor: 'rgb(255, 159, 64)',
	      stack: 'Stack 0',
	      data: [],
	      borderColor: 'white',
	      borderWidth: 2,
	      yAxisID: 'A'
	    },
	    {
	      type: 'bar',
	      label: 'Heartrate',
	      backgroundColor: 'rgb(153, 102, 255)',
        hoverBackgroundColor: 'rgb(153, 102, 255)',
        hoverBorderColor: 'rgb(153, 102, 255)',
	      stack: 'Stack 0',
	      data: [],
	      borderColor: 'white',
	      borderWidth: 2,
	      yAxisID: 'A'
	    }];

	manualOptions = {
    	responsive: false,
      maintainAspectRatio: false,
	    title: {
        display: false,
        position: 'top',
        text: '',
        fontSize: 13
      	},
      	tooltips: {
        mode: 'index',
        intersect: true
      	},
	    //barValueSpacing: 20,
	    scales: {
            yAxes: [{
              ticks: {
                  min: 0,
                  max: 100
              }
            }]
	    }
    };

    manualChartLabels =  [];

    manualChartData:any = [
    	{
	      label: 'Cardio',
	      backgroundColor: 'rgb(255, 159, 64)',
        hoverBackgroundColor: 'rgb(255, 159, 64)',
        hoverBorderColor: 'rgb(255, 159, 64)',
	      //stack: 'Stack 0',
	      data: []
	    }, 
	    {
	      label: 'Circuit Training',
	      backgroundColor: 'rgb(139,69,19)',
        hoverBackgroundColor: 'rgb(139,69,19)',
        hoverBorderColor: 'rgb(139,69,19)',
	      //stack: 'Stack 0',
	      data: []
	    },
    	{
	      label: 'Resistance / Weight Training',
	      backgroundColor: 'rgb(255, 105, 180)',
        hoverBackgroundColor: 'rgb(255, 105, 180)',
        hoverBorderColor: 'rgb(255, 105, 180)',
	      //stack: 'Stack 0',
	      data: []
	    }];

    sleepChartOptions = {
    	responsive: true,
    	showTooltips: false,
      title: {
          display: true,
          fontSize: 20,
          padding: 16,
          text: 'Sleep Record'
        },
	    scales: {
	        yAxes: [{
	          stacked: true,
	          ticks: {
	            beginAtZero: true
	          }
	        }],
	        xAxes: [{
	          stacked: true,
	          ticks: {
	            beginAtZero: true
	          }
	        }]
      	},
      	legend: {
            display: false
        }
    };

    sleepChartLabels =  [];

    sleepChartData:any = [
    	{
        label: 'Disturbed Sleep',
        backgroundColor: [],
        hoverBackgroundColor:[],
        borderColor: 'white',
        data: []
      }, 
      {
        label: 'Regular sleep',
        backgroundColor: [],
        hoverBackgroundColor:[],
        borderColor: 'white',
        data: []
      }, {
        label: 'Deep Sleep',
        backgroundColor: [],
        hoverBackgroundColor:[],
        borderColor: 'white',
        data: []
      }, {
        label: 'Awake',
        backgroundColor: [],
        hoverBackgroundColor:[],
        borderColor: 'white',
        data: []
      }];

    tag1Arr: any=[];
    tag2Arr: any=[];
    tag3Arr: any=[];
    tag4Arr: any=[];
    tag5Arr: any=[];
    tag6Arr: any=[];
    tag7Arr: any=[];
    tag8Arr: any=[];
    tag10Arr: any=[];
    tag11Arr: any=[];
    tag12Arr: any=[];
    medical_condition: any;
	  sunburstchart: any;
    private chartObj: any;
    private targetchartObj: any;

  constructor(private message:MessageService ,private global: Global,private authService: AuthService,
    private router: Router, private commonService: CommonService, config: NgbPopoverConfig,private route: ActivatedRoute) {
  	this.userid = this.route.snapshot.params['id'];
  	this.hasStatutory = false;
  	this.asyncInit();
    Chart.Tooltip.positioners.custom = function(elements, eventPosition) {
      /** @type {Chart.Tooltip} */
      var tooltip = this;
      return {
          x: eventPosition.x,
          y: eventPosition.y
      };
    }
   }

  ngOnInit() {
  	this.user_data = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')):'';
    if(this.user_data && this.user_data.user_statutatory && this.user_data.user_statutatory.is_agree ==1){
      this.hasStatutory = true;
    }else{
      this.hasStatutory = false;
    }

    this.commonService.checkaccessmodules.subscribe((data)=>{
      this.permissions = data.permissions;
      //console.log(this.permissions);
      //this.isLoaded = true;
    });
  }
/**
* Display user report questionnaire,meal,activity
*/
  asyncInit(){
  	this.commonService.show(this.global.apiUrl + '/api/user/report',this.userid)
  		.subscribe((data)=>{
          this.user_detail = data.user_detail;
  			  this.options.series  = data.radial_chart;
          this.userlast_qsnans = data.user_last_qsn_ans;
          this.medical_condition = data.medical_condition;
          if(typeof this.userlast_qsnans.HC1 != "undefined"){
            this.tag1Arr = Object.entries(this.userlast_qsnans.HC1);
          }
          
          if(typeof this.userlast_qsnans.HC2 != "undefined"){
            this.tag2Arr = Object.entries(this.userlast_qsnans.HC2);
          }

          if(typeof this.userlast_qsnans.HC3 != "undefined"){
            this.tag3Arr = Object.entries(this.userlast_qsnans.HC3);
          }

          if(typeof this.userlast_qsnans.HC4 != "undefined"){
            this.tag4Arr = Object.entries(this.userlast_qsnans.HC4);
          }

          if(typeof this.userlast_qsnans.HC5 != "undefined"){
            this.tag5Arr = Object.entries(this.userlast_qsnans.HC5);
          }

          if(typeof this.userlast_qsnans.HC6 != "undefined"){
            this.tag6Arr = Object.entries(this.userlast_qsnans.HC6);
          }

          if(typeof this.userlast_qsnans.HC7 != "undefined"){
            this.tag7Arr = Object.entries(this.userlast_qsnans.HC7);
          }

          if(typeof this.userlast_qsnans.HC8 != "undefined"){
            this.tag8Arr = Object.entries(this.userlast_qsnans.HC8);
          }

          if(typeof this.userlast_qsnans.HC10 != "undefined"){
            this.tag10Arr = Object.entries(this.userlast_qsnans.HC10);
          }

          if(typeof this.userlast_qsnans.HC11 != "undefined"){
            this.tag11Arr = Object.entries(this.userlast_qsnans.HC11);
          }

          if(typeof this.userlast_qsnans.HC12 != "undefined"){
            this.tag12Arr = Object.entries(this.userlast_qsnans.HC12);
          }

          this.targetoptions.xaxis.categories = data.usertargets[0];
	        this.targetoptions.series[0].data = data.usertargets[1];
	        this.targetoptions.series[1].data = data.usertargets[2];
	        this.targetoptions.series[2].data = data.usertargets[3];
	        this.targetoptions.series[3].data = data.usertargets[4];
	        this.fatmaslChartLabels = data.date_activities[0];
	        this.fatmaslChartData[0].data = data.energykcal;
	        this.fatmaslChartData[1].data = data.date_activities[7];
	        this.fatmaslChartData[2].data = data.date_activities[3];
	        this.personalcrossData = (data.profileanalytics.length !=0)?data.profileanalytics[0]:[];
	        this.personalcrossLabels = (data.profileanalytics.length !=0)?data.profileanalytics[2]:[];
          this.activityChartLabels = data.date_activities[0];
        	this.activityChartData[0].data = data.date_activities[1];
        	this.activityChartData[1].data = data.date_activities[2];
        	this.activityChartData[2].data = data.date_activities[6];
        	this.activityChartData[3].data = data.date_activities[10];
        	this.manualChartLabels = data.activity_ccr[0];
        	this.manualChartData[0].data = data.activity_ccr[1];
        	this.manualChartData[1].data = data.activity_ccr[2];
        	this.manualChartData[2].data = data.activity_ccr[3];
        	this.sleepChartLabels = data.date_sleep[0];
	        this.sleepChartData[0].data = data.date_sleep[1];
	        this.sleepChartData[1].data = data.date_sleep[2];
	        this.sleepChartData[2].data = data.date_sleep[3];
          this.sleepChartData[3].data = data.date_sleep[8];
	        this.sleepChartData[0].backgroundColor = data.date_sleep[4];
	        this.sleepChartData[1].backgroundColor = data.date_sleep[5];
	        this.sleepChartData[2].backgroundColor = data.date_sleep[6];
          this.sleepChartData[3].backgroundColor = data.date_sleep[9];
          this.sleepChartData[3].hoverBackgroundColor = data.date_sleep[9];

	        this.allnutrns = (data.allnutrns.length>0)?data.allnutrns[0]:[];
	        this.bardata.push(this.allnutrns.carbohydrate);
	        this.bardata.push(this.allnutrns.protein);
	        this.bardata.push(this.allnutrns.fat);
	        this.bardata.push(this.allnutrns.sugars);
	        this.bardata.push(this.allnutrns.fibre);
	        this.bardata.push(this.allnutrns.n3poly);
	        this.bardata.push(this.allnutrns.calcium);
	        this.bardata.push(this.allnutrns.magnesium);
	        this.bardata.push(this.allnutrns.iron);
	        this.bardata.push(this.allnutrns.zinc);
	        this.bardata.push(this.allnutrns.selenium);
	        this.bardata.push(this.allnutrns.iodine);
	        this.bardata.push(this.allnutrns.sodium);
	        this.bardata.push(this.allnutrns.vita);
	        this.bardata.push(this.allnutrns.vitd);
	        this.bardata.push(this.allnutrns.vite);
	        this.bardata.push(this.allnutrns.vitk);
	        this.bardata.push(this.allnutrns.thiamin);
	        this.bardata.push(this.allnutrns.riboflavin);
	        this.bardata.push(this.allnutrns.niacineqv);
	        this.bardata.push(this.allnutrns.pantothenate);
	        this.bardata.push(this.allnutrns.vitb6);
	        this.bardata.push(this.allnutrns.folate);
	        this.bardata.push(this.allnutrns.vitb12);
	        this.bardata.push(this.allnutrns.biotin);
	        this.bardata.push(this.allnutrns.vitc);
	        this.pieChartData[0].data = this.bardata;
	        this.topChartLabels = data.mealnutrients[0];
	        this.topChartData[0].data = data.mealnutrients[1];
	        this.topChartData[1].data = data.mealnutrients[2];
	        this.topChartData[2].data = data.mealnutrients[3];
	        this.topChartData[3].data = data.mealnutrients[4];
  			  this.chartObj  = new ApexCharts(
            document.querySelector("#chart"),
            this.options
	        );

	        this.targetchartObj  = new ApexCharts(
	          document.querySelector("#targetchart"),
	          this.targetoptions
	        );

	        this.render();
	        this.targetrender();
  		}, (error)=>{

  		});

      this.commonService.show(this.global.apiUrl + '/api/user/food-diary',this.userid)
      .subscribe((data)=>{
          this.meals = data.user_meals;
          this.totalmeals = data.totalmeals;
        }, (error)=>{

      });
  }

  render(): Promise<void> {
    return this.chartObj.render();
  }

  targetrender(): Promise<void> {
    return this.targetchartObj.render();
  }

  /**
* Food list pagination
*/
  viewMore(){
      let limit = 10 + this.meals.length;
      this.commonService.getAll(this.global.apiUrl + '/api/user/food-diary/'+this.userid+'?limit='+ Number(limit))
        .subscribe((data)=>{
          this.meals = data.user_meals;
          this.totalmeals = data.totalmeals;
        }, (error)=>{});
    }

}
