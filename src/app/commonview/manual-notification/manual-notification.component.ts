import { Component, OnInit } from '@angular/core';
import {Global} from '../../global';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart  } from '@angular/router';

@Component({
  selector: 'app-manual-notification',
  templateUrl: './manual-notification.component.html',
  styleUrls: ['./manual-notification.component.css']
})
export class ManualNotificationComponent implements OnInit {
	notifyform:FormGroup;
	ServerValidation: any;
	groups: any=[];
  constructor(private global: Global,private authService: AuthService,private router: Router,private fb:FormBuilder,private commonService:CommonService,private message:MessageService ) {
  	this.asyncInit();
   }

  	ngOnInit() {
	  	this.notifyform = this.fb.group({
	  		group:[''],
			  notify_msg:[''],
        action_url:['']
	  	});

	  	this.notifyform.controls['group'].setValue('all');
  	}

  	asyncInit(){
  		this.commonService.getAll(this.global.apiUrl + '/api/hc/group/list')
	  		.subscribe((data)=>{
	  			this.groups = data.data;
	  		}, (error)=>{});
	}

  sendNotification(){
  	this.commonService.create(this.global.apiUrl + '/api/manual/notification',this.notifyform.value)
      .subscribe((data)=>{
        if(data.status ===200){
          this.message.success(data.status_text);
          this.notifyform.controls['group'].setValue('all');
          this.notifyform.controls['notify_msg'].setValue('');
          this.notifyform.controls['action_url'].setValue('');
          this.ServerValidation = [];
        }

        if(data.status ===422){
        	this.ServerValidation = data.error;
        }
      },(error)=>{});
  }

}
