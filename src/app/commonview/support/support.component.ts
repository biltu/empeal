import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart  } from '@angular/router';
declare var swal: any;

@Component({
  selector: 'app-support',
  templateUrl: './support.component.html',
  styleUrls: ['./support.component.css']
})
export class SupportComponent implements OnInit {

  supportForm: FormGroup;
  validationError: any; 
  user:any={}; 
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
    private router: Router, private commonService: CommonService) {

  	this.user = localStorage.getItem('user') ?JSON.parse(localStorage.getItem('user')):'';
    }

  ngOnInit() {
  	this.supportForm = this.fb.group({
  		f_name:[''],
  		l_name:[''],
  		email:[''],
  		mobile:[''],
  		support_type:[''],
  		support_description:['']
  	});
  	this.supportForm.patchValue({
  		f_name:this.user.user_detail.firstname,
  		l_name:this.user.user_detail.lastname,
  		email:this.user.email,
  		mobile:this.user.user_detail.mobile,
  	});
  }

  get sp_f() { return this.supportForm.controls; }

/**
* Create Support
*/
  create(){
  	this.validationError ='';
  	this.commonService.create(this.global.apiUrl + '/api/support',this.supportForm.value)
  		.subscribe(data=>{
  			if(data.status ==200){
  				this.resetForm();
  				this.message.success(data.status_text);
  			}else if(data.status ==422){
  				this.validationError = data.error;
  				console.log(data);
  			}
  		},error=>{});
  }

/**
* Reset support form
*/
  resetForm(){
  	this.supportForm.get('support_type').setValue('');
  	this.supportForm.get('support_description').setValue('');
  }

}
