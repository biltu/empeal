import { Component, OnInit } from '@angular/core';
import {Global} from '../../global';
import {NgbPopoverConfig} from '@ng-bootstrap/ng-bootstrap';
import {AuthService} from '../../services/auth.service';
import {CommonService} from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart  } from '@angular/router';

@Component({
  selector: 'app-group-dashboard',
  templateUrl: './group-dashboard.component.html',
  styleUrls: ['./group-dashboard.component.css']
})
export class GroupDashboardComponent implements OnInit {
	corporate_id: string;
	dashboard:any={};
  	group_det: any=[];
  	heal_coach_det: any=[];
  constructor(private message:MessageService ,private global: Global,private authService: AuthService,
   private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) 
  { 
  	this.corporate_id = this.activeRoute.snapshot.paramMap.get("id");
  	this.asyncInit();
  }

  ngOnInit() {
  }

/**
* Get Corporate dashborad detail data
*/
  asyncInit(){
  	this.commonService.getAll(this.global.apiUrl + '/api/corporate/dashboard?id='+this.corporate_id)
  		.subscribe((data)=>{
  			this.dashboard = data.data;
        this.group_det = data.data.groups.group_detail;
        this.heal_coach_det = data.data.groups.health_coach;
  			console.log(data);
  		}, (error)=>{

  		});
  }

/**
* Check percentage
*/
  getPercentage(percentage){
    return percentage ? percentage: 0;
  }

}
