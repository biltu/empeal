import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WearableCallbackComponent } from './wearable-callback.component';

describe('WearableCallbackComponent', () => {
  let component: WearableCallbackComponent;
  let fixture: ComponentFixture<WearableCallbackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WearableCallbackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WearableCallbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
