import { Component, OnInit } from '@angular/core';
import {Global} from '../../global';
import {NgbPopoverConfig} from '@ng-bootstrap/ng-bootstrap';
import {AuthService} from '../../services/auth.service';
import {CommonService} from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart  } from '@angular/router';

@Component({
  selector: 'app-wearable-callback',
  templateUrl: './wearable-callback.component.html',
  styleUrls: ['./wearable-callback.component.css']
})
export class WearableCallbackComponent implements OnInit {

  constructor(private message:MessageService ,private global: Global,private authService: AuthService,
   private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) { 
  	this.asyncInit();
  }

  ngOnInit() {
  }

/**
* get wearable response data
*/

  asyncInit(){
  	this.commonService.getAll(this.global.apiUrl + '/api/wearable/response')
  		.subscribe((data)=>{
  			this.message = data.status_text;
  		}, (error)=>{
  		});
  }

}
