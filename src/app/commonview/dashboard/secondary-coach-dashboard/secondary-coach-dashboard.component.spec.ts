import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecondaryCoachDashboardComponent } from './secondary-coach-dashboard.component';

describe('SecondaryCoachDashboardComponent', () => {
  let component: SecondaryCoachDashboardComponent;
  let fixture: ComponentFixture<SecondaryCoachDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecondaryCoachDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecondaryCoachDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
