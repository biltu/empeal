import { Component, OnInit } from '@angular/core';
import {Global} from '../../../global';
import {AuthService} from '../../../services/auth.service';
import { CommonService } from '../../../services/common.service';
import {MessageService} from '../../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart  } from '@angular/router';
@Component({
  selector: 'app-secondary-coach-dashboard',
  templateUrl: './secondary-coach-dashboard.component.html',
  styleUrls: ['./secondary-coach-dashboard.component.css']
})
export class SecondaryCoachDashboardComponent implements OnInit {
	corporates: any=[];
  appointments: any=[];
  constructor(private message:MessageService ,private global: Global,private authService: AuthService,
   private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) { 
  	this.asyncInit();
  }

	ngOnInit() {
    this.commonService.currentAppointment.subscribe((data)=>{
      this.appointments = data.appointments;
    });
	}

/**
* Get health coach groups
*/
  	asyncInit(){
  		this.commonService.getAll(this.global.apiUrl + '/api/hc/group/list')
	  		.subscribe((data)=>{
	  			this.corporates = data.data;
	  		}, (error)=>{});
  	}

}
