import { Component, OnInit } from '@angular/core';
import {Global} from '../../../global';
import {NgbPopoverConfig} from '@ng-bootstrap/ng-bootstrap';
import {AuthService} from '../../../services/auth.service';
import {CommonService} from '../../../services/common.service';
import {MessageService} from '../../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart  } from '@angular/router';
declare var ApexCharts: any;
declare var $:any;

@Component({
  selector: 'app-corporate-dashboard',
  templateUrl: './corporate-dashboard.component.html',
  styleUrls: ['./corporate-dashboard.component.css'],
  providers: [NgbPopoverConfig]
})
export class CorporateDashboardComponent implements OnInit {
  isLoaded: boolean =false;
	dashboard:any={};
  group_det: any;
  heal_coach_det: any=[];
  user_data: any;
  groups: any=[];
  current_month: number;
  pointchartkey: any;
  pointchartdata: any;
  medicalchartkey: any=[];
  medicalchartdata: any;
  goalchartkey: any=[];
  goalchartdata: any;
  teamengagementkey: any=[];
  teamengagementdata: any;
  selectTeam: String ='';
  percentageData :any={
    present:'',
    absent:''
  };
  corporate_teams: any=[];
  permissions: any=[];
  param: number;
  pieChartOptions = {
        responsive: false,
        maintainAspectRatio: false,
        legend: {
            display: false,
          },
          title: {
            display: true,
            fontSize: 20,
            padding: 16,
            text: 'Health Assessment Footprint'
          },
          scales: {
            yAxes: [{
              ticks: {
                display: false
              }
            }],
            xAxes: [{
              ticks: {
                display: false
                //fontSize: 11
              }
            }]
          }
    };
    pieChartLabels =  [];
    medicaloptions = {
            chart: {
              height: 0,
              type: 'bar',
              stacked: false,
              toolbar: {
                show: false
              },
              zoom: {
                enabled: true
              }
            },
            yaxis: {
              show: false,
            },
            tooltip: {
              enabled: true,
              y: {
                title: {
                    formatter: (seriesName) => "Users",
                },
                formatter: (value) => { return value },
              }
            },
            title: {
                text: 'Employee Medical Issues Trends',
                align: 'center',
                margin: 10,
                offsetX: 0,
                offsetY: 0,
                floating: false,
                style: {
                  fontSize:  '16px',
                  color:  '#263238'
                },
            },

            responsive: [{
                breakpoint: 480,
                options: {
                  legend: {
                    position: 'bottom',
                    offsetX: -10,
                    offsetY: 0
                  }
                }
            }],
            plotOptions: {
              bar: {
                horizontal: false,
              },
            },
            series: [{data:[]}],
            xaxis: {
                categories: [],
            },
            legend: {
                position: 'bottom',
                horizontalAlign: 'center',
                //offsetY: 40
            },
            fill: {
                opacity: 1
            }
        };

    goaloptions = {
        chart: {
            height: 0,
            type: 'bar',
            stacked: false,
            toolbar: {
                show: false
            },
            zoom: {
                enabled: true
            }
        },
        yaxis: {
              show: false,
            },
        tooltip: {
              enabled: true,
              y: {
                title: {
                    formatter: (seriesName) => "Users",
                },
                formatter: (value) => { return value },
              }
            },
        title: {
                text: 'Employee Lifestyle Goals Trends',
                align: 'center',
                margin: 10,
                offsetX: 0,
                offsetY: 0,
                floating: false,
                style: {
                  fontSize:  '16px',
                  color:  '#263238'
                },
            },
        responsive: [{
            breakpoint: 480,
            options: {
                legend: {
                    position: 'bottom',
                    offsetX: -10,
                    offsetY: 0
                }
            }
        }],
        plotOptions: {
            bar: {
                horizontal: false,
            },
        },
        series: [{data:[]}],
        xaxis: {
            categories: [],
        },
        legend: {
            position: 'bottom',
            horizontalAlign: 'center',
            //offsetY: 40
        },
        fill: {
            opacity: 1
        }
    };

    teamengageoptions = {
      chart: {
          height: 400,
          type: 'bar',
          stacked: false,
          toolbar: {
              show: false
          },
          zoom: {
              enabled: true
          }
      },
      yaxis: {
        show: false,
      },
      tooltip: {
        enabled: true,
        y: {
          title: {
              formatter: (seriesName) => "Users",
          },
          formatter: (value) => { return value },
        }
      },title: {
          text: 'Employee Engagement Tracker',
          align: 'center',
          margin: 10,
          offsetX: 0,
          offsetY: 0,
          floating: false,
          style: {
            fontSize:  '16px',
            color:  '#263238'
          },
      },
      responsive: [{
          breakpoint: 480,
          options: {
              legend: {
                  position: 'bottom',
                  offsetX: -10,
                  offsetY: 0
              }
          }
      }],
      plotOptions: {
          bar: {
              horizontal: false,
          },
      },

      series: [{data:[]}],
      xaxis: {
          categories: [],
      },
      legend: {
          position: 'bottom',
          horizontalAlign: 'center',
          //offsetY: 40
      },
      fill: {
          opacity: 1
      }
    };

    teampointoptions = {
      chart: {
          height: 400,
          type: 'bar',
          stacked: true,
          toolbar: {
              show: false
          },
          zoom: {
              enabled: true
          }
      },title: {
          text: 'Employee Points Table',
          align: 'center',
          margin: 10,
          offsetX: 0,
          offsetY: 0,
          floating: false,
          style: {
            fontSize:  '16px',
            color:  '#263238'
          },
      },
      responsive: [{
          breakpoint: 480,
          options: {
              legend: {
                  position: 'bottom',
                  offsetX: -10,
                  offsetY: 0
              }
          }
      }],
      plotOptions: {
          bar: {
              horizontal: true,
          },
      },
      series: [{
            name: "Campaign",
            data: []
        }, {
            name: "Daily Targets",
            data: []
        }, {
            name: "Social Interactions",
            data: []
        }],
      xaxis: {
          categories: [],
      },
      legend: {
          position: 'bottom',
          horizontalAlign: 'center',
          //offsetY: 40
      },
      fill: {
          opacity: 1
      }
    }
    // CHART COLOR.
    pieChartData:any = [{
      backgroundColor:[],
      data: [],
      hoverBackgroundColor: [],
    }];
    contestreport: any=[];
    contestname: any;
    isteamid: any;
    mdconditionempty:string;
    goalempty:string;

  constructor(private message:MessageService ,private global: Global,private authService: AuthService,
    private router: Router, private commonService: CommonService, config: NgbPopoverConfig, private activeRoute: ActivatedRoute) {
    config.placement = 'right';
    config.triggers = 'hover';
    this.mdconditionempty='none';
    this.goalempty='none';

    this.user_data = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')):'';
    this.param = this.activeRoute.snapshot.params['id']? this.activeRoute.snapshot.params['id']:'';
    this.isteamid = localStorage.getItem('teamid')?localStorage.getItem('teamid'):'';
    if(this.isteamid !=''){
      this.selectTeam = this.isteamid ? this.isteamid: '';
      this.changeTeam(this.selectTeam);
    }else{
      this.asyncInit();
    }
    // this.router.events.subscribe((e: any) => {
    //  // If it is a NavigationEnd event re-initalise the component
    //  if (e instanceof NavigationEnd) {
    //    //this.selectTeam = this.activeRoute.snapshot.queryParams['team_id']? this.activeRoute.snapshot.queryParams['team_id']:'';
    //    this.asyncInit();
    //  }
   //});
  }

  ngOnInit() {
    var d = new Date();
    this.current_month= d.getMonth();
    this.commonService.checkaccessmodules.subscribe((data)=>{
      this.permissions = data.permissions;
      //console.log(this.permissions);
      //this.isLoaded = true;
    });
  }

  asyncInit(){
    let url = '';
    if(this.param){
      if(this.isteamid !=''){
        url = '/api/corporate/dashboard?id=' + this.param + '&team=' + this.isteamid;
      }else{
       url = '/api/corporate/dashboard?id=' + this.param;       
     }
    }else{
      if(this.isteamid!=''){
        url = '/api/corporate/dashboard?team=' + this.isteamid;
      }else{
         url = '/api/corporate/dashboard';
      }
     
    }

  	this.commonService.getAll(this.global.apiUrl + url)
  		.subscribe((data)=>{
        this.isLoaded = true;
  			this.dashboard = data.data;
        this.group_det = (data.data.groups.length!= 0)?data.data.groups.group_detail[0].id:0;
        this.groups = data.data.groups.group_detail;
        this.heal_coach_det = data.data.groups.health_coach;
        
        this.medicaloptions.chart.height = data.data.mdconditionskey!=''?400:100;
        this.medicaloptions.xaxis.categories = data.data.mdconditionskey!=''?data.data.mdconditionskey:[];
        this.medicaloptions.series[0].data = data.data.mdconditionskey!=''?data.data.mdconditionsvalue:'';
        this.goaloptions.chart.height = data.data.goalskey!=''?400:100;
        this.goaloptions.xaxis.categories = data.data.goalskey!=''?data.data.goalskey:[];
        this.goaloptions.series[0].data = data.data.goalskey!=''?data.data.goalsvalue:'';
        this.teampointoptions.xaxis.categories = data.data.maxpointinteam[0];
        this.pointchartdata = data.data.maxpointinteam[1];
        this.teampointoptions.series[0].data = this.pointchartdata.length > 0 ?this.pointchartdata[0].data:[];
        this.teampointoptions.series[1].data = this.pointchartdata.length > 0 ?this.pointchartdata[1].data:[];
        this.teampointoptions.series[2].data = this.pointchartdata.length > 0 ?this.pointchartdata[2].data:[];
        this.teamengageoptions.xaxis.categories = data.data.teamengagement[0];
        this.teamengagementkey = data.data.teamengagement[0];
        this.teamengageoptions.series[0].data = data.data.teamengagement[1];
        this.pieChartData[0].backgroundColor = data.data.teamhelthreport[1];
        this.pieChartData[0].data = data.data.teamhelthreport[0];
        this.pieChartData[0].hoverBackgroundColor = data.data.teamhelthreport[1];
        this.pieChartLabels = data.data.teamhelthreport[2];
        this.corporate_teams = data.data.corporate_team;
        setTimeout(() => {
          this.teampointchart();
          this.medicalchart();
          this.teamgoalchart();
          this.teamengagementchart();
        },600);

        if(data.data.mdconditionskey!=''){
            this.mdconditionempty='none';
        }else{
            this.mdconditionempty='block';
        }

        if(data.data.goalskey!=''){
            this.goalempty='none';
        }else{
            this.goalempty='block';
        }

        this.commonService.getAll(this.global.apiUrl + '/api/hr/contest/report?id=' + this.param+ '&team=' +this.isteamid)
         .subscribe((cdata)=>{
          this.contestreport = Object.entries(cdata.data);
          this.contestreport.forEach((report,i) => {
          //this.contestname  = report[1].contest_name;
          setTimeout(() => {
          if( Object.entries(report[1].groupchart_name).map(data=>data[1]).length > 1 ){
            this.groupWisebarchart(i,report[1].groupchart_data,report[1].groupchart_name,report[1].unit);
          }
          },200);
          });
        }, (error)=>{

          });
  		}, (error)=>{

  		});
  }

 
  groupWisebarchart(id,groupchart_data,groupchart_name,unit){
    var options = {
          chart: {
            height: 200,
            type: 'bar',
            toolbar: {
            show: false
            }
          },
          yaxis: {
            show: false,
          },
          tooltip: {
            enabled: true,
            y: {
              title: {
                formatter: (seriesName) => "",
              },
              formatter: (value) => { return value>1?value + ' ' + unit+'s':value + ' ' + unit },
              show: true,
            }
          },
          plotOptions: {
              bar: {
                dataLabels: {
                  position: "top" // top, center, bottom
                }
              }
          },
          dataLabels: {
              enabled: true,
              formatter: function(val) {
                return val>1?val + ' ' + unit+'s':val + ' ' + unit;
              },
              offsetY: -20,
              style: {
                fontSize: "12px",
                colors: ["#304758"]
              }
          },
          series: [{
            data: Object.entries(groupchart_data).map(data=>data[1])
          }],
          stroke: {
            show: true,
            width: 1,
            colors: ['#fff']
          },
          xaxis: {
            categories: Object.entries(groupchart_name).map(data=>data[1]),
          }
      }

     var chart = new ApexCharts(
        document.querySelector("#groupbarchart"+id),
        options
      );
    if($("#groupbarchart"+id).length>0){
      chart.render();
      chart.resetSeries();          
    }
  }
  medicalchart(){        
    if($("#medicalchart").length >0){
      var chart = new ApexCharts(
        document.querySelector("#medicalchart"),
        this.medicaloptions
    );
      chart.render();
      chart.resetSeries();          
    }else{

    }
  }

  teamgoalchart(){
    if($("#teamgoalchart").length >0){
      var chart = new ApexCharts(
        document.querySelector("#teamgoalchart"),
        this.goaloptions
      );
      chart.render();
      chart.resetSeries();          
    }
  }

  teamengagementchart(){
        if($("#teamengagechart").length > 0){
          var chart = new ApexCharts(
            document.querySelector("#teamengagechart"),
            this.teamengageoptions
          );
          chart.render();
          chart.resetSeries();          
        }
  }

  teampointchart(){
        if($("#teampointchart").length >0){
          var chart = new ApexCharts(
            document.querySelector("#teampointchart"),
            this.teampointoptions
          );
          chart.render();
          chart.resetSeries();          
        }

  }

  getPercentage(percentage){
    return percentage ? percentage: 0;
  }

  logOut(){
       this.authService.logOut(this.global.apiUrl + '/api/logout').subscribe((data)=>{
       if(data.status ==200){
         localStorage.removeItem('token');
         localStorage.removeItem('user');
         this.router.navigate(['login']);
       }
     });
  }

  changeTeam(value){
    localStorage.setItem('teamid',value);
    this.isteamid = localStorage.getItem('teamid')?localStorage.getItem('teamid'):'';
   this.asyncInit();
  }

  redirectTo(uri) {
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() =>
    this.router.navigate([uri]));
  }

}
