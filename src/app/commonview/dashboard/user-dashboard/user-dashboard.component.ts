import { Component, OnInit, ElementRef } from '@angular/core';
import {Global} from '../../../global';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {NgbPopoverConfig} from '@ng-bootstrap/ng-bootstrap';
import {AuthService} from '../../../services/auth.service';
import {CommonService} from '../../../services/common.service';
import {MessageService} from '../../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart  } from '@angular/router';
import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexXAxis,
  ApexTitleSubtitle,
  ApexPlotOptions,
  ApexDataLabels,
  ApexYAxis,
  ApexTooltip,
  ApexLegend,
  ApexStroke,
  ApexFill,
  ApexResponsive,
  ApexGrid,
  ApexStates,
  ApexTheme

} from "ng-apexcharts";

export type aChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  yaxis: ApexYAxis | ApexYAxis[];
  title: ApexTitleSubtitle;
  colors: string[];
  dataLabels: ApexDataLabels;
  stroke: ApexStroke;
  labels: string[];
  legend: ApexLegend;
  fill: ApexFill;
  tooltip: ApexTooltip;
  plotOptions: ApexPlotOptions;
  responsive: ApexResponsive[];
  grid: ApexGrid;
  states: ApexStates;
  subtitle: ApexTitleSubtitle;
  theme: ApexTheme;
};
declare var ApexCharts: any;
declare var $;

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.css'],
  providers: [NgbPopoverConfig]
})
export class UserDashboardComponent implements OnInit {
  user_data: any={};
  hasStatutory: boolean;
  statutaries :FormGroup;
	dashboard:any={};
  group_det: any=[];
  heal_coach_det: any=[];
  today_targets: any=[];
  appointments: any=[];
  checkedModel: any={};
  radialchartoption: any={};
  submitted: boolean=false;
  termOfAgree: boolean=false;
  barcolor: any=[];
  bardata: any=[];
  showElement:boolean=false;
  setValue: any=[];
  isloaded:boolean=false;
  permissions: any=[];
  notificationtoken:any;
  targetoptions: any= {};

    fatmaslChartOptions = {
      //responsive: true,
      maintainAspectRatio: false,
      title: {
        display: true,
        text: ''
      },
      legend: {
          display: true,
          position: 'bottom'
      },
      tooltips: {
        mode: 'index',
        intersect: true
      },
      scales: {
        xAxes: [{
        display: false,
        id: "main-x",
        categoryPercentage: 1,
        stacked: true
      }, {
        display: false,
        id: "sub-x",
        type: 'category',
        categoryPercentage: 0.8,
        barPercentage: 1,
        stacked: true
      }],
        yAxes: [{
          id: 'A',
          type: 'linear',
          position: 'left'
        }, {
          id: 'B',
          type: 'linear',
          position: 'right',
          ticks: {
            min: 0
          },
          gridLines: {
              display:false
          } 
        }]
      }
    };
    fatmaslChartLabels =  [];

    fatmaslChartData:any = [{
      label: 'Calories Consumed',
      backgroundColor: 'rgba(255, 127, 15, 0.2)',
      hoverBackgroundColor: 'rgba(255, 127, 15, 0.2)',
      data: [],
      borderWidth: 2,
      borderColor: 'red',
      yAxisID: 'A',
      xAxisID: "sub-x",
      fill: false
    }, {
      label: 'Calories Burned',
      backgroundColor: 'blue',
      hoverBackgroundColor: 'blue',
      data: [],
      borderWidth: 2,
      yAxisID: 'A',
      xAxisID: "sub-x",
      fill: false
    },
    
    {
      type: 'line',
      label: 'Activity Minutes',
      backgroundColor: 'purple',
      borderColor: 'purple',
      hoverBackgroundColor: 'purple',
      fill: false,
      data: [],
      yAxisID: 'B'
    }];

    pieChartOptions = {
        responsive: false,
        maintainAspectRatio: false,
        tooltips: {
            // callbacks: {
            //     label: function(tooltipItem) {
            //         return "$" + Number(tooltipItem.yLabel) + " and so worth it !";
            //     }
            // },
            callbacks: {
                label: function(tooltipItem, data) {
                    var dataset = data.datasets[tooltipItem.datasetIndex];
                    var index = tooltipItem.index;
                    return dataset.labels[index] + ': ' + dataset.data[index];
                }
            },
            displayColors: false

        },
        legend: {
                display: false,
            },
            title: {
                display: true,
                fontSize: 14,
                padding: 16,
                text: 'Personal Cross Profile Analysis'
            },
            "scales": {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        steps: 20,
                        stepSize: 20,
                        stepValue: 20,
                        max: 50,
                    }
                }],
                xAxes: [{
                    ticks: {
                        autoSkip: false,
                        //fontSize: 11
                    }
                }]
            }
    };
    pieChartLabels:any =  [];
    // CHART COLOR.
    pieChartData:any = [];


    private chartObj: any;
    private targetchartObj: any;
    singlecontest: any=[];
    radial_chart: any=[];
  constructor(private elementRef:ElementRef, private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
    private router: Router, private commonService: CommonService, config: NgbPopoverConfig) {
    this.user_data = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')):'';
    this.notificationtoken = localStorage.getItem('userpushtoken');
    this.hasStatutory = false;
    config.placement = 'right';
    config.triggers = 'hover'; 
  	this.asyncInit();
    let that = this;
    $(document).on('click','.contestJoinProcess', function(e){
      that.contestUpdate($(this).data('cnid'),$(this).val());
    });
    $(document).on('click','.epastcontest', function(e){
       let pcid = $(this).data('pcid');
       $('.pastcontest'+pcid).show();
    });
    if(localStorage.getItem('contestStatus-set')){
      let constData :any = localStorage.getItem('contestStatus-set')? JSON.parse(localStorage.getItem('contestStatus-set')):'';
      this.commonService.update(this.global.apiUrl + '/api/user/contest/join', constData.contest_id,{status:constData.status})
          .subscribe(data=>{
            if(data.status ==200){
              this.message.success(data.status_text);
              localStorage.removeItem('contestStatus-set');
              this.redirectTo(this.router.url);  
            }
          },error=>{});
    }

  }

  ngOnInit() {
    this.statutaries = this.fb.group({
      is_agree:['', [Validators.required]]
    });

    this.commonService.currentAppointment.subscribe((data)=>{
      this.appointments = data.appointments;
    });
    
    if(this.user_data && this.user_data.user_statutatory && this.user_data.user_statutatory.is_agree ==1){
      this.hasStatutory = true;
    }else{
      this.hasStatutory = false;
    }

    this.statutaries.get('is_agree').valueChanges.subscribe((value)=>{
      this.termOfAgree = value;
    });

    this.commonService.checkDashboardLoad.subscribe(data=>{
      if(data.contest){
        let setTime = data.url =='/dashboard'?200:1000;
            setTimeout(() => {
              this.openContest(data.contest);
            }, setTime);
      }
      
    });

    this.commonService.checkaccessmodules.subscribe((data)=>{
      this.permissions = data.permissions;
    });

  }
/**
* Display open contest
*/
    openContest(data){
      if(this.isloaded){
        this.singlecontest = data;
        let divclass = '';
        if(this.singlecontest[0].is_joined==0){
          divclass = 'upcomingCon';
        }else if(this.singlecontest[0].is_joined==1){
          divclass = 'accepetedcon';
        }else if(this.singlecontest[0].is_joined==2){
          divclass = 'rejectedCon';
        }else{
           divclass = '';
        }
        var html='<div class="contentOuter contestResult '+divclass+'"><a href="javascript:void(0);" class="remove contestrmv" data-contestid="'+this.singlecontest[0].id+'"><img src="assets/images/cross.png" alt="cross"></a><div class="contestHead"><div class="row"><div class="col-lg-9 col-md-9 col-sm-9 col-12"><p>'+this.singlecontest[0].question.question+'</p></div><div class="col-lg-3 col-md-3 col-sm-3 col-12"><div class="contentRight radioOuter contentRightSec"><label><input type="radio" name="isjoin" value="1" class="contestJoinProcess" data-cnid="'+this.singlecontest[0].id+'"><span>Accept</span></label><label><input type="radio" name="isjoin" value="2" class="contestJoinProcess" data-cnid="'+this.singlecontest[0].id+'"><span>Reject</span></label></div></div></div></div></div>';
        var node = document.createElement("div");
        node.className = 'col-lg-12 col-md-12 col-sm-12 col-12 contest'+this.singlecontest[0].id;
        //node.innerHTML = html;
        document.getElementById("contestsection").appendChild(node);
        document.getElementsByClassName('contest'+this.singlecontest[0].id)[0].innerHTML = html; 
      }
  }

/**
* Get user contest report and user dashboard data
*/
  asyncInit(){
  	this.commonService.getAll(this.global.apiUrl + '/api/user/dashboard')
  		.subscribe((data)=>{        
  			this.dashboard = data.data;
        this.group_det = data.data.groups.group_detail;
        this.heal_coach_det = data.data.groups.health_coach;
        this.today_targets = data.data.today_targets;
        this.radial_chart = (data !="")?data.radial_chart:[];
        this.radialchart(this.radial_chart);
        this.dashboardtargetchart(data.usertargets[0],data.usertargets[1],data.usertargets[2],data.usertargets[3],data.usertargets[4]);
        this.fatmaslChartLabels = data.date_activities[0];
        this.fatmaslChartData[0].data = data.data.energykcal;
        this.fatmaslChartData[1].data = data.date_activities[7];
        this.fatmaslChartData[2].data = data.date_activities[3];
        this.pieChartData = data.data.profileanalytics[0];
        this.pieChartLabels = data.data.profileanalytics[2];
        this.commonService.callUpdatePoint(data.user_points);
  		}, (error)=>{
          if(error.status == 401 && error.statusText=="OK" && error.error.message =="Unauthenticated."){
          localStorage.removeItem('token');
          localStorage.removeItem('user');
          localStorage.removeItem('userpushtoken');
          localStorage.removeItem('conversationId');
          localStorage.removeItem('teamid');
          this.router.navigate(['login']);
          window.location.href = this.global.domain_url +"/login";
        }
  		});

      this.commonService.getAll(this.global.apiUrl + '/api/user/contest/report')
        .subscribe(res=>{
         //this.setValue =Object.entries(res.data);
          this.isloaded = true;
      }, (error)=>{

      });
  }

  // render(): Promise<void> {
  //   return this.chartObj.render();
  // }

  // targetrender(): Promise<void> {
  //   return this.targetchartObj.render();
  // }

  getPercentage(percentage){
    return percentage ? percentage: 0;
  }
/**
* Update target answer
*/
  changeStatus(e,id,i){
    let ans = e.target.value;
    let data = {ans:ans};
    this.commonService.update(this.global.apiUrl + '/api/user/target/answer',id,data)
      .subscribe((data)=>{
        if(data.status ==200){
          this.message.success(data.status_text);
          this.today_targets = [];
          this.commonService.callUpdatePoint(data.user_points);
        }
      }, (error)=>{});
  }

  get si_f() { return this.statutaries.controls; }
/**
* Store Terms & condition and Privacy policy
*/
  storeData(){
    this.submitted = true;
    if(this.statutaries.invalid){
      return;
    }else if(this.statutaries.value.is_agree !=true){
      return false;
    }else if(!this.termOfAgree){
      return false;
    }
    this.commonService.create(this.global.apiUrl + '/api/user-statutory-info/update', this.statutaries.value)
      .subscribe((data)=>{
        if(data.status ==200){
          this.message.success(data.status_text);
            localStorage.setItem('user', JSON.stringify(data.data));
            this.router.navigate(['profile']);
        }
        else if(500){
              this.message.error(data.status_text);
          }
      }, (error)=>{});
  }
/**
* Join a contest
*/
  contestUpdate(id,val){
    let data = {status:val};
    this.commonService.update(this.global.apiUrl + '/api/user/contest/join',id,data)
      .subscribe((data)=>{
        if(data.status ==200){
          this.message.success(data.status_text);
          this.redirectTo(this.router.url);  
        }
        else if(500){
              this.message.error(data.status_text);
          }
      }, (error)=>{});  
  }

  redirectTo(uri) {
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() =>
    this.router.navigate([uri]));
  }
/**
* Store push token
*/
  pushTokenUpdate(value){
    this.commonService.create(this.global.apiUrl + '/api/user/push-token/update', {push_token:value})
      .subscribe(data=>{
        if(data.status ==200){
        }  
      },error=>{});
  }

  radialchart(data){
    this.radialchartoption = {
      chart: {
          height: 280,
          type: 'radialBar',
      },
      plotOptions: {
        radialBar: {
          offsetY: -10,
          startAngle: 0,
          endAngle: 270,
          hollow: {
            margin: 5,
            size: '30%',
            background: 'transparent',
            image: undefined,
          },
          dataLabels: {
            name: {
              show: false,
            },
            value: {
              show: false,
            }
          }
        }
      },
      series: data,

      labels: ['Nutrition', 'Fitness', 'Behavioural', 'Stress'],
      legend: {
      show: true,
      floating: true,
      fontSize: '14px',
      position: 'left',
      offsetX: 100,
      offsetY: 0,
      labels: {
        useSeriesColors: true,
      },
      markers: {
        size: 0
      },
      formatter: function(seriesName, opts) {
        if(seriesName=='Stress'){
          return seriesName + " <span style='font-size:10px;'>(Lower is better)</span>:  " + (opts.w.globals.series[opts.seriesIndex]/10).toFixed(1)
        }else{
          return seriesName + ":  " + (opts.w.globals.series[opts.seriesIndex]/10).toFixed(1)
        }
      },
      itemMargin: {
        horizontal: 1,
      }
    },
    responsive: [{
        breakpoint: 480,
        options: {
          legend: {
            show: true,
            floating: true,
            fontSize: '12px',
            position: 'left',
            offsetX: -50,
            offsetY: 0,
            labels: {
              useSeriesColors: true,
            },
            markers: {
              size: 0
            },
            formatter: function(seriesName, opts) {
              if(seriesName=='Stress'){
                return seriesName + " <span style='font-size:10px;'>(Lower is better)</span>:  " + (opts.w.globals.series[opts.seriesIndex]/10).toFixed(1)
              }else{
                return seriesName + ":  " + (opts.w.globals.series[opts.seriesIndex]/10).toFixed(1)
              }
            },
            itemMargin: {
              horizontal: 1,
            }
          }
        }
      }] 
    };
  }

  dashboardtargetchart(categories,a,b,c,d){
    this.targetoptions = {
      chart: {
        height: 275,
        width: 550,
        type: 'area',
        toolbar: {
          show: false, 
        }
      },
      colors: ['#2E93fA', '#66DA26', '#E91E63','#546E7A'],
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: 'smooth'
      },
      series: [
        {
          name: 'Target',
          data: a
        }, {
          name: 'Achieved',
          data: b
        }, {
          name: 'Not Achieved',
          data: c
        }, {
          name: 'Expired',
          data: d
        }
      ],

      xaxis: {
        type: 'date',
        categories: categories,                
      },
      tooltip: {
        x: {
          format: 'dd/mm'
        },
      }
    }
  }

}
