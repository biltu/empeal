import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart  } from '@angular/router';

@Component({
  selector: 'app-contest-redirection',
  templateUrl: './contest-redirection.component.html',
  styleUrls: ['./contest-redirection.component.css']
})
export class ContestRedirectionComponent implements OnInit {

  constructor(private router: Router, private activateRoute: ActivatedRoute) { 
  	let status = this.activateRoute.snapshot.queryParams['status'];
	let contId = this.activateRoute.snapshot.queryParams['contest_id'];
  	let contestData: any= {'contest_id':contId, 'status':status};
  	localStorage.setItem('contestStatus-set', JSON.stringify(contestData));

  	if(localStorage.getItem('token')){
  		this.router.navigate(['/dashboard']);
  	}else{
  		this.router.navigate(['/login']);
  	}
  }

  ngOnInit() {
  }

}
