import { Component, OnInit, ElementRef, Input } from '@angular/core';
import {Global} from '../../../global';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {NgbPopoverConfig} from '@ng-bootstrap/ng-bootstrap';
import {AuthService} from '../../../services/auth.service';
import {CommonService} from '../../../services/common.service';
import {MessageService} from '../../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
declare var ApexCharts: any;
declare var $;
@Component({
  selector: 'app-joined-contest',
  templateUrl: './joined-contest.component.html',
  styleUrls: ['./joined-contest.component.css']
})
export class JoinedContestComponent implements OnInit {
  leaderboardusers:any=[];
  contestreport: any=[];
  isLoaded: boolean=true;
  user_data: any={};
  contestname:any =[];
  zxcvzxc: any=[];
  userPostcount: number
  userPostlist: any={};
  unreadNotifications: any={};
  @Input() getArray;
  contestPercentage: any=[];
  totalstar:number;
  contstcommentcount:number=0;
  contestnotifications:any=[];
  unreadpostlists:any=[];
  postcount:number=0;
  finalcountpost: number=0;
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
    private router: Router, private commonService: CommonService, config: NgbPopoverConfig) { 
  	this.user_data = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')):'';
    this.userPostlist = localStorage.getItem('user_postlist') ? JSON.parse(localStorage.getItem('user_postlist')):'';
    this.unreadNotifications = localStorage.getItem('notificationlist') ? JSON.parse(localStorage.getItem('notificationlist')):'';
    
    $(document).on('click','.pcontestrmv', function(e){
       let pcid = $(this).data('pcid');
       $('.pastcontest'+pcid).hide();
    });
  }

  ngOnInit() {
   // this.fetchdata();
   // this.commonService.getAll(this.global.apiUrl + '/api/user/unopened/list')
   //        .subscribe((data)=>{
   //          this.userPostcount = data.data ?data.data.user_postcount:'';
   //          this.userPostlist = data.data ?data.data.user_postlist:'';
   //          this.notificationlist = data.data? data.data.notificationlist:[];
   //          console.log(this.userPostlist);
   //        }, (error)=>{

   //      });

    setTimeout(() => {
      this.contestreport = this.getArray;
      this.contestreport.forEach((report,i) => {
      this.leaderboardusers = report[1].ldboard;
      this.contestname[i]  = report[1].contest_name;
      this.contestnotifications = this.unreadNotifications?this.unreadNotifications.filter(obj=>obj.data.route == "contest" && obj.data.chat_group_id == report[0]):[];
      this.unreadpostlists = this.userPostlist?this.userPostlist.filter(obj=>obj.route == "contest" && obj.chat_group_id == report[0]):[];
      this.postcount += this.unreadpostlists.length;
      this.contstcommentcount += this.contestnotifications.length;
      //this.finalcountpost = parseInt(this.contstpostcount)+parseInt(this.postcountnumber);
      let achivedper = report[1].achiveddata >0 ?Math.round(report[1].achiveddata*100/report[1].activity_number):0;
      this.contestPercentage[i] = achivedper;
      this.totalstar = (((achivedper/100) % 1) < 0.5)?Math.round(achivedper/100):Math.floor(achivedper/100);
      //this.totaltargetchart(i,report[1].activity_number,report[1].achiveddata,report[1].unit,achivedper);
      //this.daycountchart(i,report[1].total_days,report[1].completed_days);
       if( Object.entries(report[1].groupchart_name).map(data=>data[1]).length > 1 ){
        this.groupWisebarchart(i,report[1].groupchart_data,report[1].groupchart_name, report[1].unit);
       }

        if(report[1].is_past==1){
          $('.pastcontest'+report[0]).hide();
        }
      $('.gpostcount'+i).html(String(this.postcount+this.contstcommentcount));
      this.contstcommentcount = 0;
      this.postcount = 0;
      $('.starCount'+i).html(this.totalstar);
      $('#activitynorp'+i).html('<span style="color: #249df9;">Activity : </span>'+Number(report[1].achiveddata)+'/'+ report[1].activity_number +' '+report[1].unit);
      $('#noofdaysrp'+i).html('<span style="color: #24e5a3;">Remaining :</span> '+ Number(report[1].completed_days)+' Days');
      if(report[1].total_days==report[1].completed_days){
        $('#noofdaysrp'+i).html('<span style="color: #24e5a3;">Completed :</span> '+ Number(report[1].completed_days)+' (of '+report[1].total_days+') Days');
      }else{
        $('#noofdaysrp'+i).html('<span style="color: #24e5a3;">Remaining :</span> '+ Number(report[1].total_days-report[1].completed_days)+' (of '+report[1].total_days+') Days');
      }
      //this.mobiletotaltargetchart(i,report[1].activity_number,report[1].achiveddata,report[1].unit,achivedper);
      //this.mobiledaycountchart(i,report[1].total_days,report[1].completed_days);
      this.contestreportchart(i,report[1].activity_number,report[1].achiveddata,report[1].unit,achivedper,report[1].total_days,report[1].completed_days,this.totalstar);
      this.mobilecontestreportchart(i,report[1].activity_number,report[1].achiveddata,report[1].unit,achivedper,report[1].total_days,report[1].completed_days,this.totalstar);
      this.tabcontestreportchart(i,report[1].activity_number,report[1].achiveddata,report[1].unit,achivedper,report[1].total_days,report[1].completed_days,this.totalstar);
        });
    },2000);

  }


  // asyncInit(){
  // 	this.commonService.getAll(this.global.apiUrl + '/api/user/contest/report')
  // 		.subscribe((data)=>{
  // 			this.contestreport = Object.entries(data.data);
  //       this.contestreport.forEach((report,i) => {
  //         this.leaderboardusers = report[1].ldboard;
  //         this.contestname  = report[1].contest_name;
  //         console.log(Object.entries(report[1].groupchart_data).map(data=>data[1]));
  //         this.totaltargetchart(i,report[1].activity_number,report[1].achiveddata);
  //         this.daycountchart(i,report[1].total_days,report[1].completed_days);
  //         this.groupWisebarchart(i,report[1].groupchart_data,report[1].groupchart_name);

  //         this.mobiletotaltargetchart(i,report[1].activity_number,report[1].achiveddata);
  //         this.mobiledaycountchart(i,report[1].total_days,report[1].completed_days);
  //       });
  // 	}, (error)=>{

  // 		});
  // 	}

    // totaltargetchart(id,activity_number,achiveddata,unit,achivedper){
    //   var firstchartoptions = {
    //         chart: {
    //             height: 250,
    //             type: 'radialBar',
    //         },
    //         tooltip: {
    //             enabled: true,
    //             onDatasetHover: {
    //                 highlightDataSeries: false,
    //             },
    //             x: {
    //                 format: 'dd MMM',
    //                 formatter: undefined,
    //             },
    //             y: {
    //                 formatter: (value) => { return Number(achiveddata)+' '+unit },
    //             },
    //             marker: {
    //                 show: true,
    //             },
    //             fixed: {
    //                 enabled: false,
    //                 position: 'topRight',
    //                 offsetX: 0,
    //                 offsetY: 0,
    //             },
    //         },
    //         plotOptions: {
    //             radialBar: {
    //                 dataLabels: {
    //                     name: {
    //                         fontSize: '16px',
    //                     },
    //                     value: {
    //                         fontSize: '12px',
    //                     },
    //                     total: {
    //                         show: true,
    //                         label: achivedper+'%',
    //                         formatter: function (w) {
    //                             // By default this function returns the average of all series. The below is just an example to show the use of custom formatter function
    //                             return ''
    //                         }
    //                     }
    //                 }
    //             }
    //         },
    //         series: [achivedper],
    //         labels: ['Activity'],
    //         responsive: [{
    //           options: {
    //             height: 150,
    //           }
    //       }]
            
    //     }

    //    var firstchart = new ApexCharts(
    //         document.querySelector("#totaltargesstchart"+id),
    //         firstchartoptions
    //     );
        
    //     firstchart.render();
    // }

    // daycountchart(id,total_days,completed_days){
    //   var secondchartoptions = {
    //         chart: {
    //             height: 250,
    //             type: 'radialBar',
    //         },
    //         tooltip: {
    //             enabled: true,
    //             onDatasetHover: {
    //                 highlightDataSeries: false,
    //             },
    //             x: {
    //                 format: 'dd MMM',
    //                 formatter: undefined,
    //             },
    //             y: {
    //                 formatter: (value) => { return Number(completed_days)},
    //                 title:{
    //                 formatter: (seriesName) => { return 'Completed days'},
    //                 },
    //             },
                
    //         },
    //         colors: ["#32CD32"],
    //         plotOptions: {
    //             radialBar: {
    //                 dataLabels: {
    //                     name: {
    //                         fontSize: '16px',
    //                     },
    //                     value: {
    //                         fontSize: '12px',
    //                     },
    //                     total: {
    //                         show: true,
    //                         label: total_days-completed_days+' to Go',
    //                         formatter: function (w) {
    //                             // By default this function returns the average of all series. The below is just an example to show the use of custom formatter function
    //                             return ''
    //                         }
    //                     }
    //                 }
    //             }
    //         },
    //         series: [Math.round(100-(100*completed_days/total_days))],
    //         labels: ['Days'],         
            
    //     }
    //    var secondchart = new ApexCharts(
    //         document.querySelector("#daycountchart"+id),
    //         secondchartoptions
    //     );
    //     secondchart.render();
    // }

    // mobiletotaltargetchart(id,activity_number,achiveddata,unit,achivedper){
    //   var firstchartoptions = {
    //         chart: {
    //             height: 150,
    //             type: 'radialBar',
    //         },
    //         tooltip: {
    //             enabled: true,
    //             onDatasetHover: {
    //                 highlightDataSeries: false,
    //             },
    //             style: {
    //               fontSize: '8px',
    //               fontFamily: undefined
    //             },
    //             x: {
    //                 format: 'dd MMM',
    //                 formatter: undefined,
    //             },
    //             y: {
    //                 formatter: (value) => { return Number(achiveddata)},
    //             },
    //             marker: {
    //                 show: true,
    //             },
    //             fixed: {
    //                 enabled: false,
    //                 position: 'topRight',
    //                 offsetX: 0,
    //                 offsetY: 0,
    //             },
    //         },
    //         plotOptions: {
    //             radialBar: {
    //                 dataLabels: {
    //                     name: {
    //                         fontSize: '10px',
    //                     },
    //                     value: {
    //                         fontSize: '9px',
    //                         offsetY: -5,
    //                     },
    //                     total: {
    //                         show: true,
    //                         label: achivedper+'%',
    //                         formatter: function (w) {
    //                             // By default this function returns the average of all series. The below is just an example to show the use of custom formatter function
    //                             return ''
    //                         }
    //                     }
    //                 },
    //                 //offsetY: 25,
    //                 // hollow: {
    //                 //   size: '80%',
    //                 // },
    //             }
    //         },
    //         series: [achivedper],
    //         labels: ['Activity'],
    //         responsive: [{
    //           options: {
    //           }
    //       }]
            
    //     }

    //    var firstchart = new ApexCharts(
    //         document.querySelector("#mobiletotaltargesstchart"+id),
    //         firstchartoptions
    //     );
        
    //     firstchart.render();
    // }

    // mobiledaycountchart(id,total_days,completed_days){
    //   var secondchartoptions = {
    //         chart: {
    //             height: 150,
    //             type: 'radialBar',
    //         },
    //         tooltip: {
    //             enabled: true,
    //             onDatasetHover: {
    //                 highlightDataSeries: false,
    //             },
    //             style: {
    //               fontSize: '8px',
    //               fontFamily: undefined
    //             },
    //             x: {
    //                 format: 'dd MMM',
    //                 formatter: undefined,
    //             },
    //             y: {
    //                 formatter: (value) => { return Number(completed_days)},
    //                 title:{
    //                 formatter: (seriesName) => { return 'Completed days'},
    //                 },
    //             },
    //             marker: {
    //                 show: true,
    //             },
    //             fixed: {
    //                 enabled: true,
    //                 position: 'top',
    //                 offsetX: 0,
    //                 offsetY: 0,
    //             },
                
    //         },
    //         colors: ["#32CD32"],
    //         plotOptions: {
    //             radialBar: {
    //                 dataLabels: {
    //                     name: {
    //                         fontSize: '10px',
    //                     },
    //                     value: {
    //                         fontSize: '9px',
    //                         offsetY: -5,
    //                     },
    //                     total: {
    //                         show: true,
    //                         label: total_days-completed_days+' to Go',
    //                         formatter: function (w) {
    //                             // By default this function returns the average of all series. The below is just an example to show the use of custom formatter function
    //                             return ''
    //                         }
    //                     }
    //                 }
    //             }
    //         },
    //         series: [Math.round(100-(100*completed_days/total_days))],
    //         labels: ['Days'],
            
    //     }
    //    var secondchart = new ApexCharts(
    //         document.querySelector("#mobiledaycountchart"+id),
    //         secondchartoptions
    //     );
    //     secondchart.render();
    // }
/**
* Display contest chart with all group for the company 
*/
    groupWisebarchart(id,groupchart_data,groupchart_name,unit){
      var options = {
            chart: {
                height: 200,
                type: 'bar',
                toolbar: {
                show: false
              }
            },
            plotOptions: {
                bar: {
                  dataLabels: {
                    position: "top" // top, center, bottom
                  }
                }
            },
            dataLabels: {
                enabled: true,
                formatter: function(val) {
                  return val>1?val + ' ' + unit+'s':val + ' ' + unit;
                },
                offsetY: -20,
                style: {
                  fontSize: "12px",
                  colors: ["#304758"]
                }
            },
            yaxis: {
              show: false,
            },
            tooltip: {
              enabled: true,
              y: {
                title: {
                    formatter: (seriesName) => "",
                },
                formatter: (value) => { return value>1?value + ' ' + unit+'s':value + ' ' + unit },
                show: false,
              }
            },
            // dataLabels: {
            //     enabled: false
            // },
            series: [{
                data: Object.entries(groupchart_data).map(data=>data[1])
            }],
            xaxis: {
                categories: Object.entries(groupchart_name).map(data=>data[1]),
            }
        }

       var chart = new ApexCharts(
            document.querySelector("#groupbarchart"+id),
            options
        );
        
        chart.render();
    }

/**
* Display contest chart in Desktop
*/
    contestreportchart(id,activity_number,achiveddata,unit,achivedper,total_days,completed_days,totalstar){
      var options = {
          chart: {
            height: 175,
            type: 'radialBar',
          },
          tooltip: {
            enabled: true,
            onDatasetHover: {
                highlightDataSeries: false,
            },
            style: {
              fontSize: '12px',
              fontFamily: undefined
            },
            x: {
                format: 'dd MMM',
                formatter: undefined,
            },
            y: {
                formatter: (value,seriesName) => { 
                  // if(seriesName=='Days'){
                  //   var valrs = Number(total_days-completed_days);
                  // }else{
                  //   var valrs = Number(value);
                  // }
                  return value+' %';
                },
                title:{
                formatter: (seriesName) => { 
                    if(seriesName=='Days'){
                      return 'Days left';
                    }else{
                      return 'Activity Completed';
                    }
                  },
                },
            },
          },
          plotOptions: {
              radialBar: {
                  size:90,
                  offsetY: 20,
                  hollow: {
                    margin: 15,
                    size: '60%',
                    image: 'assets/images/award2.png',
                    imageWidth: 30,
                    imageHeight: 30,
                    imageClipped: false
                  },
                  dataLabels: {
                      show: true,
                      name: {
                        show: false
                      },
                      value: {
                        show: false
                      },
                      total: {
                        show: false
                      }
                  }
              }
          },
          series: [achivedper, (total_days==completed_days)?Math.round(100*completed_days/total_days):Math.round(100-(100*completed_days/total_days))],
          labels: ['Activity', 'Days']
      }

     var chart = new ApexCharts(
          document.querySelector("#contestreportchart"+id),
          options
      );
      chart.render();
    }
/**
* Display contest chart in mobile
*/
    mobilecontestreportchart(id,activity_number,achiveddata,unit,achivedper,total_days,completed_days,totalstar){
      var options = {
          chart: {
            height: 150,
            type: 'radialBar',
          },
          tooltip: {
            enabled: true,
            onDatasetHover: {
                highlightDataSeries: false,
            },
            style: {
              fontSize: '9px',
              fontFamily: undefined
            },
            x: {
                format: 'dd MMM',
                formatter: undefined,
            },
            y: {
                formatter: (value,seriesName) => { 
                  // if(seriesName=='Days'){
                  //   var valrs = Number(total_days-completed_days);
                  // }else{
                  //   var valrs = Number(value);
                  // }
                  return value+' %';
                },
                title:{
                formatter: (seriesName) => { 
                    if(seriesName=='Days'){
                      return 'Days left';
                    }else{
                      return 'Activity Completed';
                    }
                  },
                },
            },
          },
          plotOptions: {
              radialBar: {
                  hollow: {
                    margin: 15,
                    size: '40%',
                    image: 'assets/images/award2.png',
                    imageWidth: 20,
                    imageHeight: 20,
                    imageClipped: false
                  },
                  dataLabels: {
                      name: {
                        show: false
                      },
                      value: {
                        show: false,
                      },
                      total: {
                        show: false
                      }
                  }
              }
          },
          series: [achivedper, Math.round(100-(100*completed_days/total_days))],
          labels: ['Activity', 'Days']
      }

     var chart = new ApexCharts(
          document.querySelector("#mobilecontestreportchart"+id),
          options
      );
      chart.render();
    }
/**
* Display contest chart in tab
*/
    tabcontestreportchart(id,activity_number,achiveddata,unit,achivedper,total_days,completed_days,totalstar){
      var options = {
          chart: {
            height: 200,
            type: 'radialBar',
            offsetY: -10,
          },
          tooltip: {
            enabled: true,
            onDatasetHover: {
                highlightDataSeries: false,
            },
            style: {
              fontSize: '12px',
              fontFamily: undefined
            },
            x: {
                format: 'dd MMM',
                formatter: undefined,
            },
            y: {
                formatter: (value,seriesName) => { 
                  // if(seriesName=='Days'){
                  //   var valrs = Number(total_days-completed_days);
                  // }else{
                  //   var valrs = Number(value);
                  // }
                  return value+' %';
                },
                title:{
                formatter: (seriesName) => { 
                    if(seriesName=='Days'){
                      return 'Days left';
                    }else{
                      return 'Activity Completed';
                    }
                  },
                },
            },
          },
          plotOptions: {
              radialBar: {
                  hollow: {
                    margin: 15,
                    size: '40%',
                    image: 'assets/images/award2.png',
                    imageWidth: 20,
                    imageHeight: 20,
                    imageClipped: false
                  },
                  dataLabels: {
                      name: {
                        show: false
                      },
                      value: {
                        show: false,
                      },
                      total: {
                        show: false
                      }
                  }
              }
          },
          series: [achivedper, Math.round(100-(100*completed_days/total_days))],
          labels: ['Activity', 'Days']
      }

     var chart = new ApexCharts(
          document.querySelector("#tabcontestreportchart"+id),
          options
      );
      chart.render();
    }

    ordinal_suffix_of(i) {
        var j = i % 10,
            k = i % 100;
        if (j == 1 && k != 11) {
            return i + "st";
        }
        if (j == 2 && k != 12) {
            return i + "nd";
        }
        if (j == 3 && k != 13) {
            return i + "rd";
        }
        return i + "th";
    }

}
