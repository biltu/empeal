import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  user_role: any={};
  user_data: any={};
  constructor(private authService: AuthService) {
  	this.user_data = this.authService.isUserLoginIn ? JSON.parse(localStorage.getItem('user')):'';
   	this.user_role = this.user_data ? this.user_data.role:'';
   }

  ngOnInit() {
  }

}
