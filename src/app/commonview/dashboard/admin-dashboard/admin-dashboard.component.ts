import { Component, OnInit } from '@angular/core';
import {Global} from '../../../global';
import {NgbPopoverConfig} from '@ng-bootstrap/ng-bootstrap';
import {AuthService} from '../../../services/auth.service';
import {CommonService} from '../../../services/common.service';
import {MessageService} from '../../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart  } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
declare var Swal: any;

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {
  isModalOpen: boolean;
	users: any=[];
	userSearchForm: FormGroup;
	userAddForm: FormGroup;
  groupAssignForm: FormGroup;
  bcaimportForm:FormGroup;
  usergroupupdate:FormGroup;
	count_data:number;
	active_page: number;
  display: string;
  ugpupdate: string;
  groups: any=[];
  checkedModel: any={};
  groupAssignModal: any;
  bcaDataimport: any;
  hcServerValidation: any;
  masterSelected:boolean;
  checkedList:any;
  teams: any=[];

  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
   private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) {
  	this.count_data =0;
  	this.active_page =0;
    this.asyncInit();
    this.groupAssignModal = 'none';
    this.bcaDataimport = 'none';
    this.isModalOpen = false;
    this.getCheckedItemList();
   }

  ngOnInit() {
    this.userAddForm = this.fb.group({
        coachtype:['hc', [Validators.required]],
        firstname:['', [Validators.required]],
        lastname:['', [Validators.required]],
        email:['', [Validators.required]],
        gender:['', [Validators.required]],
        country:['', [Validators.required]],
      });

    this.userSearchForm = this.fb.group({
      sname:[''],
      group: [''],
      team: [''],
      type: [''],
      gender:['']
    });

    this.groupAssignForm = this.fb.group({
      user_id:[''],
      group_id:['', [Validators.required]]
    });

    this.usergroupupdate = this.fb.group({
      userids:[''],
      group_id:['', [Validators.required]]
    });

    this.bcaimportForm = this.fb.group({
      bca_record:[''],
      bca_file:['']
    });

  }

  checkUncheckAll() {
    for (var i = 0; i < this.users.length; i++) {
      this.users[i].isSelected = this.masterSelected;
    }
    this.getCheckedItemList();
  }
  isAllSelected() {
    this.masterSelected = this.users.every(function(user:any) {
        return user.isSelected == true;
      })
    this.getCheckedItemList();
  }
 
  getCheckedItemList(){
    this.checkedList = [];
    for (var i = 0; i < this.users.length; i++) {
      if(this.users[i].isSelected)
      this.checkedList.push(this.users[i].id);
    }
  }

/**
* Get users and group data
* @returns  user data
*/
  asyncInit(){
  	this.commonService.getAll(this.global.apiUrl + '/api/admin/user?off_set=' + this.active_page)
  		.subscribe((data)=>{
  			this.users = data.data;
        this.groups = data.groups;
        this.teams = data.teams;
  			this.count_data = data.data_count;
        this.users.forEach((user, i)=>{
            if(user.sts =='1'){
                this.checkedModel[user.id] = true;
            }             
        });
  			this.pagination();
  		}, (error)=>{});
  }

  pagination(){
  	let tot_number =  Math.ceil(Number(this.count_data) /25);
  	var items: number[] = [];
	for(var i = 1; i <= tot_number; i++){
	   items.push(i);
	}
	return items;
  }

  paginationCall(index){
  	this.active_page = index;
  	let search_f = this.userSearchForm.controls;
  	if(search_f.sname.value =='' && search_f.group.value =='' && search_f.type.value =='' && 
  		search_f.gender.value==''){
  		this.asyncInit();
  	}else{
  		this.searchUser(index);
  	}
  }

/**
* Search user
* @returns  user data  
*/
  searchUser(index){
    this.active_page =index;
    let search_cont = '';
    if(this.userSearchForm.get('sname').value){
      search_cont += '&sname=' + this.userSearchForm.get('sname').value;
    }if(this.userSearchForm.get('group').value){
      search_cont += '&group=' + this.userSearchForm.get('group').value;
    }if(this.userSearchForm.get('team').value){
      search_cont += '&team=' + this.userSearchForm.get('team').value;
    }if(this.userSearchForm.get('type').value){
      search_cont += '&type=' + this.userSearchForm.get('type').value;
    }if(this.userSearchForm.get('gender').value){
      search_cont += '&gender=' + this.userSearchForm.get('gender').value;
    }

    this.commonService.getAll(this.global.apiUrl + '/api/admin/user?off_set=' + this.active_page + search_cont)
      .subscribe((data)=>{
        this.users = data.data;
        this.count_data = data.data_count;
        this.users.forEach((user, i)=>{
            if(user.sts =='1'){
                this.checkedModel[user.id] = true;
            }             
        });
        this.pagination();
      }, (error)=>{});
  }

  openHCModalDialog(){
      this.display='block'; //Set block css
    this.isModalOpen = true;
    
  }

  closeHCModalDialog(){
    this.display='none'; //set none css after close dialog
    this.isModalOpen = false;
  }

  openUIModalDialog(){
    this.bcaDataimport='block'; //Set block css
    this.isModalOpen = true;
  }

  closeUIModalDialog(){
    this.bcaDataimport='none'; //set none css after close dialog
    this.isModalOpen = false;
  }

  openUsergpchngDialog(){
    this.ugpupdate='block'; //set none css after close dialog
    this.isModalOpen = true;
    this.setusergroupupdateForm();
  }

  UsergpchngModalClose(){
    this.ugpupdate='none'; //set none css after close dialog
    this.isModalOpen = false;
  }

/**
* Create health coach
* @returns  coach data  
*/
  CreateHc(){
    let form = {usercreate:this.userAddForm.value};
    this.commonService.create(this.global.apiUrl + '/api/admin/user',form)
        .subscribe((data)=>{
          if(data.status == 422){
            this.hcServerValidation = data.error;
          }
          if(data.status ==200){
            this.message.success(data.status_text);
            this.display='none';
            this.users.unshift(data.data);
            this.userAddForm.reset();
            this.isModalOpen = false;
            //this.router.navigate(['admin/groups']);
          }

          if(data.status ==400){
            this.message.error(data.status_text);
          }

          if(data.status ==500){
            this.message.error('Please search and select mail from dropdown list.');
          }
        }, (error)=>{
      });
  }

  setusergroupupdateForm(){
      this.usergroupupdate.patchValue({
        userids:this.checkedList,
        group_id: ''
      });
  }

/**
* Change user status
* @returns  success  
*/

  changeStatus(event, id, index){
    let status = event ? '1': '0';
    this.commonService.getAll(this.global.apiUrl +'/api/user/set-status/'+ id +'?status=' +status)
        .subscribe((data)=>{
          if(data.status ==200){
            this.message.success('Successfully change status');
            this.users[index] = data.data;
          }
        }, (error)=>{});
  }

  assignModalOpen(user_id){
    this.groupAssignForm.get('user_id').setValue(user_id);
    this.groupAssignModal ='block';
    this.isModalOpen = true;
  }

  assignModalClose(){
    this.groupAssignForm.get('user_id').setValue('');
    this.groupAssignModal ='none';
    this.isModalOpen = false;
  }

/**
* Group Assign
* @returns  success  
*/
  assignSave(){
    if(this.groupAssignForm.get('group_id').value ==''){
      this.message.error('please select group');
      return false;
    }
    this.commonService.create(this.global.apiUrl + '/api/group/company-assign', this.groupAssignForm.value)
        .subscribe((data)=>{
          if(data.status ==200){
            let userIndex =this.users.findIndex(user=> user.id == this.groupAssignForm.get('user_id').value);
            this.users[userIndex] = data.data;
            this.groupAssignForm.reset();
            this.assignModalClose();
            this.isModalOpen = false;
            this.message.success(data.status_text);
          }
        }, (error)=>{});
  }

/**
* User group update
* @returns  users 
*/
  userGroupUpdate(){
    if(this.usergroupupdate.get('group_id').value ==''){
      this.message.error('please select group');
      return false;
    }

    if(this.usergroupupdate.get('userids').value.length <= 0){
      this.message.error('please select user from user list');
      return false;
    }
    
    this.commonService.create(this.global.apiUrl + '/api/group/user-assign', this.usergroupupdate.value)
        .subscribe((data)=>{
          if(data.status ==200){
            this.isModalOpen = false;
            this.ugpupdate = 'none';
            this.message.success(data.status_text);
            this.users = data.data;
            this.checkedList = [];
          }
        }, (error)=>{});
  }

/**
* Import bca record send
* @returns  success message
*/

  importBcaRecord(){
    const formData = new FormData();
    formData.append('bca_file', this.bcaimportForm.controls['bca_file'].value);
    this.commonService.create(this.global.apiUrl + '/api/user/bcarecord/import', formData)
      .subscribe((data)=>{
        if(data.status ==200){
          this.message.success(data.status_text);
            this.bcaDataimport='none';
            this.isModalOpen = false;
          ///localStorage.setItem('user', JSON.stringify(data.data));
        }else if(data.status ==422){
          this.message.error(data.status_text.bca_file);
        } 
        this.bcaimportForm.reset();
      }, (error)=>{});
  }

/**
* upload file set
*/
  onSelectFile(event: any){
    let image = event.target.files[0];
    this.bcaimportForm.get('bca_file').setValue(image);
  }

}
