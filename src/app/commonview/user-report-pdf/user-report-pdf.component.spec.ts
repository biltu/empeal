import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserReportPdfComponent } from './user-report-pdf.component';

describe('UserReportPdfComponent', () => {
  let component: UserReportPdfComponent;
  let fixture: ComponentFixture<UserReportPdfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserReportPdfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserReportPdfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
