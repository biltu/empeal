import { Component, OnInit, ViewChild } from '@angular/core';
import {Global} from '../../global';
import {NgbPopoverConfig} from '@ng-bootstrap/ng-bootstrap';
import {AuthService} from '../../services/auth.service';
import {CommonService} from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart  } from '@angular/router';
import { Chart, ChartOptions } from 'chart.js';
declare var ApexCharts: any;
declare var $;
import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexXAxis,
  ApexTitleSubtitle,
  ApexPlotOptions,
  ApexDataLabels,
  ApexYAxis,
  ApexTooltip,
  ApexLegend,
  ApexStroke,
  ApexFill,
  ApexResponsive,
  ApexGrid,
  ApexStates,
  ApexTheme

} from "ng-apexcharts";

export type aChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  yaxis: ApexYAxis | ApexYAxis[];
  title: ApexTitleSubtitle;
  colors: string[];
  dataLabels: ApexDataLabels;
  stroke: ApexStroke;
  labels: string[];
  legend: ApexLegend;
  fill: ApexFill;
  tooltip: ApexTooltip;
  plotOptions: ApexPlotOptions;
  responsive: ApexResponsive[];
  grid: ApexGrid;
  states: ApexStates;
  subtitle: ApexTitleSubtitle;
  theme: ApexTheme;
};
declare var anychart: any;
@Component({
  selector: 'app-user-report-pdf',
  templateUrl: './user-report-pdf.component.html',
  styleUrls: ['./user-report-pdf.component.css']
})
export class UserReportPdfComponent implements OnInit {
	user_data: any={};
  user_goals: any={};
  user_medical_conds: any={};
  user_allergy: any={};
  user_statutatory: any=[];
  stepschartOptions: any;
  fatburnchartOptions: any;
  distancechartOptions: any;
  floorstepchartOptions: any;
  activitychartcat: any=[];
  calconsumdate: any=[];
  activitysteps: any={};
  activityfatburn: any={};
  activitydistance: any={};
  activityfloorstep: any={};
  calconsumChart: any={};
  calburnChart: any={};
  activminsChart: any={};
  calburnconsumChart: any={};
  targetvsactualoptions: any={};
  nutrients: any=[];
  bardata: any=[];
  allnutrns: any=[];
  calburn: any={};
  calconsum: any={};
  activitymints: any={};
  sleepchart: any={};
  threebarchart: any={};
  linechartoption: any={};
  fatmaslChartOptions = {
    //responsive: true,
    maintainAspectRatio: false,
    title: {
      display: true,
      text: ''
    },
    legend: {
        display: true,
        position: 'bottom'
    },
    tooltips: {
      mode: 'index',
      intersect: true
    },
    scales: {
      xAxes: [{
      display: false,
      id: "main-x",
      categoryPercentage: 1,
      stacked: true
    }, {
      display: false,
      id: "sub-x",
      type: 'category',
      categoryPercentage: 0.8,
      barPercentage: 1,
      stacked: true
    }],
      yAxes: [{
        id: 'A',
        type: 'linear',
        position: 'left'
      }, {
        id: 'B',
        type: 'linear',
        position: 'right',
        ticks: {
          min: 0
        },
        gridLines: {
            display:false
        } 
      }]
    }
  };
  fatmaslChartLabels =  [];

  fatmaslChartData:any = [{
    label: 'Calories Consumed',
    backgroundColor: 'rgba(255, 127, 15, 0.2)',
    hoverBackgroundColor: 'rgba(255, 127, 15, 0.2)',
    data: [],
    borderWidth: 2,
    borderColor: 'red',
    yAxisID: 'A',
    xAxisID: "sub-x",
    fill: false
  }, {
    label: 'Calories Burned',
    backgroundColor: 'blue',
    hoverBackgroundColor: 'blue',
    data: [],
    borderWidth: 2,
    yAxisID: 'A',
    xAxisID: "sub-x",
    fill: false
  }, {
    type: 'line',
    label: 'Activity Minutes',
    backgroundColor: 'purple',
    borderColor: 'purple',
    hoverBackgroundColor: 'purple',
    fill: false,
    data: [],
    yAxisID: 'B'
  }];
  mealdates: any=[];
  mealmixChart: any={};
  carbohydratedata: any={};
  proteindata: any={};
  fatdata: any={};
  waterdata: any={};
  micronutrientschartoptions: any={};
  qusanslist: any=[];
  threebarchartdata: any=[];
  bcareport: any={};
  qsmodulebox: any=[];
  radialchartoption: any={};
  today: any;
  qsheetsreport: any=[];
  bcacount:number;

  constructor(private message:MessageService ,private global: Global,private authService: AuthService,
    private router: Router, private commonService: CommonService, config: NgbPopoverConfig,private route: ActivatedRoute) { 
  	this.user_data = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')):'';
    this.user_goals = this.user_data.user_goals.selected_goal;
    this.user_medical_conds = this.user_data.user_medical_cond.selected_medical_con !=''? this.user_data.user_medical_cond.selected_medical_con.split(','):[];
    this.user_allergy = this.user_data.user_allergy.selected_allergy!=''? this.user_data.user_allergy.selected_allergy.split(','): [];
    this.user_statutatory = this.user_data.user_statutatory;
    console.log(this.user_statutatory);
    this.activityReport();
    this.mealReport();
    this.targetVsactual();
    this.asyncInit();
    let currentdate = new Date;
    this.today = currentdate.getDate()+'/'+(currentdate.getMonth()+1)+'/'+currentdate.getFullYear();
  }

  ngOnInit() {
    setTimeout(() => {
        if(document.getElementById('GFG')){
            var printContents = document.getElementById('GFG').innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
    }, 10000);
  }

  asyncInit(){
    this.commonService.getAll(this.global.apiUrl + '/api/report/questionnarie/question-answer')
      .subscribe((data)=>{
        this.qusanslist = Object.entries(data.data);
        this.threebarchartdata = data.threebarchart;
        this.questionnaireReportChart(this.threebarchartdata);
        this.qsmodulebox = Object.entries(this.threebarchartdata);
        this.radialchart(data.radial_chart);
  	  }, (error)=>{
    });

    this.commonService.getAll(this.global.apiUrl + '/api/user/bca/report')
      .subscribe((data)=>{
        this.bcareport = data.data;
        this.bcacount = data.bcacount;
      }, (error)=>{
    });
    
    this.commonService.getAll(this.global.apiUrl + '/api/questionnarie/sheet/report')
      .subscribe((data)=>{
        this.qsheetsreport = Object.entries(data.data);
        //console.log(this.qsheetsreport);
        let current = '';
        let previous = '';
        let initial = '';
        let mdlscor = 0;
        let mdsts = '';
        setTimeout(() => {
          this.qsheetsreport.forEach((e,i) => {
            current = parseInt(e[1].current) > 0?e[1].current:0;
            previous = parseInt(e[1].previous) > 0?e[1].previous:0;
            initial = parseInt(e[1].initial) > 0?e[1].initial:0;
            mdlscor = Math.round(parseInt(current)+parseInt(previous)+parseInt(initial)/3);
            if(mdlscor<11){
              mdsts = 'Stable';
            }else if(mdlscor>10 && mdlscor<26){
              mdsts = 'Moderate';
            }else if(mdlscor > 26 && mdlscor<51){
              mdsts = 'High';
            }else{
              mdsts = 'Signigicant';
            }
            $("#modulests"+i).html(mdsts);
            this.linechart(i,e[1].current,e[1].previous,e[1].initial);
          });
        },500);
        //this.linechart(0,32,25,15);
      }, (error)=>{

      });
  }

  activityReport(){
    this.commonService.getAll(this.global.apiUrl + '/api/activity')
      .subscribe((data)=>{
        this.activitychartcat = data.date_activities[0];
        this.activitysteps = data.date_activities[1];
        this.activityfatburn = data.date_activities[4];
        this.activitydistance = data.date_activities[2];
        this.activityfloorstep = data.date_activities[5];
        this.calburn = data.date_activities[7];
        this.activitymints = data.date_activities[3];
        this.fatmaslChartLabels = this.activitychartcat;
        this.fatmaslChartData[0].data = this.calconsum;
        this.fatmaslChartData[1].data = data.date_activities[7];
        this.fatmaslChartData[2].data = data.date_activities[3];
        //console.log(data.date_activities[1]);
        this.stepsChart('Steps','Steps',this.activitysteps,this.activitychartcat);
        this.fatburnChart('Fat Burn','Fat Burn',this.activityfatburn,this.activitychartcat);
        this.distanceChart('Distance','Distance',this.activitydistance,this.activitychartcat);
        this.floorChart('Floor Step','Floor Step',this.activityfloorstep,this.activitychartcat);
        this.activmintsChart('Activity Minutes','Activity Minutes',this.activitymints,this.activitychartcat);
        this.calburnsChart('Calories Burned','Calories Burned',this.calburn,this.activitychartcat);
        this.sleepsChart(data.date_sleep[0],data.date_sleep[1],data.date_sleep[2],data.date_sleep[3],data.date_sleep[8])
      }, (error)=>{

      });
  }

  mealReport(){
    this.commonService.getAll(this.global.apiUrl + '/api/meal')
      .subscribe((data)=>{
          this.nutrients = (data.nutrients.length>0)?data.nutrients[0]:[];
          this.allnutrns = (data.allnutrns.length>0)?data.allnutrns[0]:[];
          this.bardata.push(this.allnutrns.carbohydrate);
          this.bardata.push(this.allnutrns.protein);
          this.bardata.push(this.allnutrns.fat);
          this.bardata.push(this.allnutrns.sugars);
          this.bardata.push(this.allnutrns.fibre);
          this.bardata.push(this.allnutrns.n3poly);
          this.bardata.push(this.allnutrns.calcium);
          this.bardata.push(this.allnutrns.magnesium);
          this.bardata.push(this.allnutrns.iron);
          this.bardata.push(this.allnutrns.zinc);
          this.bardata.push(this.allnutrns.selenium);
          this.bardata.push(this.allnutrns.iodine);
          this.bardata.push(this.allnutrns.sodium);
          this.bardata.push(this.allnutrns.vita);
          this.bardata.push(this.allnutrns.vitd);
          this.bardata.push(this.allnutrns.vite);
          this.bardata.push(this.allnutrns.vitk);
          this.bardata.push(this.allnutrns.thiamin);
          this.bardata.push(this.allnutrns.riboflavin);
          this.bardata.push(this.allnutrns.niacineqv);
          this.bardata.push(this.allnutrns.pantothenate);
          this.bardata.push(this.allnutrns.vitb6);
          this.bardata.push(this.allnutrns.folate);
          this.bardata.push(this.allnutrns.vitb12);
          this.bardata.push(this.allnutrns.biotin);
          this.bardata.push(this.allnutrns.vitc);
          this.mealdates = data.mealnutrients[0];
          this.carbohydratedata = data.mealnutrients[1];
          this.proteindata = data.mealnutrients[2];
          this.fatdata = data.mealnutrients[3];
          this.waterdata = data.mealnutrients[4];
          this.mealesMixChart(this.mealdates,this.carbohydratedata,this.proteindata,this.fatdata,this.waterdata);
          this.micronutrientsChart('','',this.bardata);
        }, (error)=>{

      });

    this.commonService.getAll(this.global.apiUrl + '/api/user/meal/single-nutrition?nutrns_name=energyKcal')
      .subscribe((data)=>{
        this.calconsum = data.data;
        this.calconsumdate = data.mealdate;
        this.calconsumesChart('Calories Consumed','Calories Consumed',this.calconsum,this.calconsumdate);
      }, (error)=>{

    });
  }

  linechart(id,current,previous,initial){
    initial = initial>0?initial:0;
    previous = previous>0?previous:0;
    current = current>0?current:0;
    var linechartoptions = {
          series: [{
            name: "",
            data: [initial, previous, current]
          }],
          chart: {
            height: 150,
            type: 'line',
            zoom: {
              enabled: false
            },
            toolbar: {
              show: false
            }
          },
          dataLabels: {
            enabled: false
          },
          stroke: {
            curve: 'straight'
          },
          title: {
            show:false
          },
          grid: {
            row: {
              colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
              opacity: 0.5
            },
          },
          xaxis: {
            show:false,
          }
        }

      var chart = new ApexCharts(
        document.querySelector("#linechart"+id),
        linechartoptions
      );
      chart.render();
  }

  radialchart(data){
    this.radialchartoption = {
    chart: {
      height: 200,
      type: 'radialBar',
    },
    plotOptions: {
      radialBar: {
        //size:90,
        //offsetY: 40,
        offsetX: -50,
        startAngle: 0,
        endAngle: 270,
        hollow: {
          //margin: 20,
          size: '30%',
          background: 'transparent',
          image: undefined,
        },
        dataLabels: {
          name: {
            show: false,
          },
          value: {
            show: false,
          }
        }
      }
    },
    series: data,
    labels: ['Nutrition', 'Fitness', 'Behavioural', 'Stress'],
    legend: {
      show: true,
      floating: true,
      fontSize: '10px',
      position: 'right',
      //offsetX: -100,
      offsetY: 50,
      labels: {
        useSeriesColors: true,
      },
      markers: {
        size: 0
      },
      formatter: function(seriesName, opts) {
        if(seriesName=='Stress'){
          return seriesName + " <span style='font-size:10px;'>(Lower is better)</span>:  " + (opts.w.globals.series[opts.seriesIndex]/10).toFixed(1)
        }else{
          return seriesName + ":  " + (opts.w.globals.series[opts.seriesIndex]/10).toFixed(1)
        }
      },
      itemMargin: {
        horizontal: 1,
      }
    },
    responsive: [{
      breakpoint: 480,
      options: {
        legend: {
          show: true,
          floating: true,
          fontSize: '12px',
          position: 'left',
          //offsetX: -50,
          //offsetY: 0,
          labels: {
            useSeriesColors: true,
          },
          markers: {
            size: 0
          },
          formatter: function(seriesName, opts) {
            if(seriesName=='Stress'){
              return seriesName + " <span style='font-size:10px;'>(Lower is better)</span>:  " + (opts.w.globals.series[opts.seriesIndex]/10).toFixed(1)
            }else{
              return seriesName + ":  " + (opts.w.globals.series[opts.seriesIndex]/10).toFixed(1)
            }
          },
          itemMargin: {
            horizontal: 1,
          }
        }
      }
    }],
    title: {
      show: true,
      text: "Current Health Status",
      align: "center"
    }, 
  };
  }
  

  targetVsactual(){
    this.commonService.getAll(this.global.apiUrl + '/api/user/target/report')
      .subscribe((data)=>{
        this.targetvsactualoptions = {
            chart: {
              height: 275,
              type: 'area',
              toolbar: {
                show: false, 
              }
            },
            colors: ['#2E93fA', '#66DA26', '#E91E63','#546E7A'],
            dataLabels: {
              enabled: false
            },
            stroke: {
              curve: 'smooth'
            },
            series: [
              {
                name: 'Target',
                data: data.data[1]
              }, {
                name: 'Achieved',
                data: data.data[2]
              }, {
                name: 'Not Achieved',
                data: data.data[3]
              }, {
                name: 'Expired',
                data: data.data[4]
              }
            ],

            xaxis: {
              type: 'date',
              categories: data.data[0],                
            },
            tooltip: {
              x: {
                format: 'dd/mm'
              },
            }
        }
      }, (error)=>{

      });
  }

  stepsChart(title,name,data,categories){
    this.stepschartOptions = {
      chart: {
          height: 200,
          type: 'bar',
          toolbar: {
          show: false
        }
      },
      plotOptions: {
          bar: {
            dataLabels: {
              position: "top", // top, center, bottom
              orientation: "vertical",
              //hideOverflowingLabels: true,
            }
          }
      },
      colors: ['#ff6384'],
      dataLabels: {
          enabled: true,
          formatter: function(val) {
            return val;
          },
          offsetY: -15,
          style: {
            fontSize: "9px",
            colors: ["#304758"]
          },
          orientation: "vertical"
      },
      yaxis: {
        show: false,
      },
      tooltip: {
        enabled: true,
        y: {
          title: {
              formatter: (seriesName) => "",
          },
          formatter: (value) => { return value },
          show: false,
        }
      },
      // dataLabels: {
      //     enabled: false
      // },
      series: [
        {
          name: name,
          data: data
        }
      ],
      title: {
        show: true,
        text: title
      },
      xaxis: {
        categories: categories,
      }
    };
  }

  sleepsChart(cat,light,deep,rem,awake){
    this.sleepchart = {
      series: [{
          name: 'Disturbed Sleep',
          data: light
        }, {
          name: 'Regular sleep',
          data: deep
        }, {
          name: 'Deep Sleep',
          data: rem
        }, {
          name: 'Awake',
          data: awake
      }],
      chart: {
        type: 'bar',
        height: 600,
        stacked: true,
        toolbar: {
          show: false
        }
      },
      colors: ['#87cefa','#1e90ff','#0000ff','#ffc107'],
      plotOptions: {
        bar: {
          horizontal: true,
        },
      },
      xaxis: {
        categories: cat,
      }
    };
  }

  micronutrientsChart(title,name,data){
    this.micronutrientschartoptions = {
      chart: {
          height: 350,
          type: 'bar',
          toolbar: {
          show: false
        }
      },
      plotOptions: {
          bar: {
            dataLabels: {
              position: "top", // top, center, bottom
              orientation: "vertical",
              //hideOverflowingLabels: true,
            }
          }
      },
      colors: ['#007bff'],
      dataLabels: {
          enabled: true,
          formatter: function(val) {
            return val;
          },
          offsetY: -12,
          style: {
            fontSize: "9px",
            colors: ["#000"]
          },
          orientation: "vertical"
      },
      tooltip: {
        enabled: true,
        y: {
          title: {
              formatter: (seriesName) => "",
          },
          formatter: (value) => { return value },
          show: false,
        }
      },
      // dataLabels: {
      //     enabled: false
      // },
      series: [
        {
          name: name,
          data: data
        }
      ],
      xaxis: {
          categories: ['Carbohydrate', 'Protien', 'Fat', 'Sugar', 'Fibre', 'O6/O3',
     'Calcium', 'Magensium', 'Iron', 'Zinc', 'Selenium', 'Iodine', 'Sodium', 'Vitamin A',
      'Vitamin D', 'Vitamin E', 'Vitamin K1', 'Thiamin B1', 'Riboflavin B2', 'Niacin B3',
       'Pantothenate B5', 'Vitamin B6', 'Folates B9', 'Vitamin B12', 'Biotin B7', 'Vitamin C'],
      },
      yaxis: {
        show: false,
      }
    };
  }
  mealesMixChart(cat,carbohydrate,protein,fat,water){
    this.mealmixChart = {
        series: [{
        name: 'Carbohydrate',
        data: carbohydrate,
        type: 'bar'
      }, {
        name: 'Protein',
        data: protein,
        type: 'bar'
      }, {
        name: 'Fat',
        data: fat,
        type: 'bar'
      }, {
        name: 'Water',
        data: water,
        type: 'line'
      }],
      chart: {
        type: 'line',
        height: 600,
        stacked: true,
        toolbar: {
          show: false
        }
      },
      dataLabels: {
        enabled: true,
        formatter: function(val) {
          return val;
        },
        //offsetY: -10,
        style: {
          fontSize: "10px",
          colors: ["#fff"]
        },
        orientation: "vertical"
        //enabledOnSeries: [3]
      },
      colors: ['#ff6384','#007bff','#fd7e14','#6610f2'],
      plotOptions: {
        bar: {
          horizontal: false,
        },
      },
      xaxis: {
        categories: cat,
      },
      // yaxis: [{
      //   title: {
      //     text: '',
      //   },
      //   min: 0,
      //   max: 300,
      // },
      // {
      //   title: {
      //     text: '',
      //   },
      //   min: 0,
      //   max: 100,
      // },
      // {
      //   title: {
      //     text: '',
      //   },
      //   min: 0,
      //   max: 100,
      // }, {
      //   opposite: true,
      //   min: 0,
      //   max: 8,
      //   title: {
      //     text: 'Water'
      //   }
      // }]
    };
  }

  calconsumesChart(title,name,data,categories){
    this.calconsumChart = {
      chart: {
          height: 200,
          type: 'bar',
          toolbar: {
          show: false
        }
      },
      plotOptions: {
          bar: {
            dataLabels: {
              position: "top", // top, center, bottom
              orientation: "vertical",
              //hideOverflowingLabels: true,
            }
          }
      },
      colors: ['#F54718'],
      dataLabels: {
        enabled: true,
        formatter: function(val) {
          return val;
        },
        offsetY: -15,
        style: {
          fontSize: "9px",
          colors: ["#304758"]
        },
        orientation: "vertical"
      },
      yaxis: {
        show: false,
      },
      tooltip: {
        enabled: true,
        y: {
          title: {
              formatter: (seriesName) => "",
          },
          formatter: (value) => { return value },
          show: false,
        }
      },
      // dataLabels: {
      //     enabled: false
      // },
      series: [
        {
          name: name,
          data: data
        }
      ],
      title: {
        show: true,
        text: title
      },
      xaxis: {
        categories: categories,
      }
    };
  }
  
  calburnsChart(title,name,data,categories){
    this.calburnChart = {
      chart: {
          height: 200,
          type: 'bar',
          toolbar: {
          show: false
        }
      },
      plotOptions: {
        bar: {
          dataLabels: {
            position: "top", // top, center, bottom
            orientation: "vertical",
            //hideOverflowingLabels: true,
          }
        }
      },
      colors: ['#0000ff'],
      dataLabels: {
        enabled: true,
        formatter: function(val) {
          return val;
        },
        offsetY: -15,
        style: {
          fontSize: "9px",
          colors: ["#304758"]
        },
        orientation: "vertical"
      },
      yaxis: {
        show: false,
      },
      tooltip: {
        enabled: true,
        y: {
          title: {
              formatter: (seriesName) => "",
          },
          formatter: (value) => { return value },
          show: false,
        }
      },
      // dataLabels: {
      //     enabled: false
      // },
      series: [
        {
          name: name,
          data: data
        }
      ],
      title: {
        show: true,
        text: title
      },
      xaxis: {
        categories: categories,
      },
      responsive: [{
        breakpoint: 1000,
        options: {
          plotOptions: {
            bar: {
              horizontal: false
            }
          }
        },
      }]
    };
  }

  questionnaireReportChart(qdata){
    this.threebarchart = {
      chart: {
          height: 200,
          type: 'bar',
          toolbar: {
          show: false
        }
      },
      plotOptions: {
          bar: {
            dataLabels: {
              position: "top", // top, center, bottom
              orientation: "vertical",
              //hideOverflowingLabels: true,
            }
          }
      },
      colors: ['#fdab5a','#51adec','#fd7894'],
      dataLabels: {
        enabled: true,
        formatter: function(val) {
          return val;
        },
        offsetY: -15,
        style: {
          fontSize: "9px",
          colors: ["#304758"]
        },
        orientation: "vertical"
      },
      
      series: qdata[0],
      xaxis: {
        categories: qdata[2],
      }
    };
  }

  activmintsChart(title,name,data,categories){
    this.activminsChart = {
      chart: {
          height: 200,
          type: 'line',
          toolbar: {
          show: false
        }
      },
      plotOptions: {
        bar: {
          dataLabels: {
            position: "center", // top, center, bottom
            orientation: "vertical",
            //hideOverflowingLabels: true,
          }
        }
      },
      colors: ['#800080'],
      dataLabels: {
        enabled: true,
        formatter: function(val) {
          return val;
        },
        //offsetY: -20,
        style: {
          fontSize: "9px",
          colors: ["#304758"]
        },
        orientation: "vertical"
      },
      yaxis: {
        show: false,
      },
      tooltip: {
        enabled: true,
        y: {
          title: {
              formatter: (seriesName) => "",
          },
          formatter: (value) => { return value },
          show: false,
        }
      },
      // dataLabels: {
      //     enabled: false
      // },
      series: [
        {
          name: name,
          data: data
        }
      ],
      title: {
        show: true,
        text: title
      },
      xaxis: {
          categories: categories,
      }
    };
  }

  fatburnChart(title,name,data,categories){
    this.fatburnchartOptions = {
      chart: {
          height: 200,
          type: 'bar',
          toolbar: {
          show: false
        }
      },
      plotOptions: {
        bar: {
          dataLabels: {
            position: "center", // top, center, bottom
            orientation: "vertical",
            //hideOverflowingLabels: true,
          }
        }
      },
      colors: ['#ff9f40'],
      dataLabels: {
        enabled: true,
        formatter: function(val) {
          return val;
        },
        //offsetY: -20,
        style: {
          fontSize: "9px",
          colors: ["#304758"]
        },
        orientation: "vertical"
      },
      yaxis: {
        show: false,
      },
      tooltip: {
        enabled: true,
        y: {
          title: {
            formatter: (seriesName) => "",
          },
          formatter: (value) => { return value },
          show: false,
        }
      },
      // dataLabels: {
      //     enabled: false
      // },
      series: [
        {
          name: name,
          data: data
        }
      ],
      title: {
        show: true,
        text: title
      },
      xaxis: {
          categories: categories,
      }
    };
  }

  distanceChart(title,name,data,categories){
    this.distancechartOptions = {
      chart: {
          height: 200,
          type: 'bar',
          toolbar: {
          show: false
        }
      },
      plotOptions: {
          bar: {
            dataLabels: {
              position: "center", // top, center, bottom
              orientation: "vertical",
              //hideOverflowingLabels: true,
            }
          }
      },
      colors: ['#36a2eb'],
      dataLabels: {
          enabled: true,
          formatter: function(val) {
            return val;
          },
          //offsetY: -20,
          style: {
            fontSize: "9px",
            colors: ["#304758"]
          },
          orientation: "vertical"
      },
      yaxis: {
        show: false,
      },
      tooltip: {
        enabled: true,
        y: {
          title: {
            formatter: (seriesName) => "",
          },
          formatter: (value) => { return value },
          show: false,
        }
      },
      // dataLabels: {
      //     enabled: false
      // },
      series: [
        {
          name: name,
          data: data
        }
      ],
      title: {
        show: true,
        text: title
      },
      xaxis: {
        categories: categories,
      }
    };
  }

  floorChart(title,name,data,categories){
    this.floorstepchartOptions = {
      chart: {
          height: 200,
          type: 'bar',
          toolbar: {
          show: false
        }
      },
      plotOptions: {
          bar: {
            dataLabels: {
              position: "center", // top, center, bottom
              orientation: "vertical",
              //hideOverflowingLabels: true,
            }
          }
      },
      colors: ['#6f42c1'],
      dataLabels: {
          enabled: true,
          formatter: function(val) {
            return val;
          },
          //offsetY: -20,
          style: {
            fontSize: "9px",
            colors: ["#304758"]
          },
          orientation: "vertical"
      },
      yaxis: {
        show: false,
      },
      tooltip: {
        enabled: true,
        y: {
          title: {
            formatter: (seriesName) => "",
          },
          formatter: (value) => { return value },
          show: false,
        }
      },
      // dataLabels: {
      //     enabled: false
      // },
      series: [
        {
          name: name,
          data: data
        }
      ],
      title: {
        show: true,
        text: title
      },
      xaxis: {
          categories: categories,
      }
    };
  }

  qsheetTableName(shortname){
    if(shortname=='PS'){
      return 'Stress & Resilience';
    }else if(shortname=='FIT'){
      return 'Exercise';
    }else if(shortname=='DTP'){
      return 'Assimilation';
    }else if(shortname=='LP'){
      return 'Biotransformation & Elimination';
    }else if(shortname=='ES'){
      return 'Energy';
    }else if(shortname=='IP'){
      return 'Defence & Repair';
    }else if(shortname=='CVP'){
      return 'Transport';
    }else if(shortname=='WO'){
      return 'Communication';
    }else if(shortname=='MO'){
      return 'Communication';
    }else if(shortname=='EF'){
      return 'Mediators';
    }else if(shortname=='DIET'){
      return 'Nutrition';
    }else{
      return '';
    }
  }

  qsheetName(shortname){
    if(shortname=='PS'){
      return 'Perceived Stress';
    }else if(shortname=='FIT'){
      return 'Movement';
    }else if(shortname=='DTP'){
      return 'Gut Health';
    }else if(shortname=='LP'){
      return 'Cleansing System';
    }else if(shortname=='ES'){
      return 'Body Communication';
    }else if(shortname=='IP'){
      return 'Defence & Repair';
    }else if(shortname=='CVP'){
      return 'Heart Health';
    }else if(shortname=='WO'){
      return 'She';
    }else if(shortname=='MO'){
      return 'He';
    }else if(shortname=='EF'){
      return 'Environment';
    }else if(shortname=='DIET'){
      return 'Food Choices';
    }else{
      return '';
    }
  }

}
