import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../../global';
import {AuthService} from '../../../services/auth.service';
import { CommonService } from '../../../services/common.service';
import {MessageService} from '../../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart  } from '@angular/router';
declare var swal: any;


@Component({
  selector: 'app-corporate-edit-profile',
  templateUrl: './corporate-edit-profile.component.html',
  styleUrls: ['./corporate-edit-profile.component.css']
})
export class CorporateEditProfileComponent implements OnInit {

  user_data: any ={};
  userForm : FormGroup;
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
    private router: Router, private commonService: CommonService) { 
  	this.user_data = this.authService.isUserLoginIn ? JSON.parse(localStorage.getItem('user')):'';
  }

  ngOnInit() {
  	this.userForm = this.fb.group({
  	  firstname:['', [Validators.required]],
      city:['', [Validators.required]],
      state:['', [Validators.required]],
      address:['', [Validators.required]],
      primary_cname:['', [Validators.required]],
      primary_cdesignation:['', [Validators.required]],
      primary_cemail:['', [Validators.required, Validators.email]],
      primary_cphone:['', [Validators.minLength(9), Validators.maxLength(13), Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
 	  adtnl_name:[''],
      adtnl_cdesignation:[''],
      adtnl_cemail:['',[Validators.email]],
      adtnl_cphone:['', [Validators.minLength(9), Validators.maxLength(13), Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],

  	});

  	this.setUserForm();
  }

/**
* Set form Value
*/

  setUserForm(){
  	this.userForm.patchValue({
  		firstname: this.user_data.user_detail.firstname,
  		city: this.user_data.user_detail.city,
  		state: this.user_data.user_detail.state,
  		address:this.user_data.user_detail.address,
  		primary_cname:this.user_data.user_detail.primary_cname,
  		primary_cdesignation:this.user_data.user_detail.primary_cdesignation,
  		primary_cemail:this.user_data.user_detail.primary_cemail,
  		primary_cphone:this.user_data.user_detail.primary_cphone,
  		adtnl_name:this.user_data.user_detail.adtnl_name,
  		adtnl_cdesignation:this.user_data.user_detail.adtnl_cdesignation,
  		adtnl_cemail:this.user_data.user_detail.adtnl_cemail,
  		adtnl_cphone: this.user_data.user_detail.adtnl_cphone,
  	});
  }

   get cp_f() { return this.userForm.controls; }


/**
* Update corporate profile
*/
   updateProfile(){
   	 this.commonService.create(this.global.apiUrl + '/api/corporate-profile/update', this.userForm.value)
   	 	.subscribe((data)=>{
   	 		if(data.status ==200){
          this.message.success(data.status_text);
            localStorage.setItem('user', JSON.stringify(data.data));
            this.router.navigate(['profile']);
        }
        else if(500){
              this.message.error(data.status_text);
          }
   	 	}, (error)=>{});
   }

}
