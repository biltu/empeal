import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorporateEditProfileComponent } from './corporate-edit-profile.component';

describe('CorporateEditProfileComponent', () => {
  let component: CorporateEditProfileComponent;
  let fixture: ComponentFixture<CorporateEditProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorporateEditProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorporateEditProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
