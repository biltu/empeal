import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../../global';
import {AuthService} from '../../../services/auth.service';
import { CommonService } from '../../../services/common.service';
import {MessageService} from '../../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart  } from '@angular/router';
declare var swal: any;


@Component({
  selector: 'app-secondary-coach-edit-profile',
  templateUrl: './secondary-coach-edit-profile.component.html',
  styleUrls: ['./secondary-coach-edit-profile.component.css']
})
export class SecondaryCoachEditProfileComponent implements OnInit {

  user_data: any={};
  user_detail: any={};
  userForm: FormGroup;
  master_data: any=[];
  user: any ={};
  dobday: any;
  validationError: any;
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
    private router: Router, private commonService: CommonService) { 
  	  this.user_data = this.authService.isUserLoginIn ? JSON.parse(localStorage.getItem('user')):'';
      this.user_detail = this.user_data? this.user_data.user_detail:'';
      this.dobday = new Date;
      this.dobday.setDate(this.dobday.getDate() - 6570);
  }

  ngOnInit() {
  	this.userForm = this.fb.group({
  		firstname: ['',[Validators.required]],
  		lastname: ['',[Validators.required]],
  		alias_name: [''],
  		gender:['',[Validators.required]],
  		country:['',[Validators.required]],
      timezone:['',[Validators.required]],
  		dob:['',[Validators.required]],
  		email:['',[Validators.required]],
  		mobile:['',[Validators.required]],
  		avatar_id:['',[Validators.required]],
  		emergency_contact_name:[''],
  		emergency_contact_phone:[''],
  		
  	});

    this.asyncInit();
    this.setUserForm();  
  }

/**
* Get common data
*/
  asyncInit(){
    this.commonService.getAll(this.global.apiUrl + '/api/master-data')
        .subscribe((data)=>{
          this.master_data = data.data;
        }, (error)=>{});
  }

/**
* Set coach form
*/
    setUserForm(){
  		this.userForm.patchValue({
  			firstname: this.user_detail.firstname,
  			lastname: this.user_detail.lastname,
	  		alias_name: this.user_detail.alias_name,
        avatar_id: String(this.user_detail.avatar_id),
	  		gender:this.user_detail.gender,
	  		country:this.user_detail.country,
        timezone:this.user_detail.timezone,
	  		dob:this.user_detail.dob,
	  		email:this.user_data.email,
	  		mobile:this.user_detail.mobile,
	  		avatar_icon:this.user.avatar_icon,
	  		emergency_contact_name:this.user_detail.emergency_contact_name,
	  		emergency_contact_phone:this.user_detail.emergency_contact_phone,
  		});
  }

  get us_f() { return this.userForm.controls; }

/**
* Store coach information
* @return cach data
*/
    storeUserInfomation(){

  	this.commonService.create(this.global.apiUrl + '/api/coach-profile/update', this.userForm.value )
  		.subscribe((data)=>{
        if(data.status == 200){
          console.log(data.data);
          this.message.success(data.status_text);
          localStorage.setItem('user', JSON.stringify(data.data));
          this.router.navigate(['profile']);
        }else if(data.status == 500){
          this.message.error(data.status_text);
        }
        else if(data.status ==422){
          this.validationError = data.error;
        }
  		}, (error)=>{      

      });
  }

}
