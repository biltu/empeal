import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../../global';
import {AuthService} from '../../../services/auth.service';
import { CommonService } from '../../../services/common.service';
import {MessageService} from '../../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart  } from '@angular/router';
declare var swal: any;

@Component({
  selector: 'app-general-information',
  templateUrl: './general-information.component.html',
  styleUrls: ['./general-information.component.css']
})
export class GeneralInformationComponent implements OnInit {

  user_data: any={};
  user_detail: any={};
  user_goal: any={};
  master_data: any=[];
  userForm: FormGroup;
  dobday: any;
  user: any ={};
  goals:any={
  	overall_fitness:'',
  	reduce_fat:'',
  	gain_weight:'',
  	improve_heart:'',
  	muscle_strength:'',
  	muscle_size:'',
  	tone_up:'',
  	flexibility:'',
  	reduce_stress:'',
    race:'',
  	injury:''

  };

  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
    private router: Router, private commonService: CommonService) {
  		this.user_data = this.authService.isUserLoginIn ? JSON.parse(localStorage.getItem('user')):'';
      this.user_detail = this.user_data? this.user_data.user_detail:'';
      this.user_goal = this.user_data? this.user_data.user_goals: '';
      this.dobday = new Date;
      this.dobday.setDate(this.dobday.getDate() - 6570);
     }

  ngOnInit() {
    this.asyncInit();

  	this.userForm = this.fb.group({
  		firstname: ['',[Validators.required]],
  		lastname: ['',[Validators.required]],
  		alias_name: [''],
  		gender:['',[Validators.required]],
  		country:['',[Validators.required]],
      timezone:['',[Validators.required]],
  		dob:['',[Validators.required]],
  		email:['',[Validators.required]],
  		mobile:['',[Validators.required]],
  		avatar_id:['',[Validators.required]],
  		is_subscribed:['',[Validators.required]],
  		emergency_contact_name:['',[Validators.required]],
  		emergency_contact_phone:['',[Validators.required]],
  		my_goals:[''],
      other_goal:['']
  		
  	});
    this.setUserForm();
    this.setgoal();
  }

/**
* Get general infromation data
*/
  asyncInit(){
    this.commonService.getAll(this.global.apiUrl + '/api/master-data')
        .subscribe((data)=>{
          this.master_data = data.data;
        }, (error)=>{});
  }

/**
* Set general infromation form
*/

  setUserForm(){
  		this.userForm.patchValue({
  			firstname: this.user_detail.firstname,
  			lastname: this.user_detail.lastname,
	  		alias_name: this.user_detail.alias_name,
        avatar_id: String(this.user_detail.avatar_id),
	  		gender:this.user_detail.gender,
	  		country:this.user_detail.country,
        timezone:this.user_detail.timezone,
	  		dob:this.user_detail.dob,
	  		email:this.user_data.email,
	  		mobile:this.user_detail.mobile,
	  		avatar_icon:this.user.avatar_icon,
        is_subscribed: this.user_detail.is_subscribed,
	  		emergency_contact_name:this.user_detail.emergency_contact_name,
	  		emergency_contact_phone:this.user_detail.emergency_contact_phone,
	  		my_goals:'',
        other_goal:this.user_goal ? this.user_goal.other_goal:'',
  		});
  }

/**
* Get Goal form
*/
  setgoal(){
    if(this.user_goal){     
        this.goals.overall_fitness =this.user_goal.overall_fitness,
        this.goals.reduce_fat=this.user_goal.reduce_fat,
        this.goals.gain_weight=this.user_goal.gain_weight,
        this.goals.improve_heart=this.user_goal.improve_heart,
        this.goals.muscle_strength=this.user_goal.muscle_strength,
        this.goals.muscle_size=this.user_goal.muscle_size,
        this.goals.tone_up=this.user_goal.tone_up,
        this.goals.flexibility=this.user_goal.flexibility,
        this.goals.reduce_stress=this.user_goal.reduce_stress,
        this.goals.race=this.user_goal.race,
        this.goals.injury=this.user_goal.injury
    }
  }

  get us_f() { return this.userForm.controls; }

/**
* Update general information
*/
  storeUserInfomation(){
    this.userForm.patchValue({
      my_goals: this.goals
    });
  	this.commonService.create(this.global.apiUrl + '/api/user-general-info/update', this.userForm.value )
  		.subscribe((data)=>{
        if(data.status == 200){
          this.message.success(data.status_text);
          localStorage.setItem('user', JSON.stringify(data.data));
          this.router.navigate(['profile']);
        }else if(500){
          this.message.error(data.status_text);
        }
        console.log(data);
  		}, (error)=>{});
  }

}
