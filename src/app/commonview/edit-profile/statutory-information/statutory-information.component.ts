import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../../global';
import {AuthService} from '../../../services/auth.service';
import { CommonService } from '../../../services/common.service';
import {MessageService} from '../../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart  } from '@angular/router';
declare var swal: any;

@Component({
  selector: 'app-statutory-information',
  templateUrl: './statutory-information.component.html',
  styleUrls: ['./statutory-information.component.css']
})
export class StatutoryInformationComponent implements OnInit {

   user_data: any={};
   user_statutory: any={};
   statutaries :FormGroup;
   checkIsAgree: boolean;
   submitted: boolean=false;
   termOfAgree: boolean=false;
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
    private router: Router, private commonService: CommonService) {
      this.user_data = this.authService.isUserLoginIn ? JSON.parse(localStorage.getItem('user')):'';
      this.user_statutory = this.user_data ? this.user_data.user_statutatory:'';
      this.checkIsAgree =this.user_statutory ? this.user_statutory.is_agree:'';
    }

  ngOnInit() {

    this.statutaries = this.fb.group({
      heart_condition:[''],
      chest_pain_during_activity:[''],
      chest_pain_not_doing_activity:[''],
      palpitation:[''],
      dizziness:[''],
      high_blood_pressure:[''],
      joint_problem:[''],
      asthma:[''],
      prescribed_medication:[''],
      //prescribed_medication_info:[''],
      pregnant:[''],
      surgery:[''],
      not_participate_physical_exercise:[''],
      not_participate_physical_exercise_info:[''],
      none_above_statutory:[''],
      is_agree:['', [Validators.required]]
    });

    this.statutaries.get('is_agree').valueChanges.subscribe((value)=>{
      this.termOfAgree = value;
    });

    this.statutaries.get('not_participate_physical_exercise').valueChanges.subscribe((value)=>{
      if(value){
        this.statutaries.controls['not_participate_physical_exercise_info'].setValidators([Validators.required]);
        this.statutaries.controls['not_participate_physical_exercise_info'].updateValueAndValidity();
      }else{
        this.statutaries.controls['not_participate_physical_exercise_info'].clearValidators();
        this.statutaries.controls['not_participate_physical_exercise_info'].updateValueAndValidity();
      }
    });

    this.setStatutory();
  }

  get si_f() { return this.statutaries.controls; } 

/**
* Create or Update user statutory information
*/

  storeData(){
    this.submitted = true;
    if(this.statutaries.invalid){
      return;
    }else if(!this.termOfAgree){
      return false;
    }
  	this.commonService.create(this.global.apiUrl + '/api/user-statutory-info/update', this.statutaries.value)
  		.subscribe((data)=>{
  			if(data.status ==200){
          this.message.success(data.status_text);
            localStorage.setItem('user', JSON.stringify(data.data));
            this.router.navigate(['profile']);
        }
        else if(500){
              this.message.error(data.status_text);
          }
  		}, (error)=>{});
  }

/**
* Set statutory information form
*/

  setStatutory(){
    if(this.user_statutory){
      this.statutaries.patchValue({
        heart_condition:this.user_statutory.heart_condition,
        chest_pain_during_activity:this.user_statutory.chest_pain_during_activity,
        chest_pain_not_doing_activity:this.user_statutory.chest_pain_not_doing_activity,
        palpitation:this.user_statutory.palpitation,
        dizziness:this.user_statutory.dizziness,
        high_blood_pressure:this.user_statutory.high_blood_pressure,
        joint_problem:this.user_statutory.joint_problem,
        asthma:this.user_statutory.asthma,
        prescribed_medication:this.user_statutory.prescribed_medication,
        //prescribed_medication_info:this.user_statutory.prescribed_medication_info,
        pregnant:this.user_statutory.pregnant,
        surgery:this.user_statutory.surgery,
        not_participate_physical_exercise:this.user_statutory.not_participate_physical_exercise,
        not_participate_physical_exercise_info:this.user_statutory.not_participate_physical_exercise_info,
        none_above_statutory:(this.user_statutory.none_above_statutory ==1)?true:'',
        is_agree:(this.user_statutory.is_agree ==1)?true:'',
      });
  	}
  }

}
