import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatutoryInformationComponent } from './statutory-information.component';

describe('StatutoryInformationComponent', () => {
  let component: StatutoryInformationComponent;
  let fixture: ComponentFixture<StatutoryInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatutoryInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatutoryInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
