import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../../global';
import {AuthService} from '../../../services/auth.service';
import { CommonService } from '../../../services/common.service';
import {MessageService} from '../../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart  } from '@angular/router';
declare var swal: any;

@Component({
  selector: 'app-medical-condition',
  templateUrl: './medical-condition.component.html',
  styleUrls: ['./medical-condition.component.css']
})
export class MedicalConditionComponent implements OnInit {
    user_data: any={};
    user_medical_cond: any={};
    medical_condForm: FormGroup;
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
    private router: Router, private commonService: CommonService) { 
  	  this.user_data = this.authService.isUserLoginIn ? JSON.parse(localStorage.getItem('user')):'';
      this.user_medical_cond = this.user_data ? this.user_data.user_medical_cond:'';
  }

  ngOnInit() {
  	this.medical_condForm = this.fb.group({
  		osteo_arthritis:[''],
  		rheumatoid_arthritis:[''],
  		gout:[''],
  		psoriatic_arthritis:[''],
  		knee:[''],
  		shoulder:[''],
  		elbow:[''],
  		wrist:[''],
  		ankle:[''],
  		back:[''],
  		neck:[''],
  		migraines:[''],
  		frequent_headache:[''],
  		frequent_stomach:[''],
  		hernia:[''],
  		diabetes_mellitus:[''],
  		cancer:[''],
  		hypoglycemia:[''],
  		asthma:[''],
  		emphysema_bronchitis:[''],
  		hbp:[''],
  		lbp:[''],
  		etl:[''],
  		ldl:[''],
  		hdl:[''],
  		hypothyroid:[''],
      hyperthyroid:[''],
      pcos:[''],
      epilepsy:[''],
      endometreosys:[''],
      physical_chest_pain:[''],
      physical_not_chest_pain:[''],
      palpitation:[''],
      msdheartbeat:[''],
      coronary:[''],
      stroke:[''],
      congenital:[''],
      peripheral:[''],
      irritable_bowel_syndrome:[''],
      gall_bladder_stone:[''],
      kidney_stone:[''],
      ulcerative_colitis:[''],
      crohn_disease:[''],
      eczema:[''],
      psorasis:[''],
      acne:[''],
      other_medical:[''],
  		none_above_medical:[''],
  		last_health_screening_date:['']
  	});
  	this.setMedicalCond();
  }

/**
* Set user medical condition form
*/

  setMedicalCond(){
  	if(this.user_medical_cond){
  	this.medical_condForm.patchValue({
  		osteo_arthritis:this.user_medical_cond.osteo_arthritis ,
  		rheumatoid_arthritis:this.user_medical_cond.rheumatoid_arthritis,
  		gout:this.user_medical_cond.gout,
  		psoriatic_arthritis:this.user_medical_cond.psoriatic_arthritis ,
  		knee:this.user_medical_cond.knee,
  		shoulder:this.user_medical_cond.shoulder,
  		elbow:this.user_medical_cond.elbow ,
  		wrist:this.user_medical_cond.wrist,
  		ankle:this.user_medical_cond.ankle ,
  		back:this.user_medical_cond.back,		
  		neck:this.user_medical_cond.neck,
  		migraines:this.user_medical_cond.migraines,
  		frequent_headache:this.user_medical_cond.frequent_headache,
  		frequent_stomach:this.user_medical_cond.frequent_stomach,
  		hernia:this.user_medical_cond.hernia,
  		diabetes_mellitus:this.user_medical_cond.diabetes_mellitus,
  		cancer:this.user_medical_cond.cancer,	
  		hypoglycemia:this.user_medical_cond.hypoglycemia,
  		asthma:this.user_medical_cond.asthma,
  		emphysema_bronchitis:this.user_medical_cond.emphysema_bronchitis,
  		hbp:this.user_medical_cond.hbp,
  		lbp:this.user_medical_cond.lbp,
  		etl:this.user_medical_cond.etl,
  		ldl:this.user_medical_cond.ldl,
  		hdl:this.user_medical_cond.hdl,
  		hypothyroid:this.user_medical_cond.hypothyroid,
      hyperthyroid:this.user_medical_cond.hyperthyroid,
      pcos:this.user_medical_cond.pcos,
      epilepsy:this.user_medical_cond.epilepsy,
      endometreosys:this.user_medical_cond.endometreosys,
      physical_chest_pain:this.user_medical_cond.physical_chest_pain,
      physical_not_chest_pain:this.user_medical_cond.physical_not_chest_pain,
      palpitation:this.user_medical_cond.palpitation,
      msdheartbeat:this.user_medical_cond.msdheartbeat,
      coronary:this.user_medical_cond.coronary,
      stroke:this.user_medical_cond.stroke,
      congenital:this.user_medical_cond.congenital,
      peripheral:this.user_medical_cond.peripheral,
      irritable_bowel_syndrome:this.user_medical_cond.irritable_bowel_syndrome,
      gall_bladder_stone:this.user_medical_cond.gall_bladder_stone,
      kidney_stone:this.user_medical_cond.kidney_stone,
      ulcerative_colitis:this.user_medical_cond.ulcerative_colitis,
      crohn_disease:this.user_medical_cond.crohn_disease,
      eczema:this.user_medical_cond.eczema,
      psorasis:this.user_medical_cond.psorasis,
      acne:this.user_medical_cond.acne,
      other_medical:this.user_medical_cond.other_medical,
  		none_above_medical:this.user_medical_cond.none_above_medical,
  		last_health_screening_date:this.user_medical_cond.last_health_screening_date
  	});	
  	}

  }

/**
* Create or Update user medical condition
*/
  storeForm(){
  	this.commonService.create(this.global.apiUrl + '/api/user-medical-cond/update', this.medical_condForm.value)
  		.subscribe((data)=>{
  			if(data.status ==200){
  				this.message.success(data.status_text);
		        localStorage.setItem('user', JSON.stringify(data.data));
		        this.router.navigate(['profile']);
  			}
  			else if(500){
          		this.message.error(data.status_text);
        	}
  		}, (error)=>{});
  }

}
