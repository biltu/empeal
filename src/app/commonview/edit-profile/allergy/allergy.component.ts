import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../../global';
import {AuthService} from '../../../services/auth.service';
import { CommonService } from '../../../services/common.service';
import {MessageService} from '../../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart  } from '@angular/router';
declare var swal: any;

@Component({
  selector: 'app-allergy',
  templateUrl: './allergy.component.html',
  styleUrls: ['./allergy.component.css']
})
export class AllergyComponent implements OnInit {

   user_data: any={};
   user_allergy: any={};
   allergyform :FormGroup;
   checkIsAgree: boolean;
   submitted: boolean=false;
   termOfAgree: boolean=false;
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
    private router: Router, private commonService: CommonService) {
      this.user_data = this.authService.isUserLoginIn ? JSON.parse(localStorage.getItem('user')):'';
      this.user_allergy = this.user_data ? this.user_data.user_allergy:'';
    }

  ngOnInit() {

    this.allergyform = this.fb.group({
      pollen_allergy:[''],
      dust_allergy:[''],
      gluten:[''],
      crustaceans:[''],
      eggs:[''],
      fish:[''],
      peanuts:[''],
      soybean:[''],
      milk:[''],
      nuts:[''],
      celery:[''],
      mustard:[''],
      sesame:[''],
      sulphites:[''],
      lupin:[''],
      molluscs:[''],
      other_allergy:[''],
      none_above:['']
    });

     this.setAllergy();
  }

  //get si_f() { return this.statutaries.controls; } 

/**
* Allergy create nd update
* @returns  Allergy data
*/

  storeData(){
  	this.commonService.create(this.global.apiUrl + '/api/user-allergy/update', this.allergyform.value)
  		.subscribe((data)=>{
  			if(data.status ==200){
          this.message.success(data.status_text);
            localStorage.setItem('user', JSON.stringify(data.data));
            this.router.navigate(['profile']);
        }
        else if(500){
              this.message.error(data.status_text);
          }
  		}, (error)=>{});
  }

/**
* If allergy was created then set form
*/
  setAllergy(){
    if(this.user_allergy){
      this.allergyform.patchValue({
        pollen_allergy:this.user_allergy.pollen_allergy,
        dust_allergy:this.user_allergy.dust_allergy,
        gluten:this.user_allergy.gluten,
        crustaceans:this.user_allergy.crustaceans,
        eggs:this.user_allergy.eggs,
        fish:this.user_allergy.fish,
        peanuts:this.user_allergy.peanuts,
        soybean:this.user_allergy.soybean,
        milk:this.user_allergy.milk,
        nuts:this.user_allergy.nuts,
        celery:this.user_allergy.celery,
        mustard:this.user_allergy.mustard,
        sesame:this.user_allergy.sesame,
        sulphites:this.user_allergy.sulphites,
        lupin:this.user_allergy.lupin,
        molluscs:this.user_allergy.molluscs,
        other_allergy:this.user_allergy.other_allergy,
        none_above:this.user_allergy.none_above
      });
  	}
  }

}
