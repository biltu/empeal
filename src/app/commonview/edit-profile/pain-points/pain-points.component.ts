import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../../global';
import {AuthService} from '../../../services/auth.service';
import { CommonService } from '../../../services/common.service';
import {MessageService} from '../../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart  } from '@angular/router';
declare var swal: any;

@Component({
  selector: 'app-pain-points',
  templateUrl: './pain-points.component.html',
  styleUrls: ['./pain-points.component.css']
})
export class PainPointsComponent implements OnInit {

   user_data: any={};
   user_painpoints: any={};
   painpoints :FormGroup;
   checkIsAgree: boolean;
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
    private router: Router, private commonService: CommonService) {
      this.user_data = this.authService.isUserLoginIn ? JSON.parse(localStorage.getItem('user')):'';
      this.user_painpoints = this.user_data ? this.user_data.user_painpoints:'';
    }

  ngOnInit() {

    this.painpoints = this.fb.group({
      neck_fever:[''],
      agonis_headache:[''],
      persistent_headache:[''],
      delusional_beliefs:[''],
      weight_loss:[''],
      vomiting:[''],
      black_tarry_stools:[''],
      stool_blood:[''],
      swelling:[''],
      spwtb:[''],
      nvc:[''],
      chest_abdomen:[''],
      lump_groin:[''],
      blood_urine:[''],
      urine_flow:[''],
      vaginal_bleeding:[''],
      breasts_lump:[''],
      none_above_painpoints:['']
    });


     this.setPainPoints();
  }

/**
* Create or Update user pain point
*/
  storeData(){
    this.commonService.create(this.global.apiUrl + '/api/user-pain-points/update', this.painpoints.value)
      .subscribe((data)=>{
        if(data.status ==200){
          this.message.success(data.status_text);
            localStorage.setItem('user', JSON.stringify(data.data));
            this.router.navigate(['profile']);
        }
        else if(500){
            this.message.error(data.status_text);
          }
      }, (error)=>{});
  }

/**
* Set pain point form
*/
  setPainPoints(){
    if(this.user_painpoints){
      this.painpoints.patchValue({
        neck_fever:this.user_painpoints.neck_fever,
        agonis_headache:this.user_painpoints.agonis_headache,
        persistent_headache:this.user_painpoints.persistent_headache,
        delusional_beliefs:this.user_painpoints.delusional_beliefs,
        weight_loss:this.user_painpoints.weight_loss,
        vomiting:this.user_painpoints.vomiting,
        black_tarry_stools:this.user_painpoints.black_tarry_stools,
        stool_blood:this.user_painpoints.stool_blood,
        swelling:this.user_painpoints.swelling,
        spwtb:this.user_painpoints.spwtb,
        nvc:this.user_painpoints.nvc,
        chest_abdomen:this.user_painpoints.chest_abdomen,
        lump_groin:this.user_painpoints.lump_groin,
        blood_urine:this.user_painpoints.blood_urine,
        urine_flow:this.user_painpoints.urine_flow,
        vaginal_bleeding:this.user_painpoints.vaginal_bleeding,
        breasts_lump:this.user_painpoints.breasts_lump,
        none_above_painpoints:this.user_painpoints.none_above_painpoints
      });
  	}
  }

}
