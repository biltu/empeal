import { Component, OnInit } from '@angular/core';
import {Global} from '../../global';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart  } from '@angular/router';
declare var swal: any;

@Component({
  selector: 'app-invitation',
  templateUrl: './invitation.component.html',
  styleUrls: ['./invitation.component.css']
})
export class InvitationComponent implements OnInit {

  invitationForm:FormGroup;

  constructor(private global: Global,private authService: AuthService,private router: Router,private fb:FormBuilder,private commonService:CommonService,private message:MessageService ) { }

  ngOnInit() {
  	this.invitationForm = this.fb.group({
      invt_show:[''],
  		invitation_file:['']
  	});
  }

/**
* Send invitation to Employees
*/

  inviteEmployee(){
  	const formData = new FormData();
  	formData.append('invitation_file', this.invitationForm.controls['invitation_file'].value);
  	this.commonService.create(this.global.apiUrl + '/api/corporate/invitation/send', formData)
      .subscribe((data)=>{
        if(data.status ==200){
          this.message.success(data.status_text);
          this.router.navigate(['dashboard']);
        } 

        if(data.status ==422){
          this.message.error(data.error.invitation_file);
        }
        this.invitationForm.reset();
        console.log(data.error.invitation_file);
      }, (error)=>{});
  }

/**
* Image select function
*/

    onSelectFile(event: any){
      let image = event.target.files[0];
      this.invitationForm.get('invitation_file').setValue(image);
    }

}
