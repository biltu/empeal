import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { CorporateDashboardComponent } from './dashboard/corporate-dashboard/corporate-dashboard.component';
import { ProfileComponent } from './profile/profile.component';
import { AuthGuard } from './../auth.guard';
import { StatutoryGuard } from './../statutory.guard';
import { EmployeeGuard } from './../employee.guard';
import { CoachGuard } from './../coach.guard';
import { GeneralInformationComponent } from './edit-profile/general-information/general-information.component';
import { StatutoryInformationComponent } from './edit-profile/statutory-information/statutory-information.component';
import { MedicalConditionComponent } from './edit-profile/medical-condition/medical-condition.component';
import { PainPointsComponent } from './edit-profile/pain-points/pain-points.component';
import { AllergyComponent } from './edit-profile/allergy/allergy.component';
import { WearableComponent } from './wearable/wearable.component';
import { CorporateEditProfileComponent } from './edit-profile/corporate-edit-profile/corporate-edit-profile.component';
import { InvitationComponent } from './invitation/invitation.component';
import { GroupDashboardComponent } from './group-dashboard/group-dashboard.component';
import { GroupUserListComponent } from './group-user-list/group-user-list.component';
import { WearableCallbackComponent } from './wearable-callback/wearable-callback.component';
import { CoachEditProfileComponent } from './edit-profile/coach-edit-profile/coach-edit-profile.component';
import { UpdatePasswordComponent } from './update-password/update-password.component';
import { TermConditionComponent } from './term-condition/term-condition.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { ExplainerVideoComponent } from './explainer-video/explainer-video.component';
import { CorporateDetailComponent } from './corporate-detail/corporate-detail.component';
import { UserReportComponent } from './user-report/user-report.component';
import { UserReportPdfComponent } from './user-report-pdf/user-report-pdf.component';
import { SupportComponent } from './support/support.component';
import { ContestRedirectionComponent } from './dashboard/contest-redirection/contest-redirection.component';
import { ManualNotificationComponent } from './manual-notification/manual-notification.component';
import { AttachmentsComponent } from './attachments/attachments.component';

const routes: Routes = [	
	{path:'dashboard', canActivate: [AuthGuard], component: DashboardComponent},
	{path:'dashboard-detail/:name', canActivate: [AuthGuard], component: CorporateDetailComponent},
	{path:'group/dashboard-detail/:id/:name', canActivate: [AuthGuard], component: CorporateDetailComponent},
	{path:'profile', canActivate: [AuthGuard, StatutoryGuard], component: ProfileComponent},
	{path:'edit-corporate-profile', canActivate: [AuthGuard], component: CorporateEditProfileComponent},
	
	{ path: 'edit-coach-profile', canActivate: [AuthGuard], component: CoachEditProfileComponent },
	
	{path:'edit-general-info', canActivate: [AuthGuard, StatutoryGuard], component: GeneralInformationComponent},
	{path:'edit-statutory-info', canActivate: [AuthGuard], component: StatutoryInformationComponent},
	{path:'edit-medical-condition', canActivate: [AuthGuard], component: MedicalConditionComponent},
	{path:'edit-pain-points', canActivate: [AuthGuard], component: PainPointsComponent},
	{path:'edit-allergy', canActivate: [AuthGuard], component: AllergyComponent},	
	{path:'wearable', canActivate: [AuthGuard, StatutoryGuard], component: WearableComponent},
	{path:'wearable/callback', canActivate: [AuthGuard], component: WearableCallbackComponent},
	{path:'group/dashboard/:id', canActivate: [AuthGuard], component: CorporateDashboardComponent},
	{path:'group/user/list/:id', canActivate: [AuthGuard], component: GroupUserListComponent},
	{path:'user/details/:id',canActivate: [AuthGuard], component: ProfileComponent},
	{path:'user/report/:id',canActivate: [AuthGuard,CoachGuard], component: UserReportComponent},
	{path:'user/practitioner-report',canActivate: [AuthGuard], component: UserReportPdfComponent},
	{path:'corporate/invitation', canActivate: [AuthGuard], component: InvitationComponent},
	{path:'update-password',canActivate: [AuthGuard, StatutoryGuard], component: UpdatePasswordComponent},
	{path:'term-conditions',canActivate: [AuthGuard], component: TermConditionComponent},
	{path:'explainer-video',canActivate: [AuthGuard], component: ExplainerVideoComponent},
	{path:'privacy-policy',canActivate: [AuthGuard], component: PrivacyPolicyComponent},
	{path:'support',canActivate:[AuthGuard], component:SupportComponent},
	{path:'contest/redirection', component:ContestRedirectionComponent},
	{path:'contest/redirection', component:ContestRedirectionComponent},
	{path:'notes/list', canActivate: [AuthGuard], component:AttachmentsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommonviewRoutingModule { }
