import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart  } from '@angular/router';
declare var swal: any;

@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.css']
})
export class UpdatePasswordComponent implements OnInit {

  updatePasswordForm: FormGroup;
  validationError: any;
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
    private router: Router, private commonService: CommonService) { }

  ngOnInit() {
  	this.updatePasswordForm = this.fb.group({
  		password:['',[Validators.required]],
  		password_confirmation:['',[Validators.required]]
  	});

  }

  get us_f() { return this.updatePasswordForm.controls; }

/**
* Update user password
*/
  updatePassword(){
  	this.commonService.create(this.global.apiUrl + '/api/update-password', this.updatePasswordForm.value)
  		.subscribe((data)=>{
  			if(data.status ==422){
  			  this.validationError = data.error;
  			}
  			if(data.status ==200){
          this.validationError = '';
  				this.message.success(data.status_text);
  			}
  		}, (error)=>{});
  }

}
