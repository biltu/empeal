import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart  } from '@angular/router';
declare var swal: any;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  user_image: any;
  user_role: any={};
  user_data: any={};
  coach_seen: boolean;
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
    private router: Router, private commonService: CommonService) {
    this.coach_seen = false;
    this.user_data = this.authService.isUserLoginIn ? JSON.parse(localStorage.getItem('user')):'';
    this.user_role = this.user_data ? this.user_data.role:'';
    this.coach_seen = this.router.url.match('user/details')? true: false;

  }

  ngOnInit() {
  }

  initData(){

  }		

  profileImageChange(){
  	const formData = new FormData();
  	formData.append('user_image',this.user_image);
  	this.commonService.create(this.global.apiUrl + '/profile-image/edit', formData)
  					  .subscribe((data)=>{
  					  	if(data.status ==200){
  					  	}
  					  }, (error)=>{});
  }	

  onSelectFile(event: any){
  		this.user_image = event.target.files[0];

  }

}
