import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../../global';
import {AuthService} from '../../../services/auth.service';
import { CommonService } from '../../../services/common.service';
import {MessageService} from '../../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart  } from '@angular/router';
declare var swal: any;
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  user_data: any={};
  user_detail: any={};
  user_goals: any ={};
  user_medical_cond: any={};
  user_statutory: any={};
  user_painpoints: any={};
  user_allergy: any={};
  user_image:string;
  userImage= new FormControl('');
  completionPercent: number;
  coach_seen: boolean;
  permissions: any=[];
  userStatutoryHas: number=0;
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
    private router: Router, private commonService: CommonService, private route: ActivatedRoute) { 
    this.coach_seen = false;
    this.coach_seen = this.router.url.match('user/details')? true: false;
    if(this.coach_seen){
      this.commonService.getAll(this.global.apiUrl + '/api/user-detail/'+ this.route.snapshot.params['id'])
          .subscribe(data=>{
            if(data.status ==200){
              this.user_data = data.data;
              this.user_detail = data.data.user_detail;
              this.user_goals = data.other_data.user_goals;
              this.user_medical_cond = data.other_data.user_medical_cond;
              this.user_statutory = data.other_data.user_statutatory;
              this.user_painpoints = data.other_data.user_painpoints;
              this.user_allergy = data.other_data.user_allergy;
              this.userStatutoryHas = this.user_data ? this.user_data.user_statutary_value:0;
            }
          }, error=>{});

    }else{
      this.userDataSet();
    } 	
    
    this.completionPercent = 10;
  }

  ngOnInit() {
    this.commonService.checkaccessmodules.subscribe((data)=>{
      this.permissions = data.permissions;
    });
  }

/**
* Profile Image update
*/
    onSelectFile(event: any){
      let image = event.target.files[0];
      this.userImage.setValue(image);
      const formData = new FormData();
      formData.append('user_image', this.userImage.value);
      this.commonService.create(this.global.apiUrl + '/api/profile-image/update', formData)
        .subscribe((data)=>{
          if(data.status ==200){
            this.message.success(data.status_text);
            localStorage.setItem('user', JSON.stringify(data.data));
            this.commonService.changeProfileImage(data.data);
            this.userDataSet();
          } 

          if(data.status ==422){
            this.message.error(data.error.user_image);
          }
        }, (error)=>{});

  }

/**
* Update user data
*/

  userDataSet(){
    this.user_data = this.authService.isUserLoginIn ? JSON.parse(localStorage.getItem('user')):'';
    this.user_detail = this.user_data? this.user_data.user_detail:'';
    this.user_goals = this.user_data? this.user_data.user_goals: '';
    this.user_medical_cond = this.user_data ? this.user_data.user_medical_cond: '';
    this.user_statutory = this.user_data ? this.user_data.user_statutatory: '';
    this.user_painpoints = this.user_data ? this.user_data.user_painpoints: '';
    this.user_allergy = this.user_data ? this.user_data.user_allergy: '';
    this.userStatutoryHas = this.user_data ? this.user_data.user_statutary_value:0;
  }

}
