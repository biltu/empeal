import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../../global';
import {AuthService} from '../../../services/auth.service';
import { CommonService } from '../../../services/common.service';
import {MessageService} from '../../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart  } from '@angular/router';
declare var swal: any;

@Component({
  selector: 'app-secondary-coach-profile',
  templateUrl: './secondary-coach-profile.component.html',
  styleUrls: ['./secondary-coach-profile.component.css']
})
export class SecondaryCoachProfileComponent implements OnInit {

  user_detail: any={};
  user_data: any={};
  userImage= new FormControl('');
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
    private router: Router, private commonService: CommonService) { 

  }

  ngOnInit() {
  	this.user_data = this.authService.isUserLoginIn ? JSON.parse(localStorage.getItem('user')):'';
    this.user_detail = this.user_data? this.user_data.user_detail:'';
  }

/**
* Profile Image update
*/

  onSelectFile(event: any){
      let image = event.target.files[0];
      this.userImage.setValue(image);
      const formData = new FormData();
      formData.append('user_image', this.userImage.value);
      this.commonService.create(this.global.apiUrl + '/api/profile-image/update', formData)
        .subscribe((data)=>{
          if(data.status ==200){
            this.message.success(data.status_text);
            localStorage.setItem('user', JSON.stringify(data.data));
            this.commonService.changeProfileImage(data.data);
            this.userDataSet();
          } 

          if(data.status ==422){
            this.message.error(data.error.user_image);
          }
          
        }, (error)=>{});

  }

/**
* Update coach data
*/

  userDataSet(){
  	this.user_data = this.authService.isUserLoginIn ? JSON.parse(localStorage.getItem('user')):'';
    this.user_detail = this.user_data? this.user_data.user_detail:'';
  }

}
