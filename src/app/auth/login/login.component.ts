import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart  } from '@angular/router';
declare var swal: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  showloader: boolean;
  submitted: boolean;
  authValidation:any={
    email_err:'',
    password_err:''
  };
  validationError: any={};
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
    private router: Router, private commonService: CommonService) { 
    if(this.authService.isUserLoginIn()){
     /* if(localStorage.getItem('previousUrl') =='/login'){
        this.router.navigate(['/dashboard']);
      }else{*/
        this.router.navigate([localStorage.getItem('previousUrl')]);
      //}
      
    }
  }

  ngOnInit() {
  	this.loginForm = this.fb.group({
  		email: ['', [Validators.required,Validators.email]],
      password: ['', [Validators.required,  Validators.minLength(6)]],
      remember:['']
  	});

  }

  get login_f(){return this.loginForm.controls}

/**
* User login 
* @returns  user data
*/

  loginStore(){
  	this.showloader = true;
    this.submitted = true;
    if(this.loginForm.invalid){
      return;
    }
    if(this.loginForm.get('remember').value){
      let checkRemember = localStorage.getItem('remember_me') ?JSON.parse(localStorage.getItem('remember_me')):''; 
      if(checkRemember && checkRemember[this.loginForm.get('email').value]!='undefined'){
        checkRemember[this.loginForm.get('email').value] = this.loginForm.get('password').value;
        localStorage.setItem('remember_me', JSON.stringify(checkRemember));
      }else{ 
        if(checkRemember && checkRemember.length){
          checkRemember[this.loginForm.get('email').value] = this.loginForm.get('password').value;
          localStorage.setItem('remember_me', JSON.stringify(checkRemember));
        }else{
          let rememberArray ={};
          let emailValue = this.loginForm.get('email').value;
          rememberArray[emailValue] =this.loginForm.get('password').value;
          localStorage.setItem('remember_me', JSON.stringify(rememberArray));
        }  

      }

    }
  	this.authService.userAuthentication(this.global.apiUrl + '/api/login', this.loginForm.value)
  		.subscribe((data)=>{
        if(data.status ==200){
            this.authValidationRemove();
            this.message.success(data.status_text);
            localStorage.setItem('token', data.token);
            localStorage.setItem('user', JSON.stringify(data.user));
            this.authService.getUserDetail();
          this.router.navigate(['/dashboard']);
          
        }else if(data.status ==422){
          this.validationError = data.error;
          console.log(this.validationError.email);
        }
  		}, (error)=>{
        if(error.status ==401){
          this.authValidationRemove();
          console.log(error.error);
          if(error.error.err_code =='em'){
            this.authValidation.email_err = error.error.status_text;
          }else if(error.error.err_code =='pwd'){
            this.authValidation.password_err = error.error.status_text;
          }
        }
      });
  }

/**
* Inner function
*/
  authValidationRemove(){
    this.authValidation.email_err = '';
    this.authValidation.password_err ='';
  }

/**
* Check remember me
* @returns  user data
*/
  checkRememberMe(event){
     let checkRemember = localStorage.getItem('remember_me') ? JSON.parse(localStorage.getItem('remember_me')):''; 
     if(checkRemember[event.target.value]){
       this.loginForm.get('password').setValue(checkRemember[event.target.value]);
     }
       
  }

}
