import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../services/message.service';
declare var Swal: any;

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  resetPasswordForm: FormGroup;
  submitted: boolean;
  validationError: string;
  tokenValue: string;
  constructor(private fb:FormBuilder, private global: Global,private authService: AuthService,
              private router: Router, private activeRoute: ActivatedRoute, private message:MessageService) {
  	this.submitted = false;
    this.router.events.subscribe((event)=>{
      if(event instanceof NavigationEnd){
          this.tokenValue = this.activeRoute.snapshot.queryParamMap.get('token');
          if(this.tokenValue==null){
            this.router.navigate(['login']);
          }
      }
    });
   }

  ngOnInit() {
  	this.resetPasswordForm = this.fb.group({
  		password:['', [Validators.required, Validators.minLength(6)]],
  		conf_password:['', [Validators.required, Validators.minLength(6), this.checkConfirmPassword]],
      token:['']
  	});

    if(this.tokenValue){
          this.resetPasswordForm.patchValue({
              token: this.tokenValue
          });
    }

  }

/**
 * Inner validation function 
 */
 
  checkConfirmPassword(c: FormControl) {
    if (!c.root && !c.root.get('password')) {
      return null;
    }
    if (c.value && c.value !== c.root.get('password').value) {
      return { error: true };
    }
  }

  get reset_f(){return this.resetPasswordForm.controls}


/**
* Password reset send
* @returns  success message
*/

  resetPasswordStore(){
  	this.submitted = true;
  	if(this.resetPasswordForm.invalid){
  		return ;
  	}
  	this.authService.create(this.global.apiUrl + '/api/reset-password', this.resetPasswordForm.value)
  		.subscribe((data)=>{
  			if(data.status ==200){
          let timerInterval;
              Swal.fire({
                title: 'Reset Password',
                html: '<p>Your password succesfully update.</p>'+
                    'I will close in <strong></strong> seconds.',
                timer: 5000,
                allowOutsideClick:false,
                onBeforeOpen: () => {
                  Swal.showLoading()
                  timerInterval = setInterval(() => {
                    Swal.getContent().querySelector('strong')
                      .textContent =(Swal.getTimerLeft() / 1000)
                        .toFixed(0)
                  }, 100)
                },
                onClose: () => {
                  clearInterval(timerInterval);
                  this.message.success(data.status_text);
                  this.router.navigate(['login']);
                }
              });
        }else if(data.status ==401){
          this.validationError = data.status_text;
        }
  		}, (error)=>{});
  }

}
