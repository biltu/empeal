import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../global';
import {AuthService} from '../../services/auth.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
declare var Swal: any;

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  forgotPasswordForm: FormGroup;
  showLoader: boolean;
  validation_error:string;
  constructor(private fb:FormBuilder, private global: Global,private authService: AuthService,
      private router: Router ) {
  	this.showLoader = false;
  }

  ngOnInit() {
  	this.forgotPasswordForm = this.fb.group({
  		email:['', [Validators.required, Validators.email]]
  	});
  }

/**
* User Forgot password send
* @returns success message
*/
  forgotPasswordStore(){
    console.log(this.forgotPasswordForm.value);
  	this.showLoader = true;
  	this.authService.create(this.global.apiUrl + '/api/forgot-password', this.forgotPasswordForm.value)
  		.subscribe((data)=>{
  			this.showLoader = false;
        if(data.status ==200){
              let timerInterval;
              Swal.fire({
                title: 'Reset Link Sent',
                html: '<p>Reset Link has been sent to you email address.</p>'+
                    'I will close in <strong></strong> seconds.',
                timer: 5000,
                allowOutsideClick:false,
                onBeforeOpen: () => {
                  Swal.showLoading()
                  timerInterval = setInterval(() => {
                    Swal.getContent().querySelector('strong')
                      .textContent =(Swal.getTimerLeft() / 1000)
                        .toFixed(0)
                  }, 100)
                },
                onClose: () => {
                  clearInterval(timerInterval);
                  this.router.navigate(['login']);
                }
              });


        }else if(data.status ==401){
          this.validation_error = data.status_text;
        }
  		}, (error)=>{});
  }

}
