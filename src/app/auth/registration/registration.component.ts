import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../services/message.service';
declare var Swal: any;
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {


  closeResult: string;
  employeeRegisterForm: FormGroup;
  corporateRegistrationForm: FormGroup;
  showloader: boolean;
  isCorporate:boolean;	
  submitted: boolean;
  termCondAgree: boolean;
  validationError: any ={};
  corporateUserId: number;
  location: any;
  team: any;

  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
   private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute, private modalService: NgbModal) {
  	this.showloader = false;
    this.isCorporate = false;
    this.submitted = false;
    this.termCondAgree = false;
    if(this.authService.isUserLoginIn()){
      this.router.navigate(['/dashboard']);
    }
    this.router.events.subscribe((event)=>{
      if(event instanceof NavigationEnd){
        this.isCorporate = event.url.includes('corporate')? true: false;
          let param1 = this.activeRoute.snapshot.queryParamMap.get('utm_source');
          this.location = this.activeRoute.snapshot.queryParamMap.get('utm_location');
          this.team = this.activeRoute.snapshot.queryParamMap.get('utm_team');
          if(!this.isCorporate && param1==null){
            this.router.navigate(['login']);
          }else{
            this.checkValidateToken(param1);
          }
      }
    });


   }

  ngOnInit() {
    this.employeeRegisterForm = this.fb.group({
      firstname: ['', [Validators.required]],
      lastname: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      conf_password:['', [Validators.required, Validators.minLength(6), this.checkConfirmPassword]],
      gender: ['', [Validators.required]],
      country: ['', [Validators.required]],
      term_policy:['', [Validators.required]],
      corporate_id:[''],
      location:[''],
      team:['']
    });

    this.corporateRegistrationForm = this.fb.group({
      firstname:['', [Validators.required]],
      city:['', [Validators.required]],
      state:['', [Validators.required]],
      zipcode:['', [Validators.required]],
      address:['', [Validators.required]],
      primary_cname:['', [Validators.required]],
      primary_cdesignation:['', [Validators.required]],
      primary_cemail:['', [Validators.required, Validators.email]],
      primary_cphone:['', [Validators.minLength(9), Validators.maxLength(13), Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
      adtnl_name:[''],
      adtnl_cdesignation:[''],
      adtnl_cemail:['',[Validators.email]],
      adtnl_cphone:['', [Validators.minLength(9), Validators.maxLength(13), Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
      user_name:['', [Validators.required]],
      email:['', [Validators.required,  Validators.email]],
      password:['', [Validators.required, Validators.minLength(6)]],
      conf_password:['', [Validators.required, Validators.minLength(6), this.checkConfirmPassword]],
      policy:['', [Validators.required]],
      primary_countrycode:['', [Validators.required]],
    });

    this.corporateRegistrationForm.get('policy').valueChanges.subscribe(value=>{
      console.log(value);
      this.termCondAgree = value;
    });

    this.employeeRegisterForm.get('term_policy').valueChanges.subscribe(value=>{
      this.termCondAgree = value;
    });
    
  }

   // convenience getter for easy access to form fields
    get cp_f() { return this.corporateRegistrationForm.controls; }

    get e_f(){return this.employeeRegisterForm.controls;}

/**
* Registration for corporate and employee
* @returns  success message
*/

  registrationStore(){
    this.submitted = true;
    if(!this.isCorporate){
      this.employeeRegisterForm.patchValue({
        corporate_id: this.corporateUserId,
        location: this.location,
        team: this.team
      });
    }
    
    let form = this.isCorporate ? this.corporateRegistrationForm: this.employeeRegisterForm;
    if(this.isCorporate){
      this.corporateRegistrationForm.get('policy').value == true?this.corporateRegistrationForm.get('policy').value: this.corporateRegistrationForm.get('policy').setValue('');
    }else{
      this.employeeRegisterForm.get('term_policy').value == true? this.employeeRegisterForm.get('term_policy').value: this.employeeRegisterForm.get('term_policy').setValue('');
    }

    if(form.invalid){
      return;
    }

    let registration_url = this.isCorporate ? '/api/registration': '/api/user/registration';

  	this.authService.create(this.global.apiUrl + registration_url, form.value)
  		.subscribe((data)=>{
  			if(data.status ==200){
           let timerInterval
              Swal.fire({
                title: 'Successfully Register',
                html: this.isCorporate ?'<p>You have registered successfully.<br/>A mail has been sent to your email <br/>address for verification.<br/>Please click on the link provided to confirm.</p>'+
                    'The window will close within <strong></strong> seconds.':'<p>You have registered successfully.<br/> You can login using email and password</p>'+
                    'The window will close within <strong></strong> seconds.</p>',
                timer: this.isCorporate ?15000:5000,
                allowOutsideClick:false,
                onBeforeOpen: () => {
                  Swal.showLoading()
                  timerInterval = setInterval(() => {
                    Swal.getContent().querySelector('strong')
                      .textContent =(Swal.getTimerLeft() / 1000)
                        .toFixed(0)
                  }, 100)
                }, 
                onClose: () => {
                  clearInterval(timerInterval);
                  window.location.href = this.global.domain_url +"/login";
                  //this.router.navigate(['login']);
                }
              });
        }else if(data.status ==422){
          this.validationError = data.error;
        }
  		}, (error)=>{});
  }

/**
* Check user token validation
* @returns  token
*/
  checkValidateToken(token){
    this.authService.create(this.global.apiUrl + '/api/user-register/token-validate',{token:token})
          .subscribe((data)=>{
            if(data.status ===200){
              this.corporateUserId = data.corporate_id;
            }else if(data.status ===422){
              return this.router.navigate(['login']);
            }
          }, (error)=>{});
  }

/**
* Inner validation funcion
*/
  checkConfirmPassword(c: FormControl) {
    if (!c.root && !c.root.get('password')) {
      return null;
    }
    if (c.value && c.value !== c.root.get('password').value) {
      return { error: true };
    }
  }

 // Open modal
  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title' ,size: 'lg'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

}
