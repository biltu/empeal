import { Component, OnInit } from '@angular/core';
import {ChatService} from '../../../services/chat.service';
import {CommonService} from '../../../services/common.service';
import {MessageService} from '../../../services/message.service';
import {Global} from '../../../global';
import * as io from 'socket.io-client';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Router, ActivatedRoute } from '@angular/router';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Component({
  selector: 'app-employee-chat',
  templateUrl: './employee-chat.component.html',
  styleUrls: ['./employee-chat.component.css']
})
export class EmployeeChatComponent implements OnInit {
  private socket;
  title = 'app';
  activeChat: any=[];
  createMessage ={
    to:'',
    from:'',
    to_name:'',
    from_name:'',
    message:'',
    attachfile:'',
    file_name:'',
    file_type:'',
    room:''
  };
  newMessage: any ={};
  search_chat_text: string;
  allUsers: any=[];
  loginUser: any ={};
  allchat: any =[];
  filterChat :any=[];
  coachData: any={};
  connectedUsers: any=[];
  coachOnline: boolean;
  conversationId : any;
  comntfilename: string;
  awsurl: string;
  utm_source: any;
  constructor(private _sanitizer: DomSanitizer, private ngxService: NgxUiLoaderService,private chatService: ChatService, private commonService: CommonService,
          private global: Global, private messageService: MessageService, private route: Router, private activeRoute: ActivatedRoute) { 
    this.loginUser = JSON.parse(localStorage.getItem('user'));
    this.coachOnline = true;
    this.conversationId = localStorage.getItem('conversationId');
    this.utm_source = this.activeRoute.snapshot.queryParams['utm_source'] ? this.activeRoute.snapshot.queryParams['utm_source']:'';
    if(this.utm_source !='' && this.conversationId !=''){
      this.botHandover(this.utm_source);
    }
  }

  ngOnInit() {
    this.socket = io.connect(this.global.node_server_url);
    this.socket.on('connectToRoom', function (room){
        this.socket.emit('new_joint', this.loginUser.id);
      }.bind(this));

    this.socket.on('userDetail', function(user){
      if(this.connectedUsers.findIndex(exist_user=> exist_user.socket_id == user.socket_id) == -1){
        this.connectedUsers.push(user);
      }
     if(this.coachData){
        this.setOnlineUser(this.coachData.id);
      }
    }.bind(this));

    this.socket.on('user_left', function(socketId){
      var index  = this.connectedUsers.findIndex(user=>user.socket_id == socketId);
      if(index >=0){
        this.connectedUsers.splice(index, 1);
      }
      if(this.coachData){
        this.setOnlineUser(this.coachData.id);
      }
      
      //this.coachOnline = false;
    }.bind(this));

    this.getAllUsers();
    this.getPreviousChat();
    this.socket.on('message', (msg) => {
      this.createMessage.message ='';
      this.createMessage.attachfile = '';
      this.createMessage.file_name = '';
      this.createMessage.file_type = '';
      this.comntfilename = '';
      this.ngxService.stop();
      msg.data.createdAt = this.timeConvert(msg.data.createdAt);
      msg.data.updatedAt = this.timeConvert(msg.data.updatedAt);
      this.awsurl = msg.awsurl;
      this.newMessage = msg.data;
      this.allchat.push(msg.data);
      this.filterChat = this.allchat.filter(chat=> (chat.room ==this.loginUser.id));
      if(this.route.url =='/chat' && msg.unread_user == this.loginUser.id){
        this.commonService.create(this.global.node_server_url + '/chat/set-read', {unread_ids:[msg.data._id],user_id:this.loginUser.id})
          .subscribe((data)=>{
          }, (error)=>{});
        
      }else if(this.route.url !='/chat' && msg.unread_user == this.loginUser.id){
        this.commonService.callChatUpdate({unread_data: msg.unread_data, unread_user: msg.unread_user});
      }

      });

  }

  /**
  * Create message
  */
   sendMessage() {
    if(this.createMessage.message ==''){
      this.messageService.error('Please put down some text');
      return false;
    }
    this.createMessage.from = this.loginUser.id;
    this.createMessage.from_name = this.loginUser.user_detail.firstname+' '+ this.loginUser.user_detail.lastname;
    this.createMessage.room = this.loginUser.id;
    this.socket.emit('message',this.createMessage);
    this.ngxService.start();
  }

  /**
  * Get all users
  */
  getAllUsers(){
    this.commonService.getAll(this.global.apiUrl + '/api/chat-user/health-coach')
      .subscribe((data)=>{
        this.coachData = data.data;
        this.createMessage.to = data.data.id;
        this.createMessage.to_name = data.data.user_detail.firstname+' '+ data.data.user_detail.lastname;

      }, (error)=>{});
  }
/**
* Set to user id
*/
  setToUserId(id){
    this.createMessage.to = id;
    
  }
/**
* Get all previous chat data
*/
  getPreviousChat(){
    this.commonService.getAllNodeApi(this.global.node_server_url + '/user-chat/' +this.loginUser.id )
      .subscribe((data)=>{
        this.allchat = data.data;
        this.awsurl = data.awsurl;
        this.filterChat = this.allchat.map((obj)=>{
          return{
            from:obj.from,from_name:obj.from_name,is_viewed:obj.is_viewed,message:obj.message,room:obj.room,
            to:obj.to,to_name:obj.to_name,_id:obj._id,attachfile:obj.attachfile,file_type:obj.file_type,createdAt:this.timeConvert(obj.createdAt),updatedAt:this.timeConvert(obj.updatedAt)
          }
        });
        
        let unreadChats =  this.filterChat.filter(obj=>obj.is_viewed ==false);
        let unreadIdArray = unreadChats.map(obj=>obj._id);
        this.commonService.create(this.global.node_server_url + '/chat/set-read', {unread_ids:unreadIdArray,user_id:this.loginUser.id})
             .subscribe((data)=>{
                 this.commonService.callChatUpdate({unread_data: data.data, unread_user: data.unread_user});
             }, (error)=>{});

      }, (error)=>{});
  }
/**
* Set whose users are online
*/
  setOnlineUser(id){
    return  this.connectedUsers.some(connUser=> String(connUser.user_id) == String(id));
    
  }

  timeConvert(date_time){
    let userTime =  new Date(date_time).toLocaleString("en-US", {timeZone: this.loginUser.user_detail.timezone});
    return new Date(userTime);
  }

  updateStyle(html) {
    return this._sanitizer.bypassSecurityTrustHtml(this.linkify(html));
  }

  /**
* Bot conversation handover to health coach
*/
  botHandover(source){
    if(this.loginUser.user_detail.conversation_id==this.conversationId){
    }else{
      this.commonService.getAll(this.global.apiUrl + '/api/user/bot/conversation?cnid='+this.conversationId)
      .subscribe((data)=>{
        // this.createMessage.from = this.loginUser.id;
        // this.createMessage.from_name = this.loginUser.user_detail.firstname+' '+ this.loginUser.user_detail.lastname;
        // this.createMessage.room = this.loginUser.id;
        // this.createMessage.message = data.data;
        // this.socket.emit('message',JSON.stringify(this.createMessage));
        // this.createMessage.message ='';
        localStorage.setItem('user', JSON.stringify(data.userdetails));
        // setTimeout(() => {
        //   window.location.reload();
        // }, 500);
        
      }, (error)=>{});
    }
  }

/**
* file select 
*/
  onFileSelected(event) {
   if(event.target.files.length > 0) 
    {
      this.comntfilename = event.target.files[0].name;
      let filetype = event.target.files[0].type;
      this.createMessage.attachfile = event.target.files[0];
      this.createMessage.file_name = this.comntfilename;
      this.createMessage.file_type = filetype;
    }else{
      this.messageService.error('Uploaded file is not a valid.');
    }
  }

  linkify(inputText) {
    var replacedText, replacePattern1, replacePattern2, replacePattern3;

    //URLs starting with http://, https://, or ftp://
    replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
    replacedText = inputText.replace(replacePattern1, '<a href="$1" target="_blank">$1</a>');

    //URLs starting with "www." (without // before it, or it'd re-link the ones done above).
    replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
    replacedText = replacedText.replace(replacePattern2, '$1<a href="http://$2" target="_blank">$2</a>');

    //Change email addresses to mailto:: links.
    replacePattern3 = /(([a-zA-Z0-9\-\_\.])+@[a-zA-Z\_]+?(\.[a-zA-Z]{2,6})+)/gim;
    replacedText = replacedText.replace(replacePattern3, '<a href="mailto:$1">$1</a>');

    return replacedText;
  }

}
