import { Component, OnInit } from "@angular/core";
import { ChatService } from "../../../services/chat.service";
import { CommonService } from "../../../services/common.service";
import { MessageService } from "../../../services/message.service";
import { Global } from "../../../global";
import * as io from "socket.io-client";
import { Router } from "@angular/router";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";
declare var $;
@Component({
  selector: "app-health-coach-chat",
  templateUrl: "./health-coach-chat.component.html",
  styleUrls: ["./health-coach-chat.component.css"],
})
export class HealthCoachChatComponent implements OnInit {
  private socket;
  title = "app";
  activeChat: any = [];
  search_text: string;
  search_chat_text: string;
  createMessage = {
    to: "",
    from: "",
    to_name: "",
    from_name: "",
    message: "",
    attachfile: "",
    file_name: "",
    file_type: "",
    room: "",
  };
  newMessage: any = {};
  allUsers: any = [];
  loginUser: any = {};
  allchat: any = [];
  filterChat: any = [];
  allGroups: any = [];
  connectedUsers: any = [];
  selectUser: any;
  unreadUserMsg: any = {};
  unreadchatCount: any = {};
  comntfilename: string;
  awsurl: string;
  lastChatDts: any = {};

  constructor(
    private _sanitizer: DomSanitizer,
    private ngxService: NgxUiLoaderService,
    private chatService: ChatService,
    private commonService: CommonService,
    private global: Global,
    private messageService: MessageService,
    private route: Router
  ) {
    this.loginUser = JSON.parse(localStorage.getItem("user"));

    $(document).on("keydown", ".usersearch", function (e) {
      let sdata = $(this).val();
      if (sdata.length > 1) {
        $(".chatlist ul").css("height", "auto");
        console.log("ssss");
      } else {
        $(".chatlist ul").css("height", "210px");
        console.log("dddd" + sdata.length);
      }
    });
  }

  ngOnInit() {
    this.socket = io.connect(this.global.node_server_url);
    this.socket.on(
      "connectToRoom",
      function (room) {
        this.socket.emit("new_joint", this.loginUser.id);
      }.bind(this)
    );

    this.socket.on(
      "userDetail",
      function (user) {
        if (
          this.connectedUsers.findIndex(
            (exist_user) => exist_user.socket_id == user.socket_id
          ) == -1
        ) {
          this.connectedUsers.push(user);
        }
        if (this.allGroups) {
          this.allGroups.forEach((group) => {
            group.group_user.forEach((user) => {
              this.setOnlineUser(user.id);
            });
          });
        }
      }.bind(this)
    );

    this.socket.on(
      "user_left",
      function (socketId) {
        var index = this.connectedUsers.findIndex(
          (user) => user.socket_id == socketId
        );

        if (index >= 0) {
          this.connectedUsers.splice(index, 1);
        }
        if (this.allGroups) {
          this.allGroups.forEach((group) => {
            group.group_user.forEach((user) => {
              this.setOnlineUser(user.id);
            });
          });
        }
      }.bind(this)
    );

    this.getAllUsers();
    this.socket.on("message", (msg) => {
      this.createMessage.message = "";
      this.createMessage.attachfile = "";
      this.createMessage.file_name = "";
      this.createMessage.file_type = "";
      this.comntfilename = "";
      this.ngxService.stop();
      msg.data.createdAt = this.timeConvert(msg.data.createdAt);
      msg.data.updatedAt = this.timeConvert(msg.data.updatedAt);
      this.awsurl = msg.awsurl;
      this.newMessage = msg.data;
      this.allchat.push(msg.data);
      this.filterChat = this.allchat.filter(
        (chat) => chat.room == (this.selectUser && this.selectUser.id)
      );

      if (
        this.route.url == "/chat" &&
        msg.unread_user == this.loginUser.id &&
        this.selectUser &&
        this.selectUser.id == msg.from_user
      ) {
        this.commonService
          .create(this.global.node_server_url + "/chat/set-read", {
            unread_ids: [msg.data._id],
            user_id: this.loginUser.id,
          })
          .subscribe(
            (data) => {},
            (error) => {}
          );
      } else {
        this.commonService.callChatUpdate({
          unread_data: msg.unread_data,
          unread_user: msg.unread_user,
        });
      }

      if (!this.selectUser) {
        this.allGroups.forEach((group) => {
          group.group_user.forEach((user) => {
            if (user.id == msg.data.room) {
              user.unread_msg.push(msg.data);
            }
          });
        });
      } else if (this.selectUser && this.selectUser.id != msg.data.room) {
        this.allGroups.forEach((group) => {
          group.group_user.forEach((user) => {
            if (user.id == msg.data.room) {
              user.unread_msg.push(msg.data);
            }
          });
        });
      }
    });
  }

  //console.log(e.target.value);
  // if(e.target.value.length >3){
  //   $('.collapse1').css('height','0px');
  // }else{
  //   $('.collapse1').css('height','210px');
  // }

  /**
   * Create message
   */
  sendMessage() {
    if (this.createMessage.to == "") {
      this.messageService.error("Please select to user whom you want to chat");
      return false;
    }
    if (this.createMessage.message == "") {
      this.messageService.error("Please put down some text");
      return false;
    }
    this.createMessage.from = this.loginUser.id;
    this.createMessage.from_name =
      this.loginUser.user_detail.firstname +
      " " +
      this.loginUser.user_detail.lastname;
    this.createMessage.room = this.selectUser.id;
    this.socket.emit("message", this.createMessage);
    this.ngxService.start();
  }

  /**
   * get all users
   */
  getAllUsers() {
    let userindex = 0;
    this.commonService
      .getAll(this.global.node_server_url + "/get/lastMessageDt")
      .subscribe((data) => {
        this.lastChatDts = data.data;
      });

    this.commonService
      .getAll(this.global.apiUrl + "/api/chat-user/group")
      .subscribe(
        (data) => {
          this.allGroups = data.data;
          this.allGroups.forEach((group) => {
            group.group_user.forEach((user) => {
                if(userindex==0){
                  this.setToUserId(user);
                }
              user.unread_msg = [];
              userindex++;
            });
          });
          this.commonService
            .getAll(
              this.global.node_server_url +
                "/chat/unread-chat/" +
                this.loginUser.id
            )
            .subscribe((data) => {
              this.allGroups.forEach((group) => {
                group.group_user.forEach((user) => {
                  user.unread_msg = data.data.filter(
                    (obj) => obj.from == user.id
                  );
                });
              });
            });
        },
        (error) => {}
      );
  }

  /**
   * Set create message user detail
   */
  setToUserId(user) {
    this.createMessage.to = user.id;
    this.createMessage.to_name =
      user.user_detail.firstname + " " + user.user_detail.lastname;
    this.selectUser = user;
    if(user.unread_msg){
      user.unread_msg.length = 0;
    }
    
    this.getPreviousChat(user);
  }

  /**
   * get previous chat
   */
  getPreviousChat(user) {


    this.commonService
      .getAllNodeApi(this.global.node_server_url + "/user-chat/" + user.id)
      .subscribe(
        (data) => {
          this.allchat = data.data;
          this.awsurl = data.awsurl;
          this.filterChat = this.allchat.map((obj) => {
            return {
              from: obj.from,
              from_name: obj.from_name,
              is_viewed: obj.is_viewed,
              message: obj.message,
              room: obj.room,
              to: obj.to,
              to_name: obj.to_name,
              _id: obj._id,
              attachfile: obj.attachfile,
              file_type: obj.file_type,
              createdAt: this.timeConvert(obj.createdAt),
              updatedAt: this.timeConvert(obj.updatedAt),
            };
          });
          let unreadChats = this.filterChat.filter(
            (obj) => obj.is_viewed == false
          );
          let unreadIdArray = unreadChats.map((obj) => obj._id);
          this.commonService
            .create(this.global.node_server_url + "/chat/set-read", {
              unread_ids: unreadIdArray,
              user_id: this.loginUser.id,
            })
            .subscribe(
              (data) => {
                this.commonService.callChatUpdate({
                  unread_data: data.data,
                  unread_user: data.unread_user,
                });
              },
              (error) => {}
            );
        },
        (error) => {}
      );
  }

  /**
   * Set online users
   */
  setOnlineUser(id) {
    //console.log(id);
    return this.connectedUsers.some((connUser) => connUser.user_id == id);
  }

  timeConvert(date_time) {
    let userTime = new Date(date_time).toLocaleString("en-US", {
      timeZone: this.loginUser.user_detail.timezone,
    });
    return new Date(userTime);
  }

  updateStyle(html) {
    return this._sanitizer.bypassSecurityTrustHtml(this.linkify(html));
  }

  /**
   * file select
   */
  onFileSelected(event) {
    if (event.target.files.length > 0) {
      this.comntfilename = event.target.files[0].name;
      let filetype = event.target.files[0].type;
      this.createMessage.attachfile = event.target.files[0];
      this.createMessage.file_name = this.comntfilename;
      this.createMessage.file_type = filetype;
    } else {
      this.messageService.error("Uploaded file is not a valid.");
    }
  }

  linkify(inputText) {
    var replacedText, replacePattern1, replacePattern2, replacePattern3;

    //URLs starting with http://, https://, or ftp://
    replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
    replacedText = inputText.replace(replacePattern1, '<a href="$1" target="_blank">$1</a>');

    //URLs starting with "www." (without // before it, or it'd re-link the ones done above).
    replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
    replacedText = replacedText.replace(replacePattern2, '$1<a href="http://$2" target="_blank">$2</a>');

    //Change email addresses to mailto:: links.
    replacePattern3 = /(([a-zA-Z0-9\-\_\.])+@[a-zA-Z\_]+?(\.[a-zA-Z]{2,6})+)/gim;
    replacedText = replacedText.replace(replacePattern3, '<a href="mailto:$1">$1</a>');

    return replacedText;
  }


}
