import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChatComponent } from './chat/chat.component';
import { AuthGuard } from './../auth.guard';
import { StatutoryGuard } from './../statutory.guard';

const routes: Routes = [
	{path:'',canActivate: [AuthGuard, StatutoryGuard], component: ChatComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChatRoutingModule { }
