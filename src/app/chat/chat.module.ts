import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CoreModule} from '../core/core.module';

import { ChatRoutingModule } from './chat-routing.module';
import { ChatComponent } from './chat/chat.component';
import { EmployeeChatComponent } from './chat/employee-chat/employee-chat.component';
import { HealthCoachChatComponent } from './chat/health-coach-chat/health-coach-chat.component';
import { SecondaryCoachChatComponent } from './chat/secondary-coach-chat/secondary-coach-chat.component';

@NgModule({
  declarations: [ChatComponent, EmployeeChatComponent, HealthCoachChatComponent,SecondaryCoachChatComponent],
  imports: [
    CommonModule,
    ChatRoutingModule,
    CoreModule
  ]
})
export class ChatModule { }
