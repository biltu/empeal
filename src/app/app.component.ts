import { Component } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart,
 Event as RouterEvent,NavigationCancel, NavigationError } from '@angular/router';
import { CommonService } from './services/common.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'frontend';
  loggedInUrl:boolean=false; 
  constructor(private router: Router, private commonService: CommonService){
  	this.router.events.subscribe((event)=>{
  		if(event instanceof NavigationEnd){
        if(event.url =='/'){
          if(localStorage.getItem('token')){
            this.router.navigate(['dashboard']);
          }else{
            this.router.navigate(['login']);
          }
        }
  			if(event.url.includes('login') || event.url.includes('register') || event.url.includes('forgot-password')
            || event.url.includes('reset-password')){
  				this.loggedInUrl = false;
  			}else{
  				this.loggedInUrl = true;
  			}
  			
  		}
  	});

    router.events.subscribe((event: RouterEvent) => {
      this.navigationInterceptor(event)
    });
  }

  navigationInterceptor(event: RouterEvent): void {
    if (event instanceof NavigationStart) {
      this.commonService.callNavigationUpdate('update');
    }
    if (event instanceof NavigationEnd) {

    }

    // Set loading state to false in both of the below events to hide the spinner in case a request fails
    if (event instanceof NavigationCancel) {

    }
    if (event instanceof NavigationError) {

    }
  }

}
