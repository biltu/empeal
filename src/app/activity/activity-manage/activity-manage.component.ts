import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../services/message.service';
import { DeviceDetectorService } from 'ngx-device-detector';
declare var Swal: any;
declare var ApexCharts: any;
declare var $:any;
@Component({
  selector: 'app-activity-manage',
  templateUrl: './activity-manage.component.html',
  styleUrls: ['./activity-manage.component.css']
})
export class ActivityManageComponent implements OnInit {
	isModalOpen: boolean;
	appointments: any=[];
	activityAddForm: FormGroup;
	display: string;
    chartdisplay: string;
    deskchart: string;
	model: any;
	getitems: any=[];
	today: any;
	activities: any=[];
	show:boolean = false;
	soption: any=[];
	timeoptions: any=[];
	dayactivities: any=[];
	sdfsf:any=[];
	sleephrs:any;
	activinfogrph: string;
	activity_source:any;
  	topChartOptions = {
    	//responsive: true,
	    title: {
	      display: false,
	      text: ''
	    },
	    tooltips: {
	        callbacks: {
	            label: function(tooltipItem,data) {
	            	//console.log(tooltipItem);
	                //return "Distance: "+Number(tooltipItem.yLabel) + " Km";
	                var labelnm = data.datasets[tooltipItem.datasetIndex].label || '';
	                var label = '';
                    if (labelnm) {
                        label += labelnm+': ';
                    }

                    label += tooltipItem.yLabel;
                    if (labelnm=='Resting' || labelnm=='Fat Burn' || labelnm=='Cardio' || labelnm=='Peak') {
                    	label += ' mins';
                    }
                    return label;
	            }
	        },
            mode: 'index',
	      	intersect: true
        },
	    scales: {
	      xAxes: [{
	        stacked: true,
	      }],
	      yAxes: [{
	        id: 'A',
	        type: 'linear',
	        position: 'left',
	        ticks: {
	          min: 0,
	          display: false,
	        }
	      }, {
	        id: 'B',
	        type: 'linear',
	        position: 'right',
	        ticks: {
	          min: 0,
	          display: false,
	        },
	        gridLines: {
	            display:false
	        }
	      }]
	    },
	    legend: {
            display: false
        }
    };
    topChartLabels =  [];

    topChartData:any = [
    	{
	      type: 'bar',
	      label: '',
	      backgroundColor: '',
	      hoverBackgroundColor:'',
	      hoverBorderColor: 'white',
	      stack: 'Stack 0',
	      data: [],
	      borderColor: 'white',
	      borderWidth: 2,
	      yAxisID: 'A'
	    },{
	      type: 'bar',
	      label: '',
	      backgroundColor: '',
	      hoverBackgroundColor:'',
	      hoverBorderColor: 'white',
	      stack: 'Stack 0',
	      data: [],
	      borderColor: 'white',
	      borderWidth: 2,
	      yAxisID: 'A'
	    }, {
	      type: 'bar',
	      label: '',
	      backgroundColor: '',
	      hoverBackgroundColor:'',
	      hoverBorderColor: 'white',
	      stack: 'Stack 0',
	      data: [],
	      borderColor: 'white',
	      borderWidth: 2,
	      yAxisID: 'A'
	    }, {
	      type: 'bar',
	      label: '',
	      backgroundColor: '',
	      hoverBackgroundColor:'',
	      hoverBorderColor: 'white',
	      stack: 'Stack 0',
	      data: [],
	      borderColor: 'white',
	      borderWidth: 2,
	      yAxisID: 'A'
	    }];

	manualOptions = {
    	//responsive: true,
	    title: {
        display: true,
        position: 'top',
        text: 'Last 30 Days Activities Data',
        fontSize: 13
      	},
      	tooltips: {
        mode: 'index',
        intersect: true
      	},
	    //barValueSpacing: 20,
	    scales: {
            yAxes: [{
              ticks: {
                  min: 0,
                  max: 100
              }
            }]
	    }
    };

    manualChartLabels =  [];

    manualChartData:any = [
    	{
	      label: 'Cardio',
	      backgroundColor: 'rgb(255, 159, 64)',
	      hoverBackgroundColor:'rgb(255, 159, 64)',
	      //stack: 'Stack 0',
	      data: []
	    }, 
	    {
	      label: 'Circuit Training',
	      backgroundColor: 'rgb(139,69,19)',
	      hoverBackgroundColor:'rgb(139,69,19)',
	      //stack: 'Stack 0',
	      data: []
	    },
    	{
	      label: 'Resistance / Weight Training',
	      backgroundColor: 'rgb(255, 105, 180)',
	      hoverBackgroundColor:'rgb(255, 105, 180)',
	      //stack: 'Stack 0',
	      data: []
	    }];

    rightSideChartOptions = {
    	title: {
            display: true,
            text: 'Last 30 Days Sleep Data'
        },
    	responsive: true,
    	showTooltips: false,
	    scales: {
	        yAxes: [{
	          stacked: true,
	          ticks: {
	            beginAtZero: true
	          }
	        }],
	        xAxes: [{
	          stacked: true,
	          ticks: {
	            beginAtZero: true
	          }
	        }]
      	},
      	legend: {
            display: false
        }
    };

    rightSideChartLabels =  [];

    rightSideChartData:any = [
    	{
    	  label: 'Disturbed Sleep',
	      backgroundColor: [],
	      hoverBackgroundColor:[],
	      borderColor: 'white',
	      data: []
	    }, 
	    {
	      label: 'Regular sleep',
	      backgroundColor: [],
	      hoverBackgroundColor:[],
	      borderColor: 'white',
	      data: []
	    }, {
	      label: 'Deep Sleep',
	      backgroundColor: [],
	      hoverBackgroundColor:[],
	      borderColor: 'white',
	      data: []
	    }, {
	      label: 'Awake',
	      backgroundColor: [],
	      hoverBackgroundColor:[],
	      borderColor: 'white',
	      data: []
	    }];


	    heartrateoptions = {
            // chart: {
            //     height: 150,
            //     type: 'bar',
            //     //stacked: true,
            //     toolbar: {
            //       show: false, 
            //     }
            // },
            // plotOptions: {
            //     bar: {
            //         horizontal: true,
            //     },
                
            // },
            // stroke: {
            //     width: 1,
            //     colors: ['#fff']
            // },
            // series: [{
            //     data: []
            // }],
            // colors: ['#008ffb', '#00e396', '#feb019', '#ff6781'],
            // dataLabels: {
            //     enabled: false,
            //     textAnchor: 'start',
            //     style: {
            //         colors: ['#fff']
            //     },
            //     formatter: function(val, opt) {
            //         return '';
            //     },
            //     offsetX: 0,
            //     dropShadow: {
            //       enabled: true
            //     }
            // },
            // xaxis: {
            // 	categories: ['Resting (mins)','Fat Burn  (mins)','Cardio  (mins)','Peak  (mins)'],
            //     labels: {
            //         show: false,
            //     },
            //     show: false,
            // },
            // tooltip: {
            //     theme: 'dark',
            //     x: {
            //         show: false
            //     },
            //     y: {
            //         title: {
            //             formatter: function(fv) {
            //             	if(fv=='series-1'){
            //             		return 'Resting (mins)';
            //             	}else{
            //             		return ''
            //             	}
            //                 console.log();
            //             }
            //         }
            //     }
            // },
            
            // fill: {
            //     opacity: 1
                
            // },
            
            // legend: {
            //     show: false
            // }

            chart: {
                height: 150,
                type: 'bar',
                toolbar: {
                  show: false, 
                }
            },
            plotOptions: {
                bar: {
                    barHeight: '100%',
                    distributed: true,
                    horizontal: true,
                    dataLabels: {
                        position: 'bottom'
                    },
                }
            },
            colors: ['#fd7e14', '#20c997', '#007bff', '#2698ff'],
            dataLabels: {
                enabled: true,
                textAnchor: 'start',
                style: {
                    colors: ['#fff']
                },
                formatter: function(val, opt) {
                    return opt.w.globals.labels[opt.dataPointIndex] + ":  " + val
                },
                offsetX: 0,
                dropShadow: {
                  enabled: true
                }
            },
            series: [{
                data: [400, 430, 448, 470]
            }],
            stroke: {
                width: 1,
              colors: ['#fff']
            },
            xaxis: {
                categories: ['Cardio  (mins)','Fat Burn  (mins)','Resting (mins)','Peak  (mins)'],
            	    labels: {
                    show: false,
                },
                show: false,
            },
            yaxis: {
                labels: {
                    show: false
                }
            },
            title: {
                show: false
            },
            
            tooltip: {
                theme: 'dark',
                x: {
                    show: false
                },
                y: {
                    title: {
                        formatter: function() {
                            return ''
                        }
                    }
                }
            }
        };
     private chartObj: any;
     hrtrate:any=[];
     distanceper:any;
     activemnper:any;
     calbrnper:any;
     florsper:any;
     isLoaded: boolean=false;
     screenwidth:any;
     isMobile:any;
  constructor(private fb:FormBuilder, private deviceService: DeviceDetectorService, private message:MessageService ,private global: Global,private authService: AuthService, private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) { 
  	this.asyncInit();
  	this.today = new Date;
  	this.isModalOpen = false;
    this.screenwidth = (<any>window.innerWidth > 0) ? <any>window.innerWidth : screen.width;
    this.isMobile = this.deviceService.isMobile();
      
    if(this.isMobile && this.screenwidth<=425){
      this.deskchart ='none';
        }else{
      this.deskchart ='block';
    }

    setTimeout(()=>{
        $('.chart').easyPieChart({
            animate: 2000,
            lineWidth: 3,
            lineCap: 'butt',
            scaleColor: false,      
            barColor: '#a4d439',
            trackColor: '#ddd',
        });
    },1000);
  }

  	ngOnInit() {
  		this.activityAddForm = this.fb.group({
	  		activitydate:['', [Validators.required]],
	  		activitytype:['', [Validators.required]],
	 		  activitytime:['', [Validators.required]],
	 		  activitypre:['', []]
	  	});
  	}
/**
* Display activity data 
*/
  	asyncInit(){
  	this.commonService.getAll(this.global.apiUrl + '/api/activity')
  		.subscribe((data)=>{
        this.activities = data.data;
        this.dayactivities = (data.dayactivities.length>0)?data.dayactivities[0]:[];
        this.distanceper = (data.dayactivities.length>0 && this.dayactivities.distance!='')?this.dayactivities.distance.replace(/,/g, '')*100/data.userstandards.distance:0;
        this.activemnper = (data.dayactivities.length>0 && this.dayactivities.active_time!='')?this.dayactivities.active_time.replace(/,/g, '')*100/data.userstandards.active_time:0;
        this.calbrnper = (data.dayactivities.length>0 && this.dayactivities.fat_burn!='')?this.dayactivities.fat_burn.replace(/,/g, '')*100/data.userstandards.fat_burn:0;
        this.florsper = (data.dayactivities.length>0 && this.dayactivities.floors>0)?this.dayactivities.floors.replace(/,/g, '')*100/data.userstandards.floors:0;
        this.topChartLabels = data.date_activities[0];
        this.sdfsf=[data.date_activities[1],data.date_activities[2],data.date_activities[3],data.date_activities[7],data.date_activities[5],data.date_activities[10],data.date_activities[9],data.date_activities[8],data.date_activities[4]];
        this.manualChartLabels = data.activity_ccr[0];
        this.manualChartData[0].data = data.activity_ccr[1];
        this.manualChartData[1].data = data.activity_ccr[2];
        this.manualChartData[2].data = data.activity_ccr[3];
        this.rightSideChartLabels = data.date_sleep[0];
        this.rightSideChartData[0].data = data.date_sleep[1];
        this.rightSideChartData[1].data = data.date_sleep[2];
        this.rightSideChartData[2].data = data.date_sleep[3];
        this.rightSideChartData[3].data = data.date_sleep[8];
        this.rightSideChartData[0].backgroundColor = data.date_sleep[4];
        this.rightSideChartData[1].backgroundColor = data.date_sleep[5];
        this.rightSideChartData[2].backgroundColor = data.date_sleep[6];
        this.rightSideChartData[3].backgroundColor = data.date_sleep[9];
        this.rightSideChartData[0].hoverBackgroundColor = data.date_sleep[4];
        this.rightSideChartData[1].hoverBackgroundColor = data.date_sleep[5];
        this.rightSideChartData[2].hoverBackgroundColor = data.date_sleep[6];
        this.rightSideChartData[3].hoverBackgroundColor = data.date_sleep[9];
        this.sleephrs = data.date_sleep[7];
        this.activity_source = data.activity_source;
        this.hrtrate = data.heartratechart;
        this.topChartData[0].data=this.sdfsf[0];
		this.topChartData[0].label = 'Steps';
		this.topChartData[0].backgroundColor = 'rgb(255, 99, 132)';
		this.topChartData[0].hoverBackgroundColor = 'rgb(255, 99, 132)';
		this.isLoaded = true;
		if(this.hrtrate.cardio > 0 || this.hrtrate.fat_burn > 0 || this.hrtrate.resting > 0 || this.hrtrate.peak > 0){
            console.log(data.heartratechart);
			this.heartrateoptions.series[0].data[0] = data.heartratechart.cardio;
	        this.heartrateoptions.series[0].data[1] = data.heartratechart.fat_burn;
	        this.heartrateoptions.series[0].data[2] = data.heartratechart.resting;
	        this.heartrateoptions.series[0].data[3] = data.heartratechart.peak;
	        this.chartObj  = new ApexCharts(
            document.querySelector("#heartratechart"),
            this.heartrateoptions
	        );

      		this.render();
      		this.isLoaded = true;
  		}
  		}, (error)=>{});
  	}

  	render(): Promise<void> {
    	return this.chartObj.render();
  	}

  	openModalDialog(){
    	this.display='block'; //Set block css
    	this.isModalOpen = true;
    	//document.getElementsByClassName("modalend").classList.add("modal-overlay");
	  }

  	closeModalDialog(){
  		this.display='none'; //set none css after close dialog
  		this.isModalOpen = false;
  		//document.getElementsByClassName("modalend").classList.add("modal-overlay");
  	}
/**
* Get activity type 
*/
	changeActivitytype(e){
		if(e.target.value=='Sleep'){
			this.show = true;
			this.soption = [
				{ name: 'Select Perceived Freshness', value: "" },
				{ name: '1', value: "1" },
				{ name: '2', value: "2" },
				{ name: '3', value: "3" }
			];
			this.timeoptions = [
				{ name: '1 Hour', value: "1" },
				{ name: '2 Hour', value: "2" },
				{ name: '3 Hour', value: "3" },
				{ name: '4 Hours', value: "4" },
				{ name: '5 Hours', value: "5" },
				{ name: '6 Hours', value: "6" },
				{ name: '7 Hours', value: "7" },
				{ name: '8 Hours', value: "8" },
				{ name: '9 Hours', value: "9" },
				{ name: '10 Hours', value: "10" } 
			];
		}else{
			this.show = false;
			this.soption = [
				{ name: 'Select Rate Of Perceived Exertion', value: "" },
				{ name: '1', value: "1" },
				{ name: '2', value: "2" },
				{ name: '3', value: "3" },
				{ name: '4', value: "4" },
				{ name: '5', value: "5" },
				{ name: '6', value: "6" },
				{ name: '7', value: "7" },
				{ name: '8', value: "8" },
				{ name: '9', value: "9" },
				{ name: '10', value: "10" }
			];

			this.timeoptions = [
				{ name: '10 mins', value: "10" },
				{ name: '20 mins', value: "20" },
				{ name: '30 mins', value: "30" },
				{ name: '40 mins', value: "40" },
				{ name: '50 mins', value: "50" },
				{ name: '60 mins', value: "60" },
				{ name: '70 mins', value: "70" },
				{ name: '80 mins', value: "80" },
				{ name: '90 mins', value: "90" }
			];
		}
		
	}
/**
* Store manual activity data 
*/
	activityStore(){
		this.commonService.create(this.global.apiUrl + '/api/activity',this.activityAddForm.value)
      .subscribe((data)=>{
        if(data.status ==200){
        	this.activities.push(data.data);
          this.message.success(data.status_text);
          this.display='none';
          this.activityAddForm.reset();
          this.isModalOpen = false;
        }

        if(data.status ==400){
        	this.message.error(data.status_text);
        }

        if(data.status ==500){
        	this.message.error('Please search and select food/receipe from dropdown list.');
        }

      }, (error)=>{
	  });
	}

/**
* Remove manual activity data from list
*/
	activityDelete(id, index){
		Swal.fire({
		  title: 'Are you sure?',
		  text: "You won't be able to revert this!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!'
		}).then((result) => {
			if (result.value) {
				this.commonService.delete(this.global.apiUrl + '/api/activity', id)
  			.subscribe((data)=>{
  			if(data.status ==200){	 
  				 this.activities.splice(index,1);					
			    Swal.fire(
			      'Deleted!',
			      'Your meal has been deleted.',
			      'success'
			    )
  			 }
  			},(error)=>{});
		  }
		}); 
	}

  closeChartModalDialog(){
    this.chartdisplay='none'; //Set block css
    this.isModalOpen = false;
  }

/**
* Display activity chart 
*/
	topchartchange(type){
    if(this.isMobile && this.screenwidth<=425){
      this.chartdisplay='block'; //Set block css
      this.deskchart ='none';
      this.isModalOpen = true;
    }else{
      this.chartdisplay='none'; //Set block css
      this.deskchart ='block';
      this.isModalOpen = false;
    }

		if(type=='steep'){
			this.topChartData[0].data=this.sdfsf[0];
			this.topChartData[0].label = 'Steps';
			this.topChartData[0].backgroundColor = 'rgb(255, 99, 132)';
			this.topChartData[0].hoverBackgroundColor = 'rgb(255, 99, 132)';
			this.topChartData[1].label = '';
			this.topChartData[1].data='';
			this.topChartData[1].backgroundColor = '';
			this.topChartData[2].label = '';
			this.topChartData[2].data='';
			this.topChartData[2].backgroundColor = '';
			this.topChartData[3].label = '';
			this.topChartData[3].data='';
			this.topChartData[3].backgroundColor = '';
			this.topChartData[1].hoverBackgroundColor = '';
			this.topChartData[2].hoverBackgroundColor = '';
			this.topChartData[3].hoverBackgroundColor = '';
			document.getElementById("topcharthding").innerHTML = '<img src="assets/images/step_25.png" alt="icon"> Last 3o days record';
      document.getElementById("mbltopcharthding").innerHTML = '<img src="assets/images/step_25.png" alt="icon"> Last 3o days record';
    }
		if(type=='distance'){
			this.topChartData[0].data=this.sdfsf[1];
			this.topChartData[0].label = 'Distance';
			this.topChartData[0].backgroundColor = 'rgb(54, 162, 235)';
			this.topChartData[0].hoverBackgroundColor = 'rgb(54, 162, 235)';
			this.topChartData[1].label = '';
			this.topChartData[1].data='';
			this.topChartData[1].backgroundColor = '';
			this.topChartData[2].label = '';
			this.topChartData[2].data='';
			this.topChartData[2].backgroundColor = '';
			this.topChartData[3].label = '';
			this.topChartData[3].data='';
			this.topChartData[3].backgroundColor = '';
			this.topChartData[1].hoverBackgroundColor = '';
			this.topChartData[2].hoverBackgroundColor = '';
			this.topChartData[3].hoverBackgroundColor = '';
			document.getElementById("topcharthding").innerHTML = '<img src="assets/images/km_25.png" alt="icon"> Last 3o days record';
      document.getElementById("mbltopcharthding").innerHTML = '<img src="assets/images/km_25.png" alt="icon"> Last 3o days record';
		}
		if(type=='active_time'){
			this.topChartData[0].data=this.sdfsf[2];
			this.topChartData[0].label = 'Active min';
			this.topChartData[0].backgroundColor = 'rgb(255, 69, 0)';
			this.topChartData[0].hoverBackgroundColor = 'rgb(255, 69, 0)';
			this.topChartData[1].label = '';
			this.topChartData[1].data='';
			this.topChartData[1].backgroundColor = '';
			this.topChartData[2].label = '';
			this.topChartData[2].data='';
			this.topChartData[2].backgroundColor = '';
			this.topChartData[3].label = '';
			this.topChartData[3].data='';
			this.topChartData[3].backgroundColor = '';
			this.topChartData[1].hoverBackgroundColor = '';
			this.topChartData[2].hoverBackgroundColor = '';
			this.topChartData[3].hoverBackgroundColor = '';
			document.getElementById("topcharthding").innerHTML = '<img src="assets/images/active_25.png" alt="icon"> Last 3o days record';
      document.getElementById("mbltopcharthding").innerHTML = '<img src="assets/images/active_25.png" alt="icon"> Last 3o days record';
		}
		if(type=='fat_burn'){
			this.topChartData[0].data=this.sdfsf[3];
			this.topChartData[0].label = 'Calories Burn';
			this.topChartData[0].backgroundColor = 'rgb(255, 159, 64)';
			this.topChartData[0].hoverBackgroundColor = 'rgb(255, 159, 64)';
			this.topChartData[1].label = '';
			this.topChartData[1].data='';
			this.topChartData[1].backgroundColor = '';
			this.topChartData[2].label = '';
			this.topChartData[2].data='';
			this.topChartData[2].backgroundColor = '';
			this.topChartData[3].label = '';
			this.topChartData[3].data='';
			this.topChartData[3].backgroundColor = '';
			this.topChartData[1].hoverBackgroundColor = '';
			this.topChartData[2].hoverBackgroundColor = '';
			this.topChartData[3].hoverBackgroundColor = '';
			document.getElementById("topcharthding").innerHTML = '<img src="assets/images/fatBurn_25.png" alt="icon"> Last 3o days record';
      document.getElementById("mbltopcharthding").innerHTML = '<img src="assets/images/fatBurn_25.png" alt="icon"> Last 3o days record';
		}

		if(type=='floors'){
			this.topChartData[0].data=this.sdfsf[4];
			this.topChartData[0].label = 'Floors';
			this.topChartData[0].backgroundColor = 'rgb(153, 102, 255)';
			this.topChartData[0].hoverBackgroundColor = 'rgb(153, 102, 255)';
			this.topChartData[1].label = '';
			this.topChartData[1].data='';
			this.topChartData[1].backgroundColor = '';
			this.topChartData[2].label = '';
			this.topChartData[2].data='';
			this.topChartData[2].backgroundColor = '';
			this.topChartData[3].label = '';
			this.topChartData[3].data='';
			this.topChartData[3].backgroundColor = '';
			this.topChartData[1].hoverBackgroundColor = '';
			this.topChartData[2].hoverBackgroundColor = '';
			this.topChartData[3].hoverBackgroundColor = '';
			document.getElementById("topcharthding").innerHTML = '<img src="assets/images/floor_25.png" alt="icon"> Last 3o days record';
      document.getElementById("mbltopcharthding").innerHTML = '<img src="assets/images/floor_25.png" alt="icon"> Last 3o days record';
		}

		if(type=='heartrate'){
			this.topChartData[0].data=this.sdfsf[5];
			this.topChartData[0].label = 'Resting';
			this.topChartData[0].backgroundColor = 'rgb(0, 143, 251)';
			this.topChartData[0].hoverBackgroundColor = 'rgb(0, 143, 251)';
			this.topChartData[1].label = 'Fat Burn';
			this.topChartData[1].data=this.sdfsf[8];
			this.topChartData[1].backgroundColor = 'rgb(0, 227, 150)';
			this.topChartData[1].hoverBackgroundColor = 'rgb(0, 227, 150)';
			this.topChartData[2].label = 'Cardio';
			this.topChartData[2].data=this.sdfsf[7];
			this.topChartData[2].backgroundColor = 'rgb(254, 176, 25)';
			this.topChartData[2].hoverBackgroundColor = 'rgb(254, 176, 25)';
			this.topChartData[3].label = 'Peak';
			this.topChartData[3].data=this.sdfsf[6];
			this.topChartData[3].backgroundColor = 'rgb(255, 69, 96)';
			this.topChartData[3].hoverBackgroundColor = 'rgb(255, 69, 96)';
			document.getElementById("topcharthding").innerHTML = '<img src="assets/images/heart-filled_25.png" alt="icon"> Last 3o days';
      document.getElementById("mbltopcharthding").innerHTML = '<img src="assets/images/heart-filled_25.png" alt="icon"> Last 3o days';
		}		
		
	}
}
