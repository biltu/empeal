import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './../auth.guard';
import { EmployeeGuard } from './../employee.guard';
import { CoachGuard } from './../coach.guard';
import { ActivityManageComponent } from './activity-manage/activity-manage.component';
const routes: Routes = [
	{path:'', canActivate: [AuthGuard,EmployeeGuard], component: ActivityManageComponent}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ActivityRoutingModule { }
