import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CoreModule} from '../core/core.module';
import {ActivityRoutingModule} from './activity-routing.module';
import { ActivityManageComponent } from './activity-manage/activity-manage.component';

@NgModule({
  declarations: [ActivityManageComponent],
  imports: [
    CommonModule,
    ActivityRoutingModule,
    CoreModule
  ]
})
export class ActivityModule { }
