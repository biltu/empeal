import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WellnessAnalyticsRoutingModule } from './wellness-analytics-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    WellnessAnalyticsRoutingModule
  ]
})
export class WellnessAnalyticsModule { }
