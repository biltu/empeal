import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupHcsetComponent } from './group-hcset.component';

describe('GroupHcsetComponent', () => {
  let component: GroupHcsetComponent;
  let fixture: ComponentFixture<GroupHcsetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupHcsetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupHcsetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
