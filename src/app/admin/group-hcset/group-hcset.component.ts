import { Component, OnInit } from '@angular/core';
import {Global} from '../../global';
import {NgbPopoverConfig} from '@ng-bootstrap/ng-bootstrap';
import {AuthService} from '../../services/auth.service';
import {CommonService} from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart  } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-group-hcset',
  templateUrl: './group-hcset.component.html',
  styleUrls: ['./group-hcset.component.css']
})
export class GroupHcsetComponent implements OnInit {
	hcSetForm: FormGroup;
	coaches: any=[];
	groupcoach: any=[];
	group_id: any;
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
   private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) {
  	this.group_id = this.activeRoute.snapshot.paramMap.get("id");
  	this.asyncInit();
   }

  ngOnInit() {
  	this.hcSetForm = this.fb.group({
      primayhcid: ['']
    });
  }

/**
* Get all group data
*/
  asyncInit(){
  	let group_hc =[];
  	 this.commonService.getAll(this.global.apiUrl + '/api/group')
  		.subscribe((data)=>{
        	this.coaches = data.coaches;
        	
  		}, (error)=>{});

  	this.commonService.getAll(this.global.apiUrl + '/api/group/'+this.group_id)
  	.subscribe((data)=>{
  	this.groupcoach = data.data;
  	 group_hc = this.groupcoach.map(group=>group.user_id);
  	}, (error)=>{});
  	
  	setTimeout(()=>{
  		this.coaches.forEach((coach)=>{
  			this.groupcoach.forEach(group=>{
  				if(group.user_id ==coach.user_id){
  				coach.select =true;
  					if(group.primary_health_coach ==1){
  						this.hcSetForm.controls['primayhcid'].setValue(String(group.user_id));
  					}
  				}
  			});
  		});
  	},500);
  }

/**
* Submit form for set coach by group
*/

  submitForm(){
  	let checkedhc = this.coaches.filter(coach=>coach.select).map(data=>{
  		return data.user_id
  	});
  	let form = {phc:this.hcSetForm.value,selected_hc:checkedhc, gid:this.group_id};
  	this.commonService.create(this.global.apiUrl + '/api/group/hc/update',form)
        .subscribe((data)=>{
          if(data.status ==200){
            this.message.success(data.status_text);
            this.router.navigate(['admin/groups']);
          }

          if(data.status ==400){
            this.message.error(data.status_text);
          }
          
        }, (error)=>{
      });
  }

}
