import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Global } from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import { MessageService } from '../../services/message.service';
declare var Swal: any;
@Component({
  selector: 'app-contest-list',
  templateUrl: './contest-list.component.html',
  styleUrls: ['./contest-list.component.css']
})
export class ContestListComponent implements OnInit {
	contestes: any=[];
  targetSearchForm: FormGroup;
   shcAssignForm: FormGroup;
	categories: any=[];
	groups: any=[];
	count_data:number;
	active_page: number;
	targetquestions: any;
  checkedModel: any = {};
  masterSelected: boolean;
  checkedList: any;
  isModalOpen: boolean;
  display: string;
  seconderyHC: any = [];

  selectedContestName: string;
  selectedContestId: string;
  displayShc: string;
  selectedShc: any = [];
   

  
  
  constructor(private fb: FormBuilder, private message: MessageService, private global: Global, private authService: AuthService, private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) { 
  	this.count_data =0;
  	this.active_page =0;
    this.asyncInit();
  }

  ngOnInit() {
    this.targetSearchForm = this.fb.group({
      group:[''],
      category:[''],
      title:[''],
      start_date:[''],
      end_date:[''],
    });
    this.shcAssignForm = this.fb.group({
      coach_id:['',[Validators.required]],
      contest_ids:[this.checkedList,[]],
     
    });

  }

/**
* Get all created contest
*/
  asyncInit(){
  	this.commonService.getAll(this.global.apiUrl + '/api/contest?off_set=' + this.active_page)
  		.subscribe((data)=>{
  			this.contestes = data.data;
  			this.categories = data.categories;
  			this.groups = data.groups;
  			this.count_data = data.data_count;
        this.contestes.forEach((contest, i)=>{
            if(contest.status ==1){
                this.checkedModel[contest.id] = true;
            }             
        });
  			this.pagination();
      }, (error) => { });
    
    	this.commonService.getAll(this.global.apiUrl + '/api/secondery-hc/getAsOption')
  		.subscribe((data)=>{
  			this.seconderyHC = data.data;
  		 
  		}, (error)=>{});
  }

  pagination(){
  	let tot_number =  Math.ceil(Number(this.count_data) /25);
  	var items: number[] = [];
	for(var i = 1; i <= tot_number; i++){
	   items.push(i);
	}
	return items;
  }

  paginationCall(index){
  	this.active_page = index;
  	let search_f = this.targetSearchForm.controls;
  	if(search_f.category.value =='' && search_f.title.value =='' && search_f.group.value =='' && search_f.start_date.value =='' && search_f.end_date.value ==''){
  		this.asyncInit();
  	}else{
  		this.searchTarget(index);
  	}
	
  }

/**
* Search contest by category, title, group, and date
*/
  	searchTarget(index){
	  	this.active_page =index;
	  	let search_cont = '';
	  	if(this.targetSearchForm.get('category').value){
	  		search_cont += '&category=' + this.targetSearchForm.get('category').value;
	  	}if(this.targetSearchForm.get('title').value){
	  		search_cont += '&title=' + this.targetSearchForm.get('title').value;
	  	}if(this.targetSearchForm.get('group').value){
	  		search_cont += '&group=' + this.targetSearchForm.get('group').value;
	  	}if(this.targetSearchForm.get('start_date').value){
	  		search_cont += '&start_date=' + this.targetSearchForm.get('start_date').value;
	  	}if(this.targetSearchForm.get('end_date').value){
	  		search_cont += '&end_date=' + this.targetSearchForm.get('end_date').value;
	  	}

	  	this.commonService.getAll(this.global.apiUrl + '/api/contest?off_set=' + this.active_page + search_cont)
	  		.subscribe((data)=>{
	  			this.contestes = data.data;
	  			this.categories = data.categories;
	  			this.groups = data.groups;
	  			this.count_data = data.data_count;
	  			this.pagination();
	  		}, (error)=>{});
	}

/**
* Change status of contest
*/
  changeStatus(event, id, index, sts){
    let status = event==true ? 1: 0;
    Swal.fire({
      title: 'Are you sure?',
      text: "You want to publish this contest for this group. Once published you can't unpublish it for this group.",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, publish it!'
    }).then((result) => {
      //console.log(result);
      if (result.value) {
        this.commonService.getAll(this.global.apiUrl +'/api/contest/set-status/'+ id +'?status=' +status)
          .subscribe((data)=>{
            if(data.status ==200){
              this.message.success(data.status_text);
              this.redirectTo(this.router.url);
            }
          }, (error)=>{});
      }else{
        if(sts ==1){
          this.checkedModel[id] = true;
        }else{
          this.checkedModel[id] = false;
        }
      }
    });
  }

  redirectTo(uri) {
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() =>
    this.router.navigate([uri]));
  }

  checkUncheckAll() {
    for (var i = 0; i < this.contestes.length; i++) {
      this.contestes[i].isSelected = this.masterSelected;
    }
    this.getCheckedItemList();
  }
  isAllSelected() {
    this.masterSelected = this.contestes.every(function(contest:any) {
        return contest.isSelected == true;
      })
    this.getCheckedItemList();
  }
 
  getCheckedItemList(){
    this.checkedList = [];
    for (var i = 0; i < this.contestes.length; i++) {
      if(this.contestes[i].isSelected)
      this.checkedList.push(this.contestes[i].id);
    }
    // return thischeckedList;
  }

  openSHCModalDialog() {
    // console.log(this.checkedList);
    // if (this.checkedList) {
    if (Array.isArray(this.checkedList) && this.checkedList.length) {
    this.display='block'; //Set block css
    this.isModalOpen = true;  
    } else {
       
       this.message.warning('Please Select Contest');
    }
    
    
  } 

  
  
 
  closeSHCModalDialog(){
    this.display='none'; //set none css after close dialog
    this.isModalOpen = false;
  }

  assignSave() {
    // console.log(this.shcAssignForm.coach_id);
    if (this.shcAssignForm.dirty && this.shcAssignForm.valid) {
      // let formValue = [this.shcAssignForm.value,{ contest_ids: this.checkedList }];
      //  console.log(this.shcAssignForm.value)
      // formValue.push({ contest_ids: this.checkedList });

      this.shcAssignForm.controls['contest_ids'].setValue(this.checkedList);
     
    
     this.commonService.create(this.global.apiUrl + '/api/secondery-hc/assign-contest', this.shcAssignForm.value)
        .subscribe((data)=>{
          if(data.status ==200){
            // let userIndex =this.users.findIndex(user=> user.id == this.groupAssignForm.get('user_id').value);
            // this.users[userIndex] = data.data;
            this.shcAssignForm.reset();
            this.checkedList = [];
            this.checkUncheckAll();
            this.closeSHCModalDialog();

            this.message.success('Contest assign Succesfully.');
              this.redirectTo(this.router.url);

          }
        }, (error) => { });
     
    } else {
       this.message.error('Please Select Coach');
    }
    
  }
  showShc(contestName,contestId,shc) {
     
    // this.isModalOpen = true;  
    this.displayShc = 'block';
    this.selectedShc = shc;
    this.selectedContestName = contestName;
    this.selectedContestId = contestId;
  console.log(this.selectedShc)
  }

  closeModalDialog() {
    this.selectedShc = [];
    this.selectedContestName = '';
    this.selectedContestId = '';
    this.displayShc = 'none';

  }

  removeShc(assin_id) {

      this.commonService.delete(this.global.apiUrl +'/api/secondery-hc',assin_id)
          .subscribe((data)=>{
            if(data.status ==200){
              this.message.success(data.status_text);
              
              this.redirectTo(this.router.url);

            }
          }, (error)=>{});
    
  }
}
  
