import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../services/message.service';

@Component({
  selector: 'app-target-add',
  templateUrl: './target-add.component.html',
  styleUrls: ['./target-add.component.css']
})
export class TargetAddComponent implements OnInit {
	questions: FormArray;
	AddForm: FormGroup;
	categories: any=[];
	groups: any=[];
	today: any;
	target_id: any;
	target_data: any;
	targetqs: any=[];
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService, private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) {
  	this.target_id = this.activeRoute.snapshot.paramMap.get("id")?this.activeRoute.snapshot.paramMap.get("id"):'';
  	this.asyncInit();
  	this.today = new Date;
  }

  	ngOnInit() {
	  	this.AddForm = this.fb.group({
	  		questions: this.fb.array([this.createQst()]),
	  		group:[''],
	  		category:[''],
	  		start_date:[''],
	  		end_date:['']
	  	});

  	}

/**
* Get specific target
*/
  	asyncInit(){
  		if(this.target_id){
  			this.commonService.getAll(this.global.apiUrl + '/api/targets/'+this.target_id)
	  		.subscribe((data)=>{
	  			this.target_data = data.data;
	  			 this.categories = data.categories;
	  			 this.groups = data.groups;
	  			 this.targetqs = data.data.questions;
	  			 let employeeFormGroups =this.targetqs.map(target=> this.fb.group({'question':target.question, 'option':target.correct_answer, 'qid':target.id, 'is_automatic':target.is_automatic, 'activityname':target.activityname}));
	  			 let employeeFormArray = this.fb.array(employeeFormGroups);
	  			 this.AddForm.setControl('questions', employeeFormArray);
	  			 this.setForm();
	  		}, (error)=>{});
  		}else{
	  		this.commonService.getAll(this.global.apiUrl + '/api/targets/create')
	  		.subscribe((data)=>{
	  			this.categories = data.categories;
	  			this.groups = data.groups;
	  		}, (error)=>{});
  		}
  	}

/**
* Set Target Form
*/
  	setForm(){
	    this.AddForm.patchValue({
	      id:this.target_data.id,
	      category: this.target_data.category.id,
	      group:this.target_data.group.id, 
	      question:this.target_data.question,
	      start_date: this.target_data.start_date,
	      end_date: this.target_data.end_date
	    });
	}
  	createQst(): FormGroup{
	  	return this.fb.group({
	  		question:[''],
	  		option:[''],
	  		qid:[''],
	  		activityname:[''],
	  		is_automatic:['']
	  	});
  	}

  	addTargetArray(): void {
  		this.questions = this.AddForm.get('questions') as FormArray;
  		this.questions.push(this.createQst());
  	}

  	deleteQuestionArray(index: number){
  		this.questions = this.AddForm.get('questions') as FormArray;
  		this.questions.removeAt(index);
  	}

  	get formData() { return <FormArray>this.AddForm.get('questions')['controls']; }
  	get target_f(){return this.AddForm.controls;} 

/**
* Submit target
*/  	
  	submitTarget(){
	    this.commonService.create(this.global.apiUrl + '/api/targets',this.AddForm.value)
	      .subscribe((data)=>{
	        if(data.status ===200){
	          this.message.success(data.status_text);
	          this.router.navigate(['admin/grouptargets']);
	        }
	      },(error)=>{});
  	}

/**
* Update target
*/
  	updateTarget(){
	    this.commonService.update(this.global.apiUrl + '/api/targets',this.target_id,this.AddForm.value)
	        .subscribe((data)=>{
	          if(data.status ==200){
	            this.message.success(data.status_text);
	            this.router.navigate(['admin/grouptargets']);
	          }
	        }, (error)=>{});
	  	}

}
