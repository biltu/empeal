import { Component, OnInit } from '@angular/core';
import {Global} from '../../global';
import {NgbPopoverConfig} from '@ng-bootstrap/ng-bootstrap';
import {AuthService} from '../../services/auth.service';
import {CommonService} from '../../services/common.service';
import {MessageService} from '../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart  } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-group-list',
  templateUrl: './group-list.component.html',
  styleUrls: ['./group-list.component.css']
})
export class GroupListComponent implements OnInit {
	SearchForm: FormGroup;
	AddForm: FormGroup;
  hcSetForm: FormGroup;
  isModalOpen: boolean;
	count_data:number;
	active_page: number;
	display: string;
	groups: any=[];
	coaches: any=[];
  permissions: any=[];
  qssubmodules: any=[];
	groupimage: any;
  gid: any;
  createGroupValidation: any;
  gppermissions: any;
  gpqspermissions: any;

  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
   private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) { 
  	this.count_data =0;
  	this.active_page =0;
    this.asyncInit();
  }

  ngOnInit() {
  	this.AddForm = this.fb.group({
        groupname:['', [Validators.required]],
        description:['', [Validators.required]],
        group_image:[''],
        health_coachid:['', [Validators.required]]
      });

    this.SearchForm = this.fb.group({
      sname:[''],
      coach: ['']
    });
  }

/**
* Get all group, coach data
*/
  asyncInit(){
  	this.commonService.getAll(this.global.apiUrl + '/api/group?off_set=' + this.active_page)
  		.subscribe((data)=>{
        	this.groups =  data.data;
        	this.coaches = data.coaches;
  			  this.count_data = data.data_count;
          this.permissions = data.modules;
          this.qssubmodules = data.question_sub;
  			  this.pagination();
  		}, (error)=>{});
  }

  pagination(){
  	let tot_number =  Math.ceil(Number(this.count_data) /50);
  	var items: number[] = [];
  	for(var i = 1; i <= tot_number; i++){
  	   items.push(i);
  	}
  	return items;
  }

  paginationCall(index){
  	this.active_page = index;
  	let search_f = this.SearchForm.controls;
  	if(search_f.sname.value =='' && search_f.group.value =='' && search_f.type.value =='' && 
  		search_f.gender.value==''){
  		this.asyncInit();
  	}else{
  		this.searchGroup(index);
  	}
  }

/**
* search group by name, coach
*/

  searchGroup(index){
    this.active_page =index;
    let search_cont = '';
    if(this.SearchForm.get('sname').value){
      search_cont += '&sname=' + this.SearchForm.get('sname').value;
    }if(this.SearchForm.get('coach').value){
      search_cont += '&coach=' + this.SearchForm.get('coach').value;
    }

    this.commonService.getAll(this.global.apiUrl + '/api/group?off_set=' + this.active_page + search_cont)
      .subscribe((data)=>{
        this.groups = data.data;
        this.count_data = data.data_count;
        this.pagination();
      }, (error)=>{});
  }

/**
* Open group modal
*/
  openGroupModalDialog(index,id){
      this.display='block'; //Set block css
      let data:any = '';
      if(index=='edit' && id > 0){
        data = this.groups.find((jb)=>jb.id ==id);
        this.gppermissions = data.group_permissions.map(obj=>obj.permission_id);
        this.gpqspermissions = data.group_permissions.map(obj=>obj.sub_module);
        this.permissions.forEach((permission)=>{
          if(this.gppermissions.indexOf(permission.id) != -1){
            permission.select = true;
          }else{
            permission.select = false;
          }
        });
        
        this.qssubmodules.forEach((qssubmodule)=>{
          if(this.gpqspermissions.indexOf(qssubmodule.ques_type) != -1){
            qssubmodule.select = true;
          }else{
            qssubmodule.select = false;
          }
        });
        this.gid = id;
        this.setFromValue(data);
      }else{
        this.permissions.forEach((permission)=>{
          permission.select = false;
        })
        
        this.qssubmodules.forEach((qssubmodule)=>{
          qssubmodule.select = false;
        })

        this.gpqspermissions = '';
        this.gppermissions = '';
        this.gid = '';
        this.createGroupValidation = '';
        let blankdata:any = '';
        this.setFromValue(blankdata);
      }
      this.isModalOpen = true;
  }

/**
* set form
*/
  setFromValue(data){
    this.AddForm.patchValue({
      groupname : data.group_name,
      description : data.description,
      health_coachid : (data.health_coach)?data.health_coach.user_id:''
    });
  }

/**
* Close modal
*/
  closeGroupModalDialog(){
    this.display='none'; //set none css after close dialog
    this.isModalOpen = false;
  }

  openHCModalDialog(){
    this.display='block'; //Set block css
    this.isModalOpen = true;
  }

  closeHCModalDialog(){
    this.display='none'; //set none css after close dialog
    this.isModalOpen = false;
  }

/**
* Create group
*/
  CreateGroup(){
    let checkedids = this.permissions.filter(permission=>permission.select).map(data=>{
      return data.id
    });

    let checkedqssubids = this.qssubmodules.filter(qssubmodule=>qssubmodule.select).map(data=>{
      return data.ques_type
    });

    let qumoduleid = this.permissions.filter(e=>e.name=='questionnaire').map(data=>{
      return data.id
    });
  	let formdata = this.AddForm.value;
    let form = new FormData();
    form.append('groupimage',this.groupimage);
    form.append('groupname',formdata.groupname);
    form.append('description',formdata.description);
    form.append('health_coachid',formdata.health_coachid);
    form.append('group_modules',checkedids);
    form.append('question_modules',checkedqssubids);
    form.append('qumoduleid',qumoduleid);
    this.commonService.create(this.global.apiUrl + '/api/group',form)
        .subscribe((data)=>{
          if(data.status == 422){
            this.createGroupValidation = data.error;
          }
          if(data.status ==200){
            this.groups.unshift(data.data);
            this.message.success(data.status_text);
            this.display='none';
            this.AddForm.reset();
            this.isModalOpen = false;
          }

          if(data.status ==400){
            this.message.error(data.status_text);
          }
        }, (error)=>{
     });
  }

/**
* Update group
*/
 updateGroup(){
   let checkedids = this.permissions.filter(permission=>permission.select).map(data=>{
      return data.id
    });

   let checkedqssubids = this.qssubmodules.filter(qssubmodule=>qssubmodule.select).map(data=>{
      return data.ques_type
    });

    let qumoduleid = this.permissions.filter(e=>e.name=='questionnaire').map(data=>{
      return data.id
    });

    let formdata = this.AddForm.value;
    let form = new FormData();
    form.append('groupimage',this.groupimage);
    form.append('groupname',formdata.groupname);
    form.append('description',formdata.description);
    form.append('health_coachid',formdata.health_coachid);
    form.append('id', this.gid);
    form.append('group_modules',checkedids);
    form.append('question_modules',checkedqssubids);
    form.append('qumoduleid',qumoduleid);
    
    this.commonService.create(this.global.apiUrl + '/api/group/groupupdate',form)
        .subscribe((data)=>{
          if(data.status ==200){
            this.message.success(data.status_text);
            this.display='none';
            this.isModalOpen = false;
            this.redirectTo(this.router.url);
          }

          if(data.status ==400){
            this.message.error(data.status_text);
          }
        }, (error)=>{
      });
  }

/**
* Select file
*/
  onSelectFile(event: any){
  		this.groupimage = event.target.files[0];

  }

  redirectTo(uri) {
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() =>
    this.router.navigate([uri]));
  }

}
