import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../services/message.service';
declare var $;
@Component({
  selector: 'app-target-list',
  templateUrl: './target-list.component.html',
  styleUrls: ['./target-list.component.css']
})
export class TargetListComponent implements OnInit {
	targets: any=[];
	targetSearchForm: FormGroup;
	categories: any=[];
	groups: any=[];
	count_data:number;
	active_page: number;
	targetquestions: any;
  sortByField: string;
  reverse: boolean =false;
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService, private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) { 
  	this.count_data =0;
  	this.active_page =0;
    this.asyncInit();
  }

  ngOnInit() {
    this.targetSearchForm = this.fb.group({
      group:[''],
      category:[''],
      title:[''],
      start_date:[''],
      end_date:[''],
    });

  }

/**
* Get all target list
*/
  asyncInit(){
  	this.commonService.getAll(this.global.apiUrl + '/api/targets?off_set=' + this.active_page)
  		.subscribe((data)=>{
  			this.targets = data.data;
  			this.categories = data.categories;
  			this.groups = data.groups;
  			this.count_data = data.data_count;
  			this.pagination();
  		}, (error)=>{});
  }

  pagination(){
  	let tot_number =  Math.ceil(Number(this.count_data) /25);
  	var items: number[] = [];
	for(var i = 1; i <= tot_number; i++){
	   items.push(i);
	}
	return items;
  }

  paginationCall(index){
  	this.active_page = index;
  	let search_f = this.targetSearchForm.controls;
  	if(search_f.category.value =='' && search_f.title.value =='' && search_f.group.value =='' && search_f.start_date.value =='' && search_f.end_date.value ==''){
  		this.asyncInit();
  	}else{
  		this.searchTarget(index);
  	}
	
  }

/**
* Search targets by category, title, group, data
*/
  	searchTarget(index){
	  	this.active_page =index;
	  	let search_cont = '';
	  	if(this.targetSearchForm.get('category').value){
	  		search_cont += '&category=' + this.targetSearchForm.get('category').value;
	  	}if(this.targetSearchForm.get('title').value){
	  		search_cont += '&title=' + this.targetSearchForm.get('title').value;
	  	}if(this.targetSearchForm.get('group').value){
	  		search_cont += '&group=' + this.targetSearchForm.get('group').value;
	  	}if(this.targetSearchForm.get('start_date').value){
	  		search_cont += '&start_date=' + this.targetSearchForm.get('start_date').value;
	  	}if(this.targetSearchForm.get('end_date').value){
	  		search_cont += '&end_date=' + this.targetSearchForm.get('end_date').value;
	  	}

	  	this.commonService.getAll(this.global.apiUrl + '/api/targets?off_set=' + this.active_page + search_cont)
	  		.subscribe((data)=>{
	  			this.targets = data.data;
	  			this.categories = data.categories;
	  			this.groups = data.groups;
	  			this.count_data = data.data_count;
	  			this.pagination();
	  		}, (error)=>{});
	}

}
