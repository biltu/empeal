import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {Global} from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../services/message.service';
declare var Swal: any;

@Component({
  selector: 'app-content-list',
  templateUrl: './content-list.component.html',
  styleUrls: ['./content-list.component.css']
})
export class ContentListComponent implements OnInit {
	content_all: any=[];
	contentSearchForm: FormGroup;
	categories: any=[];
	groups: any=[];
  contests: any=[];
	count_data:number;
	active_page: number;
  checkedModel: any={};
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService,
   private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) { 
  	this.count_data =0;
  	this.active_page =0;
    this.asyncInit();
  }

  ngOnInit() {
  	this.contentSearchForm = this.fb.group({
  		category:[''],
      group:[''],
      contest:[''],
  		title: [''],
      username: [''],
  		status:['']
  	});
  }

/**
* Get all content, group category data
*/
  asyncInit(){
  	this.commonService.getAll(this.global.apiUrl + '/api/content?off_set=' + this.active_page)
  		.subscribe((data)=>{
  			this.content_all = data.data;
  			this.categories = data.categories;
  			this.groups = data.groups;
        this.contests = data.contests;
  			this.count_data = data.data_count;
        this.content_all.forEach((content, i)=>{
          if(content.status =='A'){
            this.checkedModel[content.id] = true;
          }             
        });
  			this.pagination();
  		}, (error)=>{});
  }


  pagination(){
  	let tot_number =  Math.ceil(Number(this.count_data) /25);
  	var items: number[] = [];
	for(var i = 1; i <= tot_number; i++){
	   items.push(i);
	}
	return items;
  }


  paginationCall(index){
  	this.active_page = index;
  	let search_f = this.contentSearchForm.controls;
  	if(search_f.category.value =='' && search_f.title.value =='' && search_f.status.value ==''){
  		this.asyncInit();
  	}else{
  		this.searchContent(index);
  	}
	
  }

/**
* Search content by category, title, status
*/
  	searchContent(index){
	  	this.active_page =index;
	  	let search_cont = '';
	  	if(this.contentSearchForm.get('category').value){
	  		search_cont += '&category=' + this.contentSearchForm.get('category').value;
	  	}if(this.contentSearchForm.get('title').value){
	  		search_cont += '&title=' + this.contentSearchForm.get('title').value;
	  	}if(this.contentSearchForm.get('username').value){
        search_cont += '&username=' + this.contentSearchForm.get('username').value;
      }if(this.contentSearchForm.get('status').value){
	  		search_cont += '&status=' + this.contentSearchForm.get('status').value;
	  	}if(this.contentSearchForm.get('group').value){
        search_cont += '&group=' + this.contentSearchForm.get('group').value;
      }if(this.contentSearchForm.get('contest').value){
        search_cont += '&contest=' + this.contentSearchForm.get('contest').value;
      }

	  	this.commonService.getAll(this.global.apiUrl + '/api/content?off_set=' + this.active_page + search_cont)
	  		.subscribe((data)=>{
	  			this.content_all = data.data;
	  			this.categories = data.categories;
	  			this.groups = data.groups;
	  			this.count_data = data.data_count;
          this.content_all.forEach((content, i)=>{
              if(content.status =='A'){
                this.checkedModel[content.id] = true;
              }
              
          });
	  			this.pagination();
	  		}, (error)=>{});
	}

/**
* Change content status
*/
  changeStatus(event, id, index){
    let status = event ? 'A': 'I';
    this.commonService.getAll(this.global.apiUrl +'/api/content/set-status/'+ id +'?status=' +status)
        .subscribe((data)=>{
          if(data.status ==200){
            this.message.success('Successfully change status');
            this.content_all[index] = data.data;
          }
        }, (error)=>{});
  }
}
