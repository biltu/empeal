import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import {Global} from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../services/message.service';

@Component({
  selector: 'app-contest-add',
  templateUrl: './contest-add.component.html',
  styleUrls: ['./contest-add.component.css']
})
export class ContestAddComponent implements OnInit {
	AddForm: FormGroup;
	categories: any=[];
	groups: any=[];
	today: any;
	contest_id: any;
	contest_data: any;
	hcServerValidation: any;
	targetqs: any=[];
  constructor(private fb:FormBuilder, private message:MessageService ,private global: Global,private authService: AuthService, private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) {
  	this.contest_id = this.activeRoute.snapshot.paramMap.get("id")?this.activeRoute.snapshot.paramMap.get("id"):'';
  	this.asyncInit();
  	this.today = new Date;
  }

  	ngOnInit() {
	  	this.AddForm = this.fb.group({
	  		contestname: [''],
	  		question: [''],
	  		group:[''],
	  		category:[''],
	  		start_date:[''],
	  		end_date:[''],
	  		activityname:['']
	  	});
  	}

/**
* Get specific content
*/
  	asyncInit(){
  		if(this.contest_id){
  			this.commonService.getAll(this.global.apiUrl + '/api/contest/'+this.contest_id)
	  		.subscribe((data)=>{
	  			this.contest_data = data.data;
	  			 this.categories = data.categories;
	  			 this.groups = data.groups;
	  			 this.setForm();
	  		}, (error)=>{});
  		}else{
	  		this.commonService.getAll(this.global.apiUrl + '/api/contest/create')
	  		.subscribe((data)=>{
	  			this.categories = data.categories;
	  			this.groups = data.groups;
	  		}, (error)=>{});
  		}
  	}
  	get contest_f(){return this.AddForm.controls;}

/**
* Set contest form
*/
  	setForm(){
	    this.AddForm.patchValue({
	      id:this.contest_data.id,
	      contestname: this.contest_data.contest_name,
	      category: this.contest_data.category.id,
	      group:this.contest_data.group.id, 
	      question:this.contest_data.question,
	      start_date: this.contest_data.start_date,
	      end_date: this.contest_data.end_date,
	      activityname: this.contest_data.activityname
	    });
	}

/**
* Create contest
*/	
  	submitContest(){
	    this.commonService.create(this.global.apiUrl + '/api/contest',this.AddForm.value)
	      .subscribe((data)=>{
	        if(data.status ===200){
	          this.message.success(data.status_text);
	          this.router.navigate(['admin/group/contest']);
	        }

	        if(data.status ===422){
	        	this.hcServerValidation = data.error;
	        }
	        //console.log(data);
	      },(error)=>{});
  	}

/**
* Update contest
*/
  	updateContest(){
	    this.commonService.update(this.global.apiUrl + '/api/contest',this.contest_id,this.AddForm.value)
	        .subscribe((data)=>{
	          if(data.status ==200){
	            this.message.success(data.status_text);
	            this.router.navigate(['admin/group/contest']);
	          }
	          
	          if(data.status ===422){
	        	this.hcServerValidation = data.error;
	          }
	        }, (error)=>{});
	  	}

}
