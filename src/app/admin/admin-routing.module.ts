import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './../auth.guard';
import { EmployeeGuard } from './../employee.guard';
import { CoachGuard } from './../coach.guard';
import { AdminGuard } from './../admin.guard';
import { ContentListComponent } from './content-list/content-list.component';
import { ContentDetailsComponent } from './content-details/content-details.component';
import { TargetListComponent } from './target-list/target-list.component';
import { TargetAddComponent } from './target-add/target-add.component';
import { ContestListComponent } from './contest-list/contest-list.component';
import { ContestAddComponent } from './contest-add/contest-add.component';
import { GroupListComponent } from './group-list/group-list.component';
import { GroupHcsetComponent } from './group-hcset/group-hcset.component';
import { SupportListComponent } from './support-list/support-list.component';
const routes: Routes = [
	{path:'contents', canActivate: [AuthGuard,AdminGuard], component: ContentListComponent},
	{path:'content/detail/:id', canActivate: [AuthGuard,AdminGuard], component: ContentDetailsComponent},
	{path:'grouptargets', canActivate: [AuthGuard,AdminGuard], component: TargetListComponent},
	{path:'grouptargets/add', canActivate: [AuthGuard,AdminGuard], component: TargetAddComponent},
	{path:'group/contest/add', canActivate: [AuthGuard,AdminGuard], component: ContestAddComponent},
	{path:'group/contest', canActivate: [AuthGuard,AdminGuard], component: ContestListComponent},
	{path:'groups', canActivate: [AuthGuard,AdminGuard], component: GroupListComponent},
	{path:'grouptargets/edit/:id', canActivate: [AuthGuard, AdminGuard], component: TargetAddComponent},
	{path:'group/contest/edit/:id', canActivate: [AuthGuard, AdminGuard], component: ContestAddComponent},
	{path:'groups/hc-set/:id', canActivate: [AuthGuard,AdminGuard], component: GroupHcsetComponent},
	{path:'supports', canActivate: [AuthGuard,AdminGuard], component: SupportListComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
