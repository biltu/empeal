import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '../core/core.module';
import { AdminRoutingModule } from './admin-routing.module';
import { ContentListComponent } from './content-list/content-list.component';
import { TargetListComponent } from './target-list/target-list.component';
import { TargetAddComponent } from './target-add/target-add.component';
import { ContestListComponent } from './contest-list/contest-list.component';
import { ContestAddComponent } from './contest-add/contest-add.component';
import { GroupListComponent } from './group-list/group-list.component';
import { SupportListComponent } from './support-list/support-list.component';
import { GroupHcsetComponent } from './group-hcset/group-hcset.component';
import { ContentDetailsComponent } from './content-details/content-details.component';
@NgModule({
  declarations: [ContentListComponent,TargetListComponent,GroupListComponent,SupportListComponent, GroupHcsetComponent, TargetAddComponent, ContentDetailsComponent, ContestListComponent, ContestAddComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    CoreModule
  ]
})
export class AdminModule { }
