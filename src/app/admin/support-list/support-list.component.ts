import { Component, OnInit } from '@angular/core';
import {Global} from '../../global';
import { CommonService } from '../../services/common.service';
import { MessageService } from '../../services/message.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart  } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-support-list',
  templateUrl: './support-list.component.html',
  styleUrls: ['./support-list.component.css']
})
export class SupportListComponent implements OnInit {

  support_list: any=[];
  supportSearchForm: FormGroup;
  statusModel: any={};
  constructor(private fb:FormBuilder, private global: Global,
    private router: Router, private commonService: CommonService, private message: MessageService) { 
  	this.asyncInit();
  }

  ngOnInit() {
  	this.supportSearchForm = this.fb.group({
  		by_name:[''],
  		by_email:[''],
  		by_type:[''],
  		by_date:[''],
  		by_status:['']
  	});
  }

/**
* Get all support list
*/
  asyncInit(){
  	this.commonService.getAll(this.global.apiUrl + '/api/support')
  		.subscribe(data=>{
  			if(data.status ==200){
  				this.support_list = data.data;
  				this.support_list.forEach(obj=>{
  					this.statusModel[obj.id] = String(obj.status);
  				});
  			}
  		},error=>{});
  }

/**
* Serach support
*/
  submitSearch(){
  	let paramData = this.setSearchParams();
  	this.commonService.getAll(this.global.apiUrl + '/api/support' + paramData)
  		.subscribe(data=>{
  			this.support_list = data.data;
  			this.support_list.forEach(obj=>{
  					this.statusModel[obj.id] = String(obj.status);
  			});
  		},error=>{});
  }

/**
* Set support search param
*/
  setSearchParams(){
  	let params ='';
  	Object.keys(this.supportSearchForm.controls).forEach((key, i)=>{
  		if(i ==0){
  			params += '?'+key+'=' + this.supportSearchForm.get(key).value;
  		}else{
  			params += '&'+key+'=' + this.supportSearchForm.get(key).value;
  		}
  	});

  	return params;
  }

/**
* change support status
*/
  statusChange(value, id){
  	this.commonService.update(this.global.apiUrl + '/api/support',id, {status:value})
  		.subscribe(data=>{
  			if(data.status ==200){
  				this.message.success(data.status_text);
  			}	
  		},error=>{});
  }

}
