import { Component, OnInit } from '@angular/core';
import {Global} from '../../global';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router, ActivatedRoute, NavigationEnd, NavigationStart } from '@angular/router';
import {MessageService} from '../../services/message.service';
@Component({
  selector: 'app-content-details',
  templateUrl: './content-details.component.html',
  styleUrls: ['./content-details.component.css']
})
export class ContentDetailsComponent implements OnInit {
	content_id: any;
  	content: any={};
  	category_name: string;
  constructor(private message:MessageService ,private global: Global,private authService: AuthService,
   private router: Router, private commonService: CommonService, private activeRoute: ActivatedRoute) { 
  	this.content_id = this.activeRoute.snapshot.paramMap.get("id");
  	this.asyncInit(this.content_id);
  }

  ngOnInit() {
  }

/**
* Get specific content and caterory name data
*/
  asyncInit(cont_id){
  	this.commonService.getAll(this.global.apiUrl + '/api/content/'+ cont_id)
  		.subscribe((data)=>{
  			this.content = data.data;
  			this.category_name = data.data.category.category_name;
  			console.log(data);
  		}, (error)=>{});
  }
  
}	
