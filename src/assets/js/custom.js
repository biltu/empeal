// $(window).bind("load", function() { 
//     $('.chart').easyPieChart({
//         animate: 2000,
//         lineWidth: 3,
//         lineCap: 'butt',
//         scaleColor: false,      
//         barColor: '#a4d439',
//         trackColor: '#ddd',
//     });
// });
$(document).ready(function(){
    //alert('ssss aa');
	// Dropdown
	$(document).on("click",'.userIcon',function(){
		$('.userIcon + ul').toggle();
	});

    $(document).on("click",'.mbContest a',function(){
        $('.contestDropdown').toggle();
    });

    $(document).on("click",'.mbbell a',function(){
        $('.bellDropdown').toggle();
    });

    $(document).on("click",'.mbcommunity a',function(){
        $('.communityDropdown').toggle();
    });

    $(document).on("click",'.mobileMenuToggle a',function(){
		$(this).toggleClass('mobileMenuVertical');
		$('.leftPanel').toggleClass('menuCollapsed');
		$('.bodyContent').toggleClass('nomargin');
	});

    $(document).on("click",'.appointment a',function(){
		$('.appointment .appointmentInner').toggleClass('appOpen');
	});

	$('select').formSelect();

	$('.dobpicker').datepicker();

    $(document).on("click",'.reportAccordian',function(){
	    $(this).collapsible();
    });

    $(document).on("hover",'.toggleOuter',function(){
        $('.manualBox').toggle();        
    });
    $('.editOuter a').hover(function(){
        $('.manualBox1').toggle();
    });

    $(document).on('click','.expandallOuter',function(){
        $('.reportAccordianSec li').toggleClass('active');
        $('.reportAccordianSec .collapsible-body').toggle();
    });
    

    if ($(window).width() > 767) {
        $('.leftPanel ul li').click(function(){
            $(this).child('ul').toggle();
        });
    }

   $(document).on('click','.collapsingBtn',function(){
        $(this).toggleClass('collapsed');
    });

    $(document).on('click','.allergyCollapse',function(){
        $('.allergyCollapse').collapsible();
    });

    $(document).on('click','.medicalCollapseLeft',function(){
        $('.medicalCollapseLeft').collapsible();
    });
    $(document).on('click','.medicalCollapseRight',function(){
        $('.medicalCollapseRight').collapsible();
    });

    $(document).on('click','.medicalSub',function(){
        $('.medicalSub').collapsible();
    });


	//Vertical Tab
    $('#questionTab').easyResponsiveTabs({
        type: 'vertical', //Types: default, vertical, accordion
        width: 'auto', //auto or any width like 600px
        fit: true, // 100% fit in a container
        closed: 'accordion', // Start closed if in accordion view
        tabidentify: 'hor_1', // The tab groups identifier
        activate: function(event) { // Callback function if tab is switched
            var $tab = $(this);
            var $info = $('#nested-tabInfo2');
            var $name = $('span', $info);
            $name.text($tab.text());
            $info.show();
        }
    });

    // Modal
    $('.modal').modal();

    $('.submitOuter button').click(function(){
    	$('#thankyouModal').show();
    });
    $('#thankyouModal .modal-close').click(function(){
    	$('#thankyouModal').hide();
    });
    $('#thankyouModal .okBtn').click(function(){
    	$('#thankyouModal').hide();
    });

    $('.submitOuter button').click(function(){
    	$('#successModal').show();
    });
    $('#successModal .modal-close').click(function(){
    	$('#successModal').hide();
    });
    $('#successModal .okBtn').click(function(){
    	$('#successModal').hide();
    });

    // Smooth Scroll
    var headerheight = $('header').outerHeight();
    $(".clickhere").on('click', function(event) { 
        if (this.hash !== "") { 
          event.preventDefault(); 
          var hash = this.hash; 
              $('html, body').animate({
                scrollTop: $(hash).offset().top  - headerheight
              }, 800, function(){
    
           // window.location.hash = hash;
          });
        }  
    });

    $(document).on('click','.arw', function(e){
      $(this).addClass('uparw');
    });
    
    $(document).on('click','.uparw', function(e){
      $(this).removeClass('uparw');
    });


    $(function() {

    });

    $('.likeBtn').click(function(){
        $('.reactionInner').toggle();
    });

    

    // if ($(window).width() < 767) {
    //     $('.statusOuter ul li').hover(function(){
    //         $(this).children('span.statusTxt').toggle();
    //     });
    // }


	// Sticky Header
	$(window).scroll(function() {
	    stickyHeader();
	});

	function stickyHeader() {
	    if ($(this).scrollTop() > 1){
	        // if ($(window).width() > 767) {
	           $('header').addClass("sticky");
	        // }
	    } else {
	        $('header').removeClass("sticky");
	    }
	}

        
});

$(document).ready(function(){
    $(document).on("mouseover",'.tooltipped',function(){
        $(this).tooltip();
    });  
     
    $(document).on('click','.workFlow a',function(){
        $(this).toggleClass('open');
        $('.processSec').toggle(500);
    });
    $(document).on('click','.opencloseicon',function(){
        //$(this).toggleClass('open');
        //$('.processSec').toggle(500);
        $('.openclosebox').toggle(500);
        $(this).toggleClass('opencloseplus');
        $('.leaderboardOuter').toggleClass('leaderboardOuterclose');
    });

    $(document).on('click','.collapsingOuter .collapsingBtn',function(){
        let slid = $(this).data('slid');
        $('.contestResultInner'+slid).toggle(500);
    });

    $(document).on('click','.contestrmv',function(){
        $('.contest'+$(this).data('contestid')).remove();
    });
    
    

});
//$( window ).on( "load", function() {
    
    //a.close();
//});
// function contestJoinProcess(id){
//     console.log(id);
// }

