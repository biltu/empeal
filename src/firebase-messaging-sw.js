// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/5.5.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/5.5.0/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.
firebase.initializeApp({
  'messagingSenderId': '455339803388'
  //'messagingSenderId': '233660997176'
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
//const messaging = firebase.messaging();

self.addEventListener('push', function(event) {
  var data = event.data.json(),
      notification = data.notification || "",
      message = notification.body || "",
      title = 'Empeal',
      location = notification.click_action || 'dashboard',
      icon = notification.icon || '',
      body = message || '';
  event.waitUntil(
    self.registration.showNotification(title, {
      body: body,
      icon: icon,
      data: location
    })
  );
});

function send_message_to_client(client, msg){
  return new Promise(function(resolve, reject){
    var msg_chan = new MessageChannel();

    msg_chan.port1.onmessage = function(event){
      if(event.data.error){
        reject(event.data.error);
      }else{
        resolve({parentLocation: event.data, client: client});
      }
    };

    client.postMessage(msg, [msg_chan.port2]);
  });
}

self.addEventListener('notificationclick', function(event) {
  event.notification.close();
  var itemsProcessed = 0,
      location = event.notification.data;

  var promise = new Promise(function(resolve, reject) {
        clients.matchAll({
          type: "window"
        })
        .then(function(clientList) {
          var promises = clientList.reduce(function(result, client) {
            if(client.frameType === 'nested') {
              var promise = new Promise(function(clientResolve, clientReject) {
                send_message_to_client(client, "get_widget_location")
                  .then(function(response) {
                    itemsProcessed++;
                    if(response.parentLocation === location) {
                      return clientResolve(response.client);
                    }
                    if(itemsProcessed === clientList.length) {
                      return clientReject("not_found");
                    }
                  }, function(reason) {
                    itemsProcessed++;
                  });
              });
              result.push(promise);
            }
            return result;
          }, []);

          if(promises && promises.length) {
            Promise.race(promises).then(function(value){
              resolve(value);
            }, function(reason) {
              reject(reason);
            });
          }
          else {
            reject("not_found");
          }

        })

      }).then(function(client) {
        client.focus();
      }, function(reason) {
        if (clients.openWindow) {
          return clients.openWindow(location);
        }
      });

  event.waitUntil(promise);

});