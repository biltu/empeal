// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBxZLGDpNp12gs-bXo9IuLSx3I1u9eUopg",
    authDomain: "wind-of-change-live.firebaseapp.com",
    databaseURL: "https://wind-of-change-live.firebaseio.com",
    projectId: "wind-of-change-live",
    storageBucket: "wind-of-change-live.appspot.com",
    messagingSenderId: "455339803388",
    appId: "1:455339803388:web:d9516a4059d48e9f100d79",
    measurementId: "G-MDE75GXDQ0",

    // apiKey: "AIzaSyBqib4mMMVgVQFioC4kiR2oEESYMUE6lYA",
    // authDomain: "wind-of-change-78bbc.firebaseapp.com",
    // databaseURL: "https://wind-of-change-78bbc.firebaseio.com",
    // projectId: "wind-of-change-78bbc",
    // storageBucket: "wind-of-change-78bbc.appspot.com",
    // messagingSenderId: "233660997176",
    // appId: "1:233660997176:web:3860855c4f4efce24e50cd",
    // measurementId: "G-C003PJ8GNZ"
  },
  domain_url: "https://abp.windofchange.ie",
  node_server_url: "https://abp.windofchange.ie:4443",
  //node_server_url: "http://localhost:3443",
  //apiUrl: "https://abp.windofchange.ie/woc-v2/wocapi",
  apiUrl: "http://localhost/emepal-laravel",

  imageUrl: "https://app.windofchange.ie/woc-v2/wocapi/public",
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
